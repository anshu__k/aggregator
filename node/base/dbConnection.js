const mysql = require('mysql');
require('dotenv').config({ path: '.env' });
/**
 * TODO we could have used promisify to convert callback to promise but using custom code to handle it just for demo
 * This class will help converting mysql db instance to usable form
 */
class DbConnection {
  constructor() {
    this.connection = null;
  }

  /**
   * Creates db connection and stores the connection
   */
  connect() {
    return new Promise((res, rej) => {
      /**
       * TODO not using pooling or anything as just wanted to use it for single query
       */
      const connection = mysql.createConnection({
      host: "localhost",
  user: "root",
  password: "Excellanto2021@",
  database: "pacific",
		port:"3306",
		});

      connection.connect((err) => {
        if (err) {
          return rej(err);
        }
        this.connection = connection;
        res();
      });
    });
  }

  close() {
    this.connection.end(function (err) {
      // The connection is terminated now
      if (err) {
        return console.error('Error killing the connection', err);
      }

      console.log('connection closed');
    });
  }

  query(text) {
    return new Promise((res, rej) => {
      this.connection.query(text, (err, result) => {
        if (err) {
          return rej(err);
        }

        return res(result);
      });
    });
  }
}

module.exports = DbConnection;
