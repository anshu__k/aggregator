/**
 * @name caCPA search
 *
 * @desc retrieves cpa licenseholder information by license number
 */


const puppeteer = require('puppeteer')
const converter  = require('json-2-csv');

var lastID =2;
var sum =100000;


var mysql = require('mysql');
var con = mysql.createConnection({
  host: "127.0.0.1",
  user: "root",
  password: "",
  database: "pacific"
});

con.connect(function(err) {
  if (err) throw err;
  con.query("SELECT licenseNumber FROM script_records order by id DESC LIMIT 1", function (err, result, fields) {
    if (err) throw err;
	console.log(result);
	if(result.length==0){
		lastID = "146393";
	}	
	else{
		
    lastID =Number(Number(result[0].licenseNumber)+1);
		
	}
	
  	sum = Number(Number(lastID)+1000);
	
	});
  });
const wait=ms=>new Promise(resolve => setTimeout(resolve, ms));
wait(2*1000).then(() => console.log(lastID));
console.log(lastID);
var fs = require('fs');
const screenshot = 'cpa_lookup_results.png'
const originHTML = 'https://search.dca.ca.gov'


var licenseNumberStart = lastID;
var licMax = sum;//144418 is the max

var outsideLicenseNumber = 0;
var theList = [];
var licenseErrors = [];
var localList = [];
const lookItUp = async function(licNum){//given a license number this function will return an object with the general info of that license holder
  var CPA = {}
  const launchOptions = {
    ignoreHTTPSErrors: true,
    args: [
      "--unlimited-storage",
      "--full-memory-crash-report",
      "--disable-gpu",
      "--ignore-certificate-errors",
      "--no-sandbox",
      "--disable-setuid-sandbox",
      "--disable-dev-shm-usage",
      "--lang=en-US;q=0.9,en;q=0.8",
      "--user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36",
    ],
  };
  try{
  outsideLicenseNumber = licNum;
        const browser = await puppeteer.launch(launchOptions)
        const page = await browser.newPage()
   //     await page.setDefaultNavigationTimeout(3000); 

      

        await page.setViewport({ width: 1280, height: 800 })
        await page.goto(originHTML)
        await page.waitForSelector('#licenseNumber')
        await page.select("select#boardCode", "19")
        await page.type('#licenseNumber', licNum)
   //****************************
   page.on('error', msg => {
    throw msg ;
  });
//****************************
        await page.click('#srchSubmitHome')
await page.waitForSelector('#wrapper')
const controlResults = await (await (await page.$('#wrapper > header.resultsHeader > h4')).getProperty('textContent')).jsonValue()
if(controlResults == '0 results(Showing 0)'){
  console.log("THERE AINT NO RESULT FOR THIS LICENSE NUMBER"+licNum)
  //should I add to theList then save it with some sort of stock - no entry line for the 0 result?
  CPA.licenseNumber = licNum
  CPA.exceptions = "NO RESULTS FOR LICENSE NUMBER "+licNum
  theList.push(CPA)
  nextSearch(page,String(Number(licNum)+1))
//  licNum = String(Number(licNum)+1)
  return console.log("THERE AINT NONE HERE")
}

 //       await page.waitForSelector('span#lic0')
        //   #\30    #0.post.yes
        const href = await page.$eval('.newTab', span => span.getAttribute('href'));
        const licNumber = await (await (await page.$('span#lic0')).getProperty('textContent')).jsonValue();
        const fullName = await (await (await page.$('h3')).getProperty('textContent')).jsonValue();
   
        const text = await page.evaluate(() => Array.from(document.querySelectorAll('ul.actions'), element => element.textContent));
       // await browser.close()

            var str = text[0];
            var n = str.search("City: ");
            n = n+6;
            var stateStart = str.search("State: ");
            var city = str.slice(n,stateStart);
            var countyStart = str.search("County: ");
            var zipStart = str.search("Zip: ");
            var state = str.slice(stateStart+7,countyStart);
            var county = str.slice(countyStart+8,zipStart);
            var zip = str.slice(zipStart+5,zipStart+10);
            
            var lastNameEnd = fullName.search(", ");
            var lastName = fullName.slice(0,lastNameEnd);  
            var firstName = fullName.slice(lastNameEnd+2,str.length);
            var detailurl=originHTML+href;
			CPA.details = originHTML+href
            CPA.licenseNumber = licNumber

            CPA.city = city
            CPA.state = state
            CPA.county = county
            CPA.zip = zip
            CPA.firstName = firstName
            CPA.lastName = lastName
            theList.push(CPA);
      //      console.log(CPA)
           // await browser.close();
          nextSearch(page,String(Number(licNum)+1))
          if(CPA.county === "LOS ANGELES" || CPA.county === "ORANGE" || CPA.county === "RIVERSIDE" || CPA.county === "VENTURA" || CPA.county === "SAN BERNARDINO"){
            localList.push(CPA)
			console.log("==============> Local ++++ ", localList, "<===================");
					

						var mysql = require('mysql');
		
		var con = mysql.createConnection({
  host: "127.0.0.1",
  user: "root",
  password: "",
  database: "pacific"
});
	con.connect(function(err) {
		  if (err) throw err;
		  console.log("Connected!");
		 var sql = "INSERT INTO `script_records` (`id`, `firstName`, `lastName`, `zip`, `city`, `state`, `county`, `licenseNumber`, `details`, `updated_at`) VALUES (NULL, '"+firstName+"', '"+lastName+"', '"+zip+"', '"+city+"', '"+state+"', '"+county+"', '"+licNumber+"','"+detailurl+"', '2021-12-23 00:00:00')";
		 con.query(sql, function (err, result) {
			if (err) throw err;
			console.log("1 record inserted");
		  });
		});

            converter.json2csv(localList, json2csvCallback2, {
              //prependHeader: false      removes the generated header of "value1,value2,value3,value4" (in case you don't want it)
              });
          }
		  
		  
          converter.json2csv(theList, json2csvCallback, {
            //prependHeader: false      removes the generated header of "value1,value2,value3,value4" (in case you don't want it)
            });
          } catch (error) {
            console.log(error);

              let theNextSearchNumber = String(Number(licNum)+1)
            
              licenseErrors.push(licNum)
                lookItUp(theNextSearchNumber); console.log("line 92 about to lookItUp with "+theNextSearchNumber)

          } finally {
            console.log(licenseErrors.length)
            console.log(licenseErrors)
      //      await browser.close();

            return CPA
          }
}
var nextSearch = async function(page,licNum){
  outsideLicenseNumber = licNum;
  var CPA = {}
  console.log(licNum)
    try{
       
      await page.waitForSelector('#licenseNumber')
      await page.select("select#boardCode", "19")

      const input = await page.$('#licenseNumber');
      await input.click({ clickCount: 3 })
      await input.type(licNum);

      await page.click('#srchSubmit')
   //****************************
   page.on('error', msg => {
    throw msg ;
  });
//****************************
 //     await page.waitForSelector('span#lic0')
 await page.waitForSelector('#wrapper')
 const controlResults = await (await (await page.$('#wrapper > header.resultsHeader > h4')).getProperty('textContent')).jsonValue()
 if(controlResults == '0 results(Showing 0)'){
   console.log("LINE 130 THERE AINT NO RESULT FOR THIS LICENSE NUMBER "+licNum)
   //should I add to theList then save it with some sort of stock - no entry line for the 0 result?
   CPA.licenseNumber = licNum
   CPA.exceptions = "NO RESULTS FOR LICENSE NUMBER "+licNum
   theList.push(CPA)
   nextSearch(page,String(Number(licNum)+1))
   return console.log("THERE AINT NONE HERE")
 }

      const href = await page.$eval('.newTab', span => span.getAttribute('href'));
      const licNumber = await (await (await page.$('span#lic0')).getProperty('textContent')).jsonValue();
      const fullName = await (await (await page.$('h3')).getProperty('textContent')).jsonValue();
      
      const text = await page.evaluate(() => Array.from(document.querySelectorAll('ul.actions'), element => element.textContent));
    // await browser.close()
        //what if I put this in a promise

          var str = text[0];
          var n = str.search("City: ");
          n = n+6;
          var stateStart = str.search("State: ");
          var city = str.slice(n,stateStart);
          var countyStart = str.search("County: ");
          var zipStart = str.search("Zip: ");
          var state = str.slice(stateStart+7,countyStart);
          var county = str.slice(countyStart+8,zipStart);
          var zip = str.slice(zipStart+5,zipStart+10);
          
          var lastNameEnd = fullName.search(", ");
          var lastName = fullName.slice(0,lastNameEnd);  
          var firstName = fullName.slice(lastNameEnd+2,str.length);
          
          
          CPA.details = originHTML+href
          CPA.licenseNumber = licNumber

          CPA.city = city
          CPA.state = state
          CPA.county = county
          CPA.zip = zip
          CPA.firstName = firstName
          CPA.lastName = lastName
          theList.push(CPA);
          console.log(CPA)

          let theNextSearchNumber = String(Number(licNum)+1)
          if(theNextSearchNumber <= licMax){
            nextSearch( page,theNextSearchNumber )
          }
          


          if(CPA.county === "LOS ANGELES" || CPA.county === "ORANGE" || CPA.county === "RIVERSIDE" || CPA.county === "VENTURA" || CPA.county === "SAN BERNARDINO"){
            localList.push(CPA)
            converter.json2csv(localList, json2csvCallback2, {
              //prependHeader: false      removes the generated header of "value1,value2,value3,value4" (in case you don't want it)
              });
          }
          converter.json2csv(theList, json2csvCallback, {
            //prependHeader: false      removes the generated header of "value1,value2,value3,value4" (in case you don't want it)
            });
        } catch (error) {


          console.log(error);
    //      await browser.close();
     //     licenseErrors.push(licNum)
         
            try {
              console.log()
         //     await page.reload(); // soft fix
              lookItUp(licNum)
              console.log("line 229looking it up again "+licNum)

            } catch (recoveringErr) {
              console.log("232")// unable to reload the page, hard fix
              try {
           //       await browser.close();
                
                console.log(licNum+"<-that's theNextSearchNumber...line 236")
             
                  lookItUp(licNum)
              } catch (err) { // browser close was not necessary
                  // you might want to log this error
                  console.log(err)
              }
              browser = await puppeteer.launch(/* .. */);
              page = await browser.newPage();
          } 

        } finally {
    //      await browser.close();
          return CPA
        }

      
}
var firstSearch = async function() {
try{
	console.log("im ahahahha");
  lookItUp(licenseNumberStart)
}catch(error){
  console.log(error)
  console.log("trying to relaunch with the ousideLicenseNumber")
  lookItUp(outsideLicenseNumber)
}
 

}

//if file does not existst named COUNTY.csv then create it (create file and save the column headers)
// open COUNTY.csv
// append CPA to COUNTY.csv
// close COUNTY.csv


let json2csvCallback = function (err, csv) {
  if (err) throw err;
  fs.writeFile('name.csv', csv, 'utf8', function(err) {
    if (err) {
      console.log('Some error occured - file either not saved or corrupted file saved.');
    } else {
      console.log('It\'s saved to name.csv!');
    }
  });
};
let json2csvCallback2 = function (err, csv) {
  if (err) throw err;
  fs.writeFile('local.csv', csv, 'utf8', function(err) {
    if (err) {
      console.log('Some error occured - file either not saved or corrupted file saved.');
    } else {
      console.log('It\'s saved! to local.csv');
    }
  });
};


firstSearch()


//console.log(lookItUp('70178'))
