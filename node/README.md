# License Validator

## Execution Steps
  1. npm install (in root directory)
  2. Provide Env variables if want to use DB operations
  3. run in terminal: node index.js (It will start executing the script)

## Points to consider
  1. I have added db code but I haven't tested it as not having db setup in local, It should work with no/minor alteration though.

  2. I have added 3 types of comments

    a. TODO: This indicates if There's something which can still update this script but considering our discussion just tried to provide the architecture how we can optimize this script to make it easily readable and executable
    b. Note: Indicates thinking or approach
    c. DB: uncomment this, to make it work with DB

  
  3. We are getting JSON data of certificates at the end, I have not added json to csv code as I think it might be straight forward using the libraries. (Also it was not getting used).

  4. I have kept default value of license number as per your suggestion: 100020.

  5. I have removed nextSearch as I thought its almost same as lookup, I could be wrong here but I was not able to find any difference,
    if there's any please let me know, would be happy to implement :) 
  
  6. I have used event driven architecture to make sure we can handle all the events error from puppeteer.

  7. I have not used any query builder as I knew We have 2 queries only, we can surely add one if we want to enhance this script

  8. I made sure that code should be easily readable and optimized at the same time.

  9. I have not used promisify for mysql as I wanted to design/convert it to promise on my own for demo purpose.

  10. I have not spend more time to convert this so, if you think its missing some feature or something, Please let me know.


I would love to get the inputs from you on how we can still upgrade/optimize the script.


### Thanks
