// Initial part of our script Execution Will be started from here
// initialize env variables
const path = require('path');
// require('dotenv').config({ path: '.env' });
const full_path = path.join(__dirname, '.env');
require('dotenv').config({ path: full_path });
const ValidatorService = require('./validator/validatorService');
// const cron = require('node-cron');

// We are going to handle uncaught exceptions here
process.on('uncaughtException', (error) => {
  console.log('Uncaught Exception Occurred', error);
  // TODO send mail to admin or log in app insights etc etc
});

// var task = cron.schedule('*/15 * * * *', () => {
  console.log('Cron Run');
  (async () => {
    const validatorService = new ValidatorService();
    await validatorService.execute();

  })();
// }, {
//   scheduled: true
// });

// task.start();
