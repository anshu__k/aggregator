
const EventEmitter = require('events');
const puppeteer = require('puppeteer');


class PuppeteerExecutor extends EventEmitter {
  /**
   * Constructor 
   * 
   */
  constructor() {
    super();

    this.browserLaunchOptions = {
      ignoreHTTPSErrors: true,
      args: [
        '--unlimited-storage',
        '--full-memory-crash-report',
        '--disable-gpu',
        '--ignore-certificate-errors',
        '--no-sandbox',
        '--disable-setuid-sandbox',
        '--disable-dev-shm-usage',
        '--lang=en-US;q=0.9,en;q=0.8',
        '--user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',
      ],
    };

    this.url = process.env.LN_SEARCH_HOST_URL;
    this.licenseDetails = {};

    this.licenseAdditionalDetails = {};
    this.detailhref = "";
  }

  /**
   * Lookup for the Licence Number
   * 
   * @param {*} licenseNumber 
   * @returns 
   */
  async lookup(licenseNumber) {
    try {

      const browser = await puppeteer.launch(this.browserLaunchOptions);
      const page = await browser.newPage();

      const controlResults = await this.openBrowserSearchPageAndAddLicenseDetails(page, licenseNumber);

      if (controlResults === '0 results(Showing 0)') {
        console.log('THERE AINT NO RESULT FOR THIS LICENSE NUMBER' + licenseNumber);
        this.licenseDetails = {
          licenseNumber,
          exceptions: `NO RESULTS FOR LICENSE NUMBER ${licenseNumber}`
        };

        // NOTE: I have not checked the complete code but it seems next search is doing the same task as lookup so merging them both
        this.emit('done', this.licenseDetails);
        return;
      }

      await this.prepareLicenseDetails(page);

      this.emit('done', this.licenseDetails);
    } catch (err) {
      console.error(`error while executing license number ${licenseNumber}`, err);
      this.emit('error', err);
    }
  }

  /**
   * Open Browser and page for the search page 
   * 
   * @param {*} page 
   * @param {*} licenseNumber 
   * @returns 
   */
  async openBrowserSearchPageAndAddLicenseDetails(page, licenseNumber) {

    console.log('🚀 ~ Initiating for ', licenseNumber);
    await page.setViewport({
      width: 1280,
      height: 800
    });
    await page.goto(this.url, { waitUntil: 'load', timeout: 0 });
    await page.waitForSelector('#licenseNumber');
    await page.select('select#boardCode', '19');
    await page.type('#licenseNumber', String(licenseNumber).split());

    //#########################
    page.on('error', msg => {
      // Note: Due to this I have converted this to event emitter and now
      // code will wait for done/error
      console.error(`error from page while executing ${licenseNumber}`, msg);
      this.emit('error', msg);
    });
    //#########################

    await page.click('#srchSubmitHome');
    await page.waitForSelector('#wrapper');

    return await (await (await page.$('#wrapper > header.resultsHeader > h4')).getProperty('textContent')).jsonValue();

  }

  /**
   * Open browser for the detail page 
   * 
   * @param {*} page 
   * @param {*} licenseNumber 
   * @returns 
   */
  async openBrowserDetailPageAndAddLicenseDetails(page, licenseNumber) {

    console.log('🚀 ~ Initiating detail page for ', licenseNumber);
    await page.setViewport({
      width: 1280,
      height: 800
    });
    await page.goto(this.url + this.detailhref, { waitUntil: 'load', timeout: 0 });

    //#########################
    page.on('error', msg => {
      // Note: Due to this I have converted this to event emitter and now
      // code will wait for done/error
      console.error(`error from page while executing detail page for ${licenseNumber}`, msg);
      this.emit('error', msg);
    });
    //#########################
    const addressdetail = await (await (await page.$('div#address')).getProperty('textContent')).jsonValue();

    const addressHtmldetail = await page.evaluate(() => document.querySelector('div#address > p:nth-child(2)').innerHTML);
    const addressArray = addressHtmldetail.split('<br>');

    const issueDate = await (await (await page.$('p#issueDate')).getProperty('textContent')).jsonValue();
    const expDate = await (await (await page.$('p#expDate')).getProperty('textContent')).jsonValue();
    // const primaryStatus = await (await (await page.$('p#primaryStatus > span.status_10')).getProperty('textContent')).jsonValue();
    const primaryStatus = await (await (await page.$('p#primaryStatus > span[class^="status_"]')).getProperty('textContent')).jsonValue();

    this.licenseAdditionalDetails = {
      full_address: addressdetail,
      address1: addressArray[0] ?? '',
      address2: '',
      issuanceDate: issueDate,
      expirationDate: expDate,
      licenseStatus: primaryStatus,
      licenseType: ''
    }
    return;
  }

  /**
   * Prepare Licence Detail Object 
   * 
   * @param {*} page 
   */
  async prepareLicenseDetails(page) {
    const href = await page.$eval('.newTab', span => span.getAttribute('href'));
    const licenseNumber = await (await (await page.$('span#lic0')).getProperty('textContent')).jsonValue();
    const fullName = await (await (await page.$('h3')).getProperty('textContent')).jsonValue();

    // eslint-disable-next-line no-undef
    const text = await page.evaluate(() => Array.from(document.querySelectorAll('ul.actions'), element => element.textContent));

    if (!text) {
      throw 'No Actions Found';
    }

    const [elementContent] = text;

    if (!elementContent) {
      throw 'No Element found';
    }

    const cityValue = this.getValueFromElement(elementContent, 'City');
    const stateStart = this.getValueFromElement(elementContent, 'State');
    const city = elementContent?.slice(parseInt(cityValue) + 6, stateStart);
    const countyStart = this.getValueFromElement(elementContent, 'County');
    const zipStart = this.getValueFromElement(elementContent, 'Zip');
    const state = elementContent?.slice(stateStart + 7, countyStart);
    const county = elementContent?.slice(countyStart + 8, zipStart);
    const zip = elementContent?.slice(zipStart + 5, zipStart + 10);

    const lastNameEnd = fullName.search(', ');
    const lastName = fullName.slice(0, lastNameEnd);
    const firstName = fullName.slice(lastNameEnd + 2, elementContent.length);

    /**
     * For additional informations of the Licence details i.e. address, Issuence Date, need to scrap detail page
     * this.url + href is detail page of that licence.
     */
    this.detailhref = href;
    const detailbrowser = await puppeteer.launch(this.browserLaunchOptions);
    const detailpage = await detailbrowser.newPage();

    await this.openBrowserDetailPageAndAddLicenseDetails(detailpage, licenseNumber);

    this.licenseDetails = {
      ...this.licenseDetails,
      details: this.url + href,
      licenseNumber,
      city: city,
      state: state,
      county: county,
      zip: zip,
      firstName: firstName,
      lastName: lastName,
      ...this.licenseAdditionalDetails
    };
  }


  getValueFromElement(elm, value) {
    return elm?.search(`${value}: `);
  }
}

module.exports = PuppeteerExecutor;
