const PuppeteerExecutor = require('./puppeteerEventDrivenExecutor');
const ValidatorRepository = require('./validatorRepository');

/**
 * This Class will help execute the script and try to find if the license number is valid or not
 */
class ValidatorService {
  constructor() {
    // creating instance of repository, currently its not adding more sense to add repo
    // but if we had some common context which we want to share between at that time it would have been great to have db code in different
    // file with ES6 syntax
    this.validatorRepository = new ValidatorRepository();
    this.licenseStartNumber = null;
    this.defaultStartNumber = 100020; // in example it was 146393 I am going to increment it by one afterward
    this.totalLicenseToCheck = 40;
    this.lastLicenseToCheck = -1;
    this.failedLicenseNumbers = [];
    this.certLists = [];
  }

  /**
   * Executes the Validation code
   */
  async execute() {
    // DB: uncomment this
    try {
      await this.validatorRepository.init();

      this.licenseStartNumber =
        // await this.validatorRepository.getLicenseNumber();
        await this.validatorRepository.getLastLicenseNumber();
    } catch (err) {
      console.error('Error while fetching the license number', err);
      return;
    }

    if (!this.licenseStartNumber || isNaN(parseInt(this.licenseStartNumber.licenseNumber))) {
      this.licenseStartNumber = this.defaultStartNumber;
    } else {
      this.licenseStartNumber = parseInt(this.licenseStartNumber.licenseNumber);
    }

    this.lastLicenseToCheck = this.licenseStartNumber + this.totalLicenseToCheck;

    const licenseIterator = this.lookupIterator();
    await this.searchAllNumbers(licenseIterator);

  }

  async searchAllNumbers(licenseIterator) {
    const item = licenseIterator.next();

    if (item.done) {
      console.log('done');
      this.executeFinalSteps();
      return;
    }

    const puppeteerExecutor = new PuppeteerExecutor();

    puppeteerExecutor.on('done', this.handleLookupCompletion(item, puppeteerExecutor, licenseIterator).bind(this));
    puppeteerExecutor.on('error', this.handleLookupError(item, puppeteerExecutor, licenseIterator).bind(this));

    puppeteerExecutor.lookup(item.value);
  }

  handleLookupCompletion(item, puppeteerExecutor, licenseIterator) {
    return async (value) => {
      console.log(`done for ${item.value}`);
      this.certLists.push(value);

      await this.insertRecordIfRequired(value);
      // clear all listeners on current object
      puppeteerExecutor.removeAllListeners('done');
      puppeteerExecutor.removeAllListeners('error');

      // trigger next task
      this.searchAllNumbers(licenseIterator);
    };
  }

  handleLookupError(item, puppeteerExecutor, licenseIterator) {
    return async (error) => {
      console.log(error);
      console.error('failed to get license details for', item.value);

      this.failedLicenseNumbers.push({ licenseNumber: item.value, error });

      // clear all listeners on current object
      puppeteerExecutor.removeAllListeners('done');
      puppeteerExecutor.removeAllListeners('error');

      // TODO here we can add retry mechanism as well as we have the puppeteerExecutor instance already we can start the process again
      // from here, skipping it for now

      // trigger next task
      this.searchAllNumbers(licenseIterator);
    };
  }

  async insertRecordIfRequired(licenseDetails) {

    switch (licenseDetails.county) {
      case 'LOS ANGELES':
      case 'ORANGE':
      case 'RIVERSIDE':
      case 'VENTURA':
      case 'SAN BERNARDINO':
        try {
          // DB: uncomment this
          await this.validatorRepository.updateOrCreateRecord(licenseDetails);
          // const licencedetailexists = await this.validatorRepository.selectRecord(licenseDetails);
          // if(licencedetailexists) {
          //   // update record
          //   await this.validatorRepository.updateRecord(licenseDetails);
          // } else {
          //   // insert record
          //   await this.validatorRepository.insertRecord(licenseDetails);
          // }
        } catch (err) {
          console.error('error while inserting record', err);
        }
        break;
      default:
        console.log('no need to insert record as country value is ', licenseDetails.county);
    }
  }

  executeFinalSteps() {
    // close connections or any cleanup tasks

    // DB: uncomment this
    this.validatorRepository.close();

    process.exit();
  }

  *lookupIterator() {
    let number = this.licenseStartNumber + 1;
    while (this.lastLicenseToCheck >= number) {
      yield number;

      number++;
    }
  }
}

module.exports = ValidatorService;
