const DbConnection = require('../base/dbConnection');

/**
 * This Class will help execute the db queries
 */
class ValidatorRepository {
  constructor() {
    /**
     * This will have the dbConnection object
     * currently keeping here if we update/upgrade the script we can move this to
     * parent class
     */
    this.dbConnection = new DbConnection();

  }

  async init() {
    // create connection first
    await this.dbConnection.connect();
  }

  async getLicenseNumber() {
    // TODO not using knex or any other query build as out of this demo's context (also as we have only one query to run)
    const [result] = await this.dbConnection.query(
      'SELECT licenseNumber FROM script_records order by id DESC LIMIT 1'
    );

    return result;
  }

  async getLastLicenseNumber() {
    // TODO not using knex or any other query build as out of this demo's context (also as we have only one query to run)
    const [result] = await this.dbConnection.query(
      'SELECT licenseNumber FROM script_records order by licenseNumber DESC LIMIT 1'
    );

    return result;
  }

  async selectRecord(record) {
    const licenumber = record.licenseNumber;
    // const licenumber = 100020;
    const [result] = await this.dbConnection.query(
      'SELECT licenseNumber FROM script_records sr WHERE sr.licenseNumber = ' + licenumber + ' LIMIT 1'
    );

    return result;
  }

  async insertRecord(record) {
    // TODO not added escape to avoid sql injection but if we think there might be some case for that
    // we can add it, but I think we are getting this data from official website so dont need it
    const expirationDate = await Promise.resolve(this.convertTextDateToDateObject(record.expirationDate));
    const issuanceDate = await Promise.resolve(this.convertTextDateToDateObject(record.issuanceDate));

    await this.dbConnection.query(
      'INSERT INTO `script_records` (`id`, `firstName`, `lastName`, `address1`, `address2`, `zip`, `city`, `state`, `county`, `full_address`, `licenseNumber`, `details`, `licenseType`, `licenseStatus`, `licence_expiration_date`, `licence_issuance_date`, `licenseSecondaryStatus`, `created_at`, `updated_at`) VALUES (NULL, \'' +
      record.firstName +
      '\', \'' +
      record.lastName +
      '\', \'' +
      record.address1 +
      '\', \'' +
      record.address2 +
      '\', \'' +
      record.zip +
      '\', \'' +
      record.city +
      '\', \'' +
      record.state +
      '\', \'' +
      record.county +
      '\', \'' +
      record.full_address +
      '\', \'' +
      record.licenseNumber +
      '\',\'' +
      record.details +
      '\', \'' +
      record.licenseType +
      '\', \'' +
      record.licenseStatus +
      '\', \'' +
      expirationDate +
      '\', \'' +
      issuanceDate +
      '\', \'' +
      record.licenseSecondaryStatus +
      '\', \'' +
      new Date().toISOString().slice(0, 19).replace('T', ' ') +
      '\', \'' +
      new Date().toISOString().slice(0, 19).replace('T', ' ') +
      '\')'
    );
  }

  async updateRecord(record) {
    const expirationDate = await Promise.resolve(this.convertTextDateToDateObject(record.expirationDate));
    const issuanceDate = await Promise.resolve(this.convertTextDateToDateObject(record.issuanceDate));

    await this.dbConnection.query(
      "UPDATE `script_records` SET address1 = '" + record.address1 + "', address2 = '" + record.address2 + "', full_address = '" + record.full_address + "', licenseType = '" + record.licenseType + "', licenseStatus = '" + record.licenseStatus + "', licence_expiration_date = '" + expirationDate + "', licence_issuance_date = '" + issuanceDate + "', licenseSecondaryStatus = '' WHERE licenseNumber = '" + record.licenseNumber + "'"
    );
    /*
    "UPDATE `script_records` SET 
        address1 = record.address1,
        address2 = record.address2, 
        full_address = record.full_address,
        licenseType = record.licenseType,
        licenseStatus = record.licenseStatus,
        licence_expiration_date = this.convertTextDateToDateObject(record.expirationDate),
        licence_issuance_date = this.convertTextDateToDateObject(record.issuanceDate),
        licenseSecondaryStatus = ''
        WHERE licenseNumber = "' + record.licenseNumber + '"
    */
  }


  async updateOrCreateRecord(record) {
    const selectRecord = await this.selectRecord(record);
    console.log("result set: ", selectRecord);
    if (selectRecord) {
      // update 
      await this.updateRecord(record);
    } else {
      // insert
      await this.insertRecord(record);
    }
  }

  async convertTextDateToDateObject(date) {
    const new_date = new Date(Date.parse(date));
    console.log('converted date: ', new_date.toISOString().slice(0, 19).replace('T', ' '));
    return new_date.toISOString().slice(0, 19).replace('T', ' ');
  }

  close() {
    // TODO closing connection as we know script is currently not gonna use it again
    this.dbConnection.close();
  }
}

module.exports = ValidatorRepository;
