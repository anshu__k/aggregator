 jQuery(document).ready(function($) {
        "use strict";
        showDetail();

    });

 function showDetail(){
     jQuery('.showdetail').click(function(){

            var className = jQuery(this).attr('className');
            var stringDataOld = jQuery('.'+className+"old").val();
            var stringDataNew = jQuery('.'+className+"new").val();
            
            
            if(stringDataNew){
                var jsonDataNew = JSON.parse(stringDataNew);
                var tr = '<tr><th>Column Name</th><th> Value</th></tr>';
                jQuery.each(jsonDataNew, function(key, item) {
                    if(key != '_token' && key != 'id' && key != 'created_at' && key != 'updated_at' && key != 'deleted_at')
                    tr += "<tr> <td>"+key+"</td> <td>"+item+"</td></tr>";
                });
                jQuery('.activityTable').html(tr);
                
            }else{
                var tr = '<tr><th>Column Name</th><th> Value</th></tr>';
                tr += '<tr><td class="datanotfound" colspan="2"> No activity details found! </td ><tr>';
                jQuery('.activityTable').html(tr);
            }


            if(stringDataOld){
                var jsonDataOld = JSON.parse(stringDataOld);
                var tr = '<tr><th>Column Name</th><th> Value</th></tr>';
                jQuery.each(jsonDataOld, function(key, item) {
                    if(key != '_token' && key != 'id' && key != 'created_at' && key != 'updated_at' && key != 'deleted_at')
                    tr += "<tr> <td>"+key+"</td> <td>"+item+"</td></tr>"
                });
                jQuery('.activityTableold').html(tr);
                
            }else{
                var tr = '<tr><th>Column Name</th><th> Value</th></tr>';
                tr += '<tr><td class="datanotfound" colspan="2"> No activity details found! </td ><tr>';
                jQuery('.activityTableold').html(tr);
            }


            jQuery('#exampleModalCenter').modal('show');
        });
 }





