<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('max_execution_time', 5000);
error_reporting(E_ALL);
require "config.php";
require "bullhorn-curl.php";
// Create a new object from Rectangle class
$obj = new bullhorn;
try {
    $authCode = $obj->getAuthCode(); //echo $authCode;die;
    $auth = $obj->doBullhornAuth($authCode); //echo $auth;die;
    $tokens = json_decode($auth); //print '<pre>';print_r($tokens);die;
    $session = $obj->doBullhornLogin($tokens->access_token);
    $accessToken = json_decode($session, true);

    if (isset($accessToken['restUrl']) && $accessToken['restUrl'] != '') {
        $clientContactStart = $candidateStart = 0;
        $clientContactCount = $candidateCount = 500;

        // Get All Active Candidates Ids,
        $getAllCandidate =  $obj->getAllCandidateIdsApi($accessToken['restUrl'], $accessToken['BhRestToken']);
        $totalCandidateRecords = $getAllCandidate['total'];

        while($candidateStart <= $totalCandidateRecords) {
            $responseData = $obj->getAllUsersInformationsApi($accessToken['restUrl'], $accessToken['BhRestToken'], 0, 'Candidate', $candidateStart, $candidateCount);
            // Store into local database for all Id
            if (!empty($responseData['data'])) {
                $sel_query = "INSERT INTO script_records_clear (bullhorn_id, entity_type, is_removed, created_date, customText11, customText8, customText7, customText6) VALUES ";
                foreach ($responseData['data'] as $key => $val) {
                    $select_query = "SELECT * FROM script_records_clear WHERE bullhorn_id = '" . $val['id'] . "' AND entity_type = 'candidate' LIMIT 1;";
                    $find_result = mysqli_query($con, $select_query);
                    $find_rowcount = mysqli_num_rows($find_result);
                    if ($find_rowcount > 0) {
                        // Write Update query 
                        $update = "UPDATE script_records_clear SET customText11 = '" . $val['customText11'] . "', customText8 = '" . $val['customText8'] . "', customText7 = '" . $val['customText7'] . "', customText6 = '" . $val['customText6'] . "' WHERE bullhorn_id = '" . $val['id'] . "' AND entity_type = 'candidate'";
                        mysqli_query($con, $update) or die(mysqli_error($con));

                    } else {
                        $sel_query .= "('" . $val['id'] . "', 'candidate', '0', '" . date('Y-m-d H:i:s') . "', '" . $val['customText11'] . "', '" . $val['customText8'] . "', '" . $val['customText7'] . "', '" . $val['customText6'] . "'), ";
                    }
                }
                $sel_query = trim(trim($sel_query), ',');
                $result = mysqli_query($con, $sel_query);
            } else {
                echo "error to insert records..";
            }

            $candidateStart += $candidateCount;
        }
        /** End candidate section  */
        

        // Get All Active Clients Contacts Ids,
        $getAllClientContact =  $obj->getAllClientContactIdsApi($accessToken['restUrl'], $accessToken['BhRestToken']);
        $totalClientRecords = $getAllClientContact['total'];

        while($clientContactStart <= $totalClientRecords) {
            $responseData = $obj->getAllUsersInformationsApi($accessToken['restUrl'], $accessToken['BhRestToken'], 0, 'ClientContact', $clientContactStart, $clientContactCount);
            // Store into local database for all Id
            if (!empty($responseData['data'])) {
                $sel_query = "INSERT INTO script_records_clear (bullhorn_id, entity_type, is_removed, created_date, customText11, customText8, customText7, customText6) VALUES ";
                foreach ($responseData['data'] as $key => $val) {
                    $select_query = "SELECT * FROM script_records_clear WHERE bullhorn_id = '" . $val['id'] . "' AND entity_type = 'client_contact' LIMIT 1;";
                    $find_result = mysqli_query($con, $select_query);
                    $find_rowcount = mysqli_num_rows($find_result);
                    if ($find_rowcount > 0) {
                        // Write Update query 
                        $update = "UPDATE script_records_clear SET customText11 = '" . $val['customText11'] . "', customText8 = '" . $val['customText8'] . "', customText7 = '" . $val['customText7'] . "', customText6 = '" . $val['customText6'] . "' WHERE bullhorn_id = '" . $val['id'] . "' AND entity_type = 'client_contact'";
                        mysqli_query($con, $update) or die(mysqli_error($con));

                    } else {
                        $sel_query .= "('" . $val['id'] . "', 'client_contact', '0', '" . date('Y-m-d H:i:s') . "', '" . $val['customText11'] . "', '" . $val['customText8'] . "', '" . $val['customText7'] . "', '" . $val['customText6'] . "'), ";
                    }
                }
                $sel_query = trim(trim($sel_query), ',');
                $result = mysqli_query($con, $sel_query);
            } else {
                echo "error to insert records..";
            }

            $clientContactStart += $clientContactCount;
        }

        /** End Client contact section  */
    } else {
        echo "Something error ";
    }
      echo $session;die;
} catch (Exception $e) {
    error_log($e->getMessage());
}
