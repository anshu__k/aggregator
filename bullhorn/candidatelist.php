<!doctype html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="//cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- Font Awesome -->
    <link href="//use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet" crossorigin="anonymous">
</head>

<body>
    <div class="container-fluid">
        <?php
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        require "config.php";
        require "bullhorn-curl.php";
        // Create a new object from Rectangle class
        $obj = new bullhorn;
        try {
            print_r($_REQUEST);
            $authCode = $obj->getAuthCode(); //echo $authCode;die;
            $auth = $obj->doBullhornAuth($authCode); //echo $auth;die;
            $tokens = json_decode($auth); //print '<pre>';print_r($tokens);die;
            $session = $obj->doBullhornLogin($tokens->access_token);
            $accessToken = json_decode($session, true);
            // print_r($_REQUEST);
            if (isset($accessToken['restUrl']) && $accessToken['restUrl'] != '' && isset($_REQUEST['EntityID']) && $_REQUEST['EntityID'] != '') {

                $is_candidate = $is_client_contact = false;

                //echo $_REQUEST['candidate_id'];exit;
                if (isset($_REQUEST['EntityType']) && $_REQUEST['EntityType'] == 'Candidate') {
                    // Candidate Data
                    $responseData =  $obj->getCandidateUserDataApiById($accessToken['restUrl'], $_REQUEST['EntityID'], $accessToken['BhRestToken']);
                    $is_candidate = true;
                } else {
                    $responseData =  $obj->getClientContactUserDataApiById($accessToken['restUrl'], $_REQUEST['EntityID'], $accessToken['BhRestToken']);
                    $is_client_contact = true;
                }

                // echo "Result data*** ";
                // echo "<pre>";print_r($responseData);exit;
                if (isset($responseData['data']['firstName']) && $responseData['data']['firstName'] != '') { ?>
                    <div class="row">
                        <!-- <div style="padding: 0 0 0 0; margin: 0 0 0 45px;"> -->
                        <h2 class="title">Bullhorn Data</h2>
                        <!-- </div> -->
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead class="table-dark">
                                    <tr>
                                        <th class="text-center align-middle" scope="col">Bullhorn ID</th>
                                        <th class="text-center align-middle" scope="col">Firstname</th>
                                        <th class="text-center align-middle" scope="col">Middlename</th>
                                        <th class="text-center align-middle" scope="col">Lastname</th>
                                        <th class="text-center align-middle" scope="col">customText11</th>
                                        <th class="text-center align-middle" scope="col">customText6</th>
                                        <th class="text-center align-middle" scope="col">customText7</th>
                                        <th class="text-center align-middle" scope="col">customText8</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center align-middle"><?php echo $responseData['data']['id']; ?></td>
                                        <td class="text-center align-middle"><?php echo $responseData['data']['firstName']; ?></td>
                                        <td class="text-center align-middle"><?php echo $responseData['data']['middleName']; ?></td>
                                        <td class="text-center align-middle"><?php echo $responseData['data']['lastName']; ?></td>
                                        <td class="text-center align-middle"><?php echo $responseData['data']['customText11']; ?></td>
                                        <td class="text-center align-middle"><?php echo $responseData['data']['customText6']; ?></td>
                                        <td class="text-center align-middle"><?php echo $responseData['data']['customText7']; ?></td>
                                        <td class="text-center align-middle"><?php echo $responseData['data']['customText8']; ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <?php

                    // $sel_query = "SELECT * FROM script_records WHERE firstName = '" . $responseData['data']['firstName'] . "' OR lastName = '" . $responseData['data']['lastName'] . "' ORDER BY id ASC;";
                    $sel_query = "SELECT * FROM script_records WHERE firstName LIKE '" . $responseData['data']['firstName'] . "%' OR lastName LIKE '" . $responseData['data']['lastName'] . "%' ORDER BY 
                        CASE
                            WHEN firstName LIKE '" . $responseData['data']['firstName'] . "' AND lastname LIKE '" . $responseData['data']['lastName'] . "' THEN 1
                            WHEN firstName LIKE '" . $responseData['data']['firstName'] . "%' AND lastname LIKE '" . $responseData['data']['lastName'] . "%' THEN 2
                            WHEN firstName LIKE '%" . $responseData['data']['firstName'] . "%' AND lastname LIKE '%" . $responseData['data']['lastName'] . "%' THEN 3
                            
                            WHEN firstName LIKE '" . $responseData['data']['firstName'] . "' THEN 4
                            WHEN firstName LIKE '" . $responseData['data']['firstName'] . "%' THEN 5
                            WHEN firstName LIKE '%" . $responseData['data']['firstName'] . "%' THEN 6
                            
                            WHEN lastName LIKE '" . $responseData['data']['lastName'] . "' THEN 7
                            WHEN lastName LIKE '" . $responseData['data']['lastName'] . "%' THEN 8
                            WHEN lastName LIKE '%" . $responseData['data']['lastName'] . "%' THEN 9
                            
                            ELSE 10
                        END;";
                    $result = mysqli_query($con, $sel_query);
                    $rowcount = mysqli_num_rows($result);
                    if ($rowcount > 0) { ?>
                        <div class="row">
                            <h2 class="title">Our Database</h2>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered">
                                    <thead class="table-dark">
                                        <tr>
                                            <th class="text-center align-middle" scope="col">Sr No.</th>
                                            <th class="text-center align-middle" scope="col">ID</th>
                                            <th class="text-center align-middle" scope="col">LicNo</th>
                                            <th class="text-center align-middle" scope="col">Firstname</th>
                                            <th class="text-center align-middle" scope="col">Lastname</th>
                                            <th class="text-center align-middle" scope="col">Country</th>
                                            <th class="text-center align-middle" scope="col">State</th>
                                            <th class="text-center align-middle" scope="col">City</th>
                                            <th class="text-center align-middle" scope="col">Details</th>
                                            <th class="text-center align-middle" scope="col">Sync</th>
                                            <th class="text-center align-middle" scope="col">LinkedIn</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $count = 1;
                                        while ($row = mysqli_fetch_assoc($result)) {
                                            /* To Search Linked In */
                                            $firstName = explode(' ', $row['firstName']);
                                            $searchFirstName = $firstName[0] ?? '';

                                            $lastName = explode(' ', $row['lastName']);
                                            $searchLastName = $lastName[0] ?? '';
                                            /* To Search Linked In */

                                            $linkedinProfileSearchKey = 'https://www.linkedin.com/search/results/people/?firstName=' . $searchFirstName . '&geoUrn=["90000049"]&keywords=accounting OR accountant OR cpa&lastName=' . $searchLastName . '&origin=FACETED_SEARCH&sid=qAC';
                                            $firstNameHighlight = $lastNameHighlight = "";
                                            // if (strtolower(trim($responseData['data']['firstName'])) == strtolower(trim($row['firstName']))) {
                                            if (strpos(strtolower(trim($row['firstName'])), strtolower(trim($responseData['data']['firstName']))) !== false) {
                                                $firstNameHighlight = 'style="background-color: #dbede4"';
                                            }
                                            // if (strtolower(trim($responseData['data']['lastName'])) == strtolower(trim($row['lastName']))) {
                                            if (strpos(strtolower(trim($row['lastName'])), strtolower(trim($responseData['data']['lastName']))) !== false) {
                                                $lastNameHighlight = 'style="background-color: #dbede4"';
                                            }
                                        ?>
                                            <tr style="line-height: 25px">
                                                <td class="text-center align-middle"> <?php echo $count; ?></td>
                                                <td class="text-center align-middle"> <?php echo $row['id']; ?></td>
                                                <td class="text-center align-middle"> <?php echo $row['licenseNumber']; ?></td>
                                                <td class="text-center align-middle" <?php echo $firstNameHighlight; ?>><?php echo $row['firstName']; ?></td>
                                                <td class="text-center align-middle" <?php echo $lastNameHighlight; ?>><?php echo $row['lastName'] ?></td>
                                                <td class="text-center align-middle"><?php echo $row['county'] ?></td>
                                                <td class="text-center align-middle"><?php echo $row['state'] ?></td>
                                                <td class="text-center align-middle"><?php echo $row['city'] ?></td>
                                                <td class="text-center align-middle">
                                                    <a href="<?php echo $row['details']; ?>" target="_blank" class="btn btn-info">DCA Link</a>
                                                </td>

                                                <td class="text-center align-middle">
                                                    <?php if ($is_candidate && ($row['syncstatusCandidate'] == '0' || $row['syncstatusCandidate'] == '1')) { ?>
                                                        <button type="button" value="Sync" style="margin: 7px; " class="blh__syncData btn btn-primary" data-eventType="candidate" data-localId="<?php echo $row['id']; ?>" data-licenceNumber="<?php echo $row['licenseNumber']; ?>" data-localDbData='<?php echo json_encode($row); ?>' data-bullhornData='<?php echo json_encode($responseData); ?>'>Sync Candidate Data</button>
                                                    <?php } else if ($is_client_contact && ($row['syncstatus'] == '0' || $row['syncstatus'] == '1')) { ?>
                                                        <button type="button" value="Sync" style="margin: 7px; " class="blh__syncData btn btn-primary" data-eventType="client_contact" data-localId="<?php echo $row['id']; ?>" data-licenceNumber="<?php echo $row['licenseNumber']; ?>" data-localDbData='<?php echo json_encode($row); ?>' data-bullhornData='<?php echo json_encode($responseData); ?>'>Sync Contact Data</button>
                                                    <?php } else { ?>
                                                        <span style="font-weight: 900; color: green;">Synced</span>
                                                    <?php } ?>

                                                </td>
                                                <td class="text-center align-middle">
                                                    <a href='<?php echo ($linkedinProfileSearchKey); ?>' target="_blank"><i class="fab fa-2x fa-linkedin" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                        <?php $count++;
                                        } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
        <?php } else {
                        echo "No data found.";
                    }
                } else {
                    echo "No data found.";
                }
            }
            //   echo $session;die;
        } catch (Exception $e) {
            error_log($e->getMessage());
        }
        ?>
    </div>
</body>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script> -->
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script>
    $(document).on('click', '.blh__syncData', function() {
        var clickedElem = $(this);
        bullHornData = clickedElem.attr('data-bullhornData');
        parseBullHornData = JSON.parse(bullHornData);
        eventType = clickedElem.attr('data-eventType');
        localDbData = clickedElem.attr('data-localDbData');
        parseLocalDbData = JSON.parse(localDbData);


        const swalWithBootstrapButtons = Swal.mixin({
            // customClass: {
            //     confirmButton: 'btn btn-success',
            //     cancelButton: 'btn btn-danger'
            // },
            // buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: 'Are you sure to sync data?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, sync it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: 'POST',
                    url: 'manualSyncBullhornData.php',
                    cache: false,
                    dataType: 'json',
                    data: {
                        'bullHornData': parseBullHornData,
                        'eventType': eventType,
                        'localData': parseLocalDbData
                    },
                    success: function(response) {
                        if (response.status) {
                            swalWithBootstrapButtons.fire(
                                'Synced!',
                                'Your data has been synced.',
                                'success'
                            )
                            clickedElem.parent('td').html('<span style="font-weight: 900; color: green;">' + response.text + '</span> ');
                            // location.reload();
                        }
                    },
                    error: function() {
                        console.log('Response Error ');
                    }
                });
            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                    'Cancelled',
                    'Your sync data request was cancelled :)',
                    'error'
                )
            }
        });
    });
</script>

</html>