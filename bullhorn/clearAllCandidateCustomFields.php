<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require "config.php";
require "bullhorn-curl.php";
// Create a new object from Rectangle class
$obj = new bullhorn;
try {
    $authCode = $obj->getAuthCode(); //echo $authCode;die;
    $auth = $obj->doBullhornAuth($authCode); //echo $auth;die;
    $tokens = json_decode($auth); //print '<pre>';print_r($tokens);die;
    $session = $obj->doBullhornLogin($tokens->access_token);
    $accessToken = json_decode($session, true);

    if (isset($accessToken['restUrl']) && $accessToken['restUrl'] != '') {
        // Get All Active Candidates Ids,
        /*$sel_query = "SELECT * FROM script_records_clear WHERE is_removed = '0' ORDER BY id ASC;";
        $result = mysqli_query($con, $sel_query);
        $rowcount = mysqli_num_rows($result);
        if ($rowcount > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                // Update candidate 
                if($row['entity_type'] == 'candidate') {
                    $responseDataCandidate2 = $obj->updateCandidateUserApi($accessToken['restUrl'], $row['bullhorn_id'], $accessToken['BhRestToken'], '', '', '');
                } else if($row['entity_type'] == 'client_contact') {
                    // Update Client contact
                    $responseData2 = $obj->updateClientContactUserApi($accessToken['restUrl'], $row['bullhorn_id'], $accessToken['BhRestToken'], '', '', '');
                }
                $sel_query = "UPDATE script_records_clear set is_removed = '1' WHERE id = '" . $row['id'] . "'";
                $result = mysqli_query($con, $sel_query);
            }
        }*/
        // $sel_query = "SELECT * FROM script_records WHERE (syncstatus = 2 OR syncstatusCandidate = 2) ORDER BY id ASC;";
        $sel_query = "SELECT * FROM script_records WHERE (syncstatus = 2 OR syncstatusCandidate = 2) ORDER BY id ASC;";
        // $sel_query = "Select * from script_records where syncstatus = 0 ORDER BY id asc;";
        $result = mysqli_query($con, $sel_query);
        while ($row = mysqli_fetch_assoc($result)) {

            if($row['syncstatus'] == 2) { // Code to clear client record
                $responseData =  $obj->getUserDataApi($accessToken['restUrl'], $row['firstName'], $row['lastName'], $accessToken['BhRestToken']);

                if ($responseData['total'] != 0) {
                    $contactId = $responseData['data'][0]['id'];

                    $contactwepidUpdate = $contactlicencenoUpdate = $contactlicenceurlUpdate = '';

                    /// update the user data 
                    $responseData2 = $obj->updateClientContactUserApi($accessToken['restUrl'], $contactId, $accessToken['BhRestToken'], $contactwepidUpdate, $contactlicencenoUpdate, $contactlicenceurlUpdate, 'customText8');

                    ///// end update user data
                    if (isset($responseData2['changeType']) && $responseData2['changeType'] == 'UPDATE') {
                        $update = "UPDATE script_records SET syncstatus = '0' WHERE id='" . $row['id'] . "'";
                        mysqli_query($con, $update) or die(mysqli_error($con));
                        $msg =  "The user data for " . $row['firstName'] . " " . $row['lastName'] . " is updated successfully";
                        $obj->write_log('ClientContactUserDataUpdate: ', $msg);
                        echo $msg;
                        echo "</br>  ";
                    }
                } else {
                    $update = "UPDATE script_records SET syncstatus = '0' WHERE id='" . $row['id'] . "'";
                    mysqli_query($con, $update) or die(mysqli_error($con));
                }
            }

            if($row['syncstatusCandidate'] == 2) { // Code for syncing Candidate infos
                $responseDataCandidate =  $obj->getUserDataCandidateApi($accessToken['restUrl'], $row['firstName'], $row['lastName'], $accessToken['BhRestToken']);
                if ($responseDataCandidate['total'] != 0) {

                    $candidateId = $responseDataCandidate['data'][0]['id'];

                    $candidatewepidUpdate = $candidatelicencenoUpdate = $candidatelicenceurlUpdate = '';
                    /// update the user data 
                    $responseDataCandidate2 = $obj->updateCandidateUserApi($accessToken['restUrl'], $candidateId, $accessToken['BhRestToken'], $candidatewepidUpdate, $candidatelicencenoUpdate, $candidatelicenceurlUpdate, 'customText6');

                    ///// end update user data
                    if (isset($responseDataCandidate2['changeType']) && $responseDataCandidate2['changeType'] == 'UPDATE') {
                        $updateCandidate = "UPDATE script_records SET syncstatusCandidate = '0' WHERE id='" . $row['id'] . "'";
                        mysqli_query($con, $updateCandidate) or die(mysqli_error($con));
                        $msg =  "The user data for " . $row['firstName'] . " " . $row['lastName'] . " is updated successfully";
                        $obj->write_log('CandidateUserDataUpdate: ', $msg);
                        echo $msg;
                        echo "</br>  ";
                    }
                } else {
                    $updateCandidate = "UPDATE script_records SET syncstatusCandidate = '0' where id='" . $row['id'] . "'";
                    mysqli_query($con, $updateCandidate) or die(mysqli_error($con));
                }
            }
        }
    }
    //   echo $session;die;
} catch (Exception $e) {
    error_log($e->getMessage());
}
?>