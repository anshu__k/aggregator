<?php
require "config.php";
require "bullhorn-curl.php";
// Create a new object from Rectangle class
$obj = new bullhorn;
try {
    // echo json_encode(['status' => true, 'data' => 'Failed', 'text' => 'Synced']);exit;
    // echo "<pre>";print_r($_REQUEST);exit;
    $authCode = $obj->getAuthCode(); //echo $authCode;die;
    $auth = $obj->doBullhornAuth($authCode); //echo $auth;die;
    $tokens = json_decode($auth); //print '<pre>';print_r($tokens);die;
    $session = $obj->doBullhornLogin($tokens->access_token);
    $accessToken = json_decode($session, true);

    if (isset($accessToken['restUrl']) && $accessToken['restUrl'] != '') {
        if (isset($_REQUEST['eventType'])) {
            $responseDataCandidate = $_REQUEST['bullHornData'];
            $row = $_REQUEST['localData'];
            $address = $obj->generate_address_array($row);
            
            $encoded_address = $address;
            // print_r(json_encode($address));exit;
            if ($_REQUEST['eventType'] == 'candidate') {

                $candidateId = $responseDataCandidate['data']['id'];
                $candidatewepid = $responseDataCandidate['data']['customText11']; //wepid
                $candidatelicenceno = $responseDataCandidate['data']['customText7']; //licecne no
                $candidatelicenceurl = $responseDataCandidate['data']['customText8']; //licecne url

                $new_candidate_id = $obj->unique_array($candidatewepid, $row['id']);
                $new_candidate_licence_no = $obj->unique_array($candidatelicenceno, $row['licenseNumber']);
                // $new_candidate_licence_url = $obj->unique_array($candidatelicenceurl, $row['details']);  // Line commented and sync only single value of URL, as persist length error of 100 characters gives for customText8
                $new_candidate_licence_url = $candidatelicenceurl;

                $candidatewepidUpdate = ($new_candidate_id != '') ? $new_candidate_id : $row['id'];
                $candidatelicencenoUpdate = ($new_candidate_licence_no != '') ? $new_candidate_licence_no : $row['licenseNumber'];
                $candidatelicenceurlUpdate = ($new_candidate_licence_url != '') ? $new_candidate_licence_url : $row['details'];

                /// update the user data 
                $obj->write_log('params from manual sync button: : ', ' ***** Rest URL: ' . $accessToken['restUrl'] . ' ***** Contact ID: ' . $candidateId . ' ***** Token: ' . $accessToken['BhRestToken'] . ' ***** Update: ' . $candidatewepidUpdate );
                $responseDataCandidate2 = $obj->updateCandidateUserApi($accessToken['restUrl'], $candidateId, $accessToken['BhRestToken'], $candidatewepidUpdate, $candidatelicencenoUpdate, $candidatelicenceurlUpdate, null, $encoded_address);

                if (isset($responseDataCandidate2['changeType']) && $responseDataCandidate2['changeType'] == 'UPDATE') {
                    $updateCandidate = "UPDATE script_records SET syncstatusCandidate = '2', bullhorn_candidate_id = '" . $candidateId . "' WHERE id='" . $row['id'] . "'";
                    mysqli_query($con, $updateCandidate) or die(mysqli_error($con));
                    $msg =  "The user data for " . $row['firstName'] . " " . $row['lastName'] . " is updated successfully";
                    $obj->write_log('CandidateUserDataUpdate: ', $msg);
                    // echo $msg;
                    // echo "</br>  ";
                } else {
                    $obj->write_log('No data match Candidate User Data Update: ', $responseDataCandidate2);
                }

                echo json_encode(['status' => true, 'data' => 'success', 'text' => 'Synced']);
                exit;
                return ['status' => true, 'data' => 'success'];
            } elseif ($_REQUEST['eventType'] == 'client_contact') {

                // $contactId = $responseData['data']['id'];
                // $contactwepid = $responseData['data']['customText11']; //wepid
                // $contactlicenceno = $responseData['data']['customText6']; //licecne no
                // $contactlicenceurl = $responseData['data']['customText7']; //licecne url
                $contactId = $responseDataCandidate['data']['id'];
                $contactwepid = $responseDataCandidate['data']['customText11']; //wepid
                $contactlicenceno = $responseDataCandidate['data']['customText6']; //licecne no
                $contactlicenceurl = $responseDataCandidate['data']['customText7']; //licecne url

                $new_contact_id = $obj->unique_array($contactwepid, $row['id']);
                $new_contact_licence_no = $obj->unique_array($contactlicenceno, $row['licenseNumber']);
                // $new_contact_licence_url = $obj->unique_array($contactlicenceurl, $row['details']);  // Line commented and sync only single value of URL, as persist length error of 100 characters gives for customText7
                $new_contact_licence_url = $contactlicenceurl;

                $contactwepidUpdate = ($new_contact_id != '') ? $new_contact_id : $row['id'];
                $contactlicencenoUpdate = ($new_contact_licence_no != '') ? $new_contact_licence_no : $row['licenseNumber'];
                $contactlicenceurlUpdate = ($new_contact_licence_url != '') ? $new_contact_licence_url : $row['details'];

                /// update the user data 
                // print_r($encoded_address);exit;
                $obj->write_log('params from manual sync button: : ', ' ***** Rest URL: ' . $accessToken['restUrl'] . ' ***** Contact ID: ' . $contactId . ' ***** Token: ' . $accessToken['BhRestToken'] . ' ***** Update: ' . $contactwepidUpdate );
                $responseData2 = $obj->updateClientContactUserApi($accessToken['restUrl'], $contactId, $accessToken['BhRestToken'], $contactwepidUpdate, $contactlicencenoUpdate, $contactlicenceurlUpdate, null, $encoded_address);

                if (isset($responseData2['changeType']) && $responseData2['changeType'] == 'UPDATE') {
                    $update = "UPDATE script_records SET syncstatus = '2', bullhorn_contact_id = '" . $contactId . "' WHERE id='" . $row['id'] . "'";
                    mysqli_query($con, $update) or die(mysqli_error($con));
                    $msg =  "The user data for " . $row['firstName'] . " " . $row['lastName'] . " is updated successfully";
                    $obj->write_log('ClientContactUserDataUpdate: ', $msg);
                    // echo $msg;
                    // echo "</br>  ";
                } else {
                    $obj->write_log('No data match Client Contact User Data Update: ', $responseData2);
                }

                echo json_encode(['status' => true, 'data' => 'success', 'text' => 'Synced']);
                exit;
                return ['status' => true, 'data' => 'success'];
            }
        }
    }
    echo json_encode(['status' => false, 'data' => 'Failed']);
    exit;
} catch (Exception $e) {
    error_log($e->getMessage());
}
