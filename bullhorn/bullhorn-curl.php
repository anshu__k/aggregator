<?php

class bullhorn
{
  public function write_log($key, $content)
  {
    $myfile = fopen(MESSAGE_LOG, 'a');
    fwrite($myfile, '******************************************************************* ');
    fwrite($myfile, date('Y-m-d H:i:s'));
    fwrite($myfile, $key);
    fwrite($myfile, $content);
    fclose($myfile);
    return true;
  }


  public function unique_array($bullhorn_db_value, $local_db_value)
  {
    $customIdsArray = [];

    $bullhorn = explode(',', $bullhorn_db_value);
    $local_id = explode(',', $local_db_value);
    $customIdsArray = array_merge($bullhorn, $local_id);

    $custom_contact_id_array = array_unique(array_values(array_filter($customIdsArray)));
    $new_custom_ids = implode(',', $custom_contact_id_array);
    return $new_custom_ids;
  }

  public function generate_address_array($data = null) {
    if(!empty($data)) {
      $addressArray =
        array(
          'address1' => !empty($data['address1']) ? $data['address1'] : '',
          'address2' => !empty($data['address2']) ? $data['address2'] : '',
          'city' => !empty($data['city']) ? $data['city'] : '',
          // 'county' => !empty($data['county']) ? $data['county'] : '',
          'countryCode' => !empty($data['country_code']) ? $data['country_code'] : '',
          'countryID' => !empty($data['country_id']) ? $data['country_id'] : '',
          'countryName' => !empty($data['country_name']) ? $data['country_name'] : '',
          'state' => !empty($data['state']) ? $data['state'] : '',
          'timezone' => 'America/Los_Angeles',
          'zip' => !empty($data['zip']) ? $data['zip'] : '',
        );

      return $addressArray;
    } else {
      return [];
    }
  }

  public function getAuthCode()
  {
    $url = 'https://auth.bullhornstaffing.com/oauth/authorize?client_id=' . CLIENT_ID . '&response_type=code&action=Login&username=' . USER . '&password=' . PASS;
    $curl  = curl_init($url);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    //curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_AUTOREFERER, true);
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 120);
    curl_setopt($curl, CURLOPT_TIMEOUT, 120);

    $content = curl_exec($curl);
    if (curl_errno($curl)) {
      echo 'Request Error:' . curl_error($curl);
    }
    curl_close($curl);

    $this->write_log('getAuthCode: ', $content);

    if (preg_match('#Location: (.*)#', $content, $r) || preg_match('#location: (.*)#', $content, $r)) {
      $l = trim($r[1]);
      $temp = preg_split("/code=/", $l);
      $authcode = $temp[1];
    }

    return $authcode;
  }

  public function doBullhornAuth($authCode)
  {
    $url = 'https://auth.bullhornstaffing.com/oauth/token?grant_type=authorization_code&code=' . $authCode . '&client_id=' . CLIENT_ID . '&client_secret=' . CLIENT_SECRET;

    $options = array(
      CURLOPT_RETURNTRANSFER => 1,
      CURLOPT_POST => true,
      CURLOPT_POSTFIELDS => array()
    );

    $ch  = curl_init($url);
    curl_setopt_array($ch, $options);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $content = curl_exec($ch);

    curl_close($ch); //die($content);

    $this->write_log('doBullhornAuth: ', $content);

    return $content;
  }

  public function doBullhornLogin($accessToken)
  {
    $url = 'https://rest.bullhornstaffing.com/rest-services/login?version=*&access_token=' . $accessToken;
    $curl  = curl_init($url);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    //curl_setopt($curl, CURLOPT_HEADER, true);
    //curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    //curl_setopt($curl, CURLOPT_AUTOREFERER, true);
    //curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 120);
    //curl_setopt($curl, CURLOPT_TIMEOUT, 120);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

    $content = curl_exec($curl);
    curl_close($curl);

    $this->write_log('doBullhornLogin: ', $content);
    //print_r($content);exit;
    return $content;
  }

  public function getUserDataApi($resturl, $firstname, $lastname, $token)
  {
    list($firstname1, $firstname2) = explode(" ", $firstname);
    if (empty($firstname2)) {
      $firstname2 = $firstname1;
    }
    ////  echo  $url1 = $accessToken['restUrl'].'search/Candidate?query=firstName:'.$row['firstName'].' AND lastName:'.$row['lastName'].'&fields=id,firstName,lastName,customText11,customText6,dateAdded&start=0&BhRestToken='.$accessToken['BhRestToken'];

    // $url1 = $resturl . 'search/ClientContact?query=firstName:' . rawurlencode($firstname) . '%20AND%20lastName:' . rawurlencode($lastname) . '&fields=id,firstName,lastName,customText11,customText6,customText7,dateAdded&start=0&BhRestToken=' . $token;

    // $url1 = $resturl . 'search/ClientContact?query=%28firstName%3AENCARNACION%20AND%20lastName%3AALAYVILLA%29%20OR%20%28firstName%3AJACQUELINE%20AND%20lastName%3AALAYVILLA%29&fields=id,firstName,lastName,customText11,customText6,customText7,dateAdded&start=0&BhRestToken=' . $token;
    $url1 = $resturl . 'search/ClientContact?query=(firstName:' . rawurlencode($firstname1) . '%20AND%20lastName:' . rawurlencode($lastname) . ')%20OR%20(firstName:' . rawurlencode($firstname2) . '%20AND%20lastName:' . rawurlencode($lastname) . ')&fields=id,firstName,lastName,customText11,customText6,customText7,dateAdded,address&start=0&BhRestToken=' . $token;

    $curl1 = curl_init($url1);
    curl_setopt($curl1, CURLOPT_URL, $url1);
    curl_setopt($curl1, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl1, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl1, CURLOPT_SSL_VERIFYPEER, 0);

    $contentData = curl_exec($curl1);
    curl_close($curl1);
    $responseData = json_decode($contentData, true);
    $this->write_log('UserData: ', $contentData);

    return $responseData;
  }

  /**
   * Get Candidate User By Email 
   * 
   * @param String
   * @param String
   * @param String
   * 
   * @return Object
   */
/*  public function getCandidateUserDataByEmailApi($resturl, $email, $token)
  {

    ////  echo  $url1 = $accessToken['restUrl'].'search/Candidate?query=firstName:'.$row['firstName'].' AND lastName:'.$row['lastName'].'&fields=id,firstName,lastName,customText11,customText6,dateAdded&start=0&BhRestToken='.$accessToken['BhRestToken'];

    $url1 = $resturl . 'search/Candidate?query=email:' . rawurlencode($email) . '&fields=id,firstName,lastName,customText11,customText6,dateAdded&start=0&BhRestToken=' . $token;

    // $url1 = $resturl . 'search/Candidate?query=firstName:' . rawurlencode('ALICE') . '&fields=id,firstName,lastName,email,customText11,customText6,dateAdded&start=0&BhRestToken=' . $token;
    // print_r($url1);

    $curl1 = curl_init($url1);
    curl_setopt($curl1, CURLOPT_URL, $url1);
    curl_setopt($curl1, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl1, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl1, CURLOPT_SSL_VERIFYPEER, 0);

    $contentData = curl_exec($curl1);
    curl_close($curl1);
    $responseData = json_decode($contentData, true);
    $this->write_log('UserDataByEmail: ', $contentData);
    // echo "<pre>";print_r($responseData);exit;
    return $responseData;
  }
  */

  /**
   * Get Client Contact User By Email 
   * 
   * @param String
   * @param String
   * @param String
   * @param String
   * 
   * @return Object
   */
  public function getClientContactUserDataByEmailApi($entity, $resturl, $email, $token)
  {
    // $url1 = $resturl . 'search/' . $entity . '?query=email:' . rawurlencode($email) . '&fields=id,email,firstName,lastName,customText11,customText6,dateAdded&start=0&BhRestToken=' . $token;
    $url1 = $resturl . 'search/' . $entity . '?query=email:' . rawurlencode($email) . '%20OR%20email2:' . rawurlencode($email) . '&fields=id,email,firstName,lastName,customText11,customText6,dateAdded&start=0&BhRestToken=' . $token;
    // print_r($url1);

    $curl1 = curl_init($url1);
    curl_setopt($curl1, CURLOPT_URL, $url1);
    curl_setopt($curl1, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl1, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl1, CURLOPT_SSL_VERIFYPEER, 0);

    $contentData = curl_exec($curl1);
    curl_close($curl1);
    $responseData = json_decode($contentData, true);
    $this->write_log('UserDataByEmail: ', $contentData);
    // echo "<pre>";print_r($responseData);exit;
    return $responseData;
  }

  public function getUserDataCandidateApi($resturl, $firstname, $lastname, $token)
  {

    list($firstname1, $firstname2) = explode(" ", $firstname);
    if (empty($firstname2)) {
      $firstname2 = $firstname1;
    }

    ////  echo  $url1 = $accessToken['restUrl'].'search/Candidate?query=firstName:'.$row['firstName'].' AND lastName:'.$row['lastName'].'&fields=id,firstName,lastName,customText11,customText6,dateAdded&start=0&BhRestToken='.$accessToken['BhRestToken'];
    // echo $url1 = $resturl . 'search/Candidate?query=firstName:' . rawurlencode($firstname) . '%20AND%20lastName:' . rawurlencode($lastname) . '&fields=id,firstName,lastName,customText11,customText6,customText7,customText8,dateAdded&start=0&BhRestToken=' . $token;

    $url1 = $resturl . 'search/Candidate?query=(firstName:' . rawurlencode($firstname1) . '%20AND%20lastName:' . rawurlencode($lastname) . ')%20OR%20(firstName:' . rawurlencode($firstname2) . '%20AND%20lastName:' . rawurlencode($lastname) . ')&fields=id,firstName,lastName,customText11,customText6,customText7,dateAdded,address&start=0&BhRestToken=' . $token;

    $curl1 = curl_init($url1);
    curl_setopt($curl1, CURLOPT_URL, $url1);
    curl_setopt($curl1, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl1, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl1, CURLOPT_SSL_VERIFYPEER, 0);

    $contentData = curl_exec($curl1);
    curl_close($curl1);
    $responseData = json_decode($contentData, true);
    $this->write_log('UserDataCandidate: ', $contentData);

    return $responseData;
  }
  public function updateClientContactUserApi($resturl, $contactId, $token, $contactwepidUpdate, $contactlicencenoUpdate, $contactlicenceurlUpdate, $extra_column = null, $address_object = null)
  {

    // echo $url2 = $resturl . 'entity/ClientContact/' . $contactId . '?BhRestToken=' . $token;
    $url2 = $resturl . 'entity/ClientContact/' . $contactId . '?BhRestToken=' . $token;

    $this->write_log('params: : ', ' ***** Rest URL: ' . $resturl . ' ***** Contact ID: ' . $contactId . ' ***** Token: ' . $token . ' ***** Update: ' . $contactwepidUpdate . ' ***** URL : ' . $url2);

    $curl2 = curl_init($url2);
    // $payload = json_encode(array("customText11" => $contactwepidUpdate, "customText7" => $contactlicencenoUpdate, "customText8" => $contactlicenceurlUpdate));
    $payloadArray = array(
      "customText11" => $contactwepidUpdate,
      "customText6" => $contactlicencenoUpdate,
      "customText7" => $contactlicenceurlUpdate
    );
    if (!empty($extra_column)) {
      $payloadArray[$extra_column] = '';
    }
    if(!empty($address_object)) {
      $payloadArray['address'] = $address_object;
    }
    $payload = json_encode($payloadArray);
    
    curl_setopt($curl2, CURLOPT_POSTFIELDS, $payload);
    curl_setopt($curl2, CURLOPT_POST, 1);
    curl_setopt($curl2, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt($curl2, CURLOPT_URL, $url2);
    curl_setopt($curl2, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl2, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl2, CURLOPT_SSL_VERIFYPEER, 0);
    $contentData2 = curl_exec($curl2);
    // print_r($contentData2);exit;
    curl_close($curl2);
    $responseData2 = json_decode($contentData2, true);
    
    $this->write_log('UserDatapdate: ', $contentData2);

    return $responseData2;
  }
  public function updateCandidateUserApi($resturl, $contactId, $token, $contactwepidUpdate, $contactlicencenoUpdate, $contactlicenceurlUpdate, $extra_column = null, $address_object = null)
  {

    // echo $url2 = $resturl . 'entity/Candidate/' . $contactId . '?BhRestToken=' . $token;
    $url2 = $resturl . 'entity/Candidate/' . $contactId . '?BhRestToken=' . $token;
    $curl2 = curl_init($url2);
    // $payload = json_encode(array("customText11" => $contactwepidUpdate, "customText6" => $contactlicencenoUpdate, "customText7" => $contactlicenceurlUpdate));
    $payloadArray = array(
      "customText11" => $contactwepidUpdate,
      "customText7" => $contactlicencenoUpdate,
      "customText8" => $contactlicenceurlUpdate
    );
    if (!empty($extra_column)) {
      $payloadArray[$extra_column] = '';
    }
    if(!empty($address_object)) {
      $payloadArray['address'] = $address_object;
    }
    $payload = json_encode($payloadArray);
    
    curl_setopt($curl2, CURLOPT_POSTFIELDS, $payload);
    curl_setopt($curl2, CURLOPT_POST, 1);
    curl_setopt($curl2, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt($curl2, CURLOPT_URL, $url2);
    curl_setopt($curl2, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl2, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl2, CURLOPT_SSL_VERIFYPEER, 0);
    $contentData2 = curl_exec($curl2);
    curl_close($curl2);
    $responseData2 = json_decode($contentData2, true);

    $this->write_log('UserDatapdateCandidate: ', $contentData2);

    return $responseData2;
  }
  //   function doBullhornRefresh()
  // {
  // 	$url = 'https://auth.bullhornstaffing.com/oauth/token?grant_type=refresh_token&refresh_token={refresh_token}&client_id='.CLIENT_ID.'&client_secret='.CLIENT_SECRET';

  //     $options = array(
  //          CURLOPT_RETURNTRANSFER => 1,
  //          CURLOPT_POST => true,
  //          CURLOPT_POSTFIELDS => array()
  //       ); 

  //     $ch  = curl_init( $url ); 
  //     curl_setopt_array( $ch, $options ); 
  //     $content = curl_exec( $ch ); 

  //     curl_close( $ch ); //die($content);

  //     return $content;

  // }

  public function getCandidateUserDataApiById($resturl, $entityid, $token)
  {

    ////  echo  $url1 = $accessToken['restUrl'].'search/Candidate?query=firstName:'.$row['firstName'].' AND lastName:'.$row['lastName'].'&fields=id,firstName,lastName,customText11,customText6,dateAdded&start=0&BhRestToken='.$accessToken['BhRestToken'];

    //  $url1 = $resturl . 'entity/Candidate/'.$entityid.'?fields=firstName,lastName&start=0&BhRestToken=' . $token;
    $url1 = $resturl . 'entity/Candidate/' . $entityid . '?fields=id,firstName,middleName,lastName,customText11,customText6,customText7,customText8,dateAdded&start=0&BhRestToken=' . $token;
    $curl1 = curl_init($url1);
    curl_setopt($curl1, CURLOPT_URL, $url1);
    curl_setopt($curl1, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl1, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl1, CURLOPT_SSL_VERIFYPEER, 0);

    $contentData = curl_exec($curl1);
    curl_close($curl1);
    $responseData = json_decode($contentData, true);
    // echo "<br/>";print_r($responseData);exit;
    $this->write_log('CandidateData: ', $contentData);

    return $responseData;
  }

  /** 
   * Client Contacts by ID 
   * 
   */
  public function getClientContactUserDataApiById($resturl, $entityid, $token)
  {

    ////  echo  $url1 = $accessToken['restUrl'].'search/Candidate?query=firstName:'.$row['firstName'].' AND lastName:'.$row['lastName'].'&fields=id,firstName,lastName,customText11,customText6,dateAdded&start=0&BhRestToken='.$accessToken['BhRestToken'];

    //  $url1 = $resturl . 'entity/Candidate/'.$entityid.'?fields=firstName,lastName&start=0&BhRestToken=' . $token;
    $url1 = $resturl . 'entity/ClientContact/' . $entityid . '?fields=id,firstName,middleName,lastName,customText11,customText6,customText7,customText8,dateAdded&start=0&BhRestToken=' . $token;
    $curl1 = curl_init($url1);
    curl_setopt($curl1, CURLOPT_URL, $url1);
    curl_setopt($curl1, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl1, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl1, CURLOPT_SSL_VERIFYPEER, 0);

    $contentData = curl_exec($curl1);
    curl_close($curl1);
    $responseData = json_decode($contentData, true);
    // echo "<br/>";print_r($responseData);exit;
    $this->write_log('ClientContactData: ', $contentData);

    return $responseData;
  }

  /**
   * Get All Candidate Ids 
   */
  public function getAllCandidateIdsApi($resturl, $token, $id_deleted = 0)
  {
    ////  echo  $url1 = $accessToken['restUrl'].'search/Candidate?query=firstName:'.$row['firstName'].' AND lastName:'.$row['lastName'].'&fields=id,firstName,lastName,customText11,customText6,dateAdded&start=0&BhRestToken='.$accessToken['BhRestToken'];
    // echo $url1 = $resturl . 'search/Candidate?fields=id,firstName,lastName,customText11,customText6,customText7,customText8,dateAdded&start=0&BhRestToken=' . $token;
    // echo $url1 = $resturl . 'query/ClientContact?where=firstName<>""&fields=id,firstName,lastName,customText11,customText6,customText7,customText8,dateAdded&start=0&BhRestToken=' . $token;
    // Get All Candidate Lists
    // echo $url1 = $resturl . 'options/Candidate?BhRestToken=' . $token;
    // echo $url1 = $resturl . 'myClientContacts?fields=firstName,lastName,address&start=0&count=5&BhRestToken=' . $token;
    // echo $url1 = $resturl . 'myCandidates?fields=firstName,lastName,address&start=0&count=5&BhRestToken=' . $token;
    // echo $url1 = $resturl . 'entity/Candidate/*?fields=id,firstName,lastName,address&BhRestToken=' . $token;

    // echo $url1 = $resturl . 'search/Candidate?query=isDeleted:' . $id_deleted . '&BhRestToken=' . $token;
    $url1 = $resturl . 'search/Candidate?query=isDeleted:' . $id_deleted . '&BhRestToken=' . $token;
    // echo $url1 = $resturl . 'search/Candidate?query=isDeleted:' . $id_deleted . '&fields=id,firstName,lastName,customText11,customText6,customText7,customText8,dateAdded&start=0&count=500&BhRestToken=' . $token;

    $curl1 = curl_init($url1);
    curl_setopt($curl1, CURLOPT_URL, $url1);
    curl_setopt($curl1, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl1, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl1, CURLOPT_SSL_VERIFYPEER, 0);

    $contentData = curl_exec($curl1);
    curl_close($curl1);
    $responseData = json_decode($contentData, true);
    $this->write_log('GetAllCandidateIds: ', $contentData);
    return $responseData;
  }

  /**
   * Get All Client Contacts Ids 
   */
  public function getAllClientContactIdsApi($resturl, $token, $id_deleted = 0)
  {
    $url1 = $resturl . 'search/ClientContact?query=isDeleted:' . $id_deleted . '&BhRestToken=' . $token;

    $curl1 = curl_init($url1);
    curl_setopt($curl1, CURLOPT_URL, $url1);
    curl_setopt($curl1, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl1, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl1, CURLOPT_SSL_VERIFYPEER, 0);

    $contentData = curl_exec($curl1);
    curl_close($curl1);
    $responseData = json_decode($contentData, true);
    $this->write_log('GetAllClientContactIds: ', $contentData);
    return $responseData;
  }

  /**
   * Get All Client Contacts Ids 
   */
  public function getAllUsersInformationsApi($resturl, $token, $id_deleted = 0, $entity_type = 'candidate', $start = '0', $count = '500')
  {
    // $url1 = $resturl . 'search/ClientContact?query=isDeleted:' . $id_deleted . '&BhRestToken=' . $token;
    // echo $url1 = $resturl . 'search/' . $entity_type . '?query=isDeleted:' . $id_deleted . '&fields=id,firstName,lastName,customText11,customText6,customText7,customText8,dateAdded&start=' . $start . '&count=' . $count . '&BhRestToken=' . $token;
    $url1 = $resturl . 'search/' . $entity_type . '?query=isDeleted:' . $id_deleted . '&fields=id,firstName,lastName,customText11,customText6,customText7,customText8,dateAdded&start=' . $start . '&count=' . $count . '&BhRestToken=' . $token;

    $curl1 = curl_init($url1);
    curl_setopt($curl1, CURLOPT_URL, $url1);
    curl_setopt($curl1, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl1, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl1, CURLOPT_SSL_VERIFYPEER, 0);

    $contentData = curl_exec($curl1);
    curl_close($curl1);
    $responseData = json_decode($contentData, true);
    $this->write_log('GetAllClientContactIds: ', $contentData);
    return $responseData;
  }
}
