<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require "config.php";
require "bullhorn-curl.php";

// Create a new object from Rectangle class
$obj = new bullhorn;
try {
    $authCode = $obj->getAuthCode(); //echo $authCode;die;
    $auth = $obj->doBullhornAuth($authCode); //echo $auth;die;
    $tokens = json_decode($auth); //print '<pre>';print_r($tokens);die;
    $session = $obj->doBullhornLogin($tokens->access_token);
    $accessToken = json_decode($session, true);
    if (isset($accessToken['restUrl']) && $accessToken['restUrl'] != '') {

        $sel_query = "SELECT * FROM script_records WHERE (syncstatus = 0 OR syncstatusCandidate = 0) ORDER BY id ASC;";
        // $sel_query = "Select * from script_records where syncstatus = 0 ORDER BY id asc;";
        $result = mysqli_query($con, $sel_query);
        while ($row = mysqli_fetch_assoc($result)) {
            
            $address = $obj->generate_address_array($row);
            $encoded_address = $address;
            
            if($row['syncstatus'] == 0) { // Code for the Client
                $responseData =  $obj->getUserDataApi($accessToken['restUrl'], $row['firstName'], $row['lastName'], $accessToken['BhRestToken']);

                if ($responseData['total'] != 0) {
                    $contactId = $responseData['data'][0]['id'];
                    $contactwepid = $responseData['data'][0]['customText11']; //wepid
                    // $contactlicenceno = $responseData['data'][0]['customText7']; //licecne no
                    // $contactlicenceurl = $responseData['data'][0]['customText8']; //licecne url
                    $contactlicenceno = $responseData['data'][0]['customText6']; //licecne no
                    $contactlicenceurl = $responseData['data'][0]['customText7']; //licecne url

                    $new_contact_id = unique_array($contactwepid, $row['id']);
                    $new_contact_licence_no = unique_array($contactlicenceno, $row['licenseNumber']);
                    $new_contact_licence_url = unique_array($contactlicenceurl, $row['details']);

                    // $contactwepidUpdate = ($contactwepid != '') ? $contactwepid . "," . $row['id'] : $row['id'];
                    // $contactlicencenoUpdate = ($contactlicenceno != '') ? $contactlicenceno . "," . $row['licenseNumber'] : $row['licenseNumber'];
                    // $contactlicenceurlUpdate = ($contactlicenceurl != '') ? $contactlicenceurl . "," . $row['details'] : $row['details'];
                    $contactwepidUpdate = ($new_contact_id != '') ? $new_contact_id : $row['id'];
                    $contactlicencenoUpdate = ($new_contact_licence_no != '') ? $new_contact_licence_no : $row['licenseNumber'];
                    $contactlicenceurlUpdate = ($new_contact_licence_url != '') ? $new_contact_licence_url : $row['details'];

                    /// update the user data 
                    $responseData2 = $obj->updateClientContactUserApi($accessToken['restUrl'], $contactId, $accessToken['BhRestToken'], $contactwepidUpdate, $contactlicencenoUpdate, $contactlicenceurlUpdate, null, $encoded_address);

                    ///// end update user data
                    if (isset($responseData2['changeType']) && $responseData2['changeType'] == 'UPDATE') {
                        $update = "UPDATE script_records SET syncstatus = '2', bullhorn_contact_id = '" . $contactId . "' WHERE id='" . $row['id'] . "'";
                        mysqli_query($con, $update) or die(mysqli_error($con));
                        $msg =  "The user data for " . $row['firstName'] . " " . $row['lastName'] . " is updated successfully";
                        $obj->write_log('ClientContactUserDataUpdate: ', $msg);
                        echo $msg;
                        echo "</br>  ";
                    }
                } else {
                    $update = "UPDATE script_records SET syncstatus = '1' WHERE id='" . $row['id'] . "'";
                    mysqli_query($con, $update) or die(mysqli_error($con));
                }
            }

            if($row['syncstatusCandidate'] == 0) { // Code for syncing Candidate infos
                $responseDataCandidate =  $obj->getUserDataCandidateApi($accessToken['restUrl'], $row['firstName'], $row['lastName'], $accessToken['BhRestToken']);
                if ($responseDataCandidate['total'] != 0) {

                    $candidateId = $responseDataCandidate['data'][0]['id'];
                    $candidatewepid = $responseDataCandidate['data'][0]['customText11']; //wepid
                    // $candidatelicenceno = $responseDataCandidate['data'][0]['customText6']; //licecne no
                    // $candidatelicenceurl = $responseDataCandidate['data'][0]['customText7']; //licecne url
                    $candidatelicenceno = $responseDataCandidate['data'][0]['customText7']; //licecne no
                    $candidatelicenceurl = $responseDataCandidate['data'][0]['customText8']; //licecne url
                    
                    $new_candidate_id = unique_array($candidatewepid, $row['id']);
                    $new_candidate_licence_no = unique_array($candidatelicenceno, $row['licenseNumber']);
                    $new_candidate_licence_url = unique_array($candidatelicenceurl, $row['details']);

                    // $candidatewepidUpdate = ($candidatewepid != '') ? $candidatewepid . "," . $row['id'] : $row['id'];
                    // $candidatelicencenoUpdate = ($candidatelicenceno != '') ? $candidatelicenceno . "," . $row['licenseNumber'] : $row['licenseNumber'];
                    // $candidatelicenceurlUpdate = ($candidatelicenceurl != '') ? $candidatelicenceurl . "," . $row['details'] : $row['details'];
                    $candidatewepidUpdate = ($new_candidate_id != '') ? $new_candidate_id : $row['id'];
                    $candidatelicencenoUpdate = ($new_candidate_licence_no != '') ? $new_candidate_licence_no : $row['licenseNumber'];
                    $candidatelicenceurlUpdate = ($new_candidate_licence_url != '') ? $new_candidate_licence_url : $row['details'];

                    /// update the user data 
                    $responseDataCandidate2 = $obj->updateCandidateUserApi($accessToken['restUrl'], $candidateId, $accessToken['BhRestToken'], $candidatewepidUpdate, $candidatelicencenoUpdate, $candidatelicenceurlUpdate, null, $encoded_address);

                    ///// end update user data
                    if (isset($responseDataCandidate2['changeType']) && $responseDataCandidate2['changeType'] == 'UPDATE') {
                        $updateCandidate = "UPDATE script_records SET syncstatusCandidate = '2', bullhorn_candidate_id = '" . $candidateId . "' WHERE id='" . $row['id'] . "'";
                        mysqli_query($con, $updateCandidate) or die(mysqli_error($con));
                        $msg =  "The user data for " . $row['firstName'] . " " . $row['lastName'] . " is updated successfully";
                        $obj->write_log('CandidateUserDataUpdate: ', $msg);
                        echo $msg;
                        echo "</br>  ";
                    }
                } else {
                    $updateCandidate = "UPDATE script_records SET syncstatusCandidate = '1' where id='" . $row['id'] . "'";
                    mysqli_query($con, $updateCandidate) or die(mysqli_error($con));
                }
            }
        }
        ////// Candidate logic here //////////////////
        // $sel_queryCandidate = "Select * from script_records where syncstatusCandidate = 0 ORDER BY id asc;";
        // $resultCandidate = mysqli_query($con, $sel_queryCandidate);
        // while ($rowCandidate = mysqli_fetch_assoc($resultCandidate)) {
        // }
    }
    //   echo $session;die;
} catch (Exception $e) {
    error_log($e->getMessage());
}


function unique_array($bullhorn_db_value, $local_db_value) {
    $customIdsArray = [];

    $bullhorn = explode(',', $bullhorn_db_value);
    $local_id = explode(',', $local_db_value);
    $customIdsArray = array_merge($bullhorn, $local_id);
    
    $custom_contact_id_array = array_unique(array_values(array_filter($customIdsArray)));
    $new_custom_ids = implode(',', $custom_contact_id_array);
    return $new_custom_ids;
}
