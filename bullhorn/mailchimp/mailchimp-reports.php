<?php
require "../config.php";
// Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, MAILCHIMP_API_URL . "campaigns?offset=0&count=" . MAILCHIMP_COUNT_RECORD);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

// curl_setopt($ch, CURLOPT_USERPWD, 'anystring' . ':' . "5bc974b7a9d733e1bbe9327b695c0281-us9");
curl_setopt($ch, CURLOPT_USERPWD, 'anystring' . ':' . MAILCHIMP_APIKEY);

$result = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}
curl_close($ch);
if (isset($result) && $result != "") {
    $resultData = json_decode($result, true);
    echo "<pre>";
    // echo "***** Campaign Listing Start ***** <br/>";
    print_r($resultData);
    // echo "***** Campaign Listing End ***** <br/>";
    /** Campaign Data */
    foreach ($resultData['campaigns'] as $resultDataKey => $resultDataVal) {
        // echo $resultDataVal['id'];
        echo "<br>";
        // echo $resultDataVal['recipients']['list_id'];

        /**
         * Insert Mailchimp Campaign Records - START Here 
         */
        // $dateObject = date_parse($resultDataVal['send_time']);
        $dateObject = date_parse($resultDataVal['create_time']);

        $year = str_pad($dateObject['year'], '4', '0', STR_PAD_LEFT);
        $month = str_pad($dateObject['month'], '2', '0', STR_PAD_LEFT);
        $day = str_pad($dateObject['day'], '2', '0', STR_PAD_LEFT);
        $hour = str_pad($dateObject['hour'], '2', '0', STR_PAD_LEFT);
        $minute = str_pad($dateObject['minute'], '2', '0', STR_PAD_LEFT);
        $second = str_pad($dateObject['second'], '2', '0', STR_PAD_LEFT);

        $campaign_created_at = $year . '-' . $month . '-' . $day . ' ' . $hour . ':' . $minute . ':' . $second;

        $sendDateObject = date_parse($resultDataVal['send_time']);

        $year = str_pad($sendDateObject['year'], '4', '0', STR_PAD_LEFT);
        $month = str_pad($sendDateObject['month'], '2', '0', STR_PAD_LEFT);
        $day = str_pad($sendDateObject['day'], '2', '0', STR_PAD_LEFT);
        $hour = str_pad($sendDateObject['hour'], '2', '0', STR_PAD_LEFT);
        $minute = str_pad($sendDateObject['minute'], '2', '0', STR_PAD_LEFT);
        $second = str_pad($sendDateObject['second'], '2', '0', STR_PAD_LEFT);

        if(!empty($sendDateObject['year'])) {
            $campaign_send_at = $year . '-' . $month . '-' . $day . ' ' . $hour . ':' . $minute . ':' . $second;
        } else {
            $campaign_send_at = date('Y-m-d H:i:s', strtotime(date('1990-03-05 10:45:00')));
        }


        $sel_query = "SELECT * FROM `mailchimp_campaigns` WHERE campaign_id = '" . $resultDataVal['id'] . "' AND web_id = '" . $resultDataVal['web_id'] . "'";
        echo $sel_query . ' <br/>';
        $result = mysqli_query($con, $sel_query);
        $rowcount = mysqli_num_rows($result);

        $jsonResultData = htmlentities(json_encode($resultDataVal));
        $jsonResultData = "";

        if ($rowcount > 0) {
            while ($row = mysqli_fetch_assoc($result)) {

                $subject_line = !empty($resultDataVal['settings']['subject_line'])
                    ? $resultDataVal['settings']['subject_line']
                    : (!empty($resultDataVal['settings']['preview_text'])
                        ? $resultDataVal['settings']['preview_text']
                        : '');

                $update = "UPDATE `mailchimp_campaigns` SET 
                                `subject` = '" . addslashes($subject_line) . "', 
                                `list_name` = '" . $resultDataVal['recipients']['list_name'] . "',
                                `campaign_json_data` = '" . $jsonResultData . "', 
                                `campaign_create_at` = '" . $campaign_created_at . "', 
                                `updated_at` = '" . date('Y-m-d H:i:s') . "' 
                            WHERE campaign_id = '" . $resultDataVal['id'] . "' AND web_id = '" . $resultDataVal['web_id'] . "'";
                echo $update . "<br/>";
                mysqli_query($con, $update) or die(mysqli_error($con));
            }
        } else {
            $subject_line = !empty($resultDataVal['settings']['subject_line'])
                ? $resultDataVal['settings']['subject_line']
                : (!empty($resultDataVal['settings']['preview_text'])
                    ? $resultDataVal['settings']['preview_text']
                    : '');

            $insert = "INSERT INTO `mailchimp_campaigns` (`campaign_id`, `web_id`, `subject`, `list_name`, `campaign_json_data`, `campaign_sent_at`, `campaign_create_at`, `created_at`, `updated_at`) 
                            VALUES ('" . $resultDataVal['id'] . "', '" . $resultDataVal['web_id'] . "', '" . addslashes($subject_line) . "', '" . $resultDataVal['recipients']['list_name'] . "', '" . $jsonResultData . "', '" . $campaign_send_at . "', '" . $campaign_created_at . "', '" . date('Y-m-d H:i:s') . "', '" . date('Y-m-d H:i:s') . "')";
            echo $insert . "<br/>";
            mysqli_query($con, $insert) or die(mysqli_error($con));
        }
        /**
         * Insert Mailchimp Campaign Records - END Here 
         */

        $ch = curl_init();

        // curl_setopt($ch, CURLOPT_URL, "https://us9.api.mailchimp.com/3.0/reports/" . $resultDataVal['id'] . "/click-details/");
        curl_setopt($ch, CURLOPT_URL, MAILCHIMP_API_URL . "reports/" . $resultDataVal['id'] . "/click-details?offset=0&count=" . MAILCHIMP_COUNT_RECORD);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        // curl_setopt($ch, CURLOPT_USERPWD, 'anystring' . ':' . "5bc974b7a9d733e1bbe9327b695c0281-us9");
        curl_setopt($ch, CURLOPT_USERPWD, 'anystring' . ':' . MAILCHIMP_APIKEY);

        $resultClick = curl_exec($ch);
        // echo "***** Response of Click Details ***** <br/>";exit;
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        if (isset($resultClick) && $resultClick != "") {
            $resultClickData = json_decode($resultClick, true);
            //    print_r($resultClickData);exit;
            // Campaign's URL Clicked
            // echo "*** click data: *** ";print_r($resultClickData);
            foreach ($resultClickData['urls_clicked'] as $resultClickDataKey => $resultClickDataVal) {

                /**
                 * Insert Mailchimp Link Details - START Here 
                 */
                // last_click
                $last_click_at_object = date_parse($resultClickDataVal['last_click']);

                $year = str_pad($last_click_at_object['year'], '4', '0', STR_PAD_LEFT);
                $month = str_pad($last_click_at_object['month'], '2', '0', STR_PAD_LEFT);
                $day = str_pad($last_click_at_object['day'], '2', '0', STR_PAD_LEFT);
                $hour = str_pad($last_click_at_object['hour'], '2', '0', STR_PAD_LEFT);
                $minute = str_pad($last_click_at_object['minute'], '2', '0', STR_PAD_LEFT);
                $second = str_pad($last_click_at_object['second'], '2', '0', STR_PAD_LEFT);

                
                if(!empty($last_click_at_object['year'])) {
                    $last_click_at = $year . '-' . $month . '-' . $day . ' ' . $hour . ':' . $minute . ':' . $second;
                } else {
                    $last_click_at = date('Y-m-d H:i:s', strtotime(date('1990-03-05 10:45:00')));
                }
                // $last_click_at = at_object
                $sel_query_camp_links = "SELECT * FROM `maichimp_campaign_links` WHERE campaign_id = '" . $resultClickDataVal['campaign_id'] . "' AND link_id = '" . $resultClickDataVal['id'] . "'";
                echo $sel_query_camp_links . ' <br/>';
                $result = mysqli_query($con, $sel_query_camp_links);
                $rowcount = mysqli_num_rows($result);
                if ($rowcount > 0) {
                    while ($row = mysqli_fetch_assoc($result)) {
                        $update_camp_links = "UPDATE `maichimp_campaign_links` SET 
                                        `total_clicks` = '" . $resultClickDataVal['total_clicks'] . "', 
                                        `click_percentage` = '" . $resultClickDataVal['click_percentage'] . "',
                                        `unique_clicks` = '" . $resultClickDataVal['unique_clicks'] . "', 
                                        `unique_click_percentage` = '" . $resultClickDataVal['unique_click_percentage'] . "', 
                                        `last_click_at` = '" . $last_click_at . "', 
                                        `updated_at` = '" . date('Y-m-d H:i:s') . "' 
                                    WHERE campaign_id = '" . $resultClickDataVal['campaign_id'] . "' AND link_id = '" . $resultClickDataVal['id'] . "'";
                        echo $update_camp_links . ' <br/>';
                        mysqli_query($con, $update_camp_links) or die(mysqli_error($con));
                    }
                } else {
                    $insert_camp_links = "INSERT INTO `maichimp_campaign_links` (`campaign_id`, `link_id`, `link_url`, `total_clicks`, `click_percentage`, `unique_clicks`, `unique_click_percentage`, `last_click_at`, `created_at`, `updated_at`) 
                                    VALUES ('" . $resultClickDataVal['campaign_id'] . "', '" . $resultClickDataVal['id'] . "', '" . $resultClickDataVal['url'] . "', '" . $resultClickDataVal['total_clicks'] . "', '" . $resultClickDataVal['click_percentage'] . "', '" . $resultClickDataVal['unique_clicks'] . "', '" . $resultClickDataVal['unique_click_percentage'] . "', '" . $last_click_at . "', '" . date('Y-m-d H:i:s') . "', '" . date('Y-m-d H:i:s') . "')";
                    echo $insert_camp_links . ' <br/>';
                    mysqli_query($con, $insert_camp_links) or die(mysqli_error($con));
                }
                /**
                 * Insert Mailchimp Link Details - END Here 
                 */

                $ch = curl_init();

                // curl_setopt($ch, CURLOPT_URL, "https://us9.api.mailchimp.com/3.0/reports/" . $resultDataVal['id'] . "/click-details/" . $resultClickDataVal['id'] . "/members");
                curl_setopt($ch, CURLOPT_URL, MAILCHIMP_API_URL . "reports/" . $resultDataVal['id'] . "/click-details/" . $resultClickDataVal['id'] . "/members?offset=0&count=" . MAILCHIMP_COUNT_RECORD);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

                // curl_setopt($ch, CURLOPT_USERPWD, 'anystring' . ':' . "5bc974b7a9d733e1bbe9327b695c0281-us9");
                curl_setopt($ch, CURLOPT_USERPWD, 'anystring' . ':' . MAILCHIMP_APIKEY);

                $resultClickEmail = curl_exec($ch);
                echo "*** click emails ***";
                print_r($resultClickEmail);
                if (curl_errno($ch)) {
                    echo 'Error:' . curl_error($ch);
                }
                curl_close($ch);
                if (isset($resultClickEmail) && $resultClickEmail != "") {
                    $resultClickEmailData = json_decode($resultClickEmail, true);
                    echo "<pre>";
                    // print_r($resultClickEmailData);
                    // echo " ***** insert/update records ***** ";
                    /**
                     * Insert Mailchimp Member Details - START Here 
                     */
                    if (count($resultClickEmailData['members']) > 0) {
                        foreach ($resultClickEmailData['members'] as $resultMemberkey => $resultMemberVal) {
                            //                            echo $resultMemberVal['email_address']; echo "<br>";
                            //                            echo $resultMemberVal['clicks'];
                            $sel_query_link_members = "SELECT * FROM `maichimp_campaign_link_members` WHERE campaign_id = '" . $resultMemberVal['campaign_id'] . "' AND link_id = '" . $resultMemberVal['list_id'] . "' AND url_id = '" . $resultMemberVal['url_id'] . "' AND email_id = '" . $resultMemberVal['email_id'] . "'";
                            echo $sel_query_link_members . '<br/>';
                            $result = mysqli_query($con, $sel_query_link_members);
                            $rowcount = mysqli_num_rows($result);
                            if ($rowcount > 0) {
                                while ($row = mysqli_fetch_assoc($result)) {
                                    $update_link_members = "UPDATE `maichimp_campaign_link_members` SET 
                                                    clicks = '" . $resultMemberVal['clicks'] . "',
                                                    vip = '" . $resultMemberVal['vip'] . "',
                                                    contact_status = '" . $resultMemberVal['contact_status'] . "',
                                                    updated_at = '" . date('Y-m-d H:i:s') . "'
                                             WHERE campaign_id = '" . $resultMemberVal['campaign_id'] . "' AND link_id = '" . $resultMemberVal['list_id'] . "' AND url_id = '" . $resultMemberVal['url_id'] . "' AND email_id = '" . $resultMemberVal['email_id'] . "'";
                                    echo $update_link_members . '<br/>';
                                    mysqli_query($con, $update_link_members) or die(mysqli_error($con));
                                }
                            } else {
                                $insert_link_members = "INSERT INTO `maichimp_campaign_link_members` (`campaign_id`, `link_id`, `url_id`, `email_id`, `email_address`, `clicks`, `vip`, `contact_status`, `created_at`, `updated_at`) 
                                                VALUES ('" . $resultMemberVal['campaign_id'] . "', '" . $resultMemberVal['list_id'] . "', '" . $resultMemberVal['url_id'] . "', '" . $resultMemberVal['email_id'] . "', '" . $resultMemberVal['email_address'] . "', '" . $resultMemberVal['clicks'] . "', '" . $resultMemberVal['vip'] . "', '" . $resultMemberVal['contact_status'] . "', '" . date('Y-m-d H:i:s') . "', '" . date('Y-m-d H:i:s') . "')";
                                echo $insert_link_members . '<br/>';
                                mysqli_query($con, $insert_link_members) or die(mysqli_error($con));
                            }
                        }
                    }
                    /**
                     * Insert Mailchimp Member Details - END Here 
                     */
                }
            }
        }
    }
}
exit;
