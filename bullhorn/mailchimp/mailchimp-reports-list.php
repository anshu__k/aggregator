<!doctype html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="//cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- Font Awesome -->
    <link href="//use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet" crossorigin="anonymous">
    <!-- <link href="//cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" rel="stylesheet" crossorigin="anonymous">
    <link href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css" rel="stylesheet" crossorigin="anonymous"> -->
</head>

<body>
    <div class="container-fluid">
        <?php
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        require "../config.php";
        try {
            // $sel_query = "SELECT * FROM mailchimp_campaigns ORDER BY campaign_create_at desc";
            $sel_query = "SELECT mc.* FROM mailchimp_campaigns AS mc JOIN maichimp_campaign_links AS mcl ON mc.campaign_id = mcl.campaign_id GROUP BY mc.`campaign_id` ORDER BY mc.campaign_create_at DESC";
            $result = mysqli_query($con, $sel_query);
            $rowcount = mysqli_num_rows($result);
            if ($rowcount > 0) { ?>
                <div class="row">
                    <h2 class="title">List of MailChimp Records</h2>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered maintable" id="campainTable">
                            <thead class="table-dark">
                                <tr>
                                    <th class="text-center align-middle" scope="col">Sr No.</th>
                                    <th class="text-center align-middle" scope="col">Campaign ID</th>
                                    <th class="text-center align-middle w-50" scope="col">Subject</th>
                                    <th class="text-center align-middle" scope="col">List Name</th>
                                    <th class="text-center align-middle" scope="col">Campaign Created At <br /> Campaign Sent At</th>
                                    <th class="text-center align-middle" scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $count = 1;
                                while ($row = mysqli_fetch_assoc($result)) {
                                    /** Sub query data for the Links */
                                    $sel_link_query = "SELECT * FROM maichimp_campaign_links WHERE campaign_id = '" . $row['campaign_id'] . "' ORDER BY id DESC";
                                    $result_link = mysqli_query($con, $sel_link_query);
                                    $rowcount_link = mysqli_num_rows($result_link);
                                    $count_link = 1;
                                    // $row['campaign_create_at'];
                                ?>
                                    <tr style="line-height: 25px">
                                        <td class="text-center align-middle"> <?php echo $count; ?> </td>
                                        <td class="text-center align-middle"> <?php echo $row['campaign_id']; ?> </td>
                                        <td class="text-center align-middle"> <?php echo $row['subject']; ?> </td>
                                        <td class="text-center align-middle"> <?php echo $row['list_name']; ?> </td>
                                        <td class="text-center align-middle"> <?php echo $row['campaign_create_at'] . "<br/>" . $row['campaign_sent_at']; ?> </td>
                                        <td class="text-center align-middle">
                                            <button class="btn btn-dark blh_mlchp__syncData" data-bullhornData="<?php echo $row['campaign_id']; ?>" data-eventType='' data-mailchimpSubject="<?php echo $row['subject']; ?>" data-campainSentDate="<?php echo $row['campaign_sent_at'];?>" data-opend_date=''>Sync</button>
                                            <button class="btn btn-dark toggle" value="<?php echo $row['campaign_id']; ?>">Toggle</button>
                                        </td>
                                    </tr>
                                    <!-- Link Details -->
                                    <tr class="subtable<?php echo $row['campaign_id']; ?>" style="display:none;">
                                        <td colspan="2">&nbsp;</td>
                                        <td colspan="4">
                                            <table class="table table-striped table-bordered ">
                                                <thead class="table-dark">
                                                    <tr>
                                                        <th class="text-center align-middle" scope="col">Sr No.</th>
                                                        <th class="text-center align-middle" scope="col">Link ID</th>
                                                        <th class="text-center align-middle w-50" scope="col">Link URL</th>
                                                        <th class="text-center align-middle" scope="col">Total Clicks</th>
                                                        <th class="text-center align-middle" scope="col">Unique Clicks</th>
                                                        <th class="text-center align-middle" scope="col">Action</th>
                                                    </tr>
                                                </thead>

                                                <?php
                                                while ($row_link = mysqli_fetch_assoc($result_link)) { 
                                                    $original_link = substr($row_link['link_url'], 0, strpos($row_link['link_url'], TRUNCATE_LINK_CHARACTER));
                                                ?>
                                                    <tr style="line-height: 25px">
                                                        <td class="text-center align-middle"> <?php echo $count_link; ?> </td>
                                                        <td class="text-center align-middle"> <?php echo $row_link['link_id']; ?> </td>
                                                        <td class="text-center align-middle"> <?php echo $original_link; ?> </td>
                                                        <td class="text-center align-middle"> <?php echo $row_link['total_clicks'] ?> </td>
                                                        <td class="text-center align-middle"> <?php echo $row_link['unique_clicks'] ?> </td>
                                                        <td class="text-center align-middle">
                                                            <button class="btn btn-dark blh_mlchp__syncData" data-bullhornData="<?php echo $row_link['campaign_id']; ?>" data-eventType="<?php echo $row_link['link_id']; ?>" data-mailchimpSubject="<?php echo $row['subject']; ?>" data-campainSentDate="<?php echo $row['campaign_sent_at'];?>" data-opend_date=''>Sync</button>
                                                            <button class="btn btn-dark toggleMember" value="<?php echo $row_link['link_id']; ?>">Toggle</button>
                                                        </td>
                                                    </tr>

                                                    <?php
                                                    $sel_link_querymember = "SELECT * FROM maichimp_campaign_link_members WHERE url_id = '" . $row_link['link_id'] . "' AND campaign_id ='" . $row_link['campaign_id'] . "' ORDER BY id DESC";
                                                    $result_linkmember = mysqli_query($con, $sel_link_querymember);
                                                    $rowcount_linkmember = mysqli_num_rows($result_linkmember);
                                                    $count_link_member = 1;
                                                    ?>
                                                    <tr class="subtablemember<?php echo $row_link['link_id']; ?>" style="display:none;">
                                                        <td colspan="2">&nbsp;</td>
                                                        <td colspan="4">
                                                            <table class="table table-striped table-bordered ">
                                                                <thead class="table-dark">
                                                                    <tr>
                                                                        <th class="text-center align-middle" scope="col">Sr No.</th>
                                                                        <th class="text-center align-middle" scope="col">Email Address</th>
                                                                        <th class="text-center align-middle" scope="col">Clicks</th>
                                                                        <th class="text-center align-middle" scope="col">Contact Status</th>
                                                                        <th class="text-center align-middle" scope="col">Action</th>
                                                                    </tr>
                                                                </thead>

                                                                <?php
                                                                while ($row_linkmember = mysqli_fetch_assoc($result_linkmember)) { ?>
                                                                    <tr style="line-height: 25px">
                                                                        <td class="text-center align-middle"> <?php echo $count_link_member; ?> </td>
                                                                        <td class="text-center align-middle"> <?php echo $row_linkmember['email_address']; ?> </td>
                                                                        <td class="text-center align-middle"> <?php echo $row_linkmember['clicks'] ?> </td>
                                                                        <td class="text-center align-middle"> <?php echo $row_linkmember['contact_status'] ?> </td>
                                                                        <td class="text-center align-middle">
                                                                            <button class="btn btn-dark blh_mlchp__syncData" data-bullhornData="<?php echo $row_linkmember['campaign_id']; ?>" data-eventType="<?php echo $row_linkmember['url_id']; ?>" data-mailchimpSubject="<?php echo $row['subject']; ?>" data-campainSentDate="<?php echo $row['campaign_sent_at'];?>" data-memberid='<?php echo $row_linkmember['id']; ?>' data-opend_date=''>Sync</button>
                                                                        </td>
                                                                    </tr>
                                                                <?php $count_link_member++;
                                                                } ?>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                <?php $count_link++;
                                                } ?>
                                            </table>
                                        </td>
                                    </tr>
                                <?php $count++;
                                } ?>
                            </tbody>

                        </table>
                    </div>
                </div>
        <?php
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            error_log($e->getMessage());
        }
        ?>
    </div>
</body>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script> -->
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<!-- <script src="//cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script> -->
<script>
    // $(document).ready(function(){
    //     $('#campainTable').DataTable({
    //         // pagingType: 'full_numbers',
    //     });
    // });

    $(document).on('click', '.blh_mlchp__syncData', function() {
        var clickedElem = $(this);
        bullHornData = clickedElem.attr('data-bullhornData');
        // parseBullHornData = JSON.parse(bullHornData);
        parseBullHornData = bullHornData;
        eventType = clickedElem.attr('data-eventType');
        // localDbData = clickedElem.attr('data-localDbData');
        mailchimpSubjectDbData = clickedElem.attr('data-mailchimpSubject');
        openDate = clickedElem.attr('data-opend_date');
        dataMemberid = clickedElem.attr('data-memberid');
        campainSentDate = clickedElem.attr('data-campainSentDate');
        //        parseLocalDbData = JSON.parse(localDbData);	
        // parseLocalDbData = localDbData;
        parseMailchimpDbData = mailchimpSubjectDbData;
        parseCampainSentDate = campainSentDate;

        const swalWithBootstrapButtons = Swal.mixin({
            // customClass: {
            //     confirmButton: 'btn btn-success',
            //     cancelButton: 'btn btn-danger'
            // },
            // buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: 'Are you sure to sync data?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, sync it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                // Add Spinner to the sync button
                clickedElem.attr('disabled', true).append('<i class="fa fa-spinner fa-spin"></i>');
                $.ajax({
                    type: 'POST',
                    url: 'manualMailchimpDataSyncToBullhornData.php',
                    cache: false,
                    dataType: 'json',
                    data: {
                        'bullHornData': parseBullHornData,
                        'eventType': eventType,
                        'openDate': openDate ?? "",
                        // 'localData': parseLocalDbData,
                        'mailchimpSubjectData': parseMailchimpDbData,
                        'campainSentDate': parseCampainSentDate,
                        'dataMemberid': dataMemberid ?? ""

                    },
                    success: function(response) {
                        console.log(response.status);
                        if (response.status) {
                            swalWithBootstrapButtons.fire(
                                'Synced!',
                                'Your data has been synced.',
                                'success'
                            )
                            // clickedElem.parent('td').html('<span style="font-weight: 900; color: green;">' + response.text + '</span> ');
                        } else {
                            swalWithBootstrapButtons.fire(
                                'Synced!',
                                'Error! in syncing data to Bullhorn.',
                                'error'
                            )
                        }
                        clickedElem.attr('disabled', false).find('.fa-spinner').remove();
                    },
                    error: function() {
                        console.log('Response Error ');
                        clickedElem.attr('disabled', false).find('.fa-spinner').remove();
                        swalWithBootstrapButtons.fire(
                            'Synced!',
                            'Error! in syncing data to Bullhorn.',
                            'error'
                        )
                    }
                });
            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                    'Cancelled',
                    'Your sync data request was cancelled :)',
                    'error'
                )
            }
        });
    });
    //    $('.maintable').each(function(){	
    //    var thead = $(this).find('toggle');	
    //    var tbody = $(this).find('tbody');	
    //    tbody.hide();	
    $(document).on('click', '.toggle', function() { //alert($(this).val());	
        var tbody = $('tr.subtable' + $(this).val());
        // var tbody = $('table.subtable' + $(this).val());
        tbody.slideToggle();
        //      alert(tbody);	
        //    })	
    });
    $(document).on('click', '.toggleMember', function() { //alert($(this).val());	
        var tbody = $('tr.subtablemember' + $(this).val());
        // var tbody = $('table.subtable' + $(this).val());
        tbody.slideToggle();
        //      alert(tbody);	
        //    })	
    });
</script>

</html>