<?php
require "../config.php";
require "../bullhorn-curl.php";
// Create a new object from Rectangle class
$obj = new bullhorn;
try {
    // echo "<pre>";print_r($_REQUEST);exit;
    $authCode = $obj->getAuthCode(); //echo $authCode;die;
    $auth = $obj->doBullhornAuth($authCode); //echo $auth;die;
    $tokens = json_decode($auth); //print '<pre>';print_r($tokens);die;
    $session = $obj->doBullhornLogin($tokens->access_token);
    $accessToken = json_decode($session, true);
    if (isset($accessToken['restUrl']) && $accessToken['restUrl'] != '') {
        // echo "<br/>";
        // print_r($_REQUEST);
        if (isset($_REQUEST['dataMemberid']) && $_REQUEST['dataMemberid'] != '') {
            $sel_query = "SELECT cm.*, cl.*, cm.id as mailchimp_campaign_link_member_id from maichimp_campaign_link_members AS cm, maichimp_campaign_links AS cl WHERE cm.campaign_id = '" . $_REQUEST['bullHornData'] . "' AND cm.url_id = '" . $_REQUEST['eventType'] . "' and cm.id='" . $_REQUEST['dataMemberid'] . "'  and cl.link_id =cm.url_id and cl.campaign_id =cm.campaign_id  ";
        } else if (isset($_REQUEST['eventType']) && $_REQUEST['eventType'] != '') {
            $sel_query = "SELECT cm.*, cl.*, cm.id as mailchimp_campaign_link_member_id FROM maichimp_campaign_link_members AS cm, maichimp_campaign_links AS cl WHERE cm.campaign_id = '" . $_REQUEST['bullHornData'] . "' AND cm.url_id = '" . $_REQUEST['eventType'] . "' and cl.link_id =cm.url_id and cl.campaign_id =cm.campaign_id ;";
        } else {
            $sel_query = "SELECT cm.*, cl.*, cm.id as mailchimp_campaign_link_member_id FROM maichimp_campaign_link_members AS cm, maichimp_campaign_links AS cl WHERE cm.campaign_id = '" . $_REQUEST['bullHornData'] . "'  and cl.link_id =cm.url_id and cl.campaign_id =cm.campaign_id ;";
        }
        // echo $sel_query;
        //exit;
        $result = mysqli_query($con, $sel_query);
        // echo "<pre>";print_r($result);exit;
        $rowcount = mysqli_num_rows($result);
        // print_r($rowcount);
        if ($rowcount > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                // $responseData =  $obj->getUserDataApi($accessToken['restUrl'], $row['email'], $accessToken['BhRestToken']);
                // echo 'inside data: ';
                //                 print_r($row);exit;
                $responseData = $obj->getClientContactUserDataByEmailApi(CLIENT_CONTACT_ENTITY, $accessToken['restUrl'], $row['email_address'], $accessToken['BhRestToken']);
                $sync_bullhorn_status = '2';    // Failed to sync
                //         $responseData =  $obj->getUserDataByemailApi($accessToken['restUrl'],'czhao3@tulane.edu',$accessToken['BhRestToken']);
                if ($responseData['total'] != 0) {
                    $contactId = $responseData['data'][0]['id'];
                    // print_r($responseData);
                    // print_r($contactId);
                    $original_link = substr($row['link_url'], 0, strpos($row['link_url'], TRUNCATE_LINK_CHARACTER));

                    $campaign_sent_date_milliseconds = (!empty($_REQUEST['campainSentDate']) ? strtotime($_REQUEST['campainSentDate']) * 1000 : '');

                    // $comments = $_REQUEST['localData'] . " + " . (!empty($_REQUEST['openDate']) ? $_REQUEST['openDate'] : '') . " + " . $row['email_address'] . " + " . $original_link . " + " . $row['clicks'];
                    // Campain Subject + Campain Sent Date + 
                    $comments = $_REQUEST['mailchimpSubjectData'] . " + " . $campaign_sent_date_milliseconds . " + " . $row['email_address'] . " + " . $original_link . " + " . $row['clicks'];
                    //            exit;
                    //  $responseData =  $obj->getUserDataApi($accessToken['restUrl'],$row['firstName'],$row['lastName'],$accessToken['BhRestToken']);
                    // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
                    $postData = array(
                        "action" => "MailChimp Campaign",
                        "personReference" => array('id' => $contactId),
                        // "dateAdded" => strtotime("now") * 1000,
                        "dateAdded" => $campaign_sent_date_milliseconds,
                        // "commentingPerson" => $contactId,
                        "comments" => $comments
                    );
                    $ch = curl_init();
                    // echo $accessToken['restUrl'] . '/entity/Note?BhRestToken=' . $accessToken['BhRestToken'];exit;
                    curl_setopt($ch, CURLOPT_URL, $accessToken['restUrl'] . '/entity/Note?BhRestToken=' . $accessToken['BhRestToken']);
                    // curl_setopt($ch, CURLOPT_URL, $accessToken['restUrl'] . '/entity/ClientContact?BhRestToken=' . $accessToken['BhRestToken']);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                    // curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"personReference\":{\"id\":".$contactId."},\"comments\":\"".$comments."\",\"action\":\"MailChimp Campaign\"}");
                    // echo json_encode($postData);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));
                    $headers = array();
                    $headers[] = 'Content-Type: application/json';
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    $curl_result = curl_exec($ch);
                    // print_r('bullhorn API response');
                    // print_r($curl_result);
                    // print_r(curl_errno($ch));exit;
                    if (curl_errno($ch)) {
                        echo 'Error:' . curl_error($ch);
                    }
                    curl_close($ch);

                    // Udate sync data to the mailchimp database and also insert contact id
                    $sync_bullhorn_status = '1';    // Sync Success
                }

                $update = "UPDATE maichimp_campaign_link_members SET sync_to_bullhorn = '" . $sync_bullhorn_status . "', bullhorn_contact_id = '" . $contactId . "', updated_at = '" . date('Y-m-d H:i:s') . "' WHERE id = '" . $row['mailchimp_campaign_link_member_id'] . "'";
                mysqli_query($con, $update) or die(mysqli_error($con));
            }
            echo json_encode(['status' => true, 'data' => 'success', 'text' => 'Synced']);
            exit;
        }
        echo json_encode(['status' => false, 'data' => 'Failed - no record found in DB']);
    }
    echo json_encode(['status' => false, 'data' => 'Failed']);
    exit;
} catch (Exception $e) {
    echo "test";
    error_log($e->getMessage());
}
