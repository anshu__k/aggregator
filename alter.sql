<!--alter for source loc -->
ALTER TABLE `sourc_locs` ADD `renewable_wipe_test` INT(11) NULL DEFAULT NULL AFTER `note_num`, ADD `installation_wipe_test` INT(11) NULL DEFAULT NULL AFTER `renewable_wipe_test`;
ALTER TABLE `users` ADD `status` TINYINT(4) NOT NULL DEFAULT '1' AFTER `email_verified_at`;
ALTER TABLE `customers` ADD `status` TINYINT(4) NOT NULL DEFAULT '1' AFTER `is_deleted`;
ALTER TABLE `contacts` DROP `status`;
ALTER TABLE `contacts` ADD `status` TINYINT NOT NULL DEFAULT '1' AFTER `deleted_at`;
ALTER TABLE `machines` ADD `status` TINYINT NOT NULL DEFAULT '1' AFTER `mach_model`;
ALTER TABLE `sources` ADD `status` TINYINT NOT NULL DEFAULT '1' AFTER `source_model`;
ALTER TABLE `users` CHANGE `title` `title` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL;
ALTER TABLE `customers` CHANGE `filing_name` `filing_name` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL;
