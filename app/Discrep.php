<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discrep extends Model
{
    protected $table = 'discreps';
    protected $fillable = [
        'discrepcod', 'descriptn'
    ];
}
