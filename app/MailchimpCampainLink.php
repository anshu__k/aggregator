<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\MailchimpCampainLinkMember;

class MailchimpCampainLink extends Model
{
    protected $table = 'maichimp_campaign_links';

    protected $fillable = [
        'campaign_id',
        'link_id',
        'link_url',
        'total_clicks',
        'click_percentage',
        'unique_clicks',
        'unique_click_percentage',
        'last_click_at'
    ];

    public function mailchimp_campaign_link_members()
    {
        return $this->hasMany(MailchimpCampainLinkMember::class, 'url_id', 'link_id');
    }

    public function getMailchimpCampaignLinkMembersAttribute()
    {
        $mailchimp_link_members = $this->mailchimp_campaign_link_members()->getQuery()->orderBy('id', 'DESC')->get();
        return $mailchimp_link_members;
    }

    public function mailchimp_campaign_link_members_ordering()
    {
        return $this->hasMany(MailchimpCampainLinkMember::class, 'url_id', 'link_id')->orderBy('id', 'DESC');
    }
}
