<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UploadDocument extends Model
{
    use SoftDeletes;

    public $timestamps = false;

    protected $table = 'upload_documents';
    
    protected $fillable = [
        'title', 'source_id', 'customer_id', 'machine_id', 'technician_id', 'contact_id', 'document', 'upload_date'
    ];

}
