<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\MailchimpCampainLink;

class MailchimpCampain extends Model
{
    protected $table = 'mailchimp_campaigns';

    protected $fillable = [
        'campaign_id',
        'web_id',
        'subject',
        'list_name',
        'campaign_json_data',
        'campaign_create_at',
        'campaign_sent_at'
    ];

    public function mailchimp_campaign_links()
    {
        return $this->hasMany(MailchimpCampainLink::class, 'campaign_id', 'campaign_id');
                // ->has('mailchimp_campaign_link_members');
    }

    public function getMailchimpCampaignLinksAttribute()
    {
        $mailchimp_links = $this->mailchimp_campaign_links()->getQuery()->orderBy('id', 'DESC')->get();
        return $mailchimp_links;
        // return $this->hasMany(MailchimpCampainLink::class, 'campaign_id', 'campaign_id');
    }

    public function mailchimp_campaign_links_ordering()
    {
        return $this->hasMany(MailchimpCampainLink::class, 'campaign_id', 'campaign_id')
                // ->has('mailchimp_campaign_link_members')
                ->orderBy('id', 'DESC');
    }

    public function mailchimp_campaign_link_members()
    {
        return $this->hasMany(MailchimpCampainLinkMember::class, 'campaign_id', 'campaign_id');
    }
}
