<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rso extends Model
{
	use SoftDeletes;
	
    protected $table = 'rso';
    protected $fillable = [
        'name'
    ];

    
}
