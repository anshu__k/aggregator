<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Machine extends Model
{
    use SoftDeletes;

    protected $table = 'machines';
    protected $fillable = [
        'mach_number', 'mach_model'
    ];

    public function ship_date(){
    	return $this->hasOne('App\MachLoc', 'machine_id', 'mach_number');
    }

    public function mach_loc(){
    	return $this->hasOne('App\MachLoc', 'machine_id', 'mach_number');
    }

    public function machloc(){
        return $this->hasMany('App\MachLoc', 'machine_id', 'mach_number');
    }

    public function wipedata(){
        return $this->hasOne('App\Wipe', 'mach_num', 'mach_number');
    }

    public function sourceloc_data(){
        return $this->hasMany('App\SourceLoc', 'mach_num', 'mach_number');
    }

    public static function getGbx($mach_num){

        $smod = DB::table('sourc_locs')
                        ->select('s_mod_num')
                        ->where('mach_num',$mach_num)
                        ->first();

        if(!isset($smod->s_mod_num)){
            return '';
        }

        $m_num = $smod->s_mod_num;      
        $gb =DB::table('smodlist')
                        ->select('gbx')
                        ->where('s_mod_num',$m_num)
                        ->first(); 

        return isset($gb->gbx)?$gb->gbx:'';
    }


}
