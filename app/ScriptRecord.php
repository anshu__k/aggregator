<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ScriptRecordSentPostcard;

class ScriptRecord extends Model
{
    protected $table = 'script_records';

    public function script_record_sent_postcard() {
        return $this->hasMany(ScriptRecordSentPostcard::class, 'script_record_id', 'id');
    }
}
