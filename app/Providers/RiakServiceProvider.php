<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Jobs\ProcessPodcast;


class RiakServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bindMethod(ProcessPodcast::class.'@handle', function ($job, $app) {
            return $job->handle($app->make(AudioProcessor::class));
        });
        });
    }

    
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
