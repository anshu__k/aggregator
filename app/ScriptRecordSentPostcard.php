<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\PoplarCampaign;

class ScriptRecordSentPostcard extends Model
{
    use SoftDeletes;

    protected $table = 'script_record_sent_postcards';
    protected $fillable = [
        'script_record_id', 'campaign_id', 'mailing_id', 'creative_id', 'response_data', 'deleted_at'
    ];

    public function poplar_campaign_detail() {
        return $this->belongsTo(PoplarCampaign::class, 'campaign_id', 'campaign_id');
    }
}
