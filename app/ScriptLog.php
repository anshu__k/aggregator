<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScriptLog extends Model
{
    protected $table = 'script_logs';
    protected $fillable = ['script_name', 'last_run', 'status'];


}
