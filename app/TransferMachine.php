<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransferMachine extends Model
{
    use SoftDeletes;

    protected $table = 'machine_transfers';
    protected $fillable = [
        'mach_num', 'customer_num', 'ship_date', 'install_date', 'source_ship_method', 'tech'
    ];

    public function source_ship_method_desc(){
    	return $this->hasOne('App\SStats', 's_status', 'source_ship_method');
    }

    public function tech_name()
    {
    	return $this->hasOne('App\Technician', 'id', 'tech');
    }
}
