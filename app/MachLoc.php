<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MachLoc extends Model
{
    use SoftDeletes;

    protected $table = 'mach_loc';
    protected $fillable = [
        'customer_id', 'machine_id', 'ship_date', 'mach_status', 'cur_mach_loc', 'note_num'
    ];

    public function machine(){
    	return $this->hasOne('App\Machine', 'mach_number', 'machine_id');
    }

    public function machine_status(){
        return $this->hasOne('App\Mstats', 'm_status', 'mach_status');
    }

    public function customer(){
    	return $this->hasOne('App\Customer', 'id', 'customer_id');
    }
}
