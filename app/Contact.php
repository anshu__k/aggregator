<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use SoftDeletes;

    protected $table = 'contacts';

    public function customer(){
    	return $this->hasOne('App\Customer', 'id', 'customer_id');
    }
}
