<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PoplarCampaign extends Model
{
    protected $table = 'poplar_campaigns';
    protected $fillable = [
        'campaign_id',
        'campaign_name'
    ];
}
