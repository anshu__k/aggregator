<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SourceTrackerDocument extends Model
{
    use SoftDeletes;

    public $timestamps = false;

    protected $table = 'source_tracker_documents';
    
    protected $fillable = [
        'title', 'source_id', 'document', 'upload_date'
    ];

}
