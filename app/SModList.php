<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SModList extends Model
{
    protected $table = 'smodlist';
    
    public function source(){
    	return $this->hasOne('App\Source', 's_mod_num','source_model');
    }

}
