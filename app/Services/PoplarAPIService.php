<?php

namespace App\Services;

use Carbon\Carbon;
use GuzzleHttp\Client;

class PoplarAPIService
{
    // protected $auth;
    protected $accessToken;
    protected $poplarAPIUrl;
    protected $httpClient;

    public function __construct()
    {
        $this->poplarAPIUrl = config('poplar.api_url');
        $this->accessToken = config('poplar.api_auth_token');

        $this->httpClient = new Client([
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => "Bearer {$this->accessToken}",
                'Accept' => 'application/json',
            ],
        ]);
    }

    public function stateInitials($search_state) {
        // Reference from : https://www.ssa.gov/international/coc-docs/states.html
        $states = [
            'AL' => 'ALABAMA',
            'AK' => 'ALASKA',
            'AS' => 'AMERICAN SAMOA',
            'AZ' => 'ARIZONA',
            'AR' => 'ARKANSAS',
            'CA' => 'CALIFORNIA',
            'CO' => 'COLORADO',
            'CT' => 'CONNECTICUT',
            'DE' => 'DELAWARE',
            'DC' => 'DISTRICT OF COLUMBIA',
            'FL' => 'FLORIDA',
            'GA' => 'GEORGIA',
            'GU' => 'GUAM',
            'HI' => 'HAWAII',
            'ID' => 'IDAHO',
            'IL' => 'ILLINOIS',
            'IN' => 'INDIANA',
            'IA' => 'IOWA',
            'KS' => 'KANSAS',
            'KY' => 'KENTUCKY',
            'LA' => 'LOUISIANA',
            'ME' => 'MAINE',
            'OH' => 'OHIO',
            'VA' => 'VIRGINIA'
        ];

        $key = array_search(strtolower($search_state), array_map('strtolower', $states)) ?? 'CA';
        return $key;
    }

    /**
     * Call GET API for the Poplar data 
     * 
     * @param String
     * @param String
     * @param Array
     * 
     * @return JsonObject
     */
    public function index($method, $slug_part, $search_param = array())
    {
        try {
            $apiUrl = $this->poplarAPIUrl . '/' . $slug_part;
            $request = $this->httpClient->request($method, $apiUrl);

            // $httpClient = new Client;
            // $request = $httpClient->request(
            //     $method,
            //     $apiUrl,
            //     [
            //         $search_param,
            //         'headers' =>
            //         [
            //             'Authorization' => "Bearer {$this->accessToken}",
            //             'Accept' => 'application/json',
            //             'Content-Type' => 'application/json'
            //         ]
            //     ]
            // );

            $response = json_decode($request->getBody()->getContents());

            return $response;
        } catch (\Exception $ex) {
            dd($ex->getMessage());
            return $ex->getMessage();
        }
    }

    /**
     * List all campaigns 
     * 
     * @return Object
     */
    public function campaign_list()
    {
        $campaigns = $this->index('GET', 'campaigns');
        return $campaigns;
    }


    /**
     * Call GET API for the Poplar data 
     * 
     * @param String
     * @param String
     * @param Array
     * 
     * @return JsonObject
     */
    public function postData($slug_part, $postData = array())
    {
        try {
            $apiUrl = $this->poplarAPIUrl . '/' . $slug_part;

            $request = $this->httpClient->post($apiUrl, [
                'body' => $postData['post_data']
            ]);

            $response = json_decode($request->getBody()->getContents());

            return $response;

            // return response()->json(['success' => true, 'message' => 'Postcard Send successfully.', 'data' => $response]);
        } catch (\Exception $ex) {
            return $ex->getMessage();
            // return response()->json(['success' => false, 'message' => $ex->getMessage()]);
        }
    }

    public function send_mailing_postcard($request, $data)
    {
        try {
            $form_params["post_data"] = json_encode([
                "campaign_id" => $request['poplar_campaign'],
                "recipient" => [
                    "city" => $data['city'] ?? '',
                    "email" => $data['email'] ?? 'kiaramistry5993@gmail.com',
                    "state" => $this->stateInitials($data['state']) ?? '',
                    "address_1" => $data['address1'] ?? '',
                    "address_2" => $data['address2'] ?? '',
                    "last_name" => $data['lastName'] ?? '',
                    "first_name" => $data['firstName'] ?? '',
                    "postal_code" => $data['zip'] ?? ''
                ]
            ]);
            // $mailing = $this->index('POST', 'mailing', $form_params);
            $mailing = $this->postData('mailing', $form_params);
            
            return $mailing;
            // return response()->json(['success' => true, 'message' => 'Postcard Send successfully.', 'data' => $mailing]);
        } catch (\Exception $ex) {
            return $ex->getMessage();
            // return response()->json(['success' => false, 'message' => $ex->getMessage()]);
        }
    }


    /**
     * Post poplar data 
     */
    public function update_campaign_data()
    {
        /*
         $guzzle = new Client(['base_uri' => self::APIURL]);

        $raw_response = $guzzle->post($endpoint, [
        'headers' => [ 'Authorization' => 'Bearer ' . $publicKey ],
        'body' => json_encode($data),
        ]);

        $response = $raw_response->getBody()->getContents();
         */
    }



    public function index_old()
    {
        // try {

        // } catch(GuzzleHttp\Exception\RequestException $e) {
        //     // This occasionally gets catched when a ConnectException (child) is thrown,
        //     // but it doesnt happen with RejectionException because it is not a child
        //     // of RequestException.
        // }
        // $apiUrl = "https://api.heypoplar.com/v1/mailing";
        $apiUrl = "https://api.heypoplar.com/v1/campaigns";
        /******************* */
        // $session = curl_init($apiUrl);
        // // if (count($attachments) > 0) {
        //     curl_setopt($session, CURLOPT_HEADER, true);
        //     curl_setopt($session, CURLOPT_HTTPHEADER, array("Authorization: Bearer {$this->accessToken}", "Accept: application/json", "Content-Type: application/json"));
        // // } else {
        // //     curl_setopt($session, CURLOPT_HEADER, false);
        // // }
        // curl_setopt($session, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        // // curl_setopt($session, CURLOPT_USERPWD, 'api:c81ac6794bb9eae066506dc0a98b49fe-52b0ea77-a284097b');
        // // curl_setopt($session, CURLOPT_POST, true);
        // // curl_setopt($session, CURLOPT_POSTFIELDS, $array_data);
        // curl_setopt($session, CURLOPT_HEADER, false);
        // curl_setopt($session, CURLOPT_ENCODING, 'UTF-8');
        // curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        // // curl_setopt($session, CURLOPT_SSL_VERIFYPEER, false);
        // $response = curl_exec($session);
        // curl_close($session);
        // $results = json_decode($response, true);
        // /******************* */
        // dd($response, $results);

        // $httpClient = new \GuzzleHttp\Client();
        $httpClient = new Client;
        $request = $httpClient->get(
            $apiUrl,
            [
                'headers' =>
                [
                    'Authorization' => "Bearer {$this->accessToken}",
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json'
                ]
            ]
        );

        // $request = $httpClient->request('GET',
        //     $apiUrl,
        //     ['debug' => true], 
        //     ['headers' =>
        //             [
        //                 'Authorization' => "Bearer {$this->accessToken}",
        //                 // 'response' => $value
        //                 'Accept' => 'application/json'
        //             ]
        //     ]
        // );

        // dd($request);
        // $request =
        //     $httpClient
        //     ->get($apiUrl);

        // dd($request);

        // $response = $request->getBody()->getContents();

        $response = json_decode($request->getBody()->getContents());

        // dd($response);
        return $response;

        // return $this->auth;
    }

    // public function getTotalCasesByCountryAndType($country, $type)
    // {
    //     $today = Carbon::now()->toDateString();

    //     $httpClient = new \GuzzleHttp\Client();
    //     $request =
    //         $httpClient
    //         ->get("https://api.covid19api.com/total/country/${country}/status/${type}?from=${today}&to=${today}");

    //     $response = json_decode($request->getBody()->getContents());

    //     return $response[count($response) - 1];
    // }
}
