<?php

namespace App\Traits;

use Log;

trait BullhornProcessingTrait
{
    // public function write_log($key, $content)
    // {
    //     $myfile = fopen($this->MESSAGE_LOG, 'a');
    //     fwrite($myfile, '******************************************************************* ');
    //     fwrite($myfile, date('Y-m-d H:i:s'));
    //     fwrite($myfile, $key);
    //     fwrite($myfile, $content);
    //     fclose($myfile);
    //     return true;
    // }

    public function unique_array($bullhorn_db_value, $local_db_value)
    {
        $customIdsArray = [];

        $bullhorn = explode(',', $bullhorn_db_value);
        $local_id = explode(',', $local_db_value);
        $customIdsArray = array_merge($bullhorn, $local_id);

        $custom_contact_id_array = array_unique(array_values(array_filter($customIdsArray)));
        $new_custom_ids = implode(',', $custom_contact_id_array);
        return $new_custom_ids;
    }

    public function generate_address_array($data = null)
    {
        if (!empty($data)) {
            $addressArray =
                array(
                    'address1' => !empty($data['address1']) ? $data['address1'] : '',
                    'address2' => !empty($data['address2']) ? $data['address2'] : '',
                    'city' => !empty($data['city']) ? $data['city'] : '',
                    'countryCode' => !empty($data['country_code']) ? $data['country_code'] : '',
                    'countryID' => !empty($data['country_id']) ? $data['country_id'] : '',
                    'countryName' => !empty($data['country_name']) ? $data['country_name'] : '',
                    'state' => !empty($data['state']) ? $data['state'] : '',
                    'timezone' => 'America/Los_Angeles',
                    'zip' => !empty($data['zip']) ? $data['zip'] : '',
                );

            return $addressArray;
        } else {
            return [];
        }
    }

    /**
     * Get Auth Code by the Client ID and username-password
     * @return String
     * 
     */
    public function getAuthCode()
    {
        $url = 'https://auth.bullhornstaffing.com/oauth/authorize?client_id=' . static::$BULLHORN_CLIENT_ID . '&response_type=code&action=Login&username=' . static::$BULLHORN_USER . '&password=' . static::$BULLHORN_PASS;
        $curl  = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        //curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_AUTOREFERER, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 120);
        curl_setopt($curl, CURLOPT_TIMEOUT, 120);

        $content = curl_exec($curl);
        if (curl_errno($curl)) {
            Log::channel('bullhornlog')->info(['Request Error:', curl_error($curl)]);
            echo 'Request Error:' . curl_error($curl);
        }
        curl_close($curl);

        Log::channel('bullhornlog')->info(['Get Auth Code', 'getAuthCode()', $content]);

        // if (preg_match('#Location: (.*)#', $content, $r)) {
        if(preg_match('#Location: (.*)#', $content, $r) || preg_match('#location: (.*)#', $content, $r)) {
            $l = trim($r[1]);
            $temp = preg_split("/code=/", $l);
            $authcode = $temp[1];
        }

        return $authcode;
    }

    /**
     * Do bullhorn authentication 
     * @param String
     * @return String
     * 
     */
    public function doBullhornAuth($authCode)
    {
        $url = 'https://auth.bullhornstaffing.com/oauth/token?grant_type=authorization_code&code=' . $authCode . '&client_id=' . static::$BULLHORN_CLIENT_ID . '&client_secret=' . static::$BULLHORN_CLIENT_SECRET;

        $options = array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => array()
        );

        $ch  = curl_init($url);
        curl_setopt_array($ch, $options);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $content = curl_exec($ch);
        if (curl_errno($ch)) {
            Log::channel('bullhornlog')->info(['Login Error:', curl_error($ch)]);
            echo 'Login Error:' . curl_error($ch);
        }

        curl_close($ch); //die($content);

        // $this->write_log('doBullhornAuth: ', $content);
        Log::channel('bullhornlog')->info(['Do Bullhorn Auth', 'doBullhornAuth()', $content]);

        return $content;
    }

    /**
     * Do Bullhorn Login 
     * @param String
     * @return String
     * 
     */
    public function doBullhornLogin($accessToken)
    {
        $url = 'https://rest.bullhornstaffing.com/rest-services/login?version=*&access_token=' . $accessToken;
        $curl  = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        //curl_setopt($curl, CURLOPT_HEADER, true);
        //curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        //curl_setopt($curl, CURLOPT_AUTOREFERER, true);
        //curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 120);
        //curl_setopt($curl, CURLOPT_TIMEOUT, 120);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

        $content = curl_exec($curl);
        curl_close($curl);

        // $this->write_log('doBullhornLogin: ', $content);
        Log::channel('bullhornlog')->info(['Do Bullhorn Login', 'doBullhornLogin()', $content]);
        //print_r($content);exit;
        return $content;
    }

    /**
     * Get USer Data by the API call
     * @param String
     * @param String
     * @param String
     * @param String 
     * 
     * @return Object
     * 
     */
    public function getUserDataApi($resturl, $firstname, $lastname, $token)
    {
        list($firstname1, $firstname2) = explode(" ", $firstname);
        if (empty($firstname2)) {
            $firstname2 = $firstname1;
        }
        ////  echo  $url1 = $accessToken['restUrl'].'search/Candidate?query=firstName:'.$row['firstName'].' AND lastName:'.$row['lastName'].'&fields=id,firstName,lastName,customText11,customText6,dateAdded&start=0&BhRestToken='.$accessToken['BhRestToken'];

        // $url1 = $resturl . 'search/ClientContact?query=firstName:' . rawurlencode($firstname) . '%20AND%20lastName:' . rawurlencode($lastname) . '&fields=id,firstName,lastName,customText11,customText6,customText7,dateAdded&start=0&BhRestToken=' . $token;

        // $url1 = $resturl . 'search/ClientContact?query=%28firstName%3AENCARNACION%20AND%20lastName%3AALAYVILLA%29%20OR%20%28firstName%3AJACQUELINE%20AND%20lastName%3AALAYVILLA%29&fields=id,firstName,lastName,customText11,customText6,customText7,dateAdded&start=0&BhRestToken=' . $token;
        $url1 = $resturl . 'search/ClientContact?query=(firstName:' . rawurlencode($firstname1) . '%20AND%20lastName:' . rawurlencode($lastname) . ')%20OR%20(firstName:' . rawurlencode($firstname2) . '%20AND%20lastName:' . rawurlencode($lastname) . ')&fields=id,firstName,lastName,customText11,customText6,customText7,dateAdded,address&start=0&BhRestToken=' . $token;

        $curl1 = curl_init($url1);
        curl_setopt($curl1, CURLOPT_URL, $url1);
        curl_setopt($curl1, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl1, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl1, CURLOPT_SSL_VERIFYPEER, 0);

        $contentData = curl_exec($curl1);
        curl_close($curl1);
        $responseData = json_decode($contentData, true);
        // $this->write_log('UserData: ', $contentData);
        Log::channel('bullhornlog')->info(['Get User Data', 'getUserDataApi()', $contentData]);

        return $responseData;
    }

    /**
     * Get Client Contact User By Email 
     * 
     * @param String
     * @param String
     * @param String
     * @param String
     * 
     * @return Object
     */
    public function getClientContactUserDataByEmailApi($entity, $resturl, $email, $token)
    {
        // $url1 = $resturl . 'search/' . $entity . '?query=email:' . rawurlencode($email) . '&fields=id,email,firstName,lastName,customText11,customText6,dateAdded&start=0&BhRestToken=' . $token;
        $url1 = $resturl . 'search/' . $entity . '?query=email:' . rawurlencode($email) . '%20OR%20email2:' . rawurlencode($email) . '&fields=id,email,firstName,lastName,customText11,customText6,dateAdded&start=0&BhRestToken=' . $token;
        // print_r($url1);

        $curl1 = curl_init($url1);
        curl_setopt($curl1, CURLOPT_URL, $url1);
        curl_setopt($curl1, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl1, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl1, CURLOPT_SSL_VERIFYPEER, 0);

        $contentData = curl_exec($curl1);
        curl_close($curl1);
        $responseData = json_decode($contentData, true);
        // $this->write_log('UserDataByEmail: ', $contentData);
        Log::channel('bullhornlog')->info(['Get Client Contact User Data API', 'getClientContactUserDataByEmailApi()', $contentData]);
        // echo "<pre>";print_r($responseData);exit;
        return $responseData;
    }

    public function getUserDataCandidateApi($resturl, $firstname, $lastname, $token)
    {

        list($firstname1, $firstname2) = explode(" ", $firstname);
        if (empty($firstname2)) {
            $firstname2 = $firstname1;
        }

        ////  echo  $url1 = $accessToken['restUrl'].'search/Candidate?query=firstName:'.$row['firstName'].' AND lastName:'.$row['lastName'].'&fields=id,firstName,lastName,customText11,customText6,dateAdded&start=0&BhRestToken='.$accessToken['BhRestToken'];
        // echo $url1 = $resturl . 'search/Candidate?query=firstName:' . rawurlencode($firstname) . '%20AND%20lastName:' . rawurlencode($lastname) . '&fields=id,firstName,lastName,customText11,customText6,customText7,customText8,dateAdded&start=0&BhRestToken=' . $token;

        $url1 = $resturl . 'search/Candidate?query=(firstName:' . rawurlencode($firstname1) . '%20AND%20lastName:' . rawurlencode($lastname) . ')%20OR%20(firstName:' . rawurlencode($firstname2) . '%20AND%20lastName:' . rawurlencode($lastname) . ')&fields=id,firstName,lastName,customText11,customText6,customText7,dateAdded,address&start=0&BhRestToken=' . $token;

        $curl1 = curl_init($url1);
        curl_setopt($curl1, CURLOPT_URL, $url1);
        curl_setopt($curl1, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl1, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl1, CURLOPT_SSL_VERIFYPEER, 0);

        $contentData = curl_exec($curl1);
        curl_close($curl1);
        $responseData = json_decode($contentData, true);
        // $this->write_log('UserDataCandidate: ', $contentData);
        Log::channel('bullhornlog')->info(['Get Contact Candidate API', 'getUserDataCandidateApi()', $contentData]);

        return $responseData;
    }
    public function updateClientContactUserApi($resturl, $contactId, $token, $contactwepidUpdate, $contactlicencenoUpdate, $contactlicenceurlUpdate, $extra_column = null, $address_object = null)
    {

        // echo $url2 = $resturl . 'entity/ClientContact/' . $contactId . '?BhRestToken=' . $token;
        $url2 = $resturl . 'entity/ClientContact/' . $contactId . '?BhRestToken=' . $token;

        // $this->write_log('params: : ', ' ***** Rest URL: ' . $resturl . ' ***** Contact ID: ' . $contactId . ' ***** Token: ' . $token . ' ***** Update: ' . $contactwepidUpdate . ' ***** URL : ' . $url2);

        Log::channel('bullhornlog')->info(['params: ' .' ***** Rest URL: ' . $resturl . ' ***** Contact ID: ' . $contactId . ' ***** Token: ' . $token . ' ***** Update: ' . $contactwepidUpdate . ' ***** URL : ' . $url2, 'Get Contact Candidate API', 'getUserDataCandidateApi()']);

        $curl2 = curl_init($url2);
        // $payload = json_encode(array("customText11" => $contactwepidUpdate, "customText7" => $contactlicencenoUpdate, "customText8" => $contactlicenceurlUpdate));
        $payloadArray = array(
            "customText11" => $contactwepidUpdate,
            "customText6" => $contactlicencenoUpdate,
            "customText7" => $contactlicenceurlUpdate
        );
        if (!empty($extra_column)) {
            $payloadArray[$extra_column] = '';
        }
        if (!empty($address_object)) {
            $payloadArray['address'] = $address_object;
        }
        $payload = json_encode($payloadArray);

        curl_setopt($curl2, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($curl2, CURLOPT_POST, 1);
        curl_setopt($curl2, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($curl2, CURLOPT_URL, $url2);
        curl_setopt($curl2, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl2, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl2, CURLOPT_SSL_VERIFYPEER, 0);
        $contentData2 = curl_exec($curl2);
        // print_r($contentData2);exit;
        curl_close($curl2);
        $responseData2 = json_decode($contentData2, true);

        // $this->write_log('UserDatapdate: ', $contentData2);
        Log::channel('bullhornlog')->info(['UserDatapdate', 'updateClientContactUserApi()', $contentData2]);

        return $responseData2;
    }
    public function updateCandidateUserApi($resturl, $contactId, $token, $contactwepidUpdate, $contactlicencenoUpdate, $contactlicenceurlUpdate, $extra_column = null, $address_object = null)
    {

        // echo $url2 = $resturl . 'entity/Candidate/' . $contactId . '?BhRestToken=' . $token;
        $url2 = $resturl . 'entity/Candidate/' . $contactId . '?BhRestToken=' . $token;
        $curl2 = curl_init($url2);
        // $payload = json_encode(array("customText11" => $contactwepidUpdate, "customText6" => $contactlicencenoUpdate, "customText7" => $contactlicenceurlUpdate));
        $payloadArray = array(
            "customText11" => $contactwepidUpdate,
            "customText7" => $contactlicencenoUpdate,
            "customText8" => $contactlicenceurlUpdate
        );
        if (!empty($extra_column)) {
            $payloadArray[$extra_column] = '';
        }
        if (!empty($address_object)) {
            $payloadArray['address'] = $address_object;
        }
        $payload = json_encode($payloadArray);

        curl_setopt($curl2, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($curl2, CURLOPT_POST, 1);
        curl_setopt($curl2, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($curl2, CURLOPT_URL, $url2);
        curl_setopt($curl2, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl2, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl2, CURLOPT_SSL_VERIFYPEER, 0);
        $contentData2 = curl_exec($curl2);
        curl_close($curl2);
        $responseData2 = json_decode($contentData2, true);

        // $this->write_log('UserDatapdateCandidate: ', $contentData2);
        Log::channel('bullhornlog')->info(['UserDatapdateCandidate', 'updateCandidateUserApi()', $contentData2]);

        return $responseData2;
    }
    public function getCandidateUserDataApiById($resturl, $entityid, $token)
    {

        ////  echo  $url1 = $accessToken['restUrl'].'search/Candidate?query=firstName:'.$row['firstName'].' AND lastName:'.$row['lastName'].'&fields=id,firstName,lastName,customText11,customText6,dateAdded&start=0&BhRestToken='.$accessToken['BhRestToken'];

        //  $url1 = $resturl . 'entity/Candidate/'.$entityid.'?fields=firstName,lastName&start=0&BhRestToken=' . $token;
        $url1 = $resturl . 'entity/Candidate/' . $entityid . '?fields=id,firstName,middleName,lastName,customText11,customText6,customText7,customText8,dateAdded&start=0&BhRestToken=' . $token;
        $curl1 = curl_init($url1);
        curl_setopt($curl1, CURLOPT_URL, $url1);
        curl_setopt($curl1, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl1, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl1, CURLOPT_SSL_VERIFYPEER, 0);

        $contentData = curl_exec($curl1);
        curl_close($curl1);
        $responseData = json_decode($contentData, true);
        // echo "<br/>";print_r($responseData);exit;
        // $this->write_log('CandidateData: ', $contentData);
        Log::channel('bullhornlog')->info(['CandidateData', 'getCandidateUserDataApiById()', $contentData]);

        return $responseData;
    }

    /** 
     * Client Contacts by ID 
     * 
     */
    public function getClientContactUserDataApiById($resturl, $entityid, $token)
    {

        ////  echo  $url1 = $accessToken['restUrl'].'search/Candidate?query=firstName:'.$row['firstName'].' AND lastName:'.$row['lastName'].'&fields=id,firstName,lastName,customText11,customText6,dateAdded&start=0&BhRestToken='.$accessToken['BhRestToken'];

        //  $url1 = $resturl . 'entity/Candidate/'.$entityid.'?fields=firstName,lastName&start=0&BhRestToken=' . $token;
        $url1 = $resturl . 'entity/ClientContact/' . $entityid . '?fields=id,firstName,middleName,lastName,customText11,customText6,customText7,customText8,dateAdded&start=0&BhRestToken=' . $token;
        $curl1 = curl_init($url1);
        curl_setopt($curl1, CURLOPT_URL, $url1);
        curl_setopt($curl1, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl1, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl1, CURLOPT_SSL_VERIFYPEER, 0);

        $contentData = curl_exec($curl1);
        curl_close($curl1);
        $responseData = json_decode($contentData, true);
        // echo "<br/>";print_r($responseData);exit;
        // $this->write_log('ClientContactData: ', $contentData);
        Log::channel('bullhornlog')->info(['ClientContactData', 'getClientContactUserDataApiById()', $contentData]);

        return $responseData;
    }

    /**
     * Get All Candidate Ids 
     */
    public function getAllCandidateIdsApi($resturl, $token, $id_deleted = 0)
    {
        ////  echo  $url1 = $accessToken['restUrl'].'search/Candidate?query=firstName:'.$row['firstName'].' AND lastName:'.$row['lastName'].'&fields=id,firstName,lastName,customText11,customText6,dateAdded&start=0&BhRestToken='.$accessToken['BhRestToken'];
        // echo $url1 = $resturl . 'search/Candidate?fields=id,firstName,lastName,customText11,customText6,customText7,customText8,dateAdded&start=0&BhRestToken=' . $token;
        // echo $url1 = $resturl . 'query/ClientContact?where=firstName<>""&fields=id,firstName,lastName,customText11,customText6,customText7,customText8,dateAdded&start=0&BhRestToken=' . $token;
        // Get All Candidate Lists
        // echo $url1 = $resturl . 'options/Candidate?BhRestToken=' . $token;
        // echo $url1 = $resturl . 'myClientContacts?fields=firstName,lastName,address&start=0&count=5&BhRestToken=' . $token;
        // echo $url1 = $resturl . 'myCandidates?fields=firstName,lastName,address&start=0&count=5&BhRestToken=' . $token;
        // echo $url1 = $resturl . 'entity/Candidate/*?fields=id,firstName,lastName,address&BhRestToken=' . $token;

        // echo $url1 = $resturl . 'search/Candidate?query=isDeleted:' . $id_deleted . '&BhRestToken=' . $token;
        $url1 = $resturl . 'search/Candidate?query=isDeleted:' . $id_deleted . '&BhRestToken=' . $token;
        // echo $url1 = $resturl . 'search/Candidate?query=isDeleted:' . $id_deleted . '&fields=id,firstName,lastName,customText11,customText6,customText7,customText8,dateAdded&start=0&count=500&BhRestToken=' . $token;

        $curl1 = curl_init($url1);
        curl_setopt($curl1, CURLOPT_URL, $url1);
        curl_setopt($curl1, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl1, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl1, CURLOPT_SSL_VERIFYPEER, 0);

        $contentData = curl_exec($curl1);
        curl_close($curl1);
        $responseData = json_decode($contentData, true);
        // $this->write_log('GetAllCandidateIds: ', $contentData);
        Log::channel('bullhornlog')->info(['GetAllCandidateIds', 'getAllCandidateIdsApi()', $contentData]);
        return $responseData;
    }

    /**
     * Get All Client Contacts Ids 
     */
    public function getAllClientContactIdsApi($resturl, $token, $id_deleted = 0)
    {
        $url1 = $resturl . 'search/ClientContact?query=isDeleted:' . $id_deleted . '&BhRestToken=' . $token;

        $curl1 = curl_init($url1);
        curl_setopt($curl1, CURLOPT_URL, $url1);
        curl_setopt($curl1, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl1, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl1, CURLOPT_SSL_VERIFYPEER, 0);

        $contentData = curl_exec($curl1);
        curl_close($curl1);
        $responseData = json_decode($contentData, true);
        // $this->write_log('GetAllClientContactIds: ', $contentData);
        Log::channel('bullhornlog')->info(['GetAllClientContactIds', 'getAllClientContactIdsApi()', $contentData]);
        return $responseData;
    }

    /**
     * Get All Client Contacts Ids 
     */
    public function getAllUsersInformationsApi($resturl, $token, $id_deleted = 0, $entity_type = 'candidate', $start = '0', $count = '500')
    {
        // $url1 = $resturl . 'search/ClientContact?query=isDeleted:' . $id_deleted . '&BhRestToken=' . $token;
        // echo $url1 = $resturl . 'search/' . $entity_type . '?query=isDeleted:' . $id_deleted . '&fields=id,firstName,lastName,customText11,customText6,customText7,customText8,dateAdded&start=' . $start . '&count=' . $count . '&BhRestToken=' . $token;
        $url1 = $resturl . 'search/' . $entity_type . '?query=isDeleted:' . $id_deleted . '&fields=id,firstName,lastName,customText11,customText6,customText7,customText8,dateAdded&start=' . $start . '&count=' . $count . '&BhRestToken=' . $token;

        $curl1 = curl_init($url1);
        curl_setopt($curl1, CURLOPT_URL, $url1);
        curl_setopt($curl1, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl1, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl1, CURLOPT_SSL_VERIFYPEER, 0);

        $contentData = curl_exec($curl1);
        curl_close($curl1);
        $responseData = json_decode($contentData, true);
        // $this->write_log('GetAllClientContactIds: ', $contentData);
        Log::channel('bullhornlog')->info(['GetAllClientContactIds', 'getAllUsersInformationsApi()', $contentData]);
        return $responseData;
    }

    /** New Code logics */
    public function default()
    {
        dd('default data ');
    }

    public function update_lab_status($request/*, $lab_order_id = null*/)
    {
        // Get LABS Referral Record.
        $labs_referral_record = $this->getLabsReferralData();
        $labs_referral_record_id = $labs_referral_record->id;

        // Get Pending Result Referral Record.
        $pending_result_referral_record = $this->getPendingResultReferralData();
        $pending_result_referral_record_id = $pending_result_referral_record->id;

        // Set Variables get from the Request...
        $lab_order_id = $request->lab_record_id;
        $status = $request->lab_status;
        $lab_testing_type_id = $request->lab_testing_type_id;

        // Get FormValueLabOrders 
        $forms_values_lab_orders = FormsValuesLabOrders::where('id', $lab_order_id)->first();

        if (!empty($forms_values_lab_orders)) {
            // Update all grouped data with the status
            FormsValuesLabOrders::where([
                'form_values_id' => $forms_values_lab_orders->form_values_id,
                'lab_testing_type_id' => $lab_testing_type_id
            ])->update([
                'status' => $status
            ]);
            $forms_values_lab_orders->status = $status;
            $forms_values_lab_orders->Save();

            // Fetch Referral data info
            if (!empty($forms_values_lab_orders->user_payment_history)) {
                $form_added_referrals = $forms_values_lab_orders->user_payment_history->referral_id ?? null;

                if (!empty($form_added_referrals)) {
                    $referrals_array = explode(',', $form_added_referrals);
                    if ($status == '2') {
                        $remove_referral = $labs_referral_record_id;
                        $append_referral = $pending_result_referral_record_id;
                    } else if ($status != '2') {
                        $remove_referral = $pending_result_referral_record_id;
                        $append_referral = $labs_referral_record_id;
                    }

                    if (($key = array_search($remove_referral, $referrals_array)) !== false) {
                        unset($referrals_array[$key]);
                    }

                    array_push($referrals_array, $append_referral);
                    $referral_id = implode(',', array_unique($referrals_array));
                    // Then need to check existing referral, remove existing referral and then apeend new referral
                } else {
                    // IF status is Pending Result, then set Referral to the Pending Result - Status = 2
                    if ($status == '2') {
                        $referral_id = $pending_result_referral_record_id;
                    } else if ($status != '2') {
                        $referral_id = $labs_referral_record_id;
                    }
                }

                $forms_values_lab_orders->user_payment_history->referral_id = $referral_id;
                $forms_values_lab_orders->user_payment_history->Save();

                return;
            }
            return;
        }
        return;
    }

    public function getLabsReferralData()
    {
        return ReferralModel::where('referral_key', 'labs')->first();
    }

    public function getPendingResultReferralData()
    {
        return ReferralModel::where('referral_key', 'pending_result')->first();
    }

    // public function update_form_value_referral($form_value_id = null)
    // {
    //     // update_form_value_referral
    //     UserPaymentHistoryModel::whereFormValueId($form_value_id)->first();
    // }

    public function insertInitialLabsReferralId($form_value_id)
    {
        $labs_referral = $this->getLabsReferralData();
        // UserPaymentHistoryModel::whereFormValueId($form_value_id)->first();

        if (!empty($labs_referral)) {
            UserPaymentHistoryModel::updateOrCreate(
                ['form_value_id' => $form_value_id],
                ['referral_id' => $labs_referral->id]
            );
        }
    }
}
