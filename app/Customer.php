<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Customer extends Model
{
    use SoftDeletes;

    protected $table = 'customers';
    // protected $fillable = [
    //     'customer_name', 'address1', 'address2', 'wipe_freq', 'own_wipe', 'zip_code', 'country', 'state', 'city', 'filing_name', 'license'
    // ];

    public function contact(){
    	return $this->hasOne('App\Contact', 'customer_id', 'id')->where('status', '1');
    }

    public function customer_contact(){
    	return $this->hasOne('App\Contact', 'customer_id', 'id');
    }

    public function contact_type_data(){
        return $this->hasOne('App\Contact', 'customer_id', 'id');
    }
    
    public function all_contact_type_data(){
        return $this->hasMany('App\Contact', 'customer_id', 'id')
        ->join('contact_types', 'contact_types.contact_type', '=', 'contacts.contact_type');
    }

    public function cust_wipedata(){
        return $this->hasMany('App\Wipe', 'cust_num', 'customer_number');
    }

    public static function get_last_wipetest($id){
        

        $customers = DB::table('wipes')
                        ->select('wipes.wipe_date')
                        ->where('cust_num',$id)
                        ->orderBy('id', 'desc')
                        ->first();
                        
        return isset($customers->wipe_date)? date('d M Y', strtotime($customers->wipe_date)):'';
    }
}
