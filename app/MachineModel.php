<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MachineModel extends Model
{
    protected $table = 'machine_models';
    protected $fillable = [
        'mach_model', 'mach_desc',
    ];
}
