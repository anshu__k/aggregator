<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wipe extends Model
{
	use SoftDeletes;
	
    protected $table = 'wipes';
    protected $fillable = [
        'pre_ass_source_id', 'source_num', 'tech', 'wipe_date','contact_num', 'discrepcod', 'wipe_conf', 'meas_date', 'print_job', 'meas_by', 'mach_num', 'cust_num','add_group_id'
    ];

    public function tech_name()
    {
    	return $this->hasOne('App\Technician', 'id', 'tech');
    }

    public function meas_name()
    {
        return $this->hasOne('App\Measurer', 'meas_by', 'meas_by');   
    }

    public function customer(){
        return $this->hasOne('App\Customer', 'customer_number', 'cust_num'); 
    }

    public function discrep_code()
    {
        return $this->hasOne('App\Discrep', 'discrepcod', 'discrepcod');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'meas_by');
    }
}
