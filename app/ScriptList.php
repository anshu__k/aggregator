<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScriptList extends Model
{
    protected $table = 'script_lists';
    protected $fillable = ['name','file_name', 'type',];


}
