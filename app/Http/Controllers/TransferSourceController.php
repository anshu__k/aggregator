<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SModList;
use App\SStats;
use App\SourceLoc;
use App\Technician;
use App\MachLoc;
use App\Wipe;
class TransferSourceController extends Controller
{
    public function index()
    {
        // $transfer_sources = SourceLoc::where('mach_num', 'idc')
        //                     ->where('s_status', '!=', 'aa')
        //                     ->orderBy('updated_at', 'desc')
        //                     ->paginate(env('page'));

        $transfer_sources = SourceLoc::where('currentloc', '3')
                            //->where('s_status', '!=', 'aa')
                            ->orderBy('updated_at', 'desc')
                            ->paginate(env('page'));

                            
        // dd($transfer_sources);
    	return view('transfer_source.index', compact('transfer_sources'));
    }

    public function create()
    {
        $source_mods = SModList::whereIn('id',[1,2,3,4,6,7,8,12])
        ->get();
        $source_status_lists = SStats::all();

    	return view('transfer_source.create', compact('source_status_lists','source_mods'));
    }

    public function store(Request $request)
    {

        if($request->active_tab == 'idc'){
            $rules = [
                'source_number'     => 'required',
                'ship_date_idc'   => 'required',
                'source_model_idc'  => 'required',
                'source_status_idc'  => 'required',
                'technician_idc'     => 'required'
            ];

            $message = [
                'source_number.required'   => 'Please select Source Number.', 
                'ship_date_idc.required'   => 'Please select ship date.', 
                'source_model_idc.required'   => 'Please select source Model.', 
                'source_status_idc.required'  => 'Please select source status.',
                'technician_idc.required'     => 'Please select technician.'
            ];

            $this->validate($request, $rules, $message);
            $data = [
                'mach_num' => 'IDC',
                's_ship_date' => date("Y-m-d", strtotime($request->ship_date_idc)),
                's_mod_num' => $request->source_model_idc,
                's_status' => $request->source_status_idc,
                'tech' => $request->technician_idc,
                'source_num' => $request->source_number,
                'mach_num'   => $request->current_mach,
                //'s_mod_num'  => $request->current_source_model,
                'currentloc' => 3
            ]; 

            //$source_transfer = SourceLoc::find($request->source_number)->update($data);
            $source_transfer = SourceLoc::create($data);
        }

    	return redirect()->route('TransferSou.index')->with('insert', 'Source Transfer Successfully.');
    }

    public function edit($id)
    {   
        $source_mods = SModList::whereIn('id',[1,2,3,4,6,7,8,12])
        ->get();

        $source_status_lists = SStats::all();

        $transfer_source = SourceLoc::find($id);
        // dd($transfer_source);

    	return view('transfer_source.edit', compact('source_status_lists', 'transfer_source','source_mods'));
    }


    public function updates(Request $request, $id)
    {

        if($request->active_tab == 'idc'){
            $rules = [
                'ship_date_idc'   => 'required',
                'source_status_idc'  => 'required',
                'technician_idc'     => 'required'
            ];

            $message = [
                'ship_date_idc.required'   => 'Please select ship date.', 
                'source_status_idc.required'  => 'Please select source status.',
                'technician_idc.required'     => 'Please select technician.'
            ];

            $this->validate($request, $rules, $message);
            $data = [
                'mach_num' => 'IDC',
                's_ship_date' => date("Y-m-d", strtotime($request->ship_date_idc)),
                's_mod_num' => $request->source_model_idc,
                's_status' => $request->source_status_idc,
                'tech' => $request->technician_idc,
                'source_num' => $request->edit_source_number,
                'mach_num'   => $request->current_mach,
                //'s_mod_num'  => $request->current_source_model,
                'currentloc' => 3
            ]; 

            //$source_transfer = SourceLoc::find($request->source_number)->update($data);
            
            $source_transfer = SourceLoc::find($id)->update($data);
        }

        return redirect()->route('TransferSou.index')->with('insert', 'Source Transfer Successfully.');
    }

    public function updatec(Request $request, $id)
    {

        $rules = [
             'edit_source_number'   => 'required',
             'edit_source_model' => 'required',
             'edit_machine_number'  => 'required',
             'edit_recieved_date' => 'required',
             'edit_source_ship_date' => 'required',
             'edit_inst_tech'     => 'required'
        ];

        $message = [
            'edit_source_number.required'   => 'Please select ship date.', 
            'edit_source_model.required' => 'Please select installation date.',
            'edit_machine_number.required'  => 'Please select source status.',
            'edit_recieved_date.required' => 'Please select recieved date.',
            'edit_source_ship_date.required' => 'Please select Source Ship date.',
            'edit_inst_tech.required'     => 'Please select technician.'
        ];

        $this->validate($request, $rules, $message);
        $data = [
            's_status' => 'aa',
            'source_num' => $request->edit_source_number,
            's_mod_num' => $request->edit_source_model,
            'mach_num' => $request->edit_machine_number,
            //'tech' => $request->edit_recieved_date,
            's_ship_date' => date('Y-m-d H:i:s',strtotime($request->edit_source_ship_date)),
            'tech' => $request->edit_inst_tech,
        ]; 
        // dd($data);
        $source_transfer = SourceLoc::find($id)->update($data);
    	return  redirect()->route('TransferSou.index')->with('update', 'Source Transfer Update Successfully.');
    }

    public function destroy($id)
    {

        $getsource = SourceLoc::find($id)->delete();
        
        return redirect()->route('TransferSou.index')->with('delete','Source Transfer Deleted Successfully.');
    }

    public function confirm($id)
    {   
        $source_mods = SModList::whereIn('id',[1,2,3,4,6,7,8,12])
        ->get();

        $source_status_lists = SStats::all();
        $techrso = Technician::where('type','4')->first();

        $transfer_source = SourceLoc::find($id);
        // dd($transfer_source);

        return view('transfer_source.confirm', compact('source_status_lists', 'transfer_source','source_mods','techrso'));
    }

    public function updateconfirmlog(Request $request) {
        
        
        $sourcep = SourceLoc::where('source_num', $request->edit_source_number)
                            ->Where('currentloc', '3')
                            ->first();
         //dd($sourcep);                   
        //$sourceupdate['renewable_wipe_test'] = $request->mach_num; 
        $source_loc = SourceLoc::find($request->id);

        $shipIdUpdate = $source_loc->installation_wipe_test;

        $techname = Technician::where('id',$request->edit_inst_tech)->first(); 
        //dd($source->mach_num);
        //dd($techname);
        

        if(isset($sourcep->mach_num)){                    
         $machLoc = MachLoc::where('machine_id', $sourcep->mach_num)->first();
        
        $custNum=@$machLoc->customer->customer_number;
        }else{
            $custNum='';
        }                            
                                                  
        //$sourceupdate['renewable_wipe_test'] = !empty($machLoc->customer->id) ? $machLoc->customer->id : '';
        
        $sourceulast['currentloc'] = 0;
            $update_source_last = SourceLoc::where('source_num', $request->edit_source_number)
                                    ->update($sourceulast);


         
         $data = [
            'source_num'        => $source_loc->source_num,
            'mach_num'          => $source_loc->mach_num,
            'cust_num'          => $custNum,//$source_loc->machine_num->mach_loc->customer->customer_number
            'print_job'         => $source_loc->print_job,
            'tech'              => isset($techname->id)?$techname->id:'',
            'contact_num'       => isset($techname->id)?$techname->tech_name:'',
            'wipe_date'         => isset($source_loc->ship_date)?date('Y-m-d',strtotime($source_loc->ship_date)):date('Y-m-d'),
            'meas_date'         => isset($source_loc->ship_date)?date('Y-m-d',strtotime($source_loc->ship_date)):date('Y-m-d'),
            'meas_by'           => isset($techname->id)?$techname->id:'',
            'discrepcod'        => 8,
            'wipe_conf'        => 0, 
        ];

        $push_wipe = Wipe::create($data);                          
        //$shipconval = Wipe::find($shipIdUpdate)->update(['wipe_conf' => 0]);


        $sourceupdate['renewable_wipe_test'] =$push_wipe->id;                            
        $sourceupdate['currentloc'] = 1;
            $update_source_loc = SourceLoc::where('id', $request->id)
                                    ->update($sourceupdate);

        $datas = [
            'source_num'        => $source_loc->source_num,
            'mach_num'          => '',
            'cust_num'          => 'IDC',
            'contact_num'       => @$source_loc->contact_name->contact_name,
            'print_job'         => $source_loc->print_job,
            'tech'              => isset($techname->id)?$techname->id:'',
            'wipe_date'         => isset($request->edit_recieved_date)?date('Y-m-d',strtotime($request->edit_recieved_date)):date('Y-m-d'),
            'meas_date'         => isset($request->edit_meas_date)?date('Y-m-d',strtotime($request->edit_meas_date)):date('Y-m-d'),
            'meas_by'           => $request->edit_meas_by,
            'discrepcod'        => 7,
            'wipe_conf'        => 0, 
        ];

        $push_wipe = Wipe::create($datas);
        if($push_wipe){
           $sourceupdate['installation_wipe_test'] = $push_wipe->id;
           $sourceupdate['inst_date'] = date('Y-m-d H:i:s',strtotime($request->edit_recieved_date));  
           $sourceupdate['tech'] = isset($techname->id)?$techname->id:'';
           $sourceupdate['mach_num'] = '';
            $update_source_loc = SourceLoc::where('id', $request->id)
                                    ->update($sourceupdate);
           }                         
       return  redirect()->route('TransferSou.index')->with('update', 'Confirm & genrated wipe test successfully');                            
        
    }

}
