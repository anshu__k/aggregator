<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Activitylog\Contracts\Activity;
use App\Machine;
use App\MachLoc;
use App\Customer;
use App\MachineModel;
use App\Mstats;
use App\Wipe;
use App\Contact;
use App\SourceLoc;
use App\UploadDocument;
use DB;

class MachineController extends Controller
{
    public function index()
    {
        $machines = Machine::orderBy('machines.id', 'desc')
                    ->select('machines.id','machines.mach_number','machines.mach_model','m_stats.mstats_desc','mach_loc.ship_date')
                    ->leftJoin('mach_loc', 'machines.mach_number', '=', 'mach_loc.machine_id')
                    ->leftJoin('m_stats', 'mach_loc.mach_status', '=', 'm_stats.m_status')
                    ->where('mach_loc.cur_mach_loc',1) 
                    ->paginate(env('page'));
        
        return view('machine.index', compact('machines'));
    }

    public function create()
    {
        $machine_status = Mstats::all();
        return view('machine.create', compact('machine_status'));
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $rules = [
            'mach_number'     => 'required|unique:machines,mach_number',
            'mach_model'      => 'required',
            'mach_status'     => 'required',
            'select_customer' => 'required',
            'ship_date'       => 'required',    
        ];

        $message = [
            'mach_number.required'     => 'Machine Number Field Is Required.',
            'mach_number.unique'       => 'Machine Number All Ready Exist.',
            'mach_model.required'      => 'Machine Model Field Is Required.',
            'mach_status.required'     => 'Machine Status Field Is Required.',
            'select_customer.required' => 'Please Select Customer.',
            'ship_date.required'       => 'Machine Ship Date Field Is Required.',
        ];

        $this->validate($request, $rules, $message);
        $data = [
            'mach_number' => $request->mach_number,
            'mach_model'  => $request->mach_model,
        ];

        $add_machine = Machine::create($data);

        $mach_loc_data = [
            'customer_id' => $request->select_customer,
            'machine_id' => $add_machine->mach_number,
            'ship_date'  => date('y-m-d', strtotime($request->ship_date)),
            'mach_status'  => $request->mach_status,
            'cur_mach_loc'=>1,
        ];

        $add_machine_loc = MachLoc::create($mach_loc_data);

        $jsonActivity = array_merge($data, $mach_loc_data);

        $user = auth()->user();
        activity()->performedOn($add_machine)
                ->causedBy($user)
                ->withProperties(['name' => $user->name, 'email' => $user->email])
                ->tap(function(Activity $activity ) use ($add_machine,$add_machine_loc, $jsonActivity) {
                    $activity->machine_id = $add_machine->id;
                    $activity->customer_id = $add_machine_loc->customer_id;
                    $activity->new_value = json_encode($jsonActivity);
                })
                ->log('Created machine and Machine Location data');

        return redirect()->route('machines.index')->with('insert','Successfully Inserted Machine.');
    }

    public function edit($id)
    {   
        $machine = Machine::find($id);
        $mach_loc = '';
        $customer = '';
        if(!empty($machine)){
            $mach_loc = $machine->mach_loc; 
            if(!empty($mach_loc)){
                $customer = Customer::find($mach_loc->customer_id);
            }
        }
        $machine_status = Mstats::all();

        return view('machine.edit', compact('machine', 'mach_loc', 'customer', 'machine_status'));
    }

    public function checkUpdateKey($oldData, $newData){
        $result['old_value']=array_diff($oldData, $newData);
        unset($result['old_value']['deleted_by']);
        unset($result['old_value']['created_at']);
        unset($result['old_value']['id']);
        $result['new_value']=array_diff($newData, $oldData);
        unset($result['new_value']['deleted_by']);
        unset($result['new_value']['created_at']);
        unset($result['new_value']['id']);
        
        return $result;
    }

    public function update(Request $request, $id)
    {
        // dd($request->all());
        $rules = [
            'mach_number'     => 'required|unique:machines,mach_number,'.$id,
            'mach_model'      => 'required',
            'mach_status'     => 'required',
            'select_customer' => 'required',
            'ship_date'       => 'required',    
        ];

        $message = [
            'mach_number.required'     => 'Machine Number Field Is Required.',
            'mach_number.unique'       => 'Machine Number All Ready Exist.',
            'mach_model.required'      => 'Machine Model Field Is Required.',
            'mach_status.required'     => 'Machine Status Field Is Required.',
            'select_customer.required' => 'Please Select Customer.',
            'ship_date.required'       => 'Machine Ship Date Field Is Required.',
        ];
        $this->validate($request, $rules, $message); 
        $machine = Machine::find($id);

        $ori['mach_number'] = $machine->mach_number;
        $ori['mach_model'] = $machine->mach_model;
        $reqData['mach_number'] = $request->mach_number;
        $reqData['mach_model'] = $request->mach_model;
        $machine_log = $this->checkUpdateKey($ori, $reqData);


        $machine->mach_number = $request->mach_number;
        $machine->mach_model = $request->mach_model;
        $machine->status = $request->status;
        $machine->save();

        $reqmachOri['customer_id'] = $request->select_customer;
        $reqmachOri['ship_date'] = date('y-m-d', strtotime($request->ship_date));
        $reqmachOri['mach_status'] = $request->mach_status;
        $machOri = [];

        $mach_loc = MachLoc::where('machine_id', $request->mach_number)->first(); 
        if($mach_loc !== null){
            $mach_loc->customer_id = $request->select_customer;
            $mach_loc->machine_id  = $request->mach_number;
            $mach_loc->ship_date   = date('y-m-d', strtotime($request->ship_date));
            $mach_loc->mach_status = $request->mach_status;
            $mach_loc->save();

            $machOri['customer_id'] = $machine->customer_id;
            $machOri['ship_date'] = $machine->ship_date;
            $machOri['mach_status'] = $machine->mach_status;
        }else{
            $mach_loc_data = [
                'customer_id'  => $request->select_customer,
                'machine_id'   => $request->mach_number,
                'ship_date'    => date('y-m-d', strtotime($request->ship_date)),
                'mach_status'  => $request->mach_status,
            ];
            MachLoc::create($mach_loc_data);
        }

        $Machinelog_logs = $this->checkUpdateKey($machOri, $reqmachOri);
        
        $user = auth()->user();
        activity()->performedOn($machine)
                ->causedBy($user)
                ->withProperties(['name' => $user->name, 'email' => $user->email])
                ->tap(function(Activity $activity ) use ($machine,$mach_loc, $Machinelog_logs, $machine_log) {
                    $activity->machine_id = $machine->id;
                    if($mach_loc !== null){
                        $activity->customer_id = $mach_loc->customer_id;
                    }else{
                        $activity->customer_id = $mach_loc_data['customer_id'];
                    }
                    $activity->new_value = json_encode(array_merge($machine_log['new_value'], $Machinelog_logs['new_value']));
                    $activity->old_value = json_encode(array_merge($machine_log['old_value'], $Machinelog_logs['old_value']));
                })
                ->log('Updated machine');

        return redirect()->route('machines.index')->with('update','Successfully Updated Machine.');
    }

    public function destroy($id)
    {
        $machine = Machine::find($id);
        $machineObj = $machine;
        $machine->delete();
        $user = auth()->user();
        $machineObj->deleted_by = "Name: ".  $user->name. " <br /> Email: ".  $user->email;
        activity()->performedOn($machineObj)
                ->causedBy($user)
                ->withProperties(['name' => $user->name, 'email' => $user->email])
                
                ->tap(function(Activity $activity ) use ($id, $machineObj) {
                    $activity->machine_id = $id;
                    $activity->old_value = json_encode($machineObj);
                })
                ->log('Deleted machine');

        return redirect()->route('machines.index')->with('delete','Successfully Deleted Machine.');
    }

    public function autocomplete(Request $request)
    {
        $term = trim($request->q);
        if (empty($term)) {
            return \Response::json([]);
        }

        $machines = MachineModel::where('mach_model', 'LIKE', $term.'%' )
                            ->limit(50)
                            ->get();

        $formatted_machines = [];

        foreach ($machines as $machine) {
            $formatted_machines[] = ['id' => $machine->mach_model, 'text' => $machine->mach_model];
        }

        return \Response::json($formatted_machines);
    }

    public function SelectMachineNum(Request $request)
    {
        $term = trim($request->q);
        if (empty($term)) {
            return \Response::json([]);
        }

        $machines = Machine::select('machines.id','machines.mach_number')
                             ->where('mach_number', 'LIKE', $term.'%' )
                             ->join('mach_loc', 'machines.mach_number', '=', 'mach_loc.machine_id')
                             ->where('cur_mach_loc', 1)
                            ->limit(50)
                            ->get();

        $formatted_machines = [];

        foreach ($machines as $machine) {
            $formatted_machines[] = ['id' => $machine->mach_number, 'text' => $machine->mach_number];
        }

        return \Response::json($formatted_machines);
    }

    public function search(Request $request){

        if($request->ajax()){
            $output = '';
            $query = $request->get('query');
            $status = $request->get('status');

            if ($query == '' &&  $status == '') {
                return;
            }

            $qry = Machine::query();

            if($status !=''){
                    $qry->where('status',$status);
            }

            if($query != ''){

                $q = $query;
                $qry->where(function ($querys) use ($q) {                
                $querys->OrWhere('mach_number','LIKE','%'.$q.'%')
                        ->orWhere('mach_model','LIKE','%'.$q.'%');
                });
                
            }
                $machines = $qry->leftJoin('mach_loc', 'machines.mach_number', '=', 'mach_loc.machine_id')
                    ->leftJoin('m_stats', 'mach_loc.mach_status', '=', 'm_stats.m_status')
                    ->where('mach_loc.cur_mach_loc',1)
                    ->paginate(env('page'));  
                      
                return view('machine.search', compact('machines'))->render();

        }
    }

    public function transferMachineAutofill(Request $request)
    {
        if(isset($request->machine_number)){
            $machine = Machine::where('mach_number', $request->machine_number)->first();
            $mach_loc = MachLoc::where('machine_id', $machine->id)->first();
            $customer = Customer::find($mach_loc->customer_id);
            $contact = Contact::where('customer_id', $customer->id)->first();
            $source_loc = SourceLoc::where('mach_num', $request->machine_number)->first();

            $salutation = !empty($contact->salutation) ? $contact->salutation : '';
            $contact_name = !empty($contact->contact_name) ? $contact->contact_name : '';

            $machine_data = [
                'mach_number' => $machine->mach_number,
                'mach_model' => $machine->mach_model,
                'mach_status' => $mach_loc->machine_status->mstats_desc,
                'current_customer' => $customer->customer_number,
                'customer_contact' => $salutation.$contact_name,
                'current_state' => $customer->state,
                'machine_source' => $source_loc->source_num,
                'source_status' => $source_loc->s_status,
                'machine_source' => $source_loc->source_num,
                'source_status' => $source_loc->source_status->sstat_desc,
            ];

            $data = array(
                'machine_data' => $machine_data
            );

            echo json_encode($data);
        }

        if($request->customer_number){
            $customer = Customer::find($request->customer_number);
            $contact = Contact::where('customer_id', $customer->id)->first();
            $salutation = !empty($contact->salutation) ? $contact->salutation : '';
            $contact_name = !empty($contact->contact_name) ? $contact->contact_name : '';

            $machine_data = [
                'current_customer' => $customer->customer_number,
                'customer_contact' => $salutation.$contact_name,
                'current_state' => $customer->state
            ];

            $data = array(
                'machine_data' => $machine_data
            );

            echo json_encode($data);
        }
    }

    public function duplicateMachine(Request $request){
        if($request->ajax()){
            $output = '';
            $source_output = '';

            $machines = MachLoc::where('customer_id', $request->customer_id)
                        ->where('machine_id', $request->machine_id)
                        ->orderBy('ship_date')
                        ->paginate(env('page'), ['*'], 'machine');

            $total_row = $machines->count();

            if($total_row > 0){

                $i = 1;

                $output .= ' <tr class="show_data">
                                        <td colspan="2"></td>
                                        <td><b>Machine Number</b></td>
                                        <td><b>Machine Model</b></td>
                                        <td><b>Ship Date</b></td>
                                        <td></td>
                                    </tr>';

                foreach($machines as $key => $machine){

                    $output .= '
                    <tr class="show_data">
                        <td colspan="2"></td>                   
                        <td>'.$machine->machine->mach_number.'</td>
                        <td>'.$machine->machine->mach_model.'</td> 
                        <td>'.date('d M Y', strtotime($machine->ship_date)).'</td> 
                        <td></td>                   
                    </tr>
                    ';
                    $i++;
                }
            }else{
                $output = '
                <tr class="show_data">
                    <td colspan="6" class="text-center">
                        <p>Machine Not Found.</p>
                    </td> 
                </tr>
                ';
            }

            $sources = SourceLoc::where('mach_num', $request->machine_num)->get();
            $source_row = $sources->count();
            if($source_row > 0){
                $i = 1;
                $source_output .= ' <tr class="show_data">
                                        <td colspan="2"></td>
                                        <td><b>Source</b></td>
                                        <td><b>Source Model</b></td>
                                        <td><b>Installation Date</b></td>
                                        <td></td>
                                    </tr>';
                foreach($sources as $key => $source){
                    $source_output .= '
                    <tr class="show_data">
                        <td colspan="2"></td>
                        <td>'.$source->source_num.'</td>
                        <td>'.$source->s_mod_num.'</td> 
                        <td>'.date('d M Y', strtotime($source->inst_date)).'</td> 
                        <td></td>
                    </tr>
                    ';
                    $i++;
                }
            }else{
                $source_output = '
                <tr class="show_data">
                    <td colspan="6" class="text-center">
                        <p>Source Not Found.</p>
                    </td> 
                </tr>
                ';
            }
            
            $data = array(
                'table_data'  => $output,
                'source_table_data'  => $source_output,
                'total_data'  => $total_row
            );
    
            echo json_encode($data);
        }
    }

     public function getHistory($machine_id){ 
        $machinelocdata  = [];
        $wipedatahistory = [];
        $uploaddoc = UploadDocument::where('machine_id',$machine_id)->get();
        $machinedata = Machine::where('id', $machine_id)->first();  
        if($machinedata['id']){
            $machinelocdata  = $machinedata->machloc; 
            $wipedatahistory = Wipe::where('mach_num', $machine_id)
                            ->orderBy('created_at', 'desc')
                            ->get();
        }
        return view('machine.history',compact('machinelocdata', 'machine_id', 'uploaddoc', 'wipedatahistory', 'machinedata'));
     }
        
}
