<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;
use DB;
class LogController extends Controller
{
    public function index()
    {
        // $logs = Activity::paginate(env('page'));
        
        $logs = DB::table('activity_log')
                    ->orderBy('id', 'desc')
                    ->paginate(env('page'));


    	return view('log.index', compact('logs'));
    }

    public function get_history(Request $request){

        $machine_id = $request->machine_id;
        $customer_id = $request->customer_id;
        $contact_id = $request->contact_id;
        $technician_id = $request->technician_id;
        $source_id = $request->source_id;


        if (is_numeric($contact_id)) {
            $log_data = DB::table('activity_log')
            ->select('activity_log.*', 'users.name as user_name')
            ->where('contact_id',$contact_id)
            ->leftJoin('users', 'users.id', '=', 'activity_log.causer_id')
            // ->leftJoin('users', 'users.id', '=', 'activity_log.causer_id')
            ->orderBy('id','desc')
            ->get();

            return view('contact.history',compact('log_data'));

        }

        
        if (is_numeric($machine_id)) {
            $log_data = DB::table('activity_log')
            ->select('activity_log.*', 'users.name as user_name')
            ->where('machine_id',$machine_id)
            ->leftJoin('users', 'users.id', '=', 'activity_log.causer_id')
            // ->leftJoin('users', 'users.id', '=', 'activity_log.causer_id')
            ->orderBy('id','desc')
            ->get();

            return view('machine.history',compact('log_data'));

        }

        



        if (is_numeric($customer_id)) {
            $log_data = DB::table('activity_log')
                     ->select('activity_log.*', 'users.name as user_name')
                     ->where('customer_id',$customer_id)
                     ->leftJoin('users', 'users.id', '=', 'activity_log.causer_id')
                     // ->leftJoin('users', 'users.id', '=', 'activity_log.causer_id')
                     ->orderBy('id','desc')
                     ->get();
             
             $customer = DB::table('customers')
                                 ->where('id',$customer_id)->get();
 
             return view('customer.history',compact('log_data','customer'));
               
         }

         

        if (is_numeric($technician_id)) {
            $log_data = DB::table('activity_log')
                     ->select('activity_log.*', 'users.name as user_name')
                     ->where('technician_id',$technician_id)
                     ->leftJoin('users', 'users.id', '=', 'activity_log.causer_id')
                     // ->leftJoin('users', 'users.id', '=', 'activity_log.causer_id')
                     ->orderBy('id','desc')
                     ->get();

 
             return view('technician.history',compact('log_data'));
               
         }

         

        if (is_numeric($source_id)) {
            $log_data = DB::table('activity_log')
                     ->select('activity_log.*', 'users.name as user_name')
                     ->where('source_id',$source_id)
                     ->leftJoin('users', 'users.id', '=', 'activity_log.causer_id')
                     // ->leftJoin('users', 'users.id', '=', 'activity_log.causer_id')
                     ->orderBy('id','desc')
                     ->get();

             return view('source.history',compact('log_data'));
               
         }


      

    }

    public function search(Request $request){

        if($request->ajax()){
            $output = '';
            $query = $request->get('query');

            if ($query == '') {
                return;
            }

            if($query != ''){

                $q = $query;
                $logs = Activity::where('description','LIKE','%'.$q.'%')
                        ->orWhere('subject_type','LIKE','%'.$q.'%')
                        ->orWhere('causer_type','LIKE','%'.$q.'%')
                        ->orWhere('properties','LIKE','%'.$q.'%')
                        ->get();
            }

            $total_row = $logs->count();

            if($total_row > 0){

                $i = 1;
                foreach($logs as $key => $log){

                    $output .= '
                    <tr>
                        <td>'.$i.'</td>
                        <td>'.$log->description.'</td>
                        <td>'.$log->subject_type.'</td> 
                        <td>'.$log->causer_type.'</td>                     
                        <td>'.$log->getExtraProperty('name').'</td>                     
                        <td>'.$log->getExtraProperty('email').'</td>                     
                        <td class="text-left">'.date('d M Y', strtotime($log->created_at)).'</td>                     
                    </tr>
                    ';
                    $i++;
                }
            }else{
                $output = '
                <tr>
                <td class="text-center" colspan="7"><p>Log Not Found.</p></td>
                </tr>
                ';
            }
            
            $data = array(
                'table_data'  => $output,
                'total_data'  => $total_row
            );
    
            echo json_encode($data);
        }
    }
}
