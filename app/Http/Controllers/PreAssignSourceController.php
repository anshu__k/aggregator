<?php

namespace App\Http\Controllers;
use Spatie\Activitylog\Contracts\Activity;

use Illuminate\Http\Request;
use App\SourceLoc;
use App\Contact;
use App\SModList;
use App\SStats;
use App\Machine;
use App\MachLoc;
use App\Customer;
use App\Measurer;
use App\Discrep;
use App\Technician;
use App\Wipe;

class PreAssignSourceController extends Controller
{
    public function index()
    {
        $source_locs = SourceLoc::where('currentloc', '2')
                                    ->orderBy('id', 'desc')
                                    ->paginate(env('page'));
        $measurers = Measurer::all();
        $discreps = Discrep::whereIn('discrepcod',[7])->get();
        return view('pre_assign_source.index', compact('source_locs', 'measurers', 'discreps'));
    }
    public function history()
    {
        $source_locs = SourceLoc::orderBy('id', 'desc')->where('currentloc', 1)->paginate(env('page'));
        $measurers = Measurer::all();
        $discreps = Discrep::whereIn('discrepcod',[7])->get();
        return view('pre_assign_source.history', compact('source_locs', 'measurers', 'discreps'));
    }

    public function create()
    {
    	$source_mods = SModList::whereIn('id',[1,2,3,4,6,7,8,12])
        ->get();
    	$ship_methods = SStats::whereIn('s_status',["im","ip","cm","cp"])
        ->get();

    	return view('pre_assign_source.create', compact('source_mods', 'ship_methods'));
    }

    public function store(Request $request)
    {   
        $rules = [
            'source_num'         => 'required',
            'machine_num'        => 'required',
            'source_model'       => 'required',
            'customer_contact'       => 'required',
            'source_ship_method' => 'required',
        ];

        $message = [
            'source_num.required'         => 'Source Being Issued Field Is Required.',
            'machine_num.required'        => 'Machine Assigning Field Is Required.',
            'source_model.required'       => 'Please Select Source Model.',
            'customer_contact.required' => 'Please Select Customer Contact.',
            'source_ship_method.required' => 'Please Select Source Ship Method.',
        ];

        $this->validate($request, $rules, $message);
        // if($request->contact_name){
        //     $contactdata  = Contact::where('contact_name', $request->customer_contact)
        //                         ->Where('contact_type', 'a')
        //                         ->get(); 
        // }
        
        $data = [
            'source_num' => $request->source_number,
            'mach_num'   => $request->machine_num,
            's_mod_num'  => $request->source_model,
            'contact_id' => $request->customer_contact,
            's_status'   => $request->source_ship_method,
            'currentloc' => '2',
        ];

        $add_sourceloc = SourceLoc::create($data);

        $user = auth()->user();
                activity()->performedOn($add_sourceloc)
                ->causedBy($user)
                ->withProperties(['name' => $user->name, 'email' => $user->email,'machine_num'=>$data['mach_num']])
                // ->tap(function(Activity $activity ) use ($customer) {
                //     $activity->customer_id = $customer->id;
                // })
                ->log('Pre Assigned Source');

    	return redirect()->route('preAssignSou.index')->with('insert', 'Pre-assign Source Add Successfully.');
    }

    public function edit($id)
    {
        $source_mods = SModList::whereIn('id',[1,2,3,4,6,7,8,12])
        ->get();
        $ship_methods = SStats::whereIn('s_status',["im","ip","cm","cp"])
        ->get();

        $source_loc = SourceLoc::find($id);

        $machine = Machine::where('mach_number', $source_loc->mach_num)->first();
        $machLoc = MachLoc::where('machine_id', $machine->mach_number)->first();
        $customer = Customer::find($machLoc->customer_id);

    	return view('pre_assign_source.edit', compact('source_mods', 'ship_methods', 'source_loc', 'machine', 'machLoc', 'customer'));
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'edit_source_num'         => 'required',
            'edit_machine_num'        => 'required',
            'edit_source_model'       => 'required',
            'edit_customer_contact'    => 'required',
            'edit_source_ship_method' => 'required',
        ];

        $message = [
            'edit_source_num.required'         => 'Source Being Issued Field Is Required.',
            'edit_machine_num.required'        => 'Machine Assigning Field Is Required.',
            'edit_source_model.required'       => 'Please Select Source Model.',
            'edit_customer_contact.required' => 'Please Select Customer Contact.',
            'edit_source_ship_method.required' => 'Please Select Source Ship Method.',
        ];
        $this->validate($request, $rules, $message);
        // if($request->contact_name){
        //     $contactdata  = Contact::where('contact_name', $request->edit_customer_contact)
        //                         ->Where('contact_type', 'a')
        //                         ->get(); 
        // }
        $data = [
            'source_num' => $request->edit_source_num,
            'mach_num'   => $request->edit_machine_num,
            's_mod_num'  => $request->edit_source_model,
            'contact_id' => $request->edit_customer_contact,
            's_status'   => $request->edit_source_ship_method,
            'currentloc' => '2',
        ];

        $add_sourceloc = SourceLoc::find($id);
        $add_sourceloc->update($data);
        // $add_sourceloc = SourceLoc::updateOrCreate(['id' => $id ,'source_num' => $request->source_number,'mach_num' => $request->machine_num], $data);


        $user = auth()->user();
        activity()->performedOn($add_sourceloc)
                ->causedBy($user)
               
                ->withProperties(['name' => $user->name, 'email' => $user->email,'machine_num'=>$data['mach_num']])
                // ->tap(function(Activity $activity ) use ($customer) {
                //     $activity->customer_id = $customer->id;
                // })
                ->log('Updated Pre Assigned Source');

    	return  redirect()->route('preAssignSou.index')->with('update', 'Pre-Assign Source Update Successfully.');
    }

    public function destroy($id)
    {
        $getsource = SourceLoc::find($id);
        
        $delList = SourceLoc::where('source_num', $getsource->source_num)
                                ->where('mach_num', $getsource->mach_num)
                                ->where('currentloc', $getsource->currentloc)
                                ->delete();
                       
        // $user = auth()->user();
        // activity()->performedOn($add_sourceloc)
        //         ->causedBy($user)
               
        //         ->withProperties(['name' => $user->name, 'email' => $user->email,'machine_num'=>$data['mach_num']])
        //         // ->tap(function(Activity $activity ) use ($customer) {
        //         //     $activity->customer_id = $customer->id;
        //         // })
        //         ->log('Updated Pre Assigned Source');
        return redirect()->route('preAssignSou.index')->with('delete','Pre-Assign Source Deleted Successfully.');
    }

    public function createWipe(Request $request){

        $source_loc = SourceLoc::find($request->source_loc_id);
        

        $rules = [
            'wipe_date'      => 'required',
            'measure_date'   => 'required',    
            'measure_by'     => 'required',
            'tech'           => 'required',    
            'discrepcod'      => 'required',    
        ];

        $message = [
            'wipe_date.required'      => 'Wipe Date Field Is Required.',
            'measure_date.required'   => 'Measurer Date Field Is Required.',
            'measure_by.required'     => 'Measurer By Field Is Required.',
            'tech.required'           => 'Technician Field Is Required.',
            'discrepcod.required'     => 'Discrepcod Field Is Required.',
        ];

        $this->validate($request, $rules, $message);
        //dd($source_loc->contact_name->contact_name);
        $data = [
            'source_num'        => $source_loc->source_num,
            'mach_num'          => $source_loc->mach_num,
            'cust_num'          => @$source_loc->machine_num->mach_loc->customer->customer_number,
            'contact_num'       => @$source_loc->contact_name->contact_name,
            'print_job'         => $source_loc->print_job,
            'tech'              => $request->tech,
            'wipe_date'         => date('Y-m-d',strtotime($request->wipe_date)),
            'meas_date'         => date('Y-m-d',strtotime($request->measure_date)),
            'meas_by'           => $request->measure_by,
            'discrepcod'        => $request->discrepcod,
            'wipe_conf'        => 1, 
        ];

        $push_wipe = Wipe::create($data);
        $sourceupdate  =  [
                        'currentloc' => '2'
                    ];
        if($push_wipe){
           $sourceupdate['installation_wipe_test'] = $push_wipe->id;
           $sourceupdate['inst_date'] = date('Y-m-d H:i:s',strtotime($request->wipe_date));  
           $sourceupdate['tech'] = $request->tech;
            $update_source_loc = SourceLoc::where('id', $request->source_loc_id)
                                    ->update($sourceupdate);

          $mstatus['mach_status']='sr';
         MachLoc::where('machine_id',$source_loc->mach_num)
                            ->where('cur_mach_loc',1)
                            ->update($mstatus);

        }

        return redirect()->route('preAssignSou.index')->with('insert','Create Wipe Successfully.');;
    }

    public function confirmShipDate(Request $request){
        
        $rules = [
            'ship_date'      => 'required',    
        ];

        $message = [
            'ship_date.required'      => 'Ship Date Field Is Required.',
        ];

        $this->validate($request, $rules, $message);

        if($request->ship_date !='')
        $source_loc = SourceLoc::find($request->sourceloc_id)->update(['s_ship_date' => date('Y-m-d', strtotime($request->ship_date)), 's_status' => 'cm']);

        return redirect()->route('preAssignSou.index')->with('insert','Change Source Ship Date.');
    }

    public function updateSourceLog(Request $request) {

        
        $sourcep = SourceLoc::where('source_num', $request->source_num)
                            ->Where('currentloc', '1')
                            ->first();

        //$sourceupdate['renewable_wipe_test'] = $request->mach_num; 
        $source_loc = SourceLoc::find($request->sourceloc_id);

        $shipIdUpdate = $source_loc->installation_wipe_test;

        $techname = Technician::where('type','4')->first(); 
        //dd($source->mach_num);
        
        $mstatus['mach_status']='aa';
         MachLoc::where('machine_id', $source_loc->mach_num)
                            ->where('cur_mach_loc',1)
                            ->update($mstatus);

        if(isset($sourcep->mach_num)){                    
         $machLoc = MachLoc::where('machine_id', $sourcep->mach_num)->first();
        
        $custNum=$machLoc->customer->customer_number;
        }else{
            $custNum='';
        }                            
                                                  
        //$sourceupdate['renewable_wipe_test'] = !empty($machLoc->customer->id) ? $machLoc->customer->id : '';
        
        $sourceulast['currentloc'] = 0;
            $update_source_last = SourceLoc::where('source_num', $request->source_num)
                                    ->update($sourceulast);


         
         $data = [
            'source_num'        => $source_loc->source_num,
            'mach_num'          => '',
            'cust_num'          => $custNum,//$source_loc->machine_num->mach_loc->customer->customer_number
            'print_job'         => $source_loc->print_job,
            'tech'              => isset($techname->id)?$techname->id:'',
            'contact_num'       => isset($techname->id)?$techname->tech_name:'',
            'wipe_date'         => isset($source_loc->ship_date)?date('Y-m-d',strtotime($source_loc->ship_date)):date('Y-m-d'),
            'meas_date'         => isset($source_loc->ship_date)?date('Y-m-d',strtotime($source_loc->ship_date)):date('Y-m-d'),
            'meas_by'           => isset($techname->id)?$techname->id:'',
            'discrepcod'        => 8,
            'wipe_conf'        => 0, 
        ];

        $push_wipe = Wipe::create($data);                          
        $shipconval = Wipe::find($shipIdUpdate)->update(['wipe_conf' => 0]);


        $sourceupdate['renewable_wipe_test'] =$push_wipe->id;                            
        $sourceupdate['currentloc'] = 1;
        $sourceupdate['s_status'] = 'aa';
        $sourceupdate['s_status_prev'] = $source_loc->s_status;
        $sourceupdate['tech'] = isset($techname->tech_name)?$techname->tech_name:'';
            $update_source_loc = SourceLoc::where('id', $request->sourceloc_id)
                                    ->update($sourceupdate);

        return ["status"=> "success", "message" => "Source log updated successfully"];
    }

    public function search(Request $request){

        if($request->ajax()){
            $output = '';
            $query = $request->get('query');
            

            if ($query == '') {
                return;
            }

            $qry = SourceLoc::query();

            if($query != ''){

                $q = $query;
                $qry->where(function ($querys) use ($q) {                
                $querys->OrWhere('source_num','LIKE','%'.$q.'%')
                        ->orWhere('mach_num','LIKE','%'.$q.'%');
                });
                
            }
                $source_locs = $qry->where('currentloc', '2')
                                    ->orderBy('id', 'desc')
                                    ->paginate(env('page'));
                 $measurers = Measurer::all();
                $discreps = Discrep::whereIn('discrepcod',[7])->get();

                return view('pre_assign_source.search', compact('source_locs','measurers','discreps'))->render();

        }
    }

}
