<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MachLoc;
use App\SStats;
use App\SourceLoc;
use App\TransferMachine;

class TransferMachineController extends Controller
{
    public function index()
    {
        $transfer_machs = TransferMachine::paginate(env('page'));

    	return view('transfer_machine.index', compact('transfer_machs'));
    }

    public function create()
    {
        $ship_methods = SStats::all();

    	return view('transfer_machine.create', compact('ship_methods'));
    }

    public function store(Request $request)
    {
        $rules = [
            'machine_number'     => 'required',
            'customer_number'    => 'required',
            'ship_date'          => 'required',
            'install_date'       => 'required',
            'source_ship_method' => 'required',
            'technician'         => 'required'
        ];

        $message = [
            'machine_number.required'     => 'Please enter machine number.', 
            'customer_number.required'    => 'Please enter customer number.',
            'ship_date.required'          => 'Please select ship date.',
            'install_date.required'       => 'Please select install date.',
            'source_ship_method.required' => 'Please select source ship method.',
            'technician.required'         => 'Please select technician.'
        ];

        $this->validate($request, $rules, $message);

        // dd($machine);
        $data = [
            'mach_num' => $request->machine_number,
            'customer_num' => $request->customer_number,
            'ship_date' => $request->ship_date,
            'install_date' => $request->install_date,
            'source_ship_method' => $request->source_ship_method,
            'tech' => $request->technician,
        ]; 

        $source_transfer = TransferMachine::create($data);

    	return redirect()->route('TransferMach.index')->with('insert', 'Source Transfer Successfully.');
    }

    public function edit($id)
    {   
        // $source_status_lists = SStats::all();

        // $transfer_source = SourceLoc::find($id);

        $ship_methods = SStats::all();
        // dd($transfer_source);

    	return view('transfer_machine.edit', compact('ship_methods'));
    }

    public function update(Request $request, $id)
    {

        $rules = [
            // 'edit_source_number'   => 'required',
            // 'edit_source_model' => 'required',
            // 'edit_machine_number'  => 'required',
            'edit_recieved_date' => 'required',
            'edit_inst_tech'     => 'required'
        ];

        $message = [
            // 'edit_source_number.required'   => 'Please select ship date.', 
            // 'edit_source_model.required' => 'Please select installation date.',
            // 'edit_machine_number.required'  => 'Please select source status.',
            'edit_recieved_date.required' => 'Please select recieved date.',
            'edit_inst_tech.required'     => 'Please select technician.'
        ];

        $this->validate($request, $rules, $message);
        $data = [
            's_status' => 'aa',
            'tech' => $request->edit_inst_tech,
        ]; 
        // dd($data);
        $source_transfer = SourceLoc::find($id)->update($data);
    	return  redirect()->route('TransferSou.index')->with('update', 'Source Transfer Update Successfully.');
    }

    public function destroy($id)
    {
        return redirect()->route('TransferSou.index')->with('delete','Source Transfer Deleted Successfully.');
    }
}
