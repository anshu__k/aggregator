<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Activitylog\Contracts\Activity;
use App\Contact;
use App\Customer;
use App\ContactType;
use App\UploadDocument;
use App\ScriptRecord;
use App\PoplarCampaign;
use DB;
use View;
use App\Services\PoplarAPIService;
use App\Jobs\ProcessPoplarPostCardSend;

class ContactController extends Controller
{
    protected $display_column;

    protected $poplar_services;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PoplarAPIService $poplarAPIService)
    {
        // $this->middleware('auth');
        $this->poplar_services = $poplarAPIService;
        $this->display_column = [
            [
                'index' => '0',
                'title' => '<input name="select_all" id="select_all" value="1" type="checkbox">',
                'key' => 'checkbox',
                'width' => '5',
                'ordering' => 'false',
                'prefix_table' => '',
            ],
            [
                'index' => '1',
                'title' => 'Id',
                'key' => 'id',
                'prefix_table' => 'script_records',
            ],
            [
                'index' => '2',
                'title' => 'First Name',
                'key' => 'firstName',
                'show_hide' => true,
                'prefix_table' => 'script_records',
            ],
            [
                'index' => '3',
                'title' => 'Last Name',
                'key' => 'lastName',
                'show_hide' => true,
                'prefix_table' => 'script_records',
            ],
            [
                'index' => '4',
                'title' => 'Zip',
                'key' => 'zip',
                'show_hide' => true,
                'default_hide' => true,
                'prefix_table' => 'script_records',
            ],
            [
                'index' => '5',
                'title' => 'Address',
                'key' => 'address',
                'show_hide' => true,
                'default_hide' => true,
                'prefix_table' => 'script_records',
            ],
            [
                'index' => '6',
                'title' => 'City',
                'key' => 'city',
                'show_hide' => true,
                'default_hide' => true,
                'prefix_table' => 'script_records',
            ],
            [
                'index' => '7',
                'title' => 'State',
                'key' => 'state',
                'show_hide' => true,
                'default_hide' => true,
                'prefix_table' => 'script_records',
            ],
            [
                'index' => '8',
                'title' => 'County',
                'key' => 'county',
                'show_hide' => true,
                'default_hide' => true,
                'prefix_table' => 'script_records',
            ],
            [
                'index' => '9',
                'title' => 'License Number',
                'key' => 'licenseNumber',
                'width' => '10',
                'show_hide' => true,
                'prefix_table' => 'script_records',
            ],
            [
                'index' => '10',
                'title' => 'License Issuance Date',
                'key' => 'licence_issuance_date',
                'width' => '10%',
                'show_hide' => true,
                'prefix_table' => 'script_records',
            ],
            [
                'index' => '11',
                'title' => 'License Expiration Date',
                'key' => 'licence_expiration_date',
                'width' => '10%',
                'show_hide' => true,
                'prefix_table' => 'script_records',
            ],
            [
                'index' => '12',
                'title' => 'License Status',
                'key' => 'licenseStatus',
                'width' => '10',
                'show_hide' => true,
                'prefix_table' => 'script_records',
            ],
            [
                'index' => '13',
                'title' => 'Details',
                'key' => 'details',
                'class' => 'notexport',
                'ordering' => 'false',
                'prefix_table' => '',
            ],
            [
                'index' => '14',
                'title' => 'Created At',
                'key' => 'created_at',
                'width' => '10%',
                'show_hide' => true,
                'prefix_table' => 'script_records',
            ],
            [
                'index' => '15',
                'title' => 'Bullhorn Candidate ID',
                'key' => 'bullhorn_candidate_id',
                'width' => '20',
                'show_hide' => true,
                'prefix_table' => 'script_records',
            ],
            [
                'index' => '16',
                'title' => 'Bullhorn Contact ID',
                'key' => 'bullhorn_contact_id',
                'width' => '20',
                'show_hide' => true,
                'prefix_table' => 'script_records',
            ],
            [
                'index' => '17',
                'title' => 'Poplar Info',
                'key' => 'poplar_info',
                'class' => 'notexport', 
                'ordering' => false,
                'prefix_table' => ''
            ],
            [
                'index' => '18',
                'title' => 'Action',
                'key' => 'action',
                'class' => 'notexport',
                'ordering' => 'false',
                'prefix_table' => '',
            ]
        ];
    }

    /**
     * Show the contact page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        // $contacts = Contact::paginate(10);

        $contacts = DB::table('contacts')
            ->leftJoin('customers', 'customers.id', '=', 'contacts.customer_id')
            ->leftJoin('contact_types', 'contact_types.contact_type', '=', 'contacts.contact_type')
            ->whereNull('contacts.deleted_at')
            // ->whereRaw('contacts.id = 3540')
            ->select('contacts.*', 'customers.customer_name', 'customers.customer_number', 'contact_types.contact_desc')
            ->orderByRaw('updated_at desc, customer_name asc, customer_number asc')
            ->paginate(10);

        $contact_types = ContactType::all();

        $data = array('contacts' => $contacts, 'contact_types' => $contact_types);

        // echo '<pre>';
        // print_r($contacts->toArray());
        // die();

        return view('contact.table', $data);
    }

    public function script_record()
    {
        // $sources = DB::table('script_records')
        //     ->orderBy('script_records.id', 'desc')
        //     ->paginate(1500);
        // return view('contact.script_record', compact('sources'));
        $data['columns'] = $this->display_column;
        $data['poplar_campaigns'] = PoplarCampaign::whereStatus('1')->get();
        // dd($data['columns']);
        return view('contact.script_record', $data);
    }

    public function script_record_paginate(Request $request)
    {
        $start = $request->start ?? 0;
        $length = $request->length ?? 50;
        $draw_val = $request->input('draw');
        $search = $request->input('search')['value'] ?? '';

        /** Ordering */
        $orderColumn = 'script_records.id';
        $orderDir = 'DESC';

        if (!empty($request->order)) {
            $columnRequest = $request->order[0]['column']; //'script_records';
            $getArray = $this->display_column[$columnRequest];
            $orderColumn = $getArray['prefix_table'] . '.' . $getArray['key'] ?? 'script_records.id';
            $orderDir = $request->order[0]['dir'] ?? 'DESC';
        }
        /** Ordering */

        // $sources = ScriptRecord::orderBy('script_records.id', 'desc');
        $sources = ScriptRecord::select('script_records.*')
            ->with('script_record_sent_postcard', 'script_record_sent_postcard.poplar_campaign_detail')
            // ->with('')
            ->leftJoin('script_record_sent_postcards', 'script_record_sent_postcards.script_record_id', 'script_records.id')
            ->leftJoin('poplar_campaigns', 'poplar_campaigns.campaign_id', 'script_record_sent_postcards.campaign_id')
            ->groupBy('script_records.id')
            ->orderBy($orderColumn, $orderDir);

        // dd($request->showBullhornData);
        /*if($request->showOnlyContact) {
            $sources = $sources->whereNotNull('bullhorn_contact_id');
        } else if ($request->showOnlyCandidate) {
            $sources = $sources->whereNotNull('bullhorn_candidate_id');
        } else if($request->showCandidateAndContact) {
            $sources = $sources->where(function($query){
                $query->whereNotNull('bullhorn_contact_id')->orWhereNotNull('bullhorn_candidate_id');
            });
        }*/

        if ($request->showBullhornData == 'unsync') {
            // Show unsync data
            $sources = $sources->where(function ($query) {
                $query->whereNull('bullhorn_contact_id')
                    ->WhereNull('bullhorn_candidate_id');
            });
        } else if ($request->showBullhornData == 'unsync_contact') {
            // Show all contact unsync 
            $sources = $sources->where(function ($query) {
                $query->whereNull('bullhorn_contact_id');
                // ->whereNotNull('bullhorn_candidate_id');
            });
        } else if ($request->showBullhornData == 'unsync_candidate') {
            // Show all contact unsync 
            $sources = $sources->where(function ($query) {
                $query->whereNull('bullhorn_candidate_id');
                // ->whereNotNull('bullhorn_contact_id');
            });
        } else if ($request->showBullhornData == 'contact') {
            // Show all candidate unsync 
            $sources = $sources->where(function ($query) {
                $query->whereNotNull('bullhorn_contact_id')
                    ->whereNull('bullhorn_candidate_id');
            });
        } else if ($request->showBullhornData == 'candidate') {
            // Show all candidate unsync 
            $sources = $sources->where(function ($query) {
                $query->whereNotNull('bullhorn_candidate_id')
                    ->whereNull('bullhorn_contact_id');
            });
        } else if ($request->showBullhornData == 'both') {
            // Show all candidate unsync 
            $sources = $sources->where(function ($query) {
                $query->whereNotNull('bullhorn_contact_id')
                    ->orWhereNotNull('bullhorn_candidate_id');
            });
        } else {
            // Show all records 
        }

        $dateField = 'script_records.created_at';
        if (!empty($request->select_field)) {
            if ($request->select_field == "issuance_date") {
                $dateField = 'script_records.licence_issuance_date';
            } elseif ($request->select_field == "expiration_date") {
                $dateField = 'script_records.licence_expiration_date';
            } else {
                $dateField = 'script_records.created_at';
            }
            // $dateField = $request->select_field;
            //licenseExpirationDate' => date('m/d/y', strtotime($item->licence_expiration_date)),
            // 'licenseIssuanceDate
        }

        if (!empty($request->fromDate) && empty($request->toDate)) {
            $from_date = date('Y-m-d', strtotime($request->fromDate));
            // $sources = $sources->where(\DB::raw('script_records.created_at'), '>=', $from_date);
            $sources = $sources->where(\DB::raw($dateField), '>=', $from_date);
        } else if (!empty($request->toDate) && empty($request->fromDate)) {
            $to_date = date('Y-m-d', strtotime($request->toDate));
            // $sources = $sources->where(\DB::raw('script_records.created_at'), '<', $to_date);
            $sources = $sources->where(\DB::raw($dateField), '<', $to_date);
        } else if (!empty($request->fromDate) && !empty($request->toDate)) {
            $from_date = date('Y-m-d', strtotime($request->fromDate));
            $to_date = date('Y-m-d', strtotime($request->toDate));
            // $sources = $sources->whereBetween(\DB::raw('script_records.created_at'), [$from_date, $to_date]);
            $sources = $sources->whereBetween(\DB::raw($dateField), [$from_date, $to_date]);
        }

        if(!empty($request->search_poplar_campaign)) {
            if($request->search_poplar_campaign == 'notsent') {
                $sources = $sources->whereNull('script_record_sent_postcards.campaign_id');
            } else {
                list($searchCampaign, $poplar_campaign_id) = explode('|||', $request->search_poplar_campaign);
                if($searchCampaign == "not_sent_to") {
                    // Show excepted data 
                    // $sources = $sources->where('script_record_sent_postcards.campaign_id', '!=', $request->search_poplar_campaign);
                    $sources = $sources->where(function($query) use($poplar_campaign_id) {
                        $query = $query->where('script_record_sent_postcards.campaign_id', '!=', $poplar_campaign_id)
                                    ->orWhereNull('script_record_sent_postcards.campaign_id', $poplar_campaign_id);
                    });
                } else {
                    // $sources = $sources->where('script_record_sent_postcards.campaign_id', $request->search_poplar_campaign);
                    $sources = $sources->where('script_record_sent_postcards.campaign_id', $poplar_campaign_id);
                }
            }
        }

        if (!empty($search)) {
            $sources = $sources->where(function ($query) use ($search) {
                $query->where('firstName', 'like', '%' . $search . '%')
                    ->orWhere('lastName', 'like', '%' . $search . '%')
                    ->orWhere('zip', 'like', '%' . $search . '%')
                    ->orWhere('city', 'like', '%' . $search . '%')
                    ->orWhere('state', 'like', '%' . $search . '%')
                    ->orWhere('county', 'like', '%' . $search . '%')
                    ->orWhere('licenseNumber', 'like', '%' . $search . '%')
                    ->orWhereRaw('CONCAT(firstName," ", lastName)  LIKE "%' . $search . '%"');
            });
        }

        $totalDataRecord = $sources;
        // $totalDataRecord = $totalDataRecord->count();
        $totalDataRecord = $totalDataRecord->get()->count();
        $totalFilteredRecord = $totalDataRecord;

        $sources = $sources->skip($start)->take($length)->get();

        $data_val = $sources->map(function ($item) {
            $campaign_names = '';
            if($item->script_record_sent_postcard->isNotEmpty()) {
                $poplar_script_records = $item->script_record_sent_postcard;
                // if($poplar_script_records->poplar_campaign_detail->is)
                if(!empty($item->script_record_sent_postcard->pluck('poplar_campaign_detail')) && $item->script_record_sent_postcard->pluck('poplar_campaign_detail')->isNotEmpty()) {
                    $poplarCampaignNames = $item->script_record_sent_postcard->pluck('poplar_campaign_detail')->pluck('campaign_name');
                    $campaign_names = $poplarCampaignNames->unique()->implode(', ');
                }
            }
            $firstName = explode(' ', $item->firstName);
            $linkedinSearchFirstName = $firstName[0];

            $lastName = explode(' ', $item->lastName);
            $linkedinSearchLastName = $lastName[0];

            $linkedinProfileSearchKey = 'https://www.linkedin.com/search/results/people/?firstName=' . $linkedinSearchFirstName . '&geoUrn=["90000049"]&keywords=accounting OR accountant OR cpa&lastName=' . $linkedinSearchLastName . '&origin=FACETED_SEARCH&sid=qAC';


            /**
             * Poplar Information button with ToolTip 
             */
            if(!empty($campaign_names)) {
                $tooltipTitle = $campaign_names;
            } else {
                $tooltipTitle = 'No Record';
            }
            $poplarDetailInfo = '<button type="button" class="btn btn-secondary py-0 px-1" data-toggle="tooltip" data-html="true" title="'. $tooltipTitle . '">
                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                </button>';

            return [
                'checkbox' => '',
                'id' => $item->id,
                'firstName' => $item->firstName,
                'lastName' => $item->lastName,
                'zip' => $item->zip,
                'address' => $item->address1,
                'city' => $item->city,
                'state' => $item->state,
                'county' => $item->county,
                'licenseNumber' => $item->licenseNumber,
                'licenseExpirationDate' => date('m/d/y', strtotime($item->licence_expiration_date)),
                'licenseIssuanceDate' => date('m/d/y', strtotime($item->licence_issuance_date)),
                'licenseStatus' => $item->licenseStatus,
                'details' => '<a target="_blank" href="' . $item->details . '">Click here </a>',
                'created_at' => date('m/d/y h:i A', strtotime($item->created_at)),
                'bullhorn_candidate_id' => $item->bullhorn_candidate_id ?? '-',
                'bullhorn_contact_id' => $item->bullhorn_contact_id ?? '-',
                'poplar_info' => $poplarDetailInfo,
                'Action' => '<a target="_blank" href="' . htmlentities($linkedinProfileSearchKey) . '"><i class="fab fa-2x fa-linkedin"></i></a>'
            ];
        });

        $get_json_data = array(
            "draw"            => intval($draw_val),
            "recordsTotal"    => intval($totalDataRecord),
            "recordsFiltered" => intval($totalFilteredRecord),
            "data"            => $data_val
        );

        return response()->json($get_json_data);
    }

    public function search(Request $request)
    {

        if ($request->ajax()) {
            $output = '';
            $query = $request->get('query');
            $status = $request->get('status');
            $contact_type = $request->get('contact_type');

            if ($query == '' && $status == '' && $contact_type == '') {
                return;
            }


            $qry = Contact::query();


            $qry->leftJoin('customers', 'customers.id', '=', 'contacts.customer_id')
                ->leftJoin('contact_types', 'contact_types.contact_type', '=', 'contacts.contact_type')
                ->select('contacts.*', 'customers.customer_name', 'customers.customer_number', 'contact_types.contact_desc');



            if ($query != '') {

                $q = $query;

                $qry->where(function ($query) use ($q) {

                    $query->orWhere('contact_name', 'LIKE', '%' . $q . '%')
                        ->orWhere('contact_email', 'LIKE', '%' . $q . '%')
                        ->orWhere('contact_phone', 'LIKE', '%' . $q . '%')
                        //->orWhere('customer_id','LIKE','%'.$q.'%')
                        ->orWhere('customer_name', 'LIKE', '%' . $q . '%')
                        ->orWhere('customer_number', 'LIKE', '%' . $q . '%');
                });
            }

            if ($status != '') {
                $qry->where('contacts.status', $status);
            }

            if ($contact_type != '') {

                $qry->where('contacts.contact_type', $contact_type);
            }

            $contacts = $qry->whereNull('contacts.deleted_at')->paginate(10);
            return view('contact.search', compact('contacts'))->render();
        }
    }


    public function create()
    {

        $contact_types = ContactType::all();
        return view('contact.create', compact('contact_types'));
    }

    public function store(Request $request)
    {

        $rules = [
            'contact_name'      => 'required',
            'contact_email'     => 'required',
            'contact_type'     => 'required',
            'contact_phone' => 'required',
            'address' => 'required',
            'state' => 'required',
            'zipcode' => 'required',
            'status'       => 'required',
        ];

        $message = [

            'contact_name.required'      => 'Contact Name Is Required',
            'contact_email.required'     => 'Contact Email Is Required.',
            'contact_type.required'      => 'Contact Type Is Required',
            'contact_phone.required' => 'Contact Phone Is Required',
            'address.required'      => 'Address Is Required',
            'state.required'      => 'State Is Required',
            'zipcode.required'      => 'Zipcode Is Required',
            'status.required'       => 'Status Is Required.',

        ];

        $this->validate($request, $rules, $message);

        $contact = new Contact();

        $contact->customer_id = 1;
        $contact->contact_name = request('contact_name');
        $contact->contact_title = request('contact_title');
        $contact->contact_email = request('contact_email');
        $contact->contact_phone = request('contact_phone');
        $contact->address = request('address');
        $contact->state = request('state');
        $contact->zipcode = request('zipcode');
        $contact->contact_type = request('contact_type');
        $contact->status = request('status');


        $contact->save();


        $user = auth()->user();
        activity()->performedOn($contact)
            ->causedBy($user)
            ->withProperties(['name' => $user->name, 'email' => $user->email])
            ->tap(function (Activity $activity) use ($contact) {
                $activity->contact_id = $contact->id;
                //$activity->customer_id = $contact->customer_id;
                $activity->new_value = json_encode($contact);
            })
            ->log('Created Contact');



        return redirect()->route('contact')->with('insert', 'Successfully Inserted Contact.');
    }

    public function edit($id)
    {
        $contact = Contact::find($id);
        $customer = '';
        if (!empty($contact)) {
            $customer = Customer::find($contact->customer_id);
        }
        $contact_types = ContactType::all();


        $data = array(
            'contact' => $contact,
            'customer' => $customer,
            'contact_types' => $contact_types,
        );

        return view('contact.edit', $data);
    }

    public function checkUpdateKey($oldData, $newData)
    {
        $result['old_value'] = array_diff($oldData, $newData);
        $user = auth()->user();
        //dd($result);
        unset($result['old_value']['deleted_by']);
        unset($result['old_value']['created_at']);
        unset($result['old_value']['id']);
        $result['old_value']['updated_by'] = "Name: " . $user->name . "<br /> Email: " . $user->email;
        $result['new_value'] = array_diff($newData, $oldData);
        //dd($result);
        unset($result['new_value']['deleted_by']);
        unset($result['new_value']['created_at']);
        unset($result['new_value']['id']);

        return $result;
    }

    public function update(Request $request, $id)
    {

        $rules = [

            'contact_name'      => 'required',
            'contact_email'     => 'required',
            'contact_type'     => 'required',
            'address' => 'required',
            'state' => 'required',
            'contact_phone' => 'required',
            'zipcode' => 'required',
            'status'       => 'required',
        ];

        $message = [

            'contact_name.required'      => 'Contact Name Is Required',
            'contact_email.required'     => 'Contact Email Is Required.',
            'contact_type.required'      => 'Contact Type Is Required',
            'contact_phone.required' => 'Contact Phone Is Required',
            'address.required'      => 'Address Is Required',
            'state.required'      => 'State Is Required',
            'zipcode.required'      => 'Zipcode Is Required',
            'status.required'       => 'Status Is Required.',

        ];

        $this->validate($request, $rules, $message);


        $contact = Contact::find($id);

        //    dump($contact->toArray());
        //      dump($request->all());
        $result = $this->checkUpdateKey($contact->toArray(), $request->all());
        //dd($result);


        $contact->customer_id = 1;
        $contact->contact_name = request('contact_name');
        $contact->contact_title = request('contact_title');
        $contact->contact_email = request('contact_email');
        $contact->contact_phone = request('contact_phone');
        $contact->address = request('address');
        $contact->state = request('state');
        $contact->zipcode = request('zipcode');

        $contact->contact_type = request('contact_type');
        $contact->status = request('status');

        $contact->save();




        $user = auth()->user();
        activity()->performedOn($contact)
            ->causedBy($user)
            ->withProperties(['name' => $user->name, 'email' => $user->email])
            ->tap(function (Activity $activity) use ($contact, $result) {
                $activity->contact_id = $contact->id;
                $activity->old_value = json_encode($result['old_value']);
                $activity->new_value = json_encode($result['new_value']);
            })
            ->log('Updated Contact');

        return redirect()->route('contact_edit', ['id' => $id])->with('update', 'Successfully Updated Contact.');
    }

    public function delete($id)
    {

        // return $id;        
        $user = auth()->user();


        $contact = Contact::where('id', $id)->first();

        $contact->deleted_by = $user->id;
        $contact->is_deleted = 1;
        $contact->save();

        // $contact->delete();

        $user = auth()->user();

        activity()->performedOn($contact)
            ->causedBy($user)

            ->withProperties(['name' => $user->name, 'email' => $user->email])
            ->tap(function (Activity $activity) use ($contact, $user) {
                $activity->contact_id = $contact->id;
                $contact->updated_by = "Name: " . $user->name . "<br /> Email: " . $user->email;
                $activity->old_value = json_encode($contact);
            })
            ->log('Deleted Contact');


        return redirect()->route('contact')->with('delete', 'Successfully Deleted Contact.');
    }

    public function getHistory($contact_id)
    {
        $uploaddoc = UploadDocument::where('contact_id', $contact_id)->get();
        $log_data = DB::table('activity_log')
            ->select('activity_log.*', 'users.name as user_name')
            ->where('contact_id', $contact_id)
            ->leftJoin('users', 'users.id', '=', 'activity_log.causer_id')
            // ->leftJoin('users', 'users.id', '=', 'activity_log.causer_id')
            ->orderBy('id', 'desc')
            ->get();

        return view('contact.history', compact('log_data', 'contact_id', 'uploaddoc'));
    }

    public function transferContactAutoComplete(Request $request)
    {
        $term = trim($request->q);
        if (empty($term)) {
            return \Response::json([]);
        }

        $contacts = Contact::where('contact_name', 'LIKE', $term . '%')
            ->limit(10)
            ->get();
        $formatted_contacts = [];

        foreach ($contacts as $contact) {
            $formatted_sources[] = ['id' => $contact->id, 'text' => $contact->contact_name];
        }

        return \Response::json($formatted_sources);
    }

    // /**
    //  * Get Poplar API data for campaigns 
    //  * 
    //  * @param Request
    //  * @return View
    //  */
    // public function getPoplarCampaigns(Request $request)
    // {
    //     try {
    //         $contactIds = [];
    //         if (!empty($request->selectedRecordObjcts)) {
    //             $contactIds = $request->selectedRecordObjcts;
    //         }
    //         // $data = $this->poplar_services->index();
    //         $data['campaign_lists'] = $this->poplar_services->campaign_list();
    //         $data['script_contact'] = $contactIds;

    //         $poplarDataHtml = View::make('includes.poplar_campaign_list', ["data" => $data])->render();
    //         return response()->json(['success' => 1, 'html' => $poplarDataHtml], 200);
    //     } catch (\Exception $ex) {
    //         return response()->json(['success' => 0, 'message' => $ex->getMessage()], 401);
    //     }

    //     // return response()->json(['success' => 0, 'message' => 'Authentication error. Please login again else User not found.'], 401);
    // }

    // public function sendPoplarPostcardCampaigns(Request $request)
    // {
    //     try {
    //         $input = $request->all();
    //         $script_ids = explode(', ', $input['script_contact_ids']);
    //         // Get all script records of selected checkbox 
    //         $contact_records = ScriptRecord::select('*')
    //             ->whereIn('id', $script_ids)->get();
            
    //         /**
    //          * Chunk Contact data object and submit for the poplar API data process.
    //          * 
    //          */
    //         $now = now();
    //         $contact_records->chunk(20)->each(function ($chunkData) use ($input, $now) {
    //             // Send PostCard Job
    //             ProcessPoplarPostCardSend::dispatch($chunkData, $input);
    //         });

    //         return response()->json(['success' => true, 'message' => 'Postcard Successfully Sent!!!']);
    //     } catch (\Exception $ex) {
    //         \Log::info($ex->getMessage());
    //         return response()->json(['success' => false, 'error_type' => 'controller exception', 'message' => $ex->getMessage()]);
    //     }
    // }
}
