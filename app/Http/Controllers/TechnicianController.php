<?php

namespace App\Http\Controllers;
use Spatie\Activitylog\Models\Activity;
use Illuminate\Http\Request;
use App\Wipe;
use App\UploadDocument;
use App\Technician;
use App\TechnicianType;
use DB;

class TechnicianController extends Controller
{
    public function index()
    {
        $technicians = Technician::orderByRaw('updated_at desc, tech_name asc')->paginate(env('page'));

        return view('technician.index', compact('technicians'));
    }

    public function create()
    {
        $uroles = TechnicianType::all();
        return view('technician.create', compact('uroles'));
    }

    public function store(Request $request)
    {
        $rules = [
            'tech_name'   => 'required|regex:/^[\pL\s\-]+$/u',
            't_firstname' => 'required|regex:/^[\pL\s\-]+$/u',
            't_lastname'  => 'required|regex:/^[\pL\s\-]+$/u',
            'current'     => 'required',
            'type'     => 'required',
        ];

        $message = [
            'tech_name.required'   => 'Technician Name Field Is Required.',
            't_firstname.required' => 'Technician First Name Field Is Required.',
            't_lastname.required'  => 'Technician Last Name Field Is Required.',
            'current.required'     => 'Please Select Technician Status.',
            'type.required'     => 'Please Select Technician Role.',
        ];

        $this->validate($request, $rules, $message);

        $data = [
            'tech_name'   => $request->tech_name,
            't_firstname' => $request->t_firstname,
            't_lastname'  => $request->t_lastname,
            'current'     => $request->current,
            'type'     => $request->type,
        ];

        $add_technician = Technician::create($data);

        $user = auth()->user();
        activity()->performedOn($add_technician)
                ->causedBy($user)
                ->withProperties(['name' => $user->name, 'email' => $user->email])
                ->tap(function(Activity $activity ) use ($add_technician, $data) {
                    $activity->technician_id = $add_technician->id;
                    $activity->new_value = json_encode($data);
                })
                ->log('Created technician');

        return redirect()->route('technicians.index')->with('insert','Successfully Inserted Technician.');
    }

    public function edit($id)
    {   
        $technician = Technician::find($id);
        $uroles = TechnicianType::all();
        return view('technician.edit', compact('technician','uroles'));
    }

    public function checkUpdateKey($oldData, $newData){
        $result['old_value']=array_diff($oldData, $newData);
        unset($result['old_value']['deleted_by']);
        unset($result['old_value']['created_at']);
        unset($result['old_value']['id']);
        $result['new_value']=array_diff($newData, $oldData);
        unset($result['new_value']['deleted_by']);
        unset($result['new_value']['created_at']);
        unset($result['new_value']['id']);
        unset($result['new_value']['_method']);
        
        
        return $result;
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'tech_name'   => 'required',
            't_firstname' => 'required',
            't_lastname'  => 'required',
            'current'     => 'required',
            'type'     => 'required',
        ];

        $message = [
            'tech_name.required'   => 'Technician Name Field Is Required.',
            't_firstname.required' => 'Technician First Name Field Is Required.',
            't_lastname.required'  => 'Technician Last Name Field Is Required.',
            'current.required'     => 'Please Select Technician Status.',
            'type.required'     => 'Please Select Technician Role.',
        ];

        $this->validate($request, $rules, $message);

        $technician = Technician::find($id);
        $technicianData['tech_name'] = $technician->tech_name;
        $technicianData['t_firstname'] = $technician->t_firstname;
        $technicianData['t_lastname'] = $technician->t_lastname;
        $technicianData['current'] = $technician->current;
        $technicianData['type'] = $technician->type;

       $acti =  $this->checkUpdateKey($technicianData, $request->all());

        $technician->tech_name   = $request->tech_name;
        $technician->t_firstname = $request->t_firstname;
        $technician->t_lastname  = $request->t_lastname;
        $technician->current     = $request->current;
        $technician->type     = $request->type;
        $technician->save();
                
        $user = auth()->user();
        activity()->performedOn($technician)
                ->causedBy($user)
               
                ->withProperties(['name' => $user->name, 'email' => $user->email])
                ->tap(function(Activity $activity ) use ($technician, $acti) {
                    $activity->technician_id = $technician->id;
                    $activity->new_value = json_encode($acti['new_value']);
                    $activity->old_value = json_encode($acti['old_value']);
                })
                ->log('Updated Technician');

        return redirect()->route('technicians.index')->with('update','Successfully Updated Technician.');
    }

    public function destroy($id)
    {
        $technician = Technician::find($id);
        $technicianData = $technician;
        $technician->delete();
        $user = auth()->user();
        activity()->performedOn($technicianData)
                ->causedBy($user)
                ->withProperties(['name' => $user->name, 'email' => $user->email])
                ->tap(function(Activity $activity ) use ($technicianData) {
                    $activity->technician_id = $technicianData->id;
                    $activity->old_value = json_encode($technicianData);
                })
                ->log('Deleted technician');

        return redirect()->route('technicians.index')->with('delete','Successfully Deleted Technician.');
    }

    public function search(Request $request){

        if($request->ajax()){
            $output = '';
            $query = $request->get('query');
            $status = $request->get('status');

            if ($query == '' &&  $status == '') {
                return;
            }

            $qry = Technician::query();

            if($status !=''){
                    $qry->where('current',$status);
            }

            if($query != ''){

                $q = $query;
                $qry->where(function ($querys) use ($q) {
                    $querys->OrWhere('tech_name','LIKE','%'.$q.'%')
                        ->orWhere('t_firstname','LIKE','%'.$q.'%')
                        ->orWhere('t_lastname','LIKE','%'.$q.'%');
                        
                });        
                }    
                  $technicians = $qry->paginate(10);
                  return view('technician.search', compact('technicians'))->render();
            

        }
    }

    public function autocomplete_old(Request $request)
    {
        $term = trim($request->q);
        if (empty($term)) {
            return \Response::json([]);
        }

        $technicians = Technician::where('tech_name', 'LIKE', $term.'%' )
                             ->limit(10)
                             ->get();
        $formatted_technicians = [];

        foreach ($technicians as $technician) {
            $formatted_technicians[] = ['id' => $technician->id, 'text' => $technician->tech_name];
        }

        return \Response::json($formatted_technicians);
    }
    public function autocomplete(Request $request)
    {
        

        $technicians = Technician::get();
        $formatted_technicians = [];

        foreach ($technicians as $technician) {
            $formatted_technicians[] = ['id' => $technician->id, 'text' => $technician->tech_name];
        }

        return \Response::json($formatted_technicians);
    }

    public function getHistory($technician_id){ 
        $uploaddoc = UploadDocument::where('technician_id',$technician_id)->get();
        $technician        =  Technician::where('id', $technician_id)->first();
        $wipehistory = Wipe::where('tech', $technician_id)
                            ->orderBy('wipe_date', 'desc')
                            ->get(); 
        return view('technician.history', compact('wipehistory', 'technician', 'uploaddoc'));
    }
}
