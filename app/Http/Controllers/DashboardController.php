<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Machine;
use App\Customer;
use App\Contact;
use App\Source;
use App\ActivityLog;
use App\Technician;
use App\MachLoc;
use App\MailchimpCampain;
use Carbon\Carbon;
use View;
use App\ScriptRecord;
use App\ScriptRecordSentPostcard;
use DateTime;

class DashboardController extends Controller
{
    protected $capGraphPastDays;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->capGraphPastDays = 20;
        // $this->capGraphPastDays = 80;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['all_machine'] = 10;
        $data['shipped_last_month'] = 20;
        $data['all_customer'] = Customer::whereNull('deleted_at')
            ->count();
        $data['last_month_customer'] = Customer::whereNull('deleted_at')
            ->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)
            ->count();
        $data['all_contact'] = Contact::whereNull('deleted_at')
            ->count();
        $data['all_active_contact'] = Contact::whereNull('deleted_at')
            ->where('is_deleted', '=', 0)
            ->count();
        $data['all_technician'] = 12;
        $data['all_active_technician'] = 12;
        $data['all_source'] = 12;
        $data['loaded_last_month'] = 12;
        $data['all_activity'] =      ActivityLog::orderBy('id', 'desc')->get(); //->where('login', NULL)->where('logout', NULL)     

        /** live data  */
        $data['all_script_records'] = ScriptRecord::count();
        $data['last_month_script_records'] = ScriptRecord::whereMonth('created_at', '=', Carbon::now()->subMonth()->month)
            ->count();

        $data['all_bullhorn_contact'] = ScriptRecord::whereNotNull('bullhorn_contact_id')->count();
        $data['last_month_bullhorn_contact'] = ScriptRecord::whereNotNull('bullhorn_contact_id')->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)
            ->count();

        $data['all_bullhorn_candidate'] = ScriptRecord::whereNotNull('bullhorn_candidate_id')->count();
        $data['last_month_bullhorn_candidate'] = ScriptRecord::whereNotNull('bullhorn_candidate_id')->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)
            ->count();

        $data['all_mailchimp_campaign'] = MailchimpCampain::select('mailchimp_campaigns.*')
            ->has('mailchimp_campaign_links')
            ->has('mailchimp_campaign_links.mailchimp_campaign_link_members')
            ->groupBy('mailchimp_campaigns.campaign_id')->get()->count();
        
        $data['total_postcard_sent'] = ScriptRecordSentPostcard::count();
        $data['unique_postcard_sent'] = ScriptRecordSentPostcard::groupBy('campaign_id')->groupBy('script_record_id')->get()->count();

        $data['html'] = View::make('log', ['data' => $data])->render();


        return view('dashboard', ['data' => $data]);
    }


    public function getGraphData()
    {
        try {
            $Customer = $Machine = $Technician = [];

            for ($i = 1; $i <= 6; $i++) {
                $Customer[$i] = Customer::whereNull('deleted_at')->whereMonth('created_at', date('m', strtotime('-' . $i . ' month')))->count();
                $Contact[$i] = Contact::whereNull('deleted_at')->whereMonth('created_at', date('m', strtotime('-' . $i . ' month')))->count();
                $Machine[$i] = Machine::whereNull('deleted_at')->whereMonth('created_at', date('m', strtotime('-' . $i . ' month')))->count();
                $Technician[$i] = Technician::whereNull('deleted_at')->whereMonth('created_at', date('m', strtotime('-' . $i . ' month')))->count();
            }

            $data['all_customer'] = array_sum($Customer);
            $data['all_contact'] = array_sum($Contact);
            $data['all_machine'] = array_sum($Machine);
            $data['all_technician'] = array_sum($Technician);
            //dd($data['all_machine']);
            $months = [];
            for ($i = 6; $i > 0; $i--) {
                $months[] = date('M', strtotime("-$i month"));
            }

            $data['last_six_months'] = $months;
            $data['customer_six_months'] = $Customer;
            $data['contact_six_months'] = $Contact;
            $data['machine_six_months'] = $Machine;

            echo json_encode($data);
        } catch (\Exception $ex) {
            $data['last_six_months'] = 0;
            $data['customer_six_months'] = 0;
            $data['contact_six_months'] = 0;
            $data['machine_six_months'] = 0;
            echo json_encode($data);
        }
    }

    public function getLogFilterData(Request $request)
    {
        if ($request->column != 'all') {
            $data['all_activity'] = ActivityLog::orderBy('id', 'desc')->orWhereNotNull($request->column)->get();
        } else {
            $data['all_activity'] =   ActivityLog::orderBy('id', 'desc')->get();
        }
        $data['html'] = View::make('log', ['data' => $data])->render();
        $data['status'] = "success";
        return json_encode($data);
    }

    /**
     * Get CPA Graph Data 
     */
    public function getCPAAdditionData()
    {
        try {
            // $script_records = ScriptRecord::select(\DB::raw('count(id) as total_records'), \DB::raw('DATE(created_at) as created_date'))->groupBy(\DB::raw('DATE(created_at)'))->get();
            $script_records = ScriptRecord::select('id', \DB::raw('CURDATE() as currentdate'), \DB::raw('DATE_FORMAT(DATE(created_at), "%d/%m/%Y") as display_created_date'), \DB::raw('DATE(created_at) as created_date'))
                ->whereBetween(
                    \DB::raw('DATE(created_at)'),
                    [\DB::raw('CURDATE() - INTERVAL ' . $this->capGraphPastDays . ' DAY'),  \DB::raw('CURDATE()')]
                    // [\DB::raw('CURDATE() - INTERVAL 50 DAY'),  \DB::raw('CURDATE()')]
                )->get();

            $script_records = $script_records->groupBy('display_created_date');
            $dates = $counts = $data = [];

            $endDate = new DateTime();
            $startDate = new DateTime('-' . $this->capGraphPastDays . ' day');

            // foreach ($script_records as $key => $val) {
            //     $dates[$key] = $key;

            //     // $counts[$displayDate] = $count++;
            //     $counts[$key] = !empty($val) ? $val->count() : 0;
            // }

            for ($i = $startDate, $count = 0; $i <= $endDate; $i->modify('+1 day')) {
                $displayDate = $i->format("d/m/Y");
                $dates[$displayDate] = $i->format("d/m/Y");

                $total_records_count = 0;
                if (!empty($script_records[$displayDate])) {
                    $total_records_count = $script_records[$displayDate]->count();
                }

                $counts[$displayDate] = $total_records_count;
            }

            /*for($i = $startDate, $count = 0; $i <= $endDate; $i->modify('+1 day')){
                // echo $i->format("Y-m-d");
                
                // dd($script_records, $script_records->keys('created_date'));
                // $counter = $script_records->where('created_date', $i->format("Y-m-d"));
                // $db = \DB::getQueryLog();
                // dd($db, $counter);
                // if($counter->isNotEmpty()) {
                //     dd($counter);
                // }

                $displayDate = $i->format("d/m/Y");
                $dates[$displayDate] = $i->format("d/m/Y");

                // $counts[$displayDate] = $count++;
                $counts[$displayDate] = !empty($counter->total_records) ? $counter->total_records : 0;
            }
            */

            $data['dates'] = $dates;
            $data['counts'] = $counts;
            echo json_encode($data);
        } catch (\Exception $ex) {
            // dd($ex->getMessage());
            $data['dates'] = 0;
            $data['counts'] = 0;
            echo json_encode($data);
        }
    }

    /**
     * Get Bullhorn Synced and Pending Contact Candidate Informations 
     * 
     * @param Object
     * @return JSON/Object
     */
    public function getBullhornSyncedData(Request $request) {
        try {
            $query = ScriptRecord::get();
            $bullhorn_candidate_id = (clone $query)->where('bullhorn_candidate_id')->count();
            $bullhorn_contact_id = (clone $query)->where('bullhorn_contact_id')->count();

            $pending_bullhorn_candidate_id = (clone $query)->where('bullhorn_candidate_id', null)->count();
            $pending_bullhorn_contact_id = (clone $query)->where('bullhorn_contact_id', null)->count();

            $data['titles'] = ['Synced Candidate', 'Synced Contact', 'Pending to Sync Candidate', 'Pending to Sync Contact'];
            $data['counts'] = [$bullhorn_candidate_id, $bullhorn_contact_id, $pending_bullhorn_candidate_id, $pending_bullhorn_contact_id];
            echo json_encode($data);
        } catch(\Exception $ex) {
            $data['titles'] = ['Synced Candidate', 'Synced Contact', 'Pending to Sync Candidate', 'Pending to Sync Contact'];
            $data['counts'] = [0, 0, 0, 0];
            echo json_encode($data);
        }
    }
}
