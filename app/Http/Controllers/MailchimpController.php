<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MailchimpCampain;
use App\MailchimpCampainLinkMember;
use App\Traits\BullhornProcessingTrait;

class MailchimpController extends Controller
{
    use BullhornProcessingTrait;

    protected $per_page;
    protected $client_contact_entity;

    static $BULLHORN_CLIENT_ID;
    static $BULLHORN_CLIENT_SECRET;
    static $BULLHORN_USER;
    static $BULLHORN_PASS;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->per_page = 10;
        $this->client_contact_entity = env('CLIENT_CONTACT_ENTITY');

        // Static variables for traits
        static::$BULLHORN_CLIENT_ID = env('BULLHORN_CLIENT_ID');
        static::$BULLHORN_CLIENT_SECRET = env('BULLHORN_CLIENT_SECRET');
        static::$BULLHORN_USER = env('BULLHORN_USER');
        static::$BULLHORN_PASS = env('BULLHORN_PASS');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function listing(Request $request)
    {
        // $input = $request->all();

        // \DB::enableQueryLog();
        $records = $data['mailchimp_data'] = MailchimpCampain::select('mailchimp_campaigns.*')
            ->has('mailchimp_campaign_links')
            ->has('mailchimp_campaign_links.mailchimp_campaign_link_members')
            // ->whereHas('mailchimp_campaign_links', function($query2) {
            //     $query2->orderBy('id', 'DESC');
            // })
            // ->groupBy('mailchimp_campaigns.campaign_id')->orderBy('campaign_create_at', 'desc')->get();
            ->groupBy('mailchimp_campaigns.campaign_id')->orderBy('campaign_create_at', 'desc')->paginate($this->per_page);

        // $db = \DB::getQueryLog();
        // dd($db);

        $startRecordNumber = $endRecordNumber = 0;
        if (!empty($records)) {
            if ($records->lastPage() == $records->currentPage()) {
                $endRecordNumber = $records->total();
            } else {
                $endRecordNumber = $records->perPage() * $records->currentPage();
            }

            $startRecordNumber = ($records->perPage() * ($records->currentPage() - 1)) + 1;
        }

        $data['custom_pagination'] = array(
            'startRecordNumber' => $startRecordNumber,
            'endRecordNumber' => $endRecordNumber,
        );

        return view('mailchimp.table', $data);
    }

    public function iframe_listing()
    {
        $data['all_machine'] = 10;
        return view('mailchimp.iframe-table', ['data' => $data]);
    }

    /**
     * Store data and sync data to the Bullhorn
     * @param \Request
     * @return JSON
     * 
     */
    public function syncDataToBullhorn(Request $request)
    {
        try {
            $authCode = $this->getAuthCode(); //echo $authCode;die;
            $auth = $this->doBullhornAuth($authCode); //echo $auth;die;
            $tokens = json_decode($auth); //print '<pre>';print_r($tokens);die;
            $session = $this->doBullhornLogin($tokens->access_token);
            $accessToken = json_decode($session, true);
            // $accessToken['restUrl'] = '123';
            if (isset($accessToken['restUrl']) && $accessToken['restUrl'] != '') {
                // Mailchimp Campaign's Total Links
                $mailchimpData = MailchimpCampain::where('campaign_id', $request->bullHornData)->first();
                $total_links = $mailchimpData->mailchimp_campaign_links->count();
                /*if (isset($request->dataMemberid) && $request->dataMemberid != '') {
                    $mailchimpList = MailchimpCampainLinkMember::select('maichimp_campaign_link_members.*', 'maichimp_campaign_links.*')
                        ->selectRaw('maichimp_campaign_link_members.id as mailchimp_campaign_link_member_id')
                        ->selectRaw(\DB::raw('count(maichimp_campaign_link_members.id) as total_link_clicks'))
                        ->selectRaw(\DB::raw('GROUP_CONCAT(SUBSTRING_INDEX(maichimp_campaign_links.link_url, "?", 1) SEPARATOR " | ") as all_links'))
                        ->where([
                            'maichimp_campaign_link_members.campaign_id' => $request->bullHornData,
                            'maichimp_campaign_link_members.url_id' => $request->eventType,
                            'maichimp_campaign_link_members.id' => $request->dataMemberid
                        ])
                        ->join('maichimp_campaign_links', function ($query) {
                            $query->on('maichimp_campaign_links.link_id', 'maichimp_campaign_link_members.url_id')
                                ->on('maichimp_campaign_links.campaign_id', 'maichimp_campaign_link_members.campaign_id');
                        })
                        ->groupBy('maichimp_campaign_link_members.email_address', 'maichimp_campaign_link_members.campaign_id')
                        ->get();

                    // $sel_query = "SELECT cm.*, cl.*, cm.id as mailchimp_campaign_link_member_id from maichimp_campaign_link_members AS cm, maichimp_campaign_links AS cl WHERE cm.campaign_id = '" . $request['bullHornData'] . "' AND cm.url_id = '" . $request['eventType'] . "' and cm.id='" . $request['dataMemberid'] . "'  and cl.link_id =cm.url_id and cl.campaign_id =cm.campaign_id  ";
                } else if (isset($request->eventType) && $request->eventType != '') {

                    $mailchimpList = MailchimpCampainLinkMember::select('maichimp_campaign_link_members.*', 'maichimp_campaign_links.*')
                        ->selectRaw('maichimp_campaign_link_members.id as mailchimp_campaign_link_member_id')
                        ->selectRaw(\DB::raw('count(maichimp_campaign_link_members.id) as total_link_clicks'))
                        ->selectRaw(\DB::raw('GROUP_CONCAT(SUBSTRING_INDEX(maichimp_campaign_links.link_url, "?", 1) SEPARATOR " | ") as all_links'))
                        ->where([
                            'maichimp_campaign_link_members.campaign_id' => $request->bullHornData,
                            'maichimp_campaign_link_members.url_id' => $request->eventType
                        ])
                        ->join('maichimp_campaign_links', function ($query) {
                            $query->on('maichimp_campaign_links.link_id', 'maichimp_campaign_link_members.url_id')
                                ->on('maichimp_campaign_links.campaign_id', 'maichimp_campaign_link_members.campaign_id');
                        })
                        ->groupBy('maichimp_campaign_link_members.email_address', 'maichimp_campaign_link_members.campaign_id')
                        ->get();

                    // $sel_query = "SELECT cm.*, cl.*, cm.id as mailchimp_campaign_link_member_id FROM maichimp_campaign_link_members AS cm, maichimp_campaign_links AS cl WHERE cm.campaign_id = '" . $request['bullHornData'] . "' AND cm.url_id = '" . $request['eventType'] . "' and cl.link_id =cm.url_id and cl.campaign_id =cm.campaign_id ;";
                } else {*/
                // Logic for sync all data by Campaign.....
                $mailchimpList = MailchimpCampainLinkMember::select('maichimp_campaign_link_members.*', 'maichimp_campaign_links.*')
                    ->selectRaw('maichimp_campaign_link_members.id as mailchimp_campaign_link_member_id')
                    ->selectRaw(\DB::raw('count(maichimp_campaign_link_members.id) as total_link_clicks'))
                    ->selectRaw(\DB::raw('GROUP_CONCAT(SUBSTRING_INDEX(maichimp_campaign_links.link_url, "?", 1) SEPARATOR " | ") as all_links'))
                    ->where([
                        'maichimp_campaign_link_members.campaign_id' => $request->bullHornData
                    ])
                    ->join('maichimp_campaign_links', function ($query) {
                        $query->on('maichimp_campaign_links.link_id', 'maichimp_campaign_link_members.url_id')
                            ->on('maichimp_campaign_links.campaign_id', 'maichimp_campaign_link_members.campaign_id');
                    })
                    ->groupBy('maichimp_campaign_link_members.email_address', 'maichimp_campaign_link_members.campaign_id')
                    ->get();

                // Logic for sync all data by Campaign.....
                // $sel_query = "SELECT cm.*, cl.*, cm.id as mailchimp_campaign_link_member_id FROM maichimp_campaign_link_members AS cm, maichimp_campaign_links AS cl WHERE cm.campaign_id = '" . $request['bullHornData'] . "'  and cl.link_id =cm.url_id and cl.campaign_id =cm.campaign_id ;";
                //}
                // dd($mailchimpList[0]->total_link_clicks, $mailchimpList[0]->mailchimp_campaign_link->count());

                if ($mailchimpList->isNotEmpty()) {
                    $mailchimpList->each(function ($item, $key) use ($request, $accessToken, $total_links) {
                        $contactId = '';
                        $sync_bullhorn_status = '3';    // Spam data to sync

                        /** If total click = user's total click, then email is span and skip this user */
                        if ($total_links != $item->total_link_clicks) {
                            // CLIENT_CONTACT_ENTITY = '';
                            $responseData = $this->getClientContactUserDataByEmailApi($this->client_contact_entity, $accessToken['restUrl'], $item->email_address, $accessToken['BhRestToken']);
                            $sync_bullhorn_status = '2';    // Failed to sync

                            if ($responseData['total'] != 0) {
                                $contactId = $responseData['data'][0]['id'];
                                $all_clicked_links = $item->all_links;

                                $campaign_sent_date_milliseconds = (!empty($request['campainSentDate']) ? strtotime($request['campainSentDate']) * 1000 : '');

                                $comments = $request['mailchimpSubjectData'] . " + " . $campaign_sent_date_milliseconds . " + " . $item->email_address . " + " . $all_clicked_links . " + " . $item->clicks;

                                // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
                                $postData = array(
                                    "action" => "MailChimp Campaign",
                                    "personReference" => array('id' => $contactId),
                                    "dateAdded" => $campaign_sent_date_milliseconds,
                                    "comments" => $comments
                                );
                                $ch = curl_init();

                                curl_setopt($ch, CURLOPT_URL, $accessToken['restUrl'] . '/entity/Note?BhRestToken=' . $accessToken['BhRestToken']);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));
                                $headers = array();
                                $headers[] = 'Content-Type: application/json';
                                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                                $curl_result = curl_exec($ch);

                                if (curl_errno($ch)) {
                                    echo 'Error:' . curl_error($ch);
                                    \Log::info(['Error:', curl_error($ch)]);
                                } else {
                                    // Udate sync data to the mailchimp database and also insert contact id
                                    $sync_bullhorn_status = '1';    // Sync Success
                                }
                                curl_close($ch);

                                // $responseData2 = json_decode($curl_result, true);

                                // // Udate sync data to the mailchimp database and also insert contact id
                                // $sync_bullhorn_status = '1';    // Sync Success
                            }
                        }

                        // Update Mailchimp Member's link click table for bullhorn sync data if any
                        MailchimpCampainLinkMember::where(
                            [
                                // 'id' => $item->mailchimp_campaign_link_member_id     // Sync Single data which found.
                                // Following lines are for syncing data of the member by email address and campaign_id, because click on sync campaign will be also sync the data for all links in a campaign for that email address.
                                'email_address' => $item->email_address,
                                'campaign_id' => $item->campaign_id
                            ]
                        )->update([
                            'sync_to_bullhorn' => $sync_bullhorn_status,
                            'bullhorn_contact_id' => $contactId
                        ]);
                    });
                    return response()->json(['status' => true, 'data' => 'success', 'text' => 'Synced']);
                }
                return response()->json(['status' => false, 'data' => 'Failed - no record found in DB', 'text' => 'Failed - no record found in DB']);
            }
            return response()->json(['status' => false, 'data' => 'Failed']);
        } catch (\Exception $ex) {
            return response()->json(['status' => false, 'data' => $ex->getMessage()]);
        }
    }
}
