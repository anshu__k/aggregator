<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use Auth;
use DB;

class UserController extends Controller
{
    public function index()
    {
        $users = User::orderBy('users.id', 'desc')
        ->paginate(env('page'));

        

        return view('user.index', compact('users'));
    }

    public function create()
    {
        $uroles = Role::all();

        return view('user.create', compact('uroles'));
    }

    public function store(Request $request)
    {
        $rules = [
            
            'name'     => 'required',
            'phone_no' => 'required|digits:10',
            'role'     => 'required',
            'email'    => 'required|email|unique:users,email',
            'password' => 'required',
        ];

        $message = [
            'name.required'     => 'User Name Field Is Required.',
            'phone_no.required' => 'User Phone Number Field Is Required.',
            'phone_no.digits'   => 'Please Enter Only 10 Digits.',
            'email.required'    => 'User Email Field Is Required.',
            'email.email'       => 'Please Enter Valid Email Address.',
            'email.unique'      => 'This Email All Ready In Used, Please Enter Another Email.',
            'role.required'     => 'Please Select User Role.',
            'email.password'    => 'User Password Field Is Required.',
        ];

        $this->validate($request, $rules, $message);

        $data = [
            'title'    => $request->title,
            'name'     => $request->name,
            'email'    => $request->email,
            'phone_no' => $request->phone_no,
            'role'     => $request->role,
            'password' => \Hash::make($request->password),
        ];

        $add_user = User::create($data);

        $user = auth()->user();
        activity()->performedOn($add_user)
                ->causedBy($user)
                ->withProperties(['name' => $user->name, 'email' => $user->email])
                ->log('Created user');

        return redirect()->route('users.index')->with('insert','Successfully Inserted User.');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $user = User::find($id);
        $uroles = Role::all();
        return view('user.edit', compact('user','uroles'));
    }

    public function update(Request $request, $id)
    {
       $rules = [
            
            'name'     => 'required',
            'phone_no' => 'required|digits:10',
            'email'    => 'required|email|unique:users,email,'.$id,
            'role'     => 'required',
        ];

        $message = [
            'name.required'     => 'User Name Field Is Required.',
            'phone_no.required' => 'User Phone Number Field Is Required.',
            'phone_no.digits'   => 'Please Enter Only 10 Digits.',
            'email.required'    => 'User Email Field Is Required.',
            'email.email'       => 'Please Enter Valid Email Address.',
            'email.unique'      => 'This Email All Ready In Used, Please Enter Another Email.',
            'email.password'    => 'User Password Field Is Required.',
            'role.required'     => 'Please Select User Role.',
            // 'phone_no.max' => 'Please Enter Only 10 Digits.',
        ];

        $this->validate($request, $rules, $message);

        $user = User::find($id);

        if(!empty($request->password))
        {
            $user->password = \Hash::make($request->password);
        }

        $user->title    = $request->title;
        $user->name     = $request->name;
        $user->email    = $request->email;
        $user->role     = $request->role;
        $user->phone_no = $request->phone_no;
        $user->status   = $request->status;
        $user->save();

        $users = auth()->user();
        activity()->performedOn($user)
                ->causedBy($users)
                ->withProperties(['name' => $user->name, 'email' => $user->email])
                ->log('Updated user');

        return redirect()->route('users.index')->with('update','Successfully Updated User.');
    }

    public function destroy($id)
    {
        $user = User::find($id)->delete();

        $users = auth()->user();
        activity()->performedOn($user)
                ->causedBy($users)
                ->withProperties(['name' => $user->name, 'email' => $user->email])
                ->log('Deleted user');

        return redirect()->route('users.index')->with('delete','Successfully Deleted User.');
    }

    public function changeUserStatus(Request $request)
    {
        $user = User::find($request->user_id);
        $user->status = $request->status;
        $user->save();
  
        return response()->json(['success'=>'User status change successfully.']);
    }
}
