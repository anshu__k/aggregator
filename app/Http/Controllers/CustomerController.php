<?php

namespace App\Http\Controllers;

use Spatie\Activitylog\Models\Activity;
use Illuminate\Http\Request;
use \App\Customer;
use App\MachLoc;
use App\Contact;
use DB;
use App\State;
use App\Mstats; 
use App\SourceLoc;
use App\UploadDocument;


class CustomerController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       $this->middleware('auth');
    }


     /**
     * Show the customer page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   

        // // $customers = Customer::all();
        // $customers = Customer::paginate(10);
        // // $customers = Customer::paginate(env('page'));

        $customers = DB::table('customers')
                        ->whereNull('customers.deleted_at')
                        ->orderByRaw('updated_at desc, customer_name asc')
                        ->paginate(10);
        $data = array('customers'=>$customers);

        return view('customer.table',$data);


    }


    public function search(Request $request, Customer $customer){

        if($request->ajax()){
            $output = '';
                $customer = $customer->newQuery();

                
                if (null !==$request->get('status')) {
                    $stt=$request->get('status');
                    $customer->where('status',$stt);
                }
                if (null !==$request->get('state')) {
                    $st = $request->get('state');
                    $customer->where('state',$st);
                }

                if (null !==$request->get('query')) {
                     $q = $request->get('query');
                        
                       $customer->where(function ($query) use ($q) {
                        $query->OrWhere('customer_name','LIKE','%'.$q.'%')
                        ->OrWhere('customer_number','LIKE','%'.$q.'%')
                        ->OrWhere('city','LIKE','%'.$q.'%')
                        ->OrWhere('zipcode','LIKE','%'.$q.'%');
                       });

                }
                  $customers = $customer
                                    ->whereNull('customers.deleted_at')
                                    ->orderByRaw('updated_at desc, customer_name asc')
                                    ->paginate(10);
                  return view('customer.search', compact('customers'))->render();

        }
    }


    public function create(){

        return view('customer.create');

    }

    public function store(Request $request){


        $rules = [
            'customer_number'   =>      'required',
            'customer_name'     =>      'required',
            'address'           =>      'required',
            'city'              =>      'required', 
            'state'             =>      'required', 
            'zipcode'           =>      'required|numeric|min:6', 
            'country'           =>      'required',   
        ];

        $message = [
            'customer_number.required'     =>  'Customer Number Is Required.',
            'customer_name.required'       =>  'Customer Name Is Required.',
            'address.required'             =>  'Address Is Required',
            'city.required'                =>  'City Is Required.',
            'state.required'               =>  'State Is Required.',
            'zipcode.required'             =>  'Zipcode Is Required.',
            'country.required'             =>  'Country Is Required.'
 
        ];

        $this->validate($request, $rules, $message);



        $customer = new Customer();

        $customer->customer_number = request('customer_number');
        //$customer->filing_name = request('filing_name');
        $customer->customer_name = request('customer_name');
        $customer->address = request('address');
        $customer->city = request('city');
        $customer->state = request('state');
        $customer->zipcode = request('zipcode');
        $customer->country = request('country');
        // $customer->wipetest_inhouse = request('wipetest_inhouse');
        $customer->wipetest_frequency = request('wipetest_frequency');
        $customer->notes = request('notes');
        $customer->save();

        $user = auth()->user();
        activity()->performedOn($customer)
                ->causedBy($user)
               
                ->withProperties(['name' => $user->name, 'email' => $user->email])
                ->tap(function(Activity $activity ) use ($customer) {
                    $activity->customer_id = $customer->id;
                    $activity->new_value = json_encode($customer);
                })
                ->log('Created Customer');

        // return request()->all();

        return redirect('customer');

    }

    public function autocomplete(Request $request)
    {
        $term = trim($request->q);
        if (empty($term)) {
            return \Response::json([]);
        }

        $customers = Customer::where('customer_number', 'LIKE', $term.'%' )
                             ->limit(10)
                             ->get();
        $formatted_customers = [];

        foreach ($customers as $customer) {
            $formatted_customers[] = ['id' => $customer->id, 'text' => $customer->customer_number];
        }

        return \Response::json($formatted_customers);
    }

    public function filterMachineStatus(Request $request){ 
        $output = '';
        $mach_status = $request->mach_status;
        $customer_id = $request->customer_id;

        $customer = Customer::find($customer_id);

        if($mach_status != 'all'){
            $where['customer_num'] = $customer->customer_number;
            $where['mach_status'] = $mach_status;
            $where['cur_mach_loc'] = 1;
            
            $machines = MachLoc::where($where)
                    
                    ->select(DB::raw('count(*) as machine_count, machine_id, customer_id, mach_status, ship_date'))
                    ->Join('m_stats', 'm_stats.m_status', '=', 'mach_loc.mach_status')
                    ->groupBy('machine_id')
                    ->orderBy('ship_date','desc')
                    ->limit(10)
                    ->get();

        }else{
            $where['customer_num'] = $customer->customer_number;
            $where['cur_mach_loc'] = 1;
        
        $machines = MachLoc::where($where)
                    ->where('mach_status','!=','aa')
                    ->select(DB::raw('count(*) as machine_count, machine_id, customer_id, mach_status, ship_date'))
                    ->Join('m_stats', 'm_stats.m_status', '=', 'mach_loc.mach_status')
                    ->groupBy('machine_id')
                    ->orderBy('ship_date','desc')
                    ->limit(10)
                    ->get(); 
                    //dd($machines);    
        }

        $trs = [];
        foreach ($machines as $key => $machine) {
            $machine_num = $machine->machine != null ? $machine->machine->mach_number : $machine->machine_id;
            $machine_id = $machine->machine_id;
            $customer_id = $machine->customer_id;
            $trs[$key] = $this->duplicateMachine($customer_id, $machine_id, $machine_num);
        }
        $total_row = $machines->count();
        if($total_row > 0){

            $i = 1;
            foreach($machines as $key => $machine){ 
                $output .= '
                <tr>
                    <td>'.@$machine->machine->mach_number.'</td>
                    <td>'.@$machine->machine->mach_model.'</td>
                    
                    <td>'.date('d M Y', strtotime($machine->ship_date)).'</td>
                    <td>'.'</td>
                    <td>'.$machine->machine_status['mstats_desc'].'</td>               
                </tr>';
                 $output  .= $trs[$key]['table_data'];
                 $output .= $trs[$key]['source_table_data'];
                $i++;
            }
        }else{
            $output = '
            <tr>
            <td class="text-center" colspan="6"><p>Machine Not Found.</p></td>
            </tr>
            ';
        }
            $data = array(
                'table_data'  => $output,
                'total_data'  => $total_row
            );

        echo json_encode($data);               
    }

    public function view($id)
    {
        $customer = Customer::find($id); 
        $uploaddoc = UploadDocument::where('customer_id',$id)->get(); 
        $customer_id = $id;
        //$machine_status = Mstats::all(); 
        // $machines = MachLoc::where('customer_id', $customer->id)->paginate(env('page'));
        $machines = MachLoc::where('customer_num', $customer->customer_number)
                        ->where('cur_mach_loc',1)
                        ->where('mach_status','aa')
                        ->select(DB::raw('count(*) as machine_count, machine_id, customer_id, mach_status, ship_date'))
                        ->groupBy('machine_id')
                        ->orderBy('ship_date','desc')
                        ->paginate(env('page'), ['*'], 'machine'); 

        $trs = [];
        foreach ($machines as $key => $machine) {
            $machine_num = $machine->machine != null ? $machine->machine->mach_number : $machine->machine_id;
            $machine_id = $machine->machine_id;
            $customer_id = $machine->customer_id;
            $trs[$key] = $this->duplicateMachine($customer_id, $machine_id, $machine_num);
        }                
        // $contacts = Contact::where('customer_id', $customer->id)->paginate(env('page'));
        $contacts = Contact::where('customer_id', $customer->id)
        ->leftJoin('contact_types', 'contact_types.contact_type', '=', 'contacts.contact_type')
        ->paginate(env('page'), ['*'], 'contact');

        // $logs = Activity::where('causer_id', $customer->id)->paginate(env('page'));
        $logs = Activity::where('causer_id', $customer->id)->paginate(env('page'), ['*'], 'log');

   
        $log_data = DB::table('activity_log')
                    ->select('activity_log.*', 'users.name as user_name')
                    ->where('customer_id',$id)
                    ->leftJoin('users', 'users.id', '=', 'activity_log.causer_id')
                    // ->leftJoin('users', 'users.id', '=', 'activity_log.causer_id')
                    ->orderBy('id','desc')
                    ->get();

        
 

        return view('customer.view', compact('customer', 'customer_id', 'uploaddoc', 'machines', 'contacts', 'logs','log_data', 'trs'));
    }

    public function main_search(Request $request){

        if($request->ajax()){
            $output = '';
            $query = $request->get('query');

            if ($query == '') {
                return;
            }

            if($query != ''){

                $q = $query;
                $customers = Customer::where('customer_name','LIKE','%'.$q.'%')
                        ->limit(10)
                        ->get();
            }
            $total_row = $customers->count();
            if($total_row > 0){

                $i = 1;
                foreach($customers as $key => $customer){

                    // if ($customer->wipetest_inhouse == 1) {
                    //     $wipetest_inhouse = 'Themselves';
                    // }else{
                    //     $wipetest_inhouse = 'Filtec';
                    // }

                    $output .= '<a href="'.url('customer/view', $customer).'">
                                    <input type="text" style="cursor:pointer;" readonly value="'.$customer->customer_name.'">
                                </a>';
                    $i++;
                }
            }else{
                $output = '
                        <a href="#">
                            <input type="text" style="cursor:pointer;" readonly value="Customer Not Found.">
                        </a>';
            }
            $data = array(
                'customer_data'  => $output,
                'total_data'  => $total_row
            );
    
            echo json_encode($data);
        }
    }

    public function state_autocomplete(Request $request)
    {
        $term = trim($request->q);
        if (empty($term)) {
            return \Response::json([]);
        }

        $states = State::where('statename', 'LIKE', $term.'%' )
                             ->limit(10)
                             ->get();
        $formatted_states = [];

        foreach ($states as $state) {
            $formatted_states[] = ['id' => $state->state, 'text' => $state->statename];
        }

        return \Response::json($formatted_states);
    }



    public function edit($id)
    {   

        $customer = Customer::find($id);
        $data = array(
             'customer' => $customer
            );

        return view('customer.edit',$data);
    }

    public function checkUpdateKey($oldData, $newData){
        $result['old_value']=array_diff($oldData, $newData);
        unset($result['old_value']['deleted_by']);
        unset($result['old_value']['created_at']);
        unset($result['old_value']['id']);
        $result['new_value']=array_diff($newData, $oldData);
        unset($result['new_value']['deleted_by']);
        unset($result['new_value']['created_at']);
        unset($result['new_value']['id']);
        
        return $result;
    }

    public function update(Request $request,$id){

        $rules = [
            'customer_number'   =>      'required',
            'customer_name'     =>      'required',
            'address'           =>      'required',
            'city'              =>      'required', 
            'state'             =>      'required', 
            'zipcode'           =>      'required|numeric|min:6', 
            'country'           =>      'required',   
        ];

        $message = [
            'customer_number.required'     =>  'Customer Number Is Required.',
            'customer_name.required'       =>  'Customer Name Is Required.',
            'address.required'             =>  'Address Is Required',
            'city.required'                =>  'City Is Required.',
            'state.required'               =>  'State Is Required.',
            'zipcode.required'             =>  'Zipcode Is Required.',
            'country.required'             =>  'Country Is Required.'
 
        ];

        $this->validate($request, $rules, $message);

        $customer = Customer::find($id);
        $result = $this->checkUpdateKey($customer->toArray(), $request->all());
        $customer->customer_number = request('customer_number');
        $customer->customer_name = request('customer_name');
        $customer->address = request('address');
        $customer->city = request('city');
        $customer->state = request('state');
        $customer->zipcode = request('zipcode');
        $customer->country = request('country');
        //$customer->wipetest_inhouse = request('wipetest_inhouse');
        $customer->wipetest_frequency = request('wipetest_frequency');
        $customer->notes = request('notes');
        //$customer->is_deleted = request('status');
        $customer->status = request('status');
//dd($result);
        
        $customer->save();

        if($result){
            $user = auth()->user();
                activity()->performedOn($customer)
                        ->causedBy($user)
                        
                        ->withProperties(['name' => $user->name, 'email' => $user->email])
                        ->tap(function(Activity $activity ) use ($customer, $result) {
                            $activity->customer_id = $customer->id;
                            $activity->old_value = json_encode($result['old_value']);
                            $activity->new_value = json_encode($result['new_value']);
                        })
                        ->log('Updated Customer');
        }

        return redirect()->route('customer_edit', ['id' => $id])->with('update','Successfully Updated Customer.');

    }

    public function delete($id){    
        $user = auth()->user();

        $customer = Customer::where('id',$id)->first();

        $customer->deleted_by = $user->id;
        $customer->is_deleted = 1;
        $customer->save();

        // $customer->delete();
        $user = auth()->user();
        activity()->performedOn($customer)
                ->causedBy($user)
               
                ->withProperties(['name' => $user->name, 'email' => $user->email])
                ->tap(function(Activity $activity ) use ($id, $customer) {
                    $activity->customer_id = $id;
                    $activity->old_value = json_encode($customer);
                })
                ->log('Deleted Customer');

        return redirect()->route('customer')->with('delete','Successfully Deleted Customer.');
    }


    public function duplicateMachine($customer_id, $machine_id, $machine_num){
            $output = '';
            $source_output = '';

            // $machines = MachLoc::where('customer_id', $customer_id)
            //             ->where('machine_id', $machine_id)
            //             ->orderBy('ship_date')
            //             ->paginate(env('page'), ['*'], 'machine');

            // $total_row = $machines->count();

            // if($total_row > 0){

            //     $i = 1;

            //     $output .= ' <tr class="show_data">
            //                             <td colspan="1"></td>
            //                             <td><b>Machine Number</b></td>
            //                             <td><b>Machine Model</b></td>
            //                             <td><b>Ship Date</b></td>
            //                             <td></td>
            //                         </tr>';

            //     foreach($machines as $key => $machine){

            //         $output .= '
            //         <tr class="show_data">
            //             <td colspan="2"></td>                   
            //             <td>'.$machine->machine->mach_number.'</td>
            //             <td>'.$machine->machine->mach_model.'</td> 
            //             <td>'.date('d M Y', strtotime($machine->ship_date)).'</td> 
            //             <td></td>                   
            //         </tr>
            //         ';
            //         $i++;
            //     }
            // }else{
            //     $output = '
            //     <tr class="show_data">
            //         <td colspan="6" class="text-center">
            //             <p>Machine Not Found.</p>
            //         </td> 
            //     </tr>
            //     ';
            // }

            $sources = SourceLoc::where('mach_num', $machine_num)->get();

            $source_row = $sources->count();

            // print_r($source_row);
            // die();

            if($source_row > 0){

                $i = 1;
                $source_output .= ' <tr class="show_data">
                                        <td colspan="1"></td>
                                        <td><b>Source</b></td>
                                        <td><b>Source Model</b></td>
                                        <td><b>Installation Date</b></td>
                                        <td><b>Current Loc</b></td>
                                        <td></td>
                                    </tr>';
                foreach($sources as $key => $source){
                    
                    $currloc= ($source->currentloc==1)?'Active':'Inactive';
                    
                    $source_output .= '
                    <tr class="show_data">
                        <td colspan="1"></td>
                        <td>'.$source->source_num.'</td>
                        <td>'.$source->s_mod_num.'</td> 
                        <td>'.date('d M Y', strtotime($source->inst_date)).'</td> 
                        <td>'.$currloc.'</td> 
                        <td></td>
                    </tr>
                    ';
                    $i++;
                }
            }else{
                $source_output = '
                <tr class="show_data">
                    <td colspan="6" class="text-center">
                        <p>Source Not Found.</p>
                    </td> 
                </tr>
                ';
            }
            
            


            $data = array(
                'table_data'  => $output,
                'source_table_data'  => $source_output,
              //  'total_data'  => $total_row
            );

            return $data;
    }

}
