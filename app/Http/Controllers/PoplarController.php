<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use App\ScriptRecord;
use App\PoplarCampaign;
use App\Services\PoplarAPIService;
use App\Jobs\ProcessPoplarPostCardSend;
use App\Traits\BullhornProcessingTrait;

class PoplarController extends Controller
{
    use BullhornProcessingTrait;

    static $BULLHORN_CLIENT_ID;
    static $BULLHORN_CLIENT_SECRET;
    static $BULLHORN_USER;
    static $BULLHORN_PASS;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PoplarAPIService $poplarAPIService)
    {
        // $this->middleware('auth');
        $this->poplar_services = $poplarAPIService;

        static::$BULLHORN_CLIENT_ID = env('BULLHORN_CLIENT_ID');
        static::$BULLHORN_CLIENT_SECRET = env('BULLHORN_CLIENT_SECRET');
        static::$BULLHORN_USER = env('BULLHORN_USER');
        static::$BULLHORN_PASS = env('BULLHORN_PASS');
    }

    public function index()
    {
    }

    /**
     * Dump Campaign through Poplar API to local database
     * 
     * @return JSON
     */
    public function dump_poplar_campaigns()
    {
        try {
            $campaign_lists = $this->poplar_services->campaign_list();
            if (!empty($campaign_lists)) {
                foreach ($campaign_lists as $key => $item) {
                    PoplarCampaign::updateOrCreate(
                        ['campaign_id' => $item->id],
                        [
                            'campaign_id' => $item->id,
                            'campaign_name' => $item->name
                        ]
                    );
                }
                return response()->json(['success' => 'true', 'message' => 'All Campaigns Dump to Local Database!!!']);
            }
            return response()->json(['success' => 'true', 'message' => 'No Campaign Found!!!']);
        } catch (\Exception $ex) {
            return response()->json(['success' => 'true', 'message' => $ex->getMessage()]);
        }
    }

    /**
     * Get Poplar API data for campaigns 
     * 
     * @param Request
     * @return View
     */
    public function getPoplarCampaigns(Request $request)
    {
        try {
            $contactIds = [];
            if (!empty($request->selectedRecordObjcts)) {
                $contactIds = $request->selectedRecordObjcts;
            }
            // $data['campaign_lists'] = $this->poplar_services->campaign_list();
            $data['campaign_lists'] = PoplarCampaign::whereStatus('1')->get();
            $data['script_contact'] = $contactIds;

            $poplarDataHtml = View::make('includes.poplar_campaign_list', ["data" => $data])->render();
            return response()->json(['success' => 1, 'html' => $poplarDataHtml], 200);
        } catch (\Exception $ex) {
            return response()->json(['success' => 0, 'message' => $ex->getMessage()], 401);
        }
        // return response()->json(['success' => 0, 'message' => 'Authentication error. Please login again else User not found.'], 401);
    }

    /**
     * Submit Postcard mailing to poplar 
     * 
     * @param Request
     * @return JSON
     */
    public function sendPoplarPostcardCampaigns(Request $request)
    {
        try {
            $input = $request->all();
            $script_ids = explode(', ', $input['script_contact_ids']);
            // Get all script records of selected checkbox 
            $contact_records = ScriptRecord::select('*')
                ->whereIn('id', $script_ids)->get();

            /**
             * Chunk Contact data object and submit for the poplar API data process.
             * 
             */
            $now = now();
            $contact_records->chunk(20)->each(function ($chunkData) use ($input, $now) {
                // Send PostCard Job
                ProcessPoplarPostCardSend::dispatch($chunkData, $input);
            });

            return response()->json(['success' => true, 'message' => 'Postcard Successfully Sent!!!']);
        } catch (\Exception $ex) {
            \Log::info($ex->getMessage());
            return response()->json(['success' => false, 'error_type' => 'controller exception', 'message' => $ex->getMessage()]);
        }
    }

    /**
     * IFrame list of the contact data based on bullhorn ID
     * 
     * @param String
     * @return View
     */
    // public function getListOfContactsByBullhornId($bullhorn_id) {
    public function getListOfContactsByBullhornId(Request $request) {
        try {
            $request_data = $request->all();
            // if(!empty($bullhorn_id)) {
            if(!empty($request_data)) {
                /**
                 * Data from the Bullhorn - START
                 */
                // $authCode = $this->getAuthCode(); //echo $authCode;die;
                // $auth = $this->doBullhornAuth($authCode); //echo $auth;die;
                // $tokens = json_decode($auth); //print '<pre>';print_r($tokens);die;
                // $session = $this->doBullhornLogin($tokens->access_token);
                // $accessToken = json_decode($session, true);
                /** 
                 * Data from the Bullhorn - END 
                 */
                $bullhorn_id = $request_data['EntityID'];
                if($request_data['EntityType'] == 'ClientContact' || $request_data['EntityType'] == 'Client Contact') {
                    // Search Data by client Contacts ID
                    $scriptRecord = ScriptRecord::where('bullhorn_contact_id', $bullhorn_id)->first();
                } else {
                    // Search Data by Candidate ID
                    $scriptRecord = ScriptRecord::where('bullhorn_candidate_id', $bullhorn_id)->first();
                }
                
                $data = array(
                    'request_data' => $request->all()
                );
                $data['bullhorn_id'] = $bullhorn_id;
                if(!empty($scriptRecord)) {
                    $data['script_record'] = $scriptRecord;
                    $data['campaign_lists'] = PoplarCampaign::whereStatus('1')->get();
                    // $data['contacts_sent_campaign_lists'] = $scriptRecord->script_record_sent_postcard->pluck('campaign_id');
                    // dd($data, $scriptRecord->script_record_sent_postcard);
                }

                return view('poplar.contact_by_bullhorn', $data);
            }
        } catch(\Exception $ex) {
            abort('500');
            dd($ex->getMessage());

        }

    }

    /**
     * Send Postcard to the contact clicked by bullhorn data 
     * 
     * @param Request
     * @return json
     */
    public function sendPostcardToBUllhornContact(Request $request) {
        try {
            $response = $this->sendPoplarPostcardCampaigns($request);
            return $response;
            // $input = $request->all();
            // $script_ids = explode(', ', $input['script_contact_ids']);
            // // Get all script records of selected checkbox 
            // $contact_records = ScriptRecord::select('*')
            //     ->whereIn('id', $script_ids)->first();

            // /**
            //  * Chunk Contact data object and submit for the poplar API data process.
            //  * 
            //  */
            // $now = now();
            // $contact_records->chunk(20)->each(function ($chunkData) use ($input, $now) {
            //     // Send PostCard Job
            //     ProcessPoplarPostCardSend::dispatch($chunkData, $input);
            // });

            // return response()->json(['success' => true, 'message' => 'Postcard Successfully Sent!!!']);
        } catch (\Exception $ex) {
            \Log::info($ex->getMessage());
            return response()->json(['success' => false, 'error_type' => 'controller exception', 'message' => $ex->getMessage()]);
        }
    }
}
