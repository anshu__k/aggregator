<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Wipe;

class CustomerReportController extends Controller
{
    public function index()
    {
        $wipes = Wipe::paginate(env('page')); 
    	return view('customer_report.index', compact('wipes'));
    }

    // public function create()
    // {
    // 	$source_mods = SModList::all();
    // 	$ship_methods = SStats::all();

    // 	return view('pre_assign_source.create', compact('source_mods', 'ship_methods'));
    // }

    // public function store(Request $request)
    // {
    //     $rules = [
    //         'source_num'         => 'required',
    //         'machine_num'        => 'required',
    //         'source_model'       => 'required',
    //         'source_ship_method' => 'required',
    //     ];

    //     $message = [
    //         'source_num.required'         => 'Source Being Issued Field Is Required.',
    //         'machine_num.required'        => 'Machine Assigning Field Is Required.',
    //         'source_model.required'       => 'Please Select Source Model.',
    //         'source_ship_method.required' => 'Please Select Source Ship Method.',
    //     ];

    //     $this->validate($request, $rules, $message);

    //     $data = [
    //         'source_num' => $request->source_number,
    //         'mach_num'   => $request->machine_num,
    //         's_mod_num'  => $request->source_model,
    //         's_status'   => $request->source_ship_method,
    //     ];

    //     $add_sourceloc = SourceLoc::updateOrCreate(['source_num' => $request->source_number,
    //         'mach_num' => $request->machine_num], $data);

    //     $user = auth()->user();
    //     activity()->performedOn($add_sourceloc)
    //             ->causedBy($user)
               
    //             ->withProperties(['name' => $user->name, 'email' => $user->email,'machine_num'=>$data['mach_num']])
    //             // ->tap(function(Activity $activity ) use ($customer) {
    //             //     $activity->customer_id = $customer->id;
    //             // })
    //             ->log('Pre Assigned Source');

    //     return redirect()->route('pdfView', [$add_sourceloc->id, 'action' => 'preAssignSourcePdf']);

    // 	// return redirect()->route('preAssignSou.index')->with('insert', 'Pre-assign Source Add Successfully.');
    // }

    // public function edit($id)
    // {
    //     $source_mods = SModList::all();
    //     $ship_methods = SStats::all();

    //     $source_loc = SourceLoc::find($id);

    //     $machine = Machine::where('mach_number', $source_loc->mach_num)->first();
    //     $machLoc = MachLoc::where('machine_id', $machine->id)->first();
    //     $customer = Customer::find($machLoc->customer_id);

        
    // 	return view('pre_assign_source.edit', compact('source_mods', 'ship_methods', 'source_loc', 'machine', 'machLoc', 'customer'));
    // }

    // public function update(Request $request, $id)
    // {
    //     $rules = [
    //         'edit_source_num'         => 'required',
    //         'edit_machine_num'        => 'required',
    //         'edit_source_model'       => 'required',
    //         'edit_source_ship_method' => 'required',
    //     ];

    //     $message = [
    //         'edit_source_num.required'         => 'Source Being Issued Field Is Required.',
    //         'edit_machine_num.required'        => 'Machine Assigning Field Is Required.',
    //         'edit_source_model.required'       => 'Please Select Source Model.',
    //         'edit_source_ship_method.required' => 'Please Select Source Ship Method.',
    //     ];

    //     $this->validate($request, $rules, $message);

    //     $data = [
    //         'source_num' => $request->source_number,
    //         'mach_num'   => $request->edit_machine_num,
    //         's_mod_num'  => $request->edit_source_model,
    //         's_status'   => $request->edit_source_ship_method,
    //     ];

    //     $add_sourceloc = SourceLoc::find($id);
    //     $add_sourceloc->update($data);
    //     // $add_sourceloc = SourceLoc::updateOrCreate(['id' => $id ,'source_num' => $request->source_number,'mach_num' => $request->machine_num], $data);


    //     $user = auth()->user();
    //     activity()->performedOn($add_sourceloc)
    //             ->causedBy($user)
               
    //             ->withProperties(['name' => $user->name, 'email' => $user->email,'machine_num'=>$data['mach_num']])
    //             // ->tap(function(Activity $activity ) use ($customer) {
    //             //     $activity->customer_id = $customer->id;
    //             // })
    //             ->log('Updated Pre Assigned Source');

    // 	return  redirect()->route('preAssignSou.index')->with('update', 'Pre-Assign Source Update Successfully.');
    // }

    public function destroy($id)
    {
        SourceLoc::find($id)->delete();

        // $user = auth()->user();
        // activity()->performedOn($add_sourceloc)
        //         ->causedBy($user)
               
        //         ->withProperties(['name' => $user->name, 'email' => $user->email,'machine_num'=>$data['mach_num']])
        //         // ->tap(function(Activity $activity ) use ($customer) {
        //         //     $activity->customer_id = $customer->id;
        //         // })
        //         ->log('Updated Pre Assigned Source');
        return redirect()->route('preAssignSou.index')->with('delete','Pre-Assign Source Deleted Successfully.');
    }
}
