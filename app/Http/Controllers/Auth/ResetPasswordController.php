<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function reset(Request $request)
    {
        $rules = [
            'token'    => 'required',
            'email'    => 'required|email',
            'password' => 'required|confirmed|min:6',
        ];

        $message = [
            
            'token.required'     => 'Oops! something is wrong.',
            'email.required'     => 'Email field is required.',
            'email.email'        => 'Email format is invalid.',
            'password.required'  => 'Password field is required.',
            'password.confirmed' => 'Password does not match.',
            'password.min'       => 'Enter at least 6 character password.',
        ];

        $this->validate($request, $rules, $message);

        $this->broker()->validator(function ($credentials) {
            return mb_strlen($credentials['password']) >= 6;
        });

        $response = $this->broker()->reset(
            $this->credentials($request), function ($user, $password) {
                $this->resetPassword($user, $password);
            }
        );

        $user = \App\User::where('email', $request->email)->first();
        activity()->performedOn($user)
                ->causedBy($user)
                ->withProperties(['name' => $user->name, 'email' => $user->email])
                ->log('Reset password');

        return $response == Password::PASSWORD_RESET
                    ? $this->sendResetResponse($request, $response)
                    : $this->sendResetFailedResponse($request, $response);
    }
}
