<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Spatie\Activitylog\Models\Activity;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function loginview()
    {
        return view('auth.login');
    }

    protected function authenticated($request, $user) { 
        auth()->logoutOtherDevices(request('password')); 
    }
    
    public function login(Request $request)
    {
        $this->validateLogin($request);

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {

            $request->session()->regenerate();

            $this->clearLoginAttempts($request);

            $user = auth()->user();
            activity()->performedOn($user)
                    ->causedBy($user)
                    ->withProperties(['name' => $user->name, 'email' => $user->email])
                    ->tap(function(Activity $activity ) use ($user) {
                        $activity->new_value = json_encode(['Name' => $user->name, 'Email' => $user->email, "Date-Time" => date('Y-m-d H:i:s')]);
                        $activity->login = 1;
                    })
                    ->log('Login');

            return $this->authenticated($request, $this->guard()->user())
            ?: redirect()->intended($this->redirectPath());
        }

        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        if ( ! User::where('email', $request->email)->first() ) {
            return redirect()->back()
                ->withInput($request->only($this->username(), 'remember'))
                ->withErrors([
                    $this->username() => \Lang::get('Email address not found.'),
                ]);
        }

        if ( ! User::where('email', $request->email)->where('password', bcrypt($request->password))->first() ) {
            return redirect()->back()
                ->withInput($request->only($this->username(), 'remember'))
                ->withErrors([
                    'password' => \Lang::get('Incorrect Password.'),
                ]);
        }
    }

    public function logout(Request $request)
    {

        $user = auth()->user();
        activity()->performedOn($user)
                ->causedBy($user)
                ->withProperties(['name' => $user->name, 'email' => $user->email])
                 ->tap(function(Activity $activity ) use ($user) {
                        $activity->new_value = json_encode(['Name' => $user->name, 'Email' => $user->email, "Date-Time" => date('Y-m-d H:i:s')]);
                        $activity->logout = 1;
                    })
                ->log('Logout');

        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/');
    }
}
