<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Wipe;
use App\State;
use App\SourceLoc;
use DB;
use PDF;

class RegulatoryReportController extends Controller
{
    public function index()
    {
        $wipes = Wipe::paginate(env('page')); 
    	return view('regulatory_report.index', compact('wipes'));
    }

    public function getQuarterlyData(Request $request){  
        $sourcedata = SourceLoc::where('s_ship_date', '<=', date('Y-m-d', strtotime($request->to_date))) 
                            ->where('s_ship_date', '>=',  date('Y-m-d', strtotime($request->from_date)))   
                            ->get();
        $total_row = $sourcedata->count();
        $output = '';
            if($total_row > 0){
                $i = 1;   
                foreach($sourcedata as $key => $source){

               
                    $output .= '
                    <tr>
                        <td>'.$source->source_num.'</td>
                        <td>'.date('d M Y', strtotime($source->s_ship_date)) .'</td>
                        <td>'.$source->tech.'</td>
                        <td>'.$source->mach_num.'</td>
                        <td>'.date('d M Y', strtotime($source->created_at)).'</td>
                        <td style="padding-left:0px; text-align: left;">
                        </td>
                        
                            
                    </tr>
                    ';
                    $i++;
                }
            }else{
                $output = '
                <tr>
                <td class="text-center" colspan="10"><p>No Sources Found.</p></td>
                </tr>
                ';
            }
                $data = array( 
                    'source_data'  => $sourcedata,
                    'table_data'  => $output,
                    'total_data'  => $total_row
                );
    
            echo json_encode($data);                    
                           
    }

    public function viewQuarterlyPdf($from_date, $to_date, $state){
        $sourcedata =   SourceLoc::where('s_ship_date', '<=', date('Y-m-d', strtotime($to_date))) 
                            ->where('s_ship_date', '>=',  date('Y-m-d', strtotime($from_date)))   
                            ->get();
        $data['sourcedata'] = $sourcedata;  
        $data['from_date']  = $from_date;
        $data['to_date']    = $to_date;                  
        $pdf = PDF::loadView('pdf.quarterly_report', $data);
        $path = public_path(). "/pdf/";
        $file_name = $from_date."-".$to_date.".pdf";
        $full_path = $path.$file_name;

        return $pdf->stream($file_name, $data);
    }

    public function destroy($id)
    {
        SourceLoc::find($id)->delete();

        // $user = auth()->user();
        // activity()->performedOn($add_sourceloc)
        //         ->causedBy($user)
               
        //         ->withProperties(['name' => $user->name, 'email' => $user->email,'machine_num'=>$data['mach_num']])
        //         // ->tap(function(Activity $activity ) use ($customer) {
        //         //     $activity->customer_id = $customer->id;
        //         // })
        //         ->log('Updated Pre Assigned Source');
        return redirect()->route('preAssignSou.index')->with('delete','Pre-Assign Source Deleted Successfully.');
    }
}
