<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\Wipe;
use App\User;
use App\Discrep;
use App\Measurer;
use App\SourceLoc;
use App\Machine;
use App\SModList;
use App\Customer;
use App\Source;
use App\Technician;
use App\Contact;
use App\Rso;
use DB;
use Mail;
use View;

class WipeController extends Controller
{
    public function index()
    {
        $wipes = Wipe::orderBy('id', 'desc')
        ->where('wipe_conf', 0)
        ->groupBy(DB::raw('ifnull(add_group_id,id)'))
        ->paginate(env('page'));
        
        ///add_group_id
        $discreps = Discrep::all();
    	return view('wipe.index', compact('wipes', 'discreps'));
    }

    public function history()
    {
        $wipes = Wipe::orderBy('id', 'desc')->where('wipe_conf', 1)->paginate(env('page'));
        $discreps = Discrep::all();
        return view('wipe.history', compact('wipes', 'discreps'));
    }

    public function create()
    {
    	$discreps = Discrep::all();
        $users = User::get();
        $measurers = Measurer::all();
        $techname = Technician::all();
        //dd($measurers);
        $signature = DB::table('signatures')->where('user_id', auth()->user()->id)->orderBy('id','desc')->first();

    	return view('wipe.create', compact('discreps', 'users', 'signature','measurers','techname'));
    }

    public function store(Request $request)
    {
        $rules = [
            'source_num'     => 'required',
            'tech'           => 'required',
            'wipe_date'      => 'required',
            'discrepcod'     => 'required',
            'measure_date'   => 'required',    
            'measure_by'     => 'required',    
            'machine_number' => 'required',    
        ];

        $message = [
            'source_num.required'     => 'Source Number Field Is Required.',
            'tech.required'           => 'Technician Name Field Is Required.',
            'wipe_date.required'      => 'Wipe Date Field Is Required.',
            'discrepcod.required'     => 'Discrepency Field Is Required.',
            'measure_date.required'   => 'Measurer Date Field Is Required.',
            'measure_by.required'     => 'Measurer By Field Is Required.',
            'machine_number.required' => 'Machine Number Field Is Required.',
            
        ];

        $this->validate($request, $rules, $message);

        $data = [
            'source_num' => $request->source_num,
            'tech'       => $request->tech,
            'wipe_date'  => date('Y-m-d', strtotime($request->wipe_date)),
            'discrepcod' => $request->discrepcod,
            'meas_date'  => date('Y-m-d', strtotime($request->measure_date)),
            'meas_by'    => $request->measure_by,
            'mach_num'   => $request->machine_number,
          //  'cust_num'   => $request->customer_number,
         //   'print_job'  => $request->print_job,
            'wipe_conf'  => 0,
            'add_group_id'  => uniqid(),
        ];

        $add_source = Wipe::create($data);

        $signature = DB::table('signatures')->where('user_id', auth()->user()->id)->first();
        $latest_source = Wipe::find($add_source->id);

        $machine    = Machine::where('mach_number', $latest_source->mach_num)->first();
        $customer   = Customer::where('customer_number', $latest_source->cust_num)->first();
        $source     = Source::where('source_num', $latest_source->source_num)->first();
        $technician = Technician::find($latest_source->tech);
        $count_mach = SourceLoc::where('source_num', $latest_source->source_num)->count();
        $contact    = Contact::where('customer_id', $customer->id)->first();

        $source_data = [
            'title'            => 'Wipe Certificates',
            'account'          => !empty($customer->customer_name) ? $customer->customer_name : '',
            'address'          => !empty($customer->address) ? $customer->address : '',
            'country'          => !empty($customer->country) ? $customer->country : '',
            'state'            => !empty($customer->state) ? $customer->state : '',
            'city'             => !empty($customer->city) ? $customer->city : '',
            'zipcode'          => !empty($customer->zipcode) ? $customer->zipcode : '',
            'test_seal'        => !empty($technician->tech_name) ? $technician->tech_name : '',
            'test_seal_date'   => !empty($latest_source->wipe_date) ? $latest_source->wipe_date : '',
            'measurement'      => !empty($latest_source->meas_name->meas_name) ? $latest_source->meas_name->meas_name : '',
            'measurement_date' => !empty($latest_source->meas_date) ? $latest_source->meas_date : '',
            'm_model_no'       => !empty($machine->mach_model) ? $machine->mach_model : '',
            'm_serial_no'      => !empty($machine->mach_number) ? $machine->mach_number : '',
            's_mod_no'         => !empty($source->source) ? $source->source : '',
            's_serial_no'      => !empty($source->source_num) ? $source->source_num : '',
            'discrepancie'     => !empty($latest_source->discrepcod) ? $latest_source->discrepcod : '',
            'count_mach'       => !empty($count_mach) ? $count_mach : '',
            'contact_name'     => !empty($contact->contact_name) ? $contact->contact_name : '',
            'contact_phone'    => !empty($contact->contact_phone) ? $contact->contact_phone : '',
        ];

        $pdf = PDF::loadView('pdf.wipe_certificate', $source_data);

        $data = ['user_name' => auth()->user()->name, 'signature' => !empty($signature->image_name) ? $signature->image_name : '', 'source_id' => $add_source->id, 'customer_name' => !empty($customer->customer_name) ? $customer->customer_name : ''];

        Mail::send('mail.wipe_test', $data, function($message) use ($pdf) {
            $user = auth()->user();
            $user_email = $user->email;
            $user_name = $user->name;

            $add_source = DB::table('wipes')->latest('created_at')->first();

            $message->attachData($pdf->output(), 'Wipe Certificate.pdf');
            $message->to($user_email, $user_name)->subject('Wipe Test');
            $message->from('xyz@gmail.com','Virat Gandhi');
        });

        if($request->print_job == '0')
        {
            return redirect()->route('pdfView', [$add_source->id, 'action' => 'pdfview']);
        }

        return  redirect()->route('wipes')->with('insert', 'Record Wipe Insert Successfully.');
    }

    public function edit($id)
    {
        $discreps  = Discrep::all();
        $measurers = Measurer::all();
        $techname = Technician::all();
        //dd($measurers);
        $wipe_data = Wipe::find($id);
        $sourcesAll='';
        $sourcesDes='';
        $wipe_unq_data='';
        $source    = SourceLoc::where('installation_wipe_test',$id)->first();
        if($source =='')
        $source    = SourceLoc::where('renewable_wipe_test',$id)->first();

        if($source ==''){
            $sourcesAll=[];
            $sourcesDes=[];
        $wipe_unq_data =$wipe_data->add_group_id;
        $wipe_dataa = Wipe::where('add_group_id',$wipe_unq_data)->get();
        foreach ($wipe_dataa as $key => $singledata) {
            
        $sourcesAll[]    = SourceLoc::where('mach_num',$singledata->mach_num)->first();
        $sourcesDes[$key] =$singledata->discrepcod;
            }
        }

    if($source !=''){
        $source_data = [
            'mach_num'        => $source->mach_num,
            'source_num'      => $source->source_num,
            'customer_number' => $source->machine_num->mach_loc->customer->customer_number,
            'customer_name'   => $source->machine_num->mach_loc->customer->customer_name,
            's_status'        => $source->s_status,
            's_mod_num'       => $source->s_mod_num,
            'ship_date'       => date('m/d/y', strtotime($source->s_ship_date)),
            'inst_date'       => date('m/d/y', strtotime($source->inst_date)),
            'currentloc'      => $source->currentloc,
            'print_job'       => $source->print_job,
            'tech'            => $source->tech,
            'note_num'        => $source->note_num,
        ];  
        }else{
            $source_data = [
            'mach_num'        => '',
            'source_num'      => '',
            'customer_number' => '',
            'customer_name'   => '',
            's_status'        => '',
            's_mod_num'       => '',
            'ship_date'       => '',
            'inst_date'       => '',
            'currentloc'      => '',
            'print_job'       => '',
            'tech'            => '',
            'note_num'        => ''
        ]; 
        }
        //dd($techname[]);
        
        return view('wipe.edit', compact('discreps', 'measurers','techname', 'wipe_data', 'source', 'source_data','sourcesAll','sourcesDes','wipe_unq_data'));
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'source_num'     => 'required',
            'tech'           => 'required',
            'wipe_date'      => 'required',
            'discrepcod'     => 'required',
            'measure_date'   => 'required',    
            'measure_by'     => 'required',    
            //'machine_number' => 'required',    
            'customer_no'    => 'required',    
            //'print_job'      => 'required',    
        ];

        $message = [
            'source_num.required'     => 'Source Number Field Is Required.',
            'tech.required'           => 'Technician Name Field Is Required.',
            'wipe_date.required'      => 'Wipe Date Field Is Required.',
            'discrepcod.required'     => 'Discrepency Field Is Required.',
            'measure_date.required'   => 'Measurer Date Field Is Required.',
            'measure_by.required'     => 'Measurer By Field Is Required.',
            //'machine_number.required' => 'Machine Number Field Is Required.',
            'customer_no.required'    => 'Customer Number Field Is Required.',
            //'print_job.required'      => 'If You Want To Print Than Select Yes Otherwise No..',
        ];

        $this->validate($request, $rules, $message);

        $data = [
            'source_num' => $request->source_num,
            'tech'       => $request->tech,
            'wipe_date'  => date('Y-m-d', strtotime($request->wipe_date)),
            'discrepcod' => $request->discrepcod,
            'meas_date'  => date('Y-m-d', strtotime($request->measure_date)),
            'meas_by'    => $request->measure_by,
            'mach_num'   => $request->machine_number,
            'cust_num'   => $request->customer_number,
            //'print_job'  => $request->print_job,
        ];

        $update_source = Wipe::find($id);
        $update_source->update($data);

        if($request->print_job == '0')
        {
            return redirect()->route('pdfView', [$update_source->id, 'action' => 'pdfview']);
        }

        return  redirect()->route('wipes.index')->with('update', 'Record Wipe Update Successfully.');
    }

    public function destroy($id)
    {
        $wipe = Wipe::find($id)->delete();
        return redirect()->route('wipes.index')->with('delete','Successfully Deleted Wipe.');
    }

    public function discrepcod(Request $request)
    {
        if($request->ajax()){
            $discrepcod = $request->get('discrepcod');
            $discrepcod = Discrep::where('discrepcod', $discrepcod)->first();
            
            echo json_encode($discrepcod);
        }
    }

    public function pdfView($id)
    {

        if(request('action') == 'preAssignSourcePdf')
        {   
            $pre_assign_source = SourceLoc::find($id);
            $contact_number= isset($pre_assign_source->contact_name->contact_name) ?$pre_assign_source->contact_name->contact_name:'';
            $customer_salutation = isset($pre_assign_source->machine_num->mach_loc->customer->salutation) ? $pre_assign_source->machine_num->mach_loc->customer->salutation : '';
            $customer_name = isset($pre_assign_source->machine_num->mach_loc->customer->customer_name) ? $pre_assign_source->machine_num->mach_loc->customer->customer_name : '';
            $address = isset($pre_assign_source->machine_num->mach_loc->customer->address) ? $pre_assign_source->machine_num->mach_loc->customer->address : '';
            $city = isset($pre_assign_source->machine_num->mach_loc->customer->city) ? $pre_assign_source->machine_num->mach_loc->customer->city : '';
            $state = isset($pre_assign_source->machine_num->mach_loc->customer->state) ? $pre_assign_source->machine_num->mach_loc->customer->state : '';
            $country = isset($pre_assign_source->machine_num->mach_loc->customer->country) ? $pre_assign_source->machine_num->mach_loc->customer->country : '';
            $zipcode = isset($pre_assign_source->machine_num->mach_loc->customer->zipcode) ? $pre_assign_source->machine_num->mach_loc->customer->zipcode : '';
            $type = request('type');
            if(isset($pre_assign_source->s_mod_num)){
                   $smodl = SModList::where('s_mod_num',$pre_assign_source->s_mod_num)
                            ->first(); 
                  $smodlist_desc= $smodl->desc;         
            }else{
                $smodlist_desc='';
            }
            $techname = Technician::where('type','4')->first();
            $rso_name = isset($techname->tech_name)?$techname->tech_name:''; 
            
            $data = [
                'title'                           => 'Pre-Assign Source',
                'inspection_system_serial_number' => $pre_assign_source->mach_num,
                'source_serial_number'            => $pre_assign_source->source_num,
                'source_mod_number'               => $pre_assign_source->s_mod_num,
                'source_mod_desc'                 => $smodlist_desc,
                'date'                            => $pre_assign_source->s_ship_date,
                'customer_salutation'             => $customer_salutation,
                'customer_name'                   => $customer_name,
                'contact_number'                  => $contact_number,
                'address'                         => $address,
                'city'                            => $city,
                'state'                           => $state,
                'country'                         => $country,
                'zipcode'                         => $zipcode,
                'type'                            =>$type,
                'rso_name'                            =>$rso_name    
            ];
            
            $pdf = PDF::loadView('pdf.pre_assign_source', $data);

            $path = public_path(). "/pdf/";
            $file_name = $pre_assign_source->source_num."-".$pre_assign_source->id.".pdf";
            $full_path = $path.$file_name;

            return $pdf->stream($file_name, $data);

        }else{

            $add_source = Wipe::find($id);
            $custnumgrp = $add_source['add_group_id'];
            if(!empty($custnumgrp))
            $findAllcust = Wipe::where('add_group_id',$custnumgrp)->get(); 
            else
                $findAllcust=[];
            $a = [];
            $ab = [];
                
                if(count($findAllcust)>1){
                $am = [];    
                foreach ($findAllcust as $key =>$add_sourcem) {
                $machane1 = Machine::where("mach_number", $add_sourcem->mach_num)->first();
                $Source1 = Source::where("source_num", $add_sourcem->source_num)->first();
                $discreps = Discrep::where('discrepcod', $add_sourcem->discrepcod)->first();
                
                $am['mach_num'] = $add_sourcem->mach_num;
                $am['mach_model'] = (!empty($machane1->mach_model)) ? $machane1->mach_model: null;
                $am['source_num'] = $add_sourcem->source_num;
                $am['source_model'] = (!empty($Source1->source_model)) ? $Source1->source_model: null;
                $am['discrepcod'] = (!empty($discreps->discrepcod)) ? $discreps->discrepcod: null;
                $ab [] =$am;
                    }  
                                            
                }else{


            	$machane1 = Machine::where("mach_number", $add_source->mach_num)->first();
				$Source1 = Source::where("source_num", $add_source->source_num)->first();
				$discreps = Discrep::where('discrepcod', $add_source->discrepcod)->first();
				
                $a['mach_num'] = $add_source->mach_num;
				$a['mach_model'] = (!empty($machane1->mach_model)) ? $machane1->mach_model: null;
				$a['source_num'] = $add_source->source_num;
				$a['source_model'] = (!empty($Source1->source_model)) ? $Source1->source_model: null;
				$a['discrepcod'] = (!empty($discreps->discrepcod)) ? $discreps->discrepcod: null;
			 }
            $techrso = Technician::where('type','4')->first();
            //$rso =$techrso->tech_name;

            $machine    = Machine::where('mach_number', $add_source->mach_num)->first();
            $customer   = Customer::where('customer_number', $add_source->cust_num)->first();
            $source     = Source::where('source_num', $add_source->source_num)->first();
            $technician = Technician::find($add_source->tech);
            $count_mach = SourceLoc::where('source_num', $add_source->source_num)
            ->where('currentloc',1)
            ->where('s_status','aa')
            ->count();

           // $contact    = Contact::where('customer_id', $customer->id)->first();

            if($add_source->discrepcod!=8){
              $attn_name = !empty($add_source->contact_num)?$add_source->contact_num:'';
              $contact_n    = Contact::where('contact_name',$add_source->contact_num)->first();
              $attn_num = !empty($contact_n->contact_phone)?$contact_n->contact_phone:'';
             }else{
             $attn_name = !empty($add_source->tech_name->tech_name)?$add_source->tech_name->tech_name:'';
             $attn_num='';
             }
               
             $signature = DB::table('signatures')->where('user_id', auth()->user()->id)->orderBy('id','desc')->first();


            $data = [
                'title'            => 'Wipe Certificates',
                'account'          => !empty($customer->customer_number) ? $customer->customer_number : '',
                'address'          => !empty($customer->address) ? $customer->address : '',
                'country'          => !empty($customer->country) ? $customer->country : '',
                'state'            => !empty($customer->state) ? $customer->state : '',
                'city'             => !empty($customer->city) ? $customer->city : '',
                'zipcode'          => !empty($customer->zipcode) ? $customer->zipcode : '',
                'test_seal'        => !empty($technician->tech_name) ? $technician->tech_name : '',
                'test_seal_date'   => !empty($add_source->wipe_date) ? date('m/d/y',strtotime($add_source->wipe_date)) : '',
                'measurement'      => !empty($add_source->meas_name->meas_name) ? $add_source->meas_name->meas_name : '',
                'measurement_date' => !empty($add_source->meas_date) ? date('m/d/y',strtotime($add_source->meas_date)) : '',
                'm_model_no'       => !empty($machine->mach_model) ? $machine->mach_model : '',
                'm_serial_no'      => !empty($machine->mach_number) ? $machine->mach_number : '',
                's_mod_no'         => !empty($source->source_model) ? $source->source_model : '',
                's_serial_no'      => !empty($source->source_num) ? $source->source_num : '',
                'discrepancie'     => !empty($add_source->discrepcod) ? $add_source->discrepcod : '',
                'count_mach'       => !empty($count_mach) ? $count_mach : '',
                'contact_name'     => $attn_name,//!empty($contact->contact_name) ? $contact->contact_name : '',
                'contact_phone'    => $attn_num,//!empty($contact->contact_phone) ? $contact->contact_phone : '',
                'rso'              => $techrso->tech_name,
				'entrygroup'       => $a,
                'entrygroupb'      => $ab,
                'signature'        => !empty($signature->image_name) ? $signature->image_name : ''
            ];

            if(request('action') == 'pdfview'){
                $pdf = PDF::loadView('pdf.wipe_certificate', $data);
            }else if(request('action') == 'envelope'){
                $customPaper = array(0,0,660.00,290.00);
                $pdf = PDF::loadView('pdf.envelope', $data)->setPaper($customPaper, 'potrait');
            }else if(request('action') == 'certificate'){
                $customPaper = array(0,0,550.00,430.00);
                $pdf = PDF::loadView('pdf.certificate', $data)->setPaper($customPaper, 'potrait');
            }else if(request('action') == 'regulatoryReport'){
                $pdf = PDF::loadView('pdf.regulatory_report', $data);
            }

            $path = public_path(). "/pdf/";
            $file_name = $add_source->source_num."-".$add_source->id.".pdf";
            $full_path = $path.$file_name;

            return $pdf->stream($file_name, $data);
        }
    }

    public function sendEmail($id)
    {   
        $signature = DB::table('signatures')->where('user_id', auth()->user()->id)->first();
        $latest_source = Wipe::find($id);
        $a = [];
        if(!empty($latest_source->entry_group)){
        $entrygroup = Wipe::where('entry_group', $latest_source->entry_group)->get();
            
            foreach($entrygroup as $key => $value){
                $machane1 = Machine::where("mach_number", $value->mach_num)->first();
                $Source1 = Source::where("source_num", $value->source_num)->first();
                $discreps = Discrep::where('discrepcod', $value->discrepcod)->first();
                $a[$key]['mach_num'] = $value->mach_num;
                $a[$key]['mach_model'] = (!empty($machane1->mach_model)) ? $machane1->mach_model: null;
                $a[$key]['source_num'] = $value->source_num;
                $a[$key]['source_model'] = (!empty($Source1->source_model)) ? $Source1->source_model: null;
                $a[$key]['discrepcod'] = (!empty($discreps->descriptn)) ? $discreps->descriptn: null;
            }
           } 
        $rso = Rso::find(1);
            
            
        $machine    = Machine::where('mach_number', $latest_source->mach_num)->first();
        $customer   = Customer::where('customer_number', $latest_source->cust_num)->first();
        $source     = Source::where('source_num', $latest_source->source_num)->first();
        $technician = Technician::find($latest_source->tech);
        $count_mach = SourceLoc::where('source_num', $latest_source->source_num)->count();
        $contact    = Contact::where('customer_id', $customer->id)->first();

        $source_data = [
            'title'            => 'Wipe Certificates',
            'account'          => !empty($customer->customer_name) ? $customer->customer_name : '',
            'address'          => !empty($customer->address) ? $customer->address : '',
            'country'          => !empty($customer->country) ? $customer->country : '',
            'state'            => !empty($customer->state) ? $customer->state : '',
            'city'             => !empty($customer->city) ? $customer->city : '',
            'zipcode'          => !empty($customer->zipcode) ? $customer->zipcode : '',
            'test_seal'        => !empty($technician->tech_name) ? $technician->tech_name : '',
            'test_seal_date'   => !empty($latest_source->wipe_date) ? $latest_source->wipe_date : '',
            'measurement'      => !empty($latest_source->meas_name->meas_name) ? $latest_source->meas_name->meas_name : '',
            'measurement_date' => !empty($latest_source->meas_date) ? $latest_source->meas_date : '',
            'm_model_no'       => !empty($machine->mach_model) ? $machine->mach_model : '',
            'm_serial_no'      => !empty($machine->mach_number) ? $machine->mach_number : '',
            's_mod_no'         => !empty($source->source) ? $source->source : '',
            's_serial_no'      => !empty($source->source_num) ? $source->source_num : '',
            'discrepancie'     => !empty($latest_source->discrepcod) ? $latest_source->discrepcod : '',
            'count_mach'       => !empty($count_mach) ? $count_mach : '',
            'contact_name'     => !empty($contact->contact_name) ? $contact->contact_name : '',
            'contact_phone'    => !empty($contact->contact_phone) ? $contact->contact_phone : '',
            'rso'              =>  !empty($rso->name)?$rso->name:'',
            'entrygroup'       => $a
        ];

        $pdf = PDF::loadView('pdf.wipe_certificate', $source_data);
        $data = ['user_name' => auth()->user()->name, 'signature' => !empty($signature->image_name) ? $signature->image_name : '', 'source_id' => $latest_source->id, 'customer_name' => !empty($customer->customer_name) ? $customer->customer_name : ''];

        Mail::send('mail.wipe_test', $data, function($message) use ($pdf,$contact) {
            $user_email = $contact->contact_email;
            $user_name = $contact->contact_name;

            $add_source = DB::table('wipes')->latest('created_at')->first();

            $message->attachData($pdf->output(), 'Wipe Certificate.pdf');
            $message->to($user_email, $user_name)->subject('Wipe Test');
            $message->from('sourcebase@gmail.com','SourceBase Admin');
        });
        return  redirect()->route('wipes.index')->with('sendMail', 'Mail Send Successfully.');
    }

    public function updateWipe(Request $request){
         $wipe = Wipe::find($request->wipeid)->update(['wipe_conf' => 1]);

        return ["status"=> "success", "message" => "wipe updated successfully"];
   
    }

    public function wipesSearch(Request $request){
        $q = $request->input('query');
        $wipeconf = $request->input('conf');
        $wipes = Wipe::orderBy('id', 'desc')->where('wipe_conf', $wipeconf)
                    ->Where('source_num','LIKE','%'.$q.'%')
                    ->orWhere('mach_num','LIKE','%'.$q.'%')
                    ->orWhere('cust_num','LIKE','%'.$q.'%')
                    ->paginate(50);


        $discreps = Discrep::all();
        $measurers = Measurer::all();

        $data['html'] = View::make('wipe.search_table', ['wipes' => $wipes, 'discreps' => $discreps, 'measurers' => $measurers])->render();

        return $data;

    }

    public function storeMultiple(Request $request){

        try{

            $lastWipe = Wipe::orderBy('id', 'desc')->first();
            $entrygroup = $lastWipe->entry_group;

            $editedData = isset($request->edittype)?$request->edittype:'';
            if($editedData !=''){

                $wipeOld = Wipe::where('add_group_id',$editedData);
                $wipeOld->delete();
            }
           $addgrpid= uniqid();
           $customernum = Customer::where('customer_number',$request->customerNumber)->first();
                if(isset($customernum->id)){
                   $cust_nums= Contact::where('customer_id',$customernum->id)->first();
               
               if(isset($cust_nums->contact_name))           
                $con_name = $cust_nums->contact_name;
                else
                $con_name ='';

                }
            foreach ($request->sourceNumber as $key => $value) {

   
                $data[] = [
                    'source_num' => $value,
                    'tech'       => $request->technitian,
                    'wipe_date'  => date('Y-m-d', strtotime($request->wipe_date)),
                    'discrepcod' => $request->discrepcod_multiple[$key],
                    'meas_date'  => date('Y-m-d', strtotime($request->measure_date)),
                    'meas_by'    => $request->measure_by,
                    'mach_num'   => $request->machineNumber[$key],
                    'cust_num'   => $request->customerNumber,
                    'print_job'  => $request->printwipe,
                    'wipe_conf'  => 0,
                    'contact_num'  => $con_name,
                    'add_group_id'  => $addgrpid,
                    'entry_group' =>  $entrygroup+1
                ];
            }
            //dd($con_name);
            $add_source = Wipe::insert($data);
            
            return  json_encode(["status" => "success"]);

        }catch(\Exception $e){

            return  json_encode(["status" => "success", "message"=>$e->getMessage()]);
        }

    }
    public function editRSO(){
        $rso = Rso::find(1);
       
        return view('wipe.update-rso', compact('rso'));
    }

    public function updateRSO(Request $request){

        $rso = Rso::find(1);
        $rso->name = $request->input('name');
        $rso->save();

        return redirect()->route('edit-RSO')->with('success','Radiation Safety Officer detail udpated successfully');;
    }
}
