<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UploadDocument;

class UploadDocumentController extends Controller
{
    public function index()
    {
    	
    	return view('upload_document.index');
    }

    /*
    document upload
    */
    public function documentUpload(Request $request)
    {   
        $rules = [
            'title'         => 'required',
            'upload_date'   => 'required',
            'image'         => 'required|mimes:doc,docx,pdf',
        ];

        $message = [
            'title.required'   => 'Title Field Is Required.',
            'upload_date.required'   => 'Upload Date Field Is Required.',
            'image.required'   => 'Signature Field Is Required.',
            'image.image'   => 'Please Select Image.',
            'image.mimes'   => 'Please Select doc, pdf This Type Extension.'];

        $this->validate($request, $rules, $message);

        if($request->customer_id){
            $requestid = $data['customer_id'] = $request->customer_id;
        }else if($request->source_id){
            $requestid = $data['source_id'] = $request->source_id;
        }else if($request->machine_id){
            $requestid = $data['machine_id'] = $request->machine_id;
        }
        else if($request->technician_id){
            $requestid = $data['technician_id'] = $request->technician_id;
        }else{
            $requestid = $data['contact_id'] = $request->contact_id;
        }

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = pathinfo($image->getClientOriginalName(),PATHINFO_FILENAME).'_'.time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('sourcetracker/'.$requestid.'/');
            $image->move($destinationPath, $imageName);
            
        }
        $data['title'] = $request->title;
        $data['upload_date'] =  date('y-m-d', strtotime($request->upload_date));
        $data['document'] = $imageName;

        UploadDocument::create($data);
        if($request->customer_id){
        	return redirect()->route('customer.view', ['id' => $request->customer_id])->with('insert','Successfully Inserted Customer Document.');
        }else if($request->source_id){
        	return redirect()->route('source.log_history', ['id' => $request->source_id])->with('insert','Successfully Inserted Source Document.');
        }else if($request->machine_id){
        	return redirect()->route('machine.log_history', ['id' => $request->machine_id])->with('insert','Successfully Inserted Machine Document.');
        }
        else if($request->technician_id){
        	return redirect()->route('technician.log_history', ['id' => $request->technician_id])->with('insert','Successfully Inserted Technician Document.');
        }else{
        	return redirect()->route('contact.log_history', ['id' => $request->contact_id])->with('insert','Successfully Inserted Contact Document.');
        }
        
    }

}
?>    