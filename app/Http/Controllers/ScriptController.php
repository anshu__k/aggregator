<?php

namespace App\Http\Controllers;
use Spatie\Activitylog\Models\Activity;

use Illuminate\Http\Request;
use App\ScriptLog;
use App\ScriptList;
use App\ScriptRecord;
use App\Contact;
use App\Customer;
use DB;

class ScriptController extends Controller
{
    public function index()
    {

    }    
    public function list()
    {
        $sources = DB::table('script_lists')
                     ->orderBy('script_lists.id','desc')
                     ->paginate(15);
     	return view('script.index', compact('sources'));
    }

    public function logs()
    {
        $sources = DB::table('script_logs')
                     ->orderBy('script_logs.id','desc')
                     ->paginate(15);
        return view('script.logs', compact('sources'));
    }
    

    public function create()
    {
        

    	return view('script.create');	
    }

    public function store(Request $request)
    {   
        $rules = [
            'name' => 'required',
              'file_name' => ['required', 'file', 'max:5120', 'mimes:csv,doc,docx,txt,xls,xlsx'],  
            
        ];

        $message = [
            'name.required'   => 'Name is required',
            'file_name.required'   => 'File is invalid',
            
            
        ];

        $this->validate($request, $rules, $message);


        $source = new ScriptList();

if ($request->hasFile('file_name')) {
       
       $file = $request->file('file_name');

       $filename = $file->getClientOriginalName(); 
       $type = $file->getMimeType();
        
        $destinationPath = base_path('public\uploads');
        $file->move($destinationPath,$file->getClientOriginalName());

}
        
        $source->name = request('name');
        $source->file_name = $filename;
        $source->type = $type;
        $source->save();

        
        $user = auth()->user();
        activity()->performedOn($source)
                ->causedBy($user)
                ->withProperties(['name' => $user->name, 'email' => $user->email])
                ->tap(function(Activity $activity ) use ($source) {
                    $activity->source_id = $source->id;
                    $activity->new_value = json_encode($source);
                })
                ->log('Added ScriptFile');


    	return redirect()->route('script.list')->with('insert', 'Script File Added Successfully.');	
    }

    public function edit($id)
    {   
        $source = ScriptList::find($id);
        
        // return $source;
        return view('script.edit', compact('source'));
    }

    
    public function update(Request $request, $id){

        $rules = [
            'name' => 'required',
              'file_name' => ['file', 'max:5120', 'mimes:csv,doc,docx,txt,xls,xlsx'],  
            
        ];

        $message = [
            'name.required'   => 'Name is required',
            'file_name.required'   => 'File is invalid',
            
            
        ];

        $this->validate($request, $rules, $message);


        $source = ScriptList::find($id);

if ($request->hasFile('file_name')) {
       
       $file = $request->file('file_name');

       $filename = $file->getClientOriginalName(); 
       $type = $file->getMimeType();
        
        $destinationPath = base_path('public\uploads');
        $file->move($destinationPath,$file->getClientOriginalName());

        $data = [
        'name' => $request->name,
        'file_name' => $filename,
        'type' => $type
        ];
        $source->update($data);
}else{
    
        $data = [
        'name' => $request->name,
        ];
        $source->update($data);
}
        
         
        

        $user = auth()->user();
        activity()->performedOn($source)
                ->causedBy($user)
                ->withProperties(['name' => $user->name, 'email' => $user->email])
                ->log('Updated file');

        return redirect()->route('script.list')->with('update','Successfully Updated file.');

        // return redirect()->route('customer_edit', ['id' => $id])->with('update','Successfully Updated Customer.');


    }

    public function search_source(Request $request){

        if($request->ajax()){
            $output = '';
            $query = $request->get('query');
            $status = $request->get('status');

            if ($query == '' && $status == '') {
                return;
            }

            $qry = Source::query();

            if($status !=''){
                    $qry->where('status',$status);
            }

            if($query != ''){

                $q = $query;

                $qry->where(function ($querys) use ($q) {

                    $querys->OrWhere('sources.source_num','LIKE','%'.$q.'%')
                            ->OrWhere('sources.source_model','LIKE','%'.$q.'%')
                            ->OrWhere('sourc_locs.mach_num','LIKE','%'.$q.'%')
                            ->OrWhere('smodlist.desc','LIKE','%'.$q.'%');
                });
                }    
                $sources = $qry->select('sources.*', 'smodlist.desc', 'sourc_locs.mach_num', 'sourc_locs.currentloc')
                            ->leftJoin('smodlist', 'smodlist.s_mod_num', '=', 'sources.source_model')
                            ->leftJoin('sourc_locs', 'sourc_locs.source_num', '=', 'sources.source_num')
                            ->orderBy('sources.updated_at','desc')
                            ->where('sourc_locs.currentloc',1)    
                            ->paginate(15);

                  return view('source.search', compact('sources'))->render();
            
            
        }
    }

	public function searchwipe(Request $request){
       // dd($request->all());
        if($request->ajax()){
            $output = '';
            $query = $request->get('query');

            if ($query == '') {
                return;
            }

            if($query != ''){

                $q = $query;
                $machall=['aa','ms','mn'];
                $sst=['aa','ab','ac','ad','ae','am','cm','cp','st'];
                $MachLoc = MachLoc::whereIn('mach_loc.mach_status', $machall)
                ->leftJoin('sourc_locs',  'mach_loc.machine_id', '=','sourc_locs.mach_num')
                ->where('mach_loc.customer_num','LIKE','%'.$q.'%')
                ->whereIn('sourc_locs.s_status', $sst)
                ->where('sourc_locs.currentloc',1)
                ->where('mach_loc.cur_mach_loc',1)
                ->groupBy('mach_loc.machine_id')
                ->get();
            }
             $machnums = [];
             foreach ($MachLoc as $key => $value) {
                 $machnums[] = $value->machine_id;
             }

            //dd( $machnums);

            if($MachLoc->count()){
                $sources = SourceLoc::whereIn('mach_num', $machnums)->get();
                $total_row = $sources->count();
            }else{
                $total_row = 0;
            }

            

            if($total_row > 0){
                
                if($request->input('searchTyp')){
                    $className = 'fill_wipe_data_multiple';
                }else{
                    $className = 'fill_wipe_data';
                }
                $discreps = Discrep::all();
                $select = '<select class="form-control input_size" name="discrepcod_multiple">
                            <option selected disabled>Select Discrepancies</option>';
                            foreach($discreps as $discrep){
                                $select .= '<option value="'.$discrep->discrepcod.'" >'. $discrep->descriptn .'</option>';
                            }
                            
                     $select .= '</select>';

                $i = 1;
                foreach($sources as $key => $source){
                    // <td>'.$i.'</td>
                    $output .= '
                    <tr>
                        <td><input type="checkbox" name="wipe_select" class="'. $className .'" source_id="'.$source->id.'" style="width:10px;"> <input type="hidden" source="'.$source->source_num.'" mach="'.$source->mach_num.'"></td>
                        <td>'.$source->mach_num.'</td>
                        <td>'.$source->source_num.'</td>
                        <td>'.$source->source_status->sstat_desc.'</td>
                        <td>'.$source->s_mod_num.'</td>
                        <td>'.date('m/d/y', strtotime($source->inst_date)).'</td>
                        <td>'.date('m/d/y', strtotime($source->ship_date)).'</td>
                        <td>'.$source->tech.'</td>
                        <td>'.$select.'</td>
                    </tr>
                    ';
                    $i++;
                }
            }else{
                $output = '
                <tr>
                <td class="text-center" colspan="12"><p>Source Not Found.</p></td>
                </tr>
                ';
            }
            
            $data = array(
                'table_data'  => $output,
                'total_data'  => $total_row,
            );
    
            echo json_encode($data);
        }
    }


    public function search(Request $request){
        if($request->ajax()){
            $output = '';
            $query = $request->get('query');

            if ($query == '') {
                return;
            }

            if($query != ''){

                $q = $query;
                $sources = SourceLoc::where('source_num','LIKE','%'.$q.'%')->where('currentloc', 1)->get();
            }

            $total_row = $sources->count();

            if($total_row > 0){
                //echo $request->input('searchTyp');die;
                if($request->input('searchTyp')){
                    $className = 'fill_wipe_data_multiple';
                }else{
                    $className = 'fill_wipe_data';
                }
                $i = 1;
                foreach($sources as $key => $source){
                    // <td>'.$i.'</td>
                    $output .= '
                    <tr>
                        <td><input type="radio" name="wipe_select" class="'. $className .'" source_id="'.$source->id.'" style="width:10px;"></td>
                        <td>'.$source->mach_num.'</td>
                        <td>'.$source->source_num.'</td>
                        <td>'.$source->machine_num->mach_loc->customer->customer_number.'</td>
                        <td>'.$source->s_status.'</td>
                        <td>'.$source->s_mod_num.'</td>
                        <td>'.date('d M Y', strtotime($source->inst_date)).'</td>
                        <td>'.date('d M Y', strtotime($source->ship_date)).'</td>
                        <td>'.$source->currentloc.'</td>
                        <td>'.$source->print_job.'</td>
                        <td>'.$source->tech.'</td>
                        <td>'.$source->note_num.'</td>
                    </tr>
                    ';
                    $i++;
                }
            }else{
                $output = '
                <tr>
                <td class="text-center" colspan="12"><p>Source Not Found.</p></td>
                </tr>
                ';
            }
            
            $data = array(
                'table_data'  => $output,
                'total_data'  => $total_row,
            );
    
            echo json_encode($data);
        }
    }

    public function wipeDataAutoFill(Request $request)
    {
        $source = SourceLoc::find($request->source_id);

        $customer_number = isset($source->machine_num->mach_loc->customer->customer_number) ? $source->machine_num->mach_loc->customer->customer_number : null;
        $customer_name = isset($source->machine_num->mach_loc->customer->customer_name) ? $source->machine_num->mach_loc->customer->customer_name : null;
        $address = isset($source->machine_num->mach_loc->customer->address) ? $source->machine_num->mach_loc->customer->address : null;
        $city = isset($source->machine_num->mach_loc->customer->city) ? $source->machine_num->mach_loc->customer->city : null;
        $state = isset($source->machine_num->mach_loc->customer->state) ? $source->machine_num->mach_loc->customer->state : null;
        $zipcode = isset($source->machine_num->mach_loc->customer->zipcode) ? $source->machine_num->mach_loc->customer->zipcode : null;
        $salutation = isset($source->machine_num->mach_loc->customer->contact->salutation) ? $source->machine_num->mach_loc->customer->contact->salutation : null;
        $contact_name = isset($source->machine_num->mach_loc->customer->contact->contact_name) ? $source->machine_num->mach_loc->customer->contact->contact_name : null;
        $bill_to_info = isset($source->machine_num->mach_loc->customer->contact->address) ? $source->machine_num->mach_loc->customer->contact->address : null;

        $source_data = [
            'mach_num' => $source->mach_num,
            'source_num' => $source->source_num,
            'customer_number' => $customer_number,
            'customer_name' => $customer_name,
            's_status' => $source->s_status,
            's_mod_num' => $source->s_mod_num,
            'inst_date' => date('m/d/y', strtotime($source->inst_date)),
            'ship_date' => date('m/d/y', strtotime($source->s_ship_date)),
            'currentloc' => $address . ', ' .  $city . ', ' . $state . '-' . $zipcode,
            'print_job' => $source->print_job,
            'tech' => $source->tech,
            'note_num' => $source->note_num,
            'contact' => $contact_name,
            'bill_to_info' => $bill_to_info,
        ];

        $data = array(
            'source_data' => $source_data
        );

        echo json_encode($data);
    }


    public function destroy($id)
    {

        // return $id;        
        $user = auth()->user();


        $source = Source::where('id',$id)->first();

        $source->deleted_by = $user->id;
        $source->is_deleted = 1;
        $source->save();
        $sourceActivity = $source;
        $source->delete();

        $sourceActivity->deleted_by = "Name: ".  $user->name . " <br /> Email: ".$user->email;

        activity()->performedOn($source)
                ->causedBy($user)
                ->withProperties(['name' => $user->name, 'email' => $user->email])
                ->tap(function(Activity $activity ) use ($sourceActivity) {
                    $activity->source_id = $sourceActivity->id;
                    $activity->old_value = json_encode($sourceActivity);
                })
                ->log('Deleted Source');

        return redirect()->route('sources.index')->with('delete','Successfully Deleted Source.');
    }
	
	public function autocompletewipe(Request $request)
    {
        $term = trim($request->q);
        if (empty($term)) {
            return \Response::json([]);
        }
        $sources = Customer::where('customer_number','LIKE', $term.'%')
                            ->limit(10)
                            ->get();
        // dd($sources);
        $formatted_sources = [];

        foreach ($sources as $source) {
            $formatted_sources[] = ['id' => $source->customer_number, 'text' => $source->customer_number];
        }

        return \Response::json($formatted_sources);
    }
	
    public function autocomplete(Request $request)
    {
        $term = trim($request->q);
        if (empty($term)) {
            return \Response::json([]);
        }
        $sources = Source::where('source_num','LIKE', $term.'%')
                            ->limit(10)
                            ->get();
        // dd($sources);
        $formatted_sources = [];

        foreach ($sources as $source) {
            $formatted_sources[] = ['id' => $source->source_num, 'text' => $source->source_num];
        }

        return \Response::json($formatted_sources);
    }

    public function getHistory($source_id){ 
        $sourcelocdata = [];
        $wipehistory = [];
        $sourcedata = Source::where('id', $source_id)
                            ->first(); 
        if($sourcedata['id']){  
            $sourcelocdata = $sourcedata->sourceData;  
            $wipehistory = Wipe::where('source_num', $source_id)
                            ->orderBy('id', 'desc')
                            ->get(); 
        }  
        $trackerdoc = UploadDocument::where('source_id', $source_id)->orderBy('id', 'desc')->get();                    
        return view('source.history',compact('sourcelocdata', 'sourcedata', 'wipehistory', 'source_id', 'trackerdoc'));
    }


    public function preAssignSourceAutoComplete(Request $request)
    {   
        $term = trim($request->q);
        if (empty($term)) {
            return \Response::json([]);
        }
        //try {
            
            $sources = Source::select('sources.source_num')
                            ->leftJoin('sourc_locs', 'sourc_locs.source_num', '=', 'sources.source_num')
                            ->where('sources.source_num','LIKE', $term.'%')
                            ->where('sourc_locs.currentloc',1)
                            ->where('sourc_locs.s_status','ID')
                            ->limit(10)
                            ->get(); 
        // } catch(\Illuminate\Database\QueryException $ex){ 
        //   dd($ex->getMessage()); 
        // }                    
        $formatted_sources = [];

        foreach ($sources as $source) {
            $formatted_sources[] = ['id' => $source->source_num, 'text' => $source->source_num];
        }

        return \Response::json($formatted_sources);
    }

    public function transferSourceAutoComplete(Request $request)
    {
        $term = trim($request->q);
        if (empty($term)) {
            return \Response::json([]);
        }

        $sources = Source::select('sources.*', 'sourc_locs.mach_num', 'sourc_locs.currentloc')
                            ->leftJoin('sourc_locs', 'sourc_locs.source_num', '=', 'sources.source_num')
                            ->where('sources.source_num','LIKE', $term.'%')
                            ->where('sourc_locs.currentloc',1)
                            ->limit(10)
                            ->get(); 
        $formatted_sources = [];

        foreach ($sources as $source) {
            $formatted_sources[] = ['id' => $source->source_num, 'text' => $source->source_num];
        }

        return \Response::json($formatted_sources);
    }


    public function preAssignSourceAutoFill(Request $request)
    {  
        $source_data = [
                'source_num' => '',
                's_status_desc' => '',
                'source_status' => '',
                'curr_source_model' => '',
                's_mod_num' => '',
                's_mod_number' => '',
                'current_mach' => '',
                'current_mach_number' => '',
                'current_mach_status' => '',
                'current_customer' => '',
                'customer_contact' => '',
                'customer_number' => '',
            ];
            //dump($request->source_num);
            //dd($source_data);
         $source = SourceLoc::where('source_num', $request->source_num)
                            ->Where('currentloc', '1')
                            ->first(); 
        if($source != null ){
            
            //$machine = Machine::where('mach_number', $source->mach_num)->first(); 
            $machLoc = MachLoc::where('machine_id', $source->mach_num)->first();  
          //  $machLoc = MachLoc::where('machine_id', $machine->id)->first(); 
            $source_data = [
                'source_num' => !empty($source->source_num) ? $source->source_num : '',
                's_status_desc' => !empty($source->source_status->sstat_desc) ? $source->source_status->sstat_desc : '',
                's_status' => !empty($source->s_status) ? $source->s_status : '',
                'curr_source_model' => !empty($source->source_model_first->source_model) ? $source->source_model_first->source_model : '',
                //'s_mod_num' => !empty($source->source_model->desc) ? $source->source_model->desc : '',
                's_mod_number' => !empty($source->source_model->s_mod_num) ? $source->source_model->s_mod_num : '',
                'current_mach' => !empty($source->mach_num) ? $source->mach_num : '',
                //'current_mach' => !empty($machine->mach_model) ? $machine->mach_model : '',
                'current_mach_number' => !empty($source->mach_num) ? $source->mach_num : '',
                'current_mach_status' => !empty($machLoc->machine_status->mstats_desc) ? $machLoc->machine_status->mstats_desc : '',
                'current_customer' => !empty($machLoc->customer->customer_name) ? $machLoc->customer->customer_name : '',
                //'customer_contact' => !empty($machLoc->customer->customer_contact->contact_name) ? 'Mr. '.$machLoc->customer->customer_contact->contact_name : '',
                'customer_number' => !empty($machLoc->customer->customer_number) ? $machLoc->customer->customer_number : '',
            ];
        }
       

        $data = array(
            'source_data' => $source_data
        );

        echo json_encode($data);
    }

    public function preAssignMachineAutoFill(Request $request)
    {
        $machine_data = [
                'customer_number' => ''
            ];
        $customer_data = ['customer_name' => ''];

        $machLoc = MachLoc::where('machine_id', $request->machine_number)
                    ->where('cur_mach_loc', 1)
                    ->first();
        if($machLoc != null){ 
            $customer = Customer::find($machLoc->customer_id);
             $allcon=$customer->all_contact_type_data;
             $pArr=[];
             foreach ($allcon as $key => $value) {

               $c_desc =1;//Contact::contact_type_desc($value['contact_type']);
                 $pArr[$value['id']]=$value['contact_name'].' ('.$value['contact_desc'].')';
             }
            $customer_data=['customer_name'=>$pArr];
             $machine_data = [
                'customer_number' => $customer->customer_number
            ];
        }       
        $data = array(
            'machine_data' => $machine_data,
            'customer_data' => $customer_data
        );

        echo json_encode($data);
    }


    public function machineSource(Request $request){
        if($request->ajax()){
            $source_output = '';

            $sources = SourceLoc::where('mach_num', $request->machine_num)->get();

            $source_row = $sources->count();

            // print_r($source_row);
            // die();

            if($source_row > 0){

                $i = 1;
                $source_output .= ' <tr class="show_data">
                                        <td></td>
                                        <td><b>Source</b></td>
                                        <td><b>Source Model</b></td>
                                        <td><b>Installation Date</b></td>
                                        <td colspan="5"></td>
                                    </tr>';
                foreach($sources as $key => $source){
                    $source_output .= '
                    <tr class="show_data">
                        <td></td>
                        <td>'.$source->source_num.'</td>
                        <td>'.$source->s_mod_num.'</td> 
                        <td>'.date('d M Y', strtotime($source->inst_date)).'</td> 
                        <td colspan="5"></td>
                    </tr>
                    ';
                    $i++;
                }
            }else{
                $source_output = '
                <tr class="show_data">
                    <td colspan="9" class="text-center">
                        <p>Source Not Found.</p>
                    </td> 
                </tr>
                ';
            }
            
            $data = array(
                'source_table_data'  => $source_output,
            );
    
            echo json_encode($data);
        }
    }


}
