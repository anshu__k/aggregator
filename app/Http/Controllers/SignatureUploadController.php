<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Signature;

class SignatureUploadController extends Controller
{
    public function index()
    {
        $signature = Signature::where('user_id', auth()->user()->id)->orderBy('id','desc')->first();
        return view('signature.index', compact('signature'));
    }

    public function create()
    {
        return view('signature.create');
    }

    public function store(Request $request)
    {
        $rules = [
         'image'   => 'required|image|mimes:jpeg,bmp,png,jpg|max:100',

        ];

        $message = [
            'image.required'   => 'Signature Field Is Required.',
            'image.image'   => 'Please Select Image.',
            'image.mimes'   => 'Please Select jpeg, bmp, png, jpg This Type Extension.',
            'image.max'   => 'Maximum file size to upload is 0.1MB (100KB). If you are uploading a photo, try to reduce its resolution to make it under 0.1MB (100KB).',
        ];

        $this->validate($request, $rules, $message);

        if ($request->hasFile('image')) {
        	$image = $request->file('image');
            $imageName = pathinfo($image->getClientOriginalName(),PATHINFO_FILENAME).'_'.time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('signature/');
            $image->move($destinationPath, $imageName);

	        $data = [
	            'user_id'   => auth()->user()->id,
	            'image_name' => $imageName,
	        ];

	        $add_signatures = Signature::create($data);
        }

        return redirect()->route('signatures.index')->with('insert','Successfully Inserted Signature.');
    }

    public function destroy($id)
    {
        $signature = Signature::find($id)->delete();

        // $user = auth()->user();
        // activity()->performedOn($signature)
        //         ->causedBy($user)
        //         ->withProperties(['name' => $user->name, 'email' => $user->email])
        //         ->log('Deleted signature');

        return redirect()->route('signatures.index')->with('delete','Successfully Deleted Signature.');
    }
}
