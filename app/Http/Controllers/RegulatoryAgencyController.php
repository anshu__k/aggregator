<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Wipe;
use App\State;
use App\RegAgency;

class RegulatoryAgencyController extends Controller
{
    public function index()
    {
        $regdata = RegAgency::paginate(env('page'));
    	return view('regulatory_agency.index', compact('regdata'));
    }

    public function create()
    {
        $statelist = State::get(); 
        foreach ($statelist as $key => $state) {
            $statedata[$key]['state'] = $state['state'];
            $statedata[$key]['name'] = $state['statename'];
        }
    	return view('regulatory_agency.create', compact('statedata'));
    }

    public function store(Request $request){    
        $rules = [
            'r_fname'        => 'required',
            'r_lname'        => 'required',
            'r_title1'       => 'required',
            'r_title2'       => 'required',
            'r_phone'        => 'required',
            'rhonorific'        => 'required',
            'r_address1'       => 'required',
            'r_address2'       => 'required',
            'r_address3'        => 'required',
            'r_address4'        => 'required',
            'r_address5'       => 'required', 
            'r_address6'       => 'required',
            'send_reg'       => 'required',
            'must_reg'       => 'required',
            'state_id'        => 'required',
            'additional_info'        => 'required',
            'reg_days'        => 'required',
            'r_zip'        => 'required',
            'r_city'        => 'required'
        ];

        $message = [
            'r_fname.required'         => 'First Name Field Is Required.',
            'r_lname.required'         => 'Last Name Field Is Required.',
            'r_title1.required'        => 'Title1 Field Is Required.',
            'r_title2.required'        => 'Title2 Field Is Required',
            'r_phone.required'         => 'Phone Field Is Required.',
            'rhonorific.required'      => 'Rhonorific Field Is Required.',
            'r_address1.required'      => 'Address1 Field Is Required.',
            'r_address2.required'      => 'Address2 Field Is Required.',
            'r_address3.required'      => 'Address3 Field Is Required',
            'r_address4.required'      => 'Address4 Field Is Required',
            'r_address5.required'      => 'Address5 Field Is Required.',
            'r_address6.required'      => 'Address6 Field Is Required.',
            'send_reg.required'        => 'Send Regulatory Field Is Required.',
            'must_reg.required'        => 'Must Regulatory Field Is Required.',
            'state_id.required'        => 'State ID Field Is Required.',
            'additional_info.required' => 'Additional Field Is Required.',
            'reg_days.required'        => 'Reg Days Field Is Required.',
            'r_zip.required'           => 'R Zip Field Is Required.',
            'r_city.required'          => 'R City Field Is Required.'
        ];

        $this->validate($request, $rules, $message); 

        $regagency = new RegAgency();

        $regagency->r_fname = request('r_fname');
        $regagency->r_lname = request('r_lname');
        $regagency->r_title1 = request('r_title1');
        $regagency->r_title2 = request('r_title2');
        $regagency->r_phone = request('r_phone');
        $regagency->rhonorific = request('rhonorific');
        $regagency->raddress1 = request('r_address1');
        $regagency->raddress2 = request('r_address2');
        $regagency->raddress3 = request('r_address3');
        $regagency->raddress4 = request('r_address4');
        $regagency->raddress5 = request('r_address5');
        $regagency->raddress6 = request('r_address6');
        $regagency->send_reg = request('send_reg');
        $regagency->must_reg = request('must_reg');
        $regagency->state_id = request('state_id');
        $regagency->additionalinfo = request('additional_info');
        $regagency->reg_days = request('reg_days');
        $regagency->r_zip = request('r_zip');
        $regagency->r_city = request('r_city');
        $regagency->save();

    	return redirect()->route('regulatory_agency.index')->with('insert', 'Regulatory Agency Added Successfully.');
    }

    public function edit($id)
    {
        $statelist = State::get(); 
        foreach ($statelist as $key => $state) {
            $statedata[$key]['state'] = $state['state'];
            $statedata[$key]['name'] = $state['statename'];
        }

        $regagency = RegAgency::find($id); 
    	return view('regulatory_agency.edit', compact('statedata', 'regagency'));
    }

    public function update(Request $request, $id)
    {    
        $data  =  [];
        $rules = [
            'r_fname'        => 'required',
            'r_lname'        => 'required',
            'r_title1'       => 'required',
            'r_title2'       => 'required',
            'r_phone'        => 'required',
            'rhonorific'        => 'required',
            'r_address1'       => 'required',
            'r_address2'       => 'required',
            'r_address3'        => 'required',
            'r_address4'        => 'required',
            'r_address5'       => 'required', 
            'r_address6'       => 'required',
            'send_reg'       => 'required',
            'must_reg'       => 'required',
            'state_id'        => 'required',
            'additional_info'        => 'required',
            'reg_days'        => 'required',
            'r_zip'        => 'required',
            'r_city'        => 'required'
        ];

        $message = [
            'r_fname.required'         => 'First Name Field Is Required.',
            'r_lname.required'         => 'Last Name Field Is Required.',
            'r_title1.required'        => 'Title1 Field Is Required.',
            'r_title2.required'        => 'Title2 Field Is Required',
            'r_phone.required'         => 'Phone Field Is Required.',
            'rhonorific.required'      => 'Rhonorific Field Is Required.',
            'r_address1.required'      => 'Address1 Field Is Required.',
            'r_address2.required'      => 'Address2 Field Is Required.',
            'r_address3.required'      => 'Address3 Field Is Required',
            'r_address4.required'      => 'Address4 Field Is Required',
            'r_address5.required'      => 'Address5 Field Is Required.',
            'r_address6.required'      => 'Address6 Field Is Required.',
            'send_reg.required'        => 'Send Regulatory Field Is Required.',
            'must_reg.required'        => 'Must Regulatory Field Is Required.',
            'state_id.required'        => 'State ID Field Is Required.',
            'additional_info.required' => 'Additional Field Is Required.',
            'reg_days.required'        => 'Reg Days Field Is Required.',
            'r_zip.required'           => 'R Zip Field Is Required.',
            'r_city.required'          => 'R City Field Is Required.'
        ];

        $this->validate($request, $rules, $message);

        $data = RegAgency::find($id); 
        //dd($request['r_fname']);
        $data->r_fname = $request['r_fname'];
        $data->r_lname = $request['r_lname'];
        $data->r_title1 = $request['r_title1'];
        $data->r_title2 = $request['r_title2'];
        $data->r_phone = $request['r_phone'];
        $data->rhonorific = $request['rhonorific'];
        $data->raddress1 = $request['r_address1'];
        $data->raddress2 = $request['r_address2'];
        $data->raddress3 = $request['r_address3'];
        $data->raddress4 = $request['r_address4'];
        $data->raddress5 = $request['r_address5'];
        $data->raddress6 = $request['r_address6'];
        $data->send_reg = $request['send_reg'];
        $data->must_reg = $request['must_reg'];
        $data->state_id = $request['state_id'];
        $data->additionalinfo = $request['additional_info'];
        $data->reg_days = $request['reg_days'];
        $data->r_zip = $request['r_zip'];
        $data->r_city = $request['r_city'];

        $data->save();
        // $user = auth()->user();
        // activity()->performedOn($data)
        //         ->causedBy($user)
        //         ->withProperties(['name' => $user->name, 'email' => $user->email]])
        //         ->tap(function(Activity $activity ) use ($customer) {
        //             $activity->customer_id = $customer->id;
        //         })
        //        ->log('Updated Pre Assigned Source');

    	return  redirect()->route('regulatory_agency.index')->with('update', 'Pre-Assign Source Update Successfully.');
    }

    public function destroy($id)
    {   //dd($id);
        RegAgency::find($id)->delete();

        // $user = auth()->user();
        // activity()->performedOn($add_sourceloc)
        //         ->causedBy($user)
               
        //         ->withProperties(['name' => $user->name, 'email' => $user->email,'machine_num'=>$data['mach_num']])
        //         // ->tap(function(Activity $activity ) use ($customer) {
        //         //     $activity->customer_id = $customer->id;
        //         // })
        //         ->log('Updated Pre Assigned Source');
        return redirect()->route('regulatory_agency.index')->with('delete','Regulatory Agency Deleted Successfully.');
    }
}
