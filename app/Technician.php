<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Technician extends Model 
{
    use SoftDeletes;

    protected $table = 'technicians';
    protected $fillable = [
        'tech_name', 't_firstname', 't_lastname', 'current','type'
    ];

public function tech_type(){
        return $this->hasOne('App\TechnicianType','id','type');
    }
}
