<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Services\PoplarAPIService;
use App\ScriptRecordSentPostcard;

class ProcessPoplarPostCardSend implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $poplarAPI;
    protected $dataObject;
    protected $requestData;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($dataObject, $requestData = array())
    {
        // $this->poplarAPI = new PoplarAPIService;
        $this->dataObject = $dataObject;
        $this->requestData = $requestData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            // dd($this->dataObject);
            $poplarAPI = new PoplarAPIService;
            $input = $this->requestData;
            $this->dataObject->each(function($item, $key) use ($input, $poplarAPI) {
                $responsedata = $poplarAPI->send_mailing_postcard($input, $item);
                
                $storedData = ScriptRecordSentPostcard::updateOrCreate([
                    'mailing_id' => $responsedata->id,
                    'campaign_id' => $responsedata->campaign_id,
                ], [
                    'mailing_id' => $responsedata->id,
                    'campaign_id' => $responsedata->campaign_id,
                    'creative_id' => $responsedata->creative_id,
                    'response_data' => json_encode($responsedata),
                    'script_record_id' => $item->id,
                ]);
            });
            return true;
            
        } catch(\Exception $ex) {
            \Log::info('Job Failed');
            \Log::info($ex->getMessage());
        }
    }
}
