<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\MailchimpCampain;
use App\MailchimpCampainLink;

class MailchimpCampainLinkMember extends Model
{
    //maichimp_campaign_link_members
    protected $table = 'maichimp_campaign_link_members';

    protected $fillable = [
        'campaign_id',
        'link_id',
        'url_id',
        'email_id',
        'email_address',
        'click',
        'vip',
        'contact_status'
    ];

    public function mailchimp_campaign()
    {
        return $this->belongsTo(MailchimpCampain::class, 'campaign_id', 'campaign_id');
    }

    // public function mailchimp_campaign_link() {
    //     // return $this->belongsToMany(MailchimpCampainLink::class, 'maichimp_campaign_links', 'link_id', 'url_id');
    //     return $this->hasMany(MailchimpCampainLink::class, 'link_id', 'url_id');
    // }
}
