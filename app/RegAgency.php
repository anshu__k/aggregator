<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RegAgency extends Model
{
    use SoftDeletes;

    protected $table = 'reg_agency';
     protected $fillable = [
        'r_fname', 'r_lname', 'state_id', 'raddress1', 'raddress2', 'raddress3', 'raddress4', 'raddress5', 'raddress6', 'r_zip', 'r_city', 'rtitile1', 'rtitile2', 'reg_days', 'send_reg', 'must_reg', 'additionalinfo', 'r_phone', 'rhonorific'
    ];



}
