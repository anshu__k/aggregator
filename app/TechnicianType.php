<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class TechnicianType extends Model 
{

    protected $table = 'technician_type';
    protected $fillable = [
        'technician_type'
    ];
}
