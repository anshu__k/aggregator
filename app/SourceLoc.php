<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SourceLoc extends Model
{
	use SoftDeletes;

    protected $table = 'sourc_locs';

    protected $fillable = [
    	'mach_num', 'source_num', 'inst_date','contact_id', 's_mod_num', 's_ship_date', 's_status', 'currentloc', 'print_job', 'tech', 'note_num',
    ];

    public function machine_num(){
    	return $this->hasOne('App\Machine', 'mach_number', 'mach_num');
    }
    public function machine_num_id(){
        return $this->hasOne('App\Machine', 'id', 'mach_num');
    }
    public function contact_name(){
        return $this->hasOne('App\Contact', 'id', 'contact_id');
    }
    public function customer_number(){
        return $this->hasOne('App\Customer', 'id', 'renewable_wipe_test');
    }
    public function source_model(){
        return $this->hasOne('App\SModList', 's_mod_num', 's_mod_num');
    }
    public function source_model_first(){
        return $this->hasOne('App\Source', 'source_num', 'source_num');
    }

    public function source_status(){
        return $this->hasOne('App\SStats', 's_status', 's_status');
    }

    public function source_ship_method(){
    	return $this->hasOne('App\SStats', 's_status', 's_status');
    }
    public function source_ship_method_prev(){
        return $this->hasOne('App\SStats', 's_status', 's_status_prev');
    }
    public function techname(){
        return $this->hasOne('App\Technician', 'id', 'tech');
    }
}
