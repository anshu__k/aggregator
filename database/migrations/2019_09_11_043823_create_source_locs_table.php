<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSourceLocsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sourc_locs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('mach_num', 191);
            $table->string('source_num', 191);
            $table->string('inst_date', 191);
            $table->string('s_mod_num', 191);
            $table->string('s_ship_date', 191);
            $table->string('s_status', 191);
            $table->string('currentloc', 191);
            $table->string('print_job', 191);
            $table->string('tech', 191);
            $table->string('note_num', 191);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sourc_locs');
    }
}
