<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterScriptRecordsTableAddAddressColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            if (Schema::hasTable('script_records')) {
                Schema::table('script_records', function (Blueprint $table) {
                    if (!Schema::hasColumn('script_records', 'full_address')) {
                        $table->string('full_address')->after('county')->nullable();
                    }
                    if (!Schema::hasColumn('script_records', 'address1')) {
                        $table->string('address1')->after('lastName')->nullable();
                    }
                    if (!Schema::hasColumn('script_records', 'address2')) {
                        $table->string('address2')->after('address1')->nullable();
                    }

                    if (!Schema::hasColumn('script_records', 'licenseType')) {
                        $table->string('licenseType')->nullable()->after('licenseNumber');
                    }
                    if (!Schema::hasColumn('script_records', 'licenseStatus')) {
                        $table->string('licenseStatus')->nullable()->after('licenseType');
                    }
                    if (!Schema::hasColumn('script_records', 'licenseStatusAdditionalText')) {
                        $table->text('licenseStatusAdditionalText')->nullable()->after('licenseStatus');
                    }
                    if (!Schema::hasColumn('script_records', 'licence_expiration_date')) {
                        $table->timestamp('licence_expiration_date')->nullable()->after('licenseStatusAdditionalText');
                    }
                    if (!Schema::hasColumn('script_records', 'licence_issuance_date')) {
                        $table->timestamp('licence_issuance_date')->nullable()->after('licence_expiration_date');
                    }
                    if (!Schema::hasColumn('script_records', 'licenseSecondaryStatus')) {
                        $table->string('licenseSecondaryStatus')->nullable()->after('licence_issuance_date');
                    }
                });
            }
        } catch (\Exception $e) {
            echo "Something went wrong, check logs";
            Log::info($e);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            if (Schema::hasTable('script_records')) {
                Schema::table('script_records', function (Blueprint $table) {
                    if (Schema::hasColumn('script_records', 'full_address')) {
                        $table->dropColumn('full_address');
                    }
                    if (Schema::hasColumn('script_records', 'address1')) {
                        $table->dropColumn('address1');
                    }
                    if (Schema::hasColumn('script_records', 'address2')) {
                        $table->dropColumn('address2');
                    }
                    if (Schema::hasColumn('script_records', 'licenseType')) {
                        $table->dropColumn('licenseType');
                    }
                    if (Schema::hasColumn('script_records', 'licenseStatus')) {
                        $table->dropColumn('licenseStatus');
                    }
                    if (Schema::hasColumn('script_records', 'licenseStatusAdditionalText')) {
                        $table->dropColumn('licenseStatusAdditionalText');
                    }
                    if (Schema::hasColumn('script_records', 'licence_expiration_date')) {
                        $table->dropColumn('licence_expiration_date');
                    }
                    if (Schema::hasColumn('script_records', 'licence_issuance_date')) {
                        $table->dropColumn('licence_issuance_date');
                    }
                    if (Schema::hasColumn('script_records', 'licenseSecondaryStatus')) {
                        $table->dropColumn('licenseSecondaryStatus');
                    }
                });
            }
        } catch (\Exception $e) {
            echo "Something went wrong, check logs";
            Log::info($e);
        }
    }
}
