<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSModListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('smodlist', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('s_mod_num', 191);
            $table->string('millicurie', 191);
            $table->string('desc', 191);
            $table->string('gbq', 191);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('smodlist');
    }
}
