<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecordWipeTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wipes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('source_num', 191);
            $table->string('tech', 191);
            $table->string('wipe_date', 191);
            $table->string('discrepcod', 191);
            $table->string('wipe_conf', 191);
            $table->string('meas_date', 191);
            $table->string('print_job', 191);
            $table->string('meas_by', 191);
            $table->string('mach_num', 191);
            $table->string('cust_num', 191);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wipes');
    }
}
