<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('customer_id');
            $table->string('contact_name','400');
            $table->string('salutation','45')->nullable($value = true);
            $table->string('contact_title')->nullable($value = true);
            $table->string('contact_email','400')->nullable($value = true);
            $table->string('contact_phone','20')->nullable($value = true);
            $table->text('address')->nullable($value = true);
            $table->string('customer_active','250')->nullable($value = true);
            $table->string('expiration_date','250')->nullable($value = true);
            $table->date('effective_date')->nullable($value = true);
            $table->date('expiry_date')->nullable($value = true);
            $table->tinyInteger('is_deleted')->default(0);
            $table->string('status','45')->nullable(true);
            
            $table->integer('created_by')->nullable(true);
            $table->integer('updated_by')->nullable(true);
            $table->integer('deleted_by')->nullable(true);
            $table->softDeletes();
            $table->timestamps();

            // $table->foreign('customer_id')
            //         ->references('id')
            //         ->on('customers')
            //         ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
