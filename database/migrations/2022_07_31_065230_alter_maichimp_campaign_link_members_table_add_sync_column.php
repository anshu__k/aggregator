<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMaichimpCampaignLinkMembersTableAddSyncColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            if (Schema::hasTable('maichimp_campaign_link_members')) {
                Schema::table('maichimp_campaign_link_members', function (Blueprint $table) {
                    if (Schema::hasColumn('maichimp_campaign_link_members', 'updated_at')) {
                        \DB::statement("ALTER TABLE maichimp_campaign_link_members MODIFY COLUMN updated_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL;");
                    }
                    // 
                    if (!Schema::hasColumn('maichimp_campaign_link_members', 'sync_to_bullhorn')) {
                        $table->tinyInteger('sync_to_bullhorn')->after('contact_status')->default('0')->comment('0 = Not Sync | 1 = Synced');
                    }
                    if (!Schema::hasColumn('maichimp_campaign_link_members', 'bullhorn_contact_id')) {
                        $table->string('bullhorn_contact_id')->after('sync_to_bullhorn')->nullable();
                    }
                    if (!Schema::hasColumn('maichimp_campaign_link_members', 'bullhorn_candidate_id')) {
                        $table->string('bullhorn_candidate_id')->after('bullhorn_contact_id')->nullable();
                    }
                });
            }
        } catch (\Exception $e) {
            echo "Something went wrong, check logs";
            Log::info($e);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            if (Schema::hasTable('maichimp_campaign_link_members')) {
                Schema::table('maichimp_campaign_link_members', function (Blueprint $table) {
                    if (Schema::hasColumn('maichimp_campaign_link_members', 'updated_at')) {
                        \DB::statement("ALTER TABLE maichimp_campaign_link_members MODIFY COLUMN updated_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL;");
                    }
                    if (Schema::hasColumn('maichimp_campaign_link_members', 'sync_to_bullhorn')) {
                        $table->dropColumn('sync_to_bullhorn');
                    }
                    if (Schema::hasColumn('maichimp_campaign_link_members', 'bullhorn_contact_id')) {
                        $table->dropColumn('bullhorn_contact_id');
                    }
                    if (Schema::hasColumn('maichimp_campaign_link_members', 'bullhorn_candidate_id')) {
                        $table->dropColumn('bullhorn_candidate_id');
                    }
                });
            }
        } catch (\Exception $e) {
            echo "Something went wrong, check logs";
            Log::info($e);
        }
    }
}
