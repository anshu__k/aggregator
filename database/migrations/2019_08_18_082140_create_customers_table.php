<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('customer_number','255');
            $table->string('filing_name','255');
            $table->string('customer_name','400');
            $table->text('address')->nullable($value = true);
            $table->string('city','255')->nullable($value = true);
            $table->string('state')->nullable($value = true);
            $table->string('zipcode')->nullable($value = true);
            $table->string('country','255')->nullable($value = true);
            $table->tinyInteger('wipetest_inhouse')->nullable($value = true);
            $table->string('wipetest_frequency')->nullable($value = true);
            $table->text('notes')->nullable($value = true);
            $table->tinyInteger('is_deleted')->default(0);

            $table->integer('created_by')->nullable(true);
            $table->integer('updated_by')->nullable(true);
            $table->integer('deleted_by')->nullable(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
