<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoplarCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            if (!Schema::hasTable('poplar_campaigns')) {
                Schema::create('poplar_campaigns', function (Blueprint $table) {
                    $table->bigIncrements('id');
                    $table->string('campaign_id', 191);
                    $table->string('campaign_name', 512);
                    $table->tinyInteger('status')->default(1)->comment('0 = Inactive | 1 = Active');
                    $table->timestamps();
                });
            }
        } catch (\Exception $e) {
            echo "Something went wrong, check logs";
            \Log::info($e);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            Schema::dropIfExists('poplar_campaigns');
        } catch (\Exception $e) {
            echo "Something went wrong, check logs";
            \Log::info($e);
        }
    }
}
