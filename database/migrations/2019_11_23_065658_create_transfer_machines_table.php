<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransferMachinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('machine_transfers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('mach_num', 191);
            $table->string('customer_num', 191);
            $table->string('ship_date', 191);
            $table->string('install_date', 191);
            $table->string('source_ship_method', 191);
            $table->string('tech', 191);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('machine_transfers');
    }
}
