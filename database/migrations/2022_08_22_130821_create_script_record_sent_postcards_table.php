<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScriptRecordSentPostcardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('script_record_sent_postcards', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('script_record_id')->comment('Foreign Key Constrain from Script record id.');
            $table->string('mailing_id')->comment('Mailing Sent ID');
            $table->string('campaign_id')->comment('Poplar Campaign ID');
            $table->string('creative_id');
            $table->longText('response_data');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('script_record_sent_postcards');
    }
}
