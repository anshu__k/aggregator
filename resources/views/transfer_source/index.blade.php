@extends('layout.app')

@section('content')
<style type="text/css">
        .confirmWipeTest{
            height: calc(1.25rem + 2px);
        }
    </style>

	<!-- Content -->
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
        	<!--  Traffic  -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <!-- Error & Message -->
                                @if (session()->has('insert'))
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        {{ session()->get('insert') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @elseif (session()->has('update'))
                                    <div class="alert alert-info alert-dismissible" role="alert">
                                        {{ session()->get('update') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @elseif (session()->has('delete'))
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        {{ session()->get('delete') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                            <!-- Error & Message -->
                            <div class="row form-group">
                                <div class="col-md-3">
                                    <h4 class="box-title">Transfer Source List </h4>
                                </div>

                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input class="form-control"  name="search_term" id="search_wipe_term" placeholder="Search..">
                                        <div class="input-group-btn">
                                            <button type="button" id="search_wipe" class="btn btn-outline-primary" >Search</button>
                                        </div>

                                        <a class="btn input-group-btn"href="{{ route('TransferSou.index') }}" >Reset</a>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <a href="{{ route('TransferSou.create') }}#idc" class="btn btn-outline-success float-right"><i class="fa fa-plus"></i> ADD</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body nopadding">
                            <div class="col-md-12 table-stats order-table ov-h nopadding">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <!-- <th>#</th> -->
                                            <th>Source Number</th>
                                            <th>Machine Number</th>
                                            <th>Source Model</th>
                                            <th>Source Status</th>
                                            <th>Ship Date</th>
                                            <!-- <th>Installation Date</th> -->
                                            <th>Confirm</th>
                                            <th colspan="2" style="text-align: left;">Action</th>
                                        </tr>
                                    </thead>

                                    <tbody class="table_format">
                                        @forelse($transfer_sources as $key => $transfer_source)
                                            <tr>
                                                <!-- <td>{{ $transfer_sources->firstItem() + $key }}</td>  -->
                                                <td>{{ $transfer_source->source_num }}</td> 
                                                <td>{{ $transfer_source->mach_num }}</td> 
                                                <td>{{ $transfer_source->s_mod_num }}</td> 
                                                <td>{{ $transfer_source->source_status->sstat_desc }}</td> 
                                                <td>{{ date('m/d/y', strtotime($transfer_source->s_ship_date)) }}</td> 
                                                <!-- <td>{{ ($transfer_source->inst_date=="")?'-':date('m/d/y', strtotime($transfer_source->inst_date))  }}</td> -->
                                                <td> 
                                                    <!-- <input type="checkbox" sourcelogid="{{ $transfer_source->id }}" source_num="{{ $transfer_source->source_num }}" mach_number="{{ $transfer_source->mach_num }}" name="confirm" class="form-control confirmWipeTest" id="confirmWipeTest">  -->
                                                    <a href="{{ route('TransferSou.confirm', $transfer_source) }}" class="btn btn-primary">
                                                        Confirm Source Returned
                                                    </a>
                                                </td>

                                                <td style="text-align: left; width: 5%;">
                                                    
                                                    <a href="{{ route('TransferSou.edit', $transfer_source) }}">
                                                       <i class="fa fa-edit edit_btn" rel="tooltip" title="Edit"></i>
                                                    </a>
                                                    
                                                </td>
                                                <td style="text-align: left;">
                                                    <form action="{{ route('TransferSou.destroy', $transfer_source) }}" method="POST">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <button type="submit" class="fabutton" onclick="return myFunction();">
                                                            <i class="fa fa-trash dlt_btn" rel="tooltip" title="Delete"></i>
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                              <td colspan="7" class="text-center">
                                                <p>Transfer Source Not Found.</p>
                                              </td> 
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div> <!-- /.col-md-12 -->
                        </div><!-- card-body -->
                        <div class="card-body">
                            {{ $transfer_sources->onEachSide(1)->links() }}
                        </div>
                        <div class="card-body"></div>
                    </div>
                </div><!-- /# column -->
            </div>
            <!--  /Traffic -->
            <div class="clearfix"></div>
        </div>
    	<!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>

     <!-- ========================== Modal pop-up=====================-->
    <div class="modal fade" id="confirmWipe" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: darkgrey">
                    <h5 class="modal-title">Confirm Transfer source </h5>
                </div>
                <div class="modal-body">
                    <div class="row" style="padding-left: 15px;">
                        <h5> Are you sure, you want to update this transter source? </h5>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="confirmYes" class="btn btn-success">Yes</button>
                    <button type="button" id="confirmNo" class="btn btn-danger">No</button>
                </div>
            </div>      
        </div>
    </div>
    <input type="hidden" class="source_num">
    <input type="hidden" class="mach_num">
    <input type="hidden" class="sourcelogid">


    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery('.confirmWipeTest').click(function(){
                jQuery('.source_num').val(jQuery(this).attr('source_num'));
                jQuery('.mach_num').val(jQuery(this).attr('mach_number'));
                jQuery('.sourcelogid').val(jQuery(this).attr('sourcelogid'));
                jQuery('#confirmWipe').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                jQuery('#confirmWipe').modal('show');
            });

            jQuery('#confirmYes').click(function(){
                var sourcelog = jQuery('.sourcelogid').val();
                jQuery.ajax({
                    type: "GET",
                    dataType: "JSON",
                    data:{"sourceloc_id" : sourcelog},
                    url: "{{ route('update-source-log') }}",
                    success: function(data) {
                        location.reload();
                        //jQuery("tr[source-log="+sourcelog+"]").hide();
                        //jQuery('#confirmWipe').modal('hide');
                    },
                    error:function(error){
                        console.log('error');
                    }
                });
            });

            jQuery('#confirmNo').click(function(){
                var sourcelog = jQuery('.sourcelogid').val();
                jQuery("input[sourcelogid="+sourcelog+"]").prop("checked", false);
                jQuery('#confirmWipe').modal('hide');
            });
        });
    </script>

@endsection