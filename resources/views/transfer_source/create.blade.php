@extends('layout.app')
@section('content')
	
	<!-- Content -->
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
        	<!--  Traffic  -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                        	<div class="row">
                        		<div class="col-lg-6">
                    				<h4 class="box-title">Add Transfer Source </h4>
                        		</div>
                        		<div class="col-lg-6">
                        			<a href="{{ route('TransferSou.index') }}" class="btn btn-outline-secondary float-right"><i class="fa fa-angle-double-left"></i> Back</a>
                        		</div>
                        	</div>
                        </div>
                        <div class="card-body">
                        	<form action="{{ route('TransferSou.store') }}" method="POST" enctype="multipart/form-data">
                        		{{csrf_field()}}
                                <input type="hidden" name="active_tab" id="active_tab" value="">

                        		<div class="box_border">
                        			<div class="heading_font_size font-weight-bold">
                        				1. Select And Verify The Source Information.
                        			</div>

                        			<div class="form-group ml-5 mt-3">
                        				<div class="row">
                        					<div class="col-md-3">
                        						<label class="label_font_size" for="source_number">Source Being Issued :</label>
                        					</div>
                        					<div class="col-md-6">
                        						<select class="form-control select_transfer_source_model input_size" name="source_number" id="source_number" value="{{ old('source_number') }}"></select>

                        						@if ($errors->has('source_number'))
                        						<div class="text-danger">{{ $errors->first('source_number') }}</div>
                        						@endif
                        					</div>
                        				</div>
                        			</div>

                        			<div class="form-group ml-5 mt-3">
                        				<div class="row">
                        					<div class="col-md-3">
                        						<label class="label_font_size" for="source_status">Current Source Status :</label>
                        					</div>
                        					<div class="col-md-6">
                        						<input type="text" name="source_status" id="source_status" class="form-control input_size" value="{{ old('source_status') }}" readonly>
                        						@if ($errors->has('source_status'))
                        						<div class="text-danger">{{ $errors->first('source_status') }}</div>
                        						@endif
                        					</div>
                        				</div>
                        			</div>

                        			<div class="form-group ml-5 mt-3">
                        				<div class="row">
                        					<div class="col-md-3">
                        						<label class="label_font_size" for="current_source_model">Current Source Model :</label>
                        					</div>
                        					<div class="col-md-6">
                        						<input type="text" name="current_source_model" id="current_source_model" class="form-control input_size" value="{{ old('current_source_model') }}" readonly>
                        						@if ($errors->has('current_source_model'))
                        						<div class="text-danger">{{ $errors->first('current_source_model') }}</div>
                        						@endif
                        					</div>
                        				</div>
                        			</div>

                        			<div class="form-group ml-5 mt-5">
                        				<div class="row">
                        					<div class="col-md-3">
                        						<label class="label_font_size" for="current_mach">Current Machine :</label>
                        					</div>
                        					<div class="col-md-6">
                        						<input type="text" name="current_mach" id="current_mach" class="form-control input_size" value="{{ old('current_mach') }}" readonly>
                        						@if ($errors->has('current_mach'))
                        						<div class="text-danger">{{ $errors->first('current_mach') }}</div>
                        						@endif
                        					</div>
                        				</div>
                        			</div>

                        			<div class="form-group ml-5 mt-3">
                        				<div class="row">
                        					<div class="col-md-3">
                        						<label class="label_font_size" for="current_mach_status">Current Machine Status :</label>
                        					</div>
                        					<div class="col-md-6">
                        						<input type="text" name="current_mach_status" id="current_mach_status" class="form-control input_size" value="{{ old('current_mach_status') }}" readonly>
                        						@if ($errors->has('current_mach_status'))
                        						<div class="text-danger">{{ $errors->first('current_mach_status') }}</div>
                        						@endif
                        					</div>
                        				</div>
                        			</div>

                        			<div class="form-group ml-5 mt-3">
                        				<div class="row">
                        					<div class="col-md-3">
                        						<label class="label_font_size" for="current_customer">Current Customer :</label>
                        					</div>
                        					<div class="col-md-6">
                        						<input type="text" name="current_customer" id="current_customer" class="form-control input_size" value="{{ old('current_customer') }}" readonly>
                        						@if ($errors->has('current_customer'))
                        						<div class="text-danger">{{ $errors->first('current_customer') }}</div>
                        						@endif
                        					</div>
                        				</div>
                        			</div>
                        		</div>

                        		<div class="box_border mt-4">
                        			<div class="heading_font_size font-weight-bold">
                        				2. Select The Transfering Type And Enter Corresponding Data.
                        			</div>
                        			<div class="row mt-3">
                        				<div class="col-lg-12">
                        					<ul class="nav nav-tabs" role="tablist">
                        						<li class="nav-item" style="width: 10%;">
                        							<a class="nav-link active" id="machine_tab" href="#machine" role="tab" data-toggle="tab" style="text-align: center;" onclick="location.href='#machine'">Steps</a>
                        						</li>
                        						<!-- <li class="nav-item" style="width: 10%;">
                        							<a class="nav-link" href="#storage" id="storage_tab" role="tab" data-toggle="tab" style="text-align: center;" onclick="location.href='#storage'">Storage</a>
                        						</li>
                        						<li class="nav-item" style="width: 10%;">
                        							<a class="nav-link" href="#disposal" id="disposal_tab" role="tab" data-toggle="tab" style="text-align: center;" onclick="location.href='#disposal'">Disposal</a>
                        						</li>
                        						<li class="nav-item" style="width: 10%;">
                        							<a class="nav-link" href="#lost" id="lost_tab" role="tab" data-toggle="tab" style="text-align: center;" onclick="location.href='#lost'">Lost</a>
                        						</li> -->
                        						<li class="nav-item" style="width: 10%;">
                        							<a class="nav-link" href="#idc" id="idc_tab" role="tab" data-toggle="tab" style="text-align: center;" onclick="location.href='#idc'">IDC</a>
                        						</li>
                        					</ul>
                        				</div>
                        			</div>

                        			<div class="tab-content">

                                        <!-- Machine -->
                        				<div role="tabpanel" class="tab-pane mt-3 active" id="machine">
                        					<div class="box_border">
                        						<div class="heading_font_size font-weight-bold">
                        							To transfer a source to a machine: Follow these steps below.
                        						</div>

                        						<div class="form-group ml-5 mt-3">
                        							<div class="row">
                        								<div class="col-md-6">
                        									<label class="label_font_size font-weight-bold">1. Transfer the source to IDC.</label><br>
                        									<label class="label_font_size font-weight-bold">2. Enter the 'removal' wipe test.</label><br>
                        									<label class="label_font_size font-weight-bold">3. Receive the source.</label><br>
                        									<label class="label_font_size font-weight-bold">4. Pre-assign the source.</label><br>
                        									<label class="label_font_size font-weight-bold">5. Confirm ship date.</label><br>
                        									<label class="label_font_size font-weight-bold">6. Install wipe date.</label><br>
                        								</div>
                        							</div>
                        						</div>
                        					</div>
                        				</div>

                                        <!-- Storage Tab -->
                        				<div role="tabpanel" class="tab-pane mt-3" id="storage">
                        					<div class="box_border">
                                                <div class="form-group ml-5 mt-3">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="label_font_size" for="rec_source_storage">Customer recieving source :</label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input type="text" name="rec_source_storage" id="rec_source_storage" class="form-control input_size" value="{{ old('rec_source_storage') }}" >
                                                            @if ($errors->has('rec_source_storage'))
                                                            <div class="text-danger">{{ $errors->first('rec_source_storage') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group ml-5 mt-3">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="label_font_size" for="cust_contact_storage">Customer contact :</label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input type="text" name="cust_contact_storage" id="cust_contact_storage" class="form-control input_size" value="{{ old('cust_contact_storage') }}" >
                                                            @if ($errors->has('cust_contact_storage'))
                                                            <div class="text-danger">{{ $errors->first('cust_contact_storage') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group ml-5 mt-3">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="label_font_size" for="ship_date_storage">Ship Date :</label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div id="add_ship_date_storage" class="input-group date" data-date-format="mm-dd-yyyy">
                                                                <input type="text" name="ship_date_storage" id="ship_date_storage" class="form-control input_size" value="{{ old('ship_date_storage') }}">
                                                                <span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                            @if ($errors->has('ship_date_storage'))
                                                            <div class="text-danger">{{ $errors->first('ship_date_storage') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group ml-5 mt-3">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="label_font_size" for="installation_date_storage">Installation Date :</label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div id="add_installation_date_storage" class="input-group date" data-date-format="mm-dd-yyyy">
                                                                <input type="text" name="installation_date_storage" id="installation_date_storage" class="form-control input_size" value="{{ old('installation_date_storage') }}">
                                                                <span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                            @if ($errors->has('installation_date_storage'))
                                                            <div class="text-danger">{{ $errors->first('installation_date_storage') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group ml-5 mt-3">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="label_font_size" for="source_model_storage">Source model :</label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input type="text" name="source_model_storage" id="source_model_storage" class="form-control input_size" value="{{ old('source_model_storage') }}" >
                                                            @if ($errors->has('source_model_storage'))
                                                            <div class="text-danger">{{ $errors->first('source_model_storage') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group ml-5 mt-3">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="label_font_size" for="source_status_storage">Source Status :</label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <select name="source_status_storage" id="source_status_storage" class="form-control input_size" value="{{ old('source_status_storage') }}">
                                                                <option selected disabled>Select Source Status</option>
                                                                @foreach($source_status_lists as $source_status)
                                                                <option value="{{ $source_status->s_status }}">{{ $source_status->sstat_desc }}</option>
                                                                @endforeach
                                                            </select>
                                                            @if ($errors->has('source_status_storage'))
                                                            <div class="text-danger">{{ $errors->first('source_status_storage') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group ml-5 mt-3">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="label_font_size" for="technician_storage">Technician :</label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <select class="form-control select_tech_model input_size" name="technician_storage" id="technician_storage" value="{{ old('technician_storage') }}"></select>

                                                            @if ($errors->has('technician_storage'))
                                                            <div class="text-danger">{{ $errors->first('technician_storage') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                        				</div>

                                        <!-- Disposal Tab -->
                        				<div role="tabpanel" class="tab-pane mt-3" id="disposal">
                        					<div class="box_border">
                                                <div class="form-group ml-5 mt-3">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="label_font_size" for="machine_disposal">Machine disposal source :</label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input type="text" name="machine_disposal" id="machine_disposal" class="form-control input_size" value="{{ old('machine_disposal') }}" >
                                                            @if ($errors->has('machine_disposal'))
                                                                <div class="text-danger">{{ $errors->first('machine_disposal') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group ml-5 mt-3">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="label_font_size" for="cust_contact_disposal">Customer contact :</label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input type="text" name="cust_contact_disposal" id="cust_contact_disposal" class="form-control input_size" value="{{ old('cust_contact_disposal') }}" >
                                                            @if ($errors->has('cust_contact_disposal'))
                                                                <div class="text-danger">{{ $errors->first('cust_contact_disposal') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group ml-5 mt-3">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="label_font_size" for="disposal_date">Disposal Date :</label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div id="add_disposal_date_disposal" class="input-group date" data-date-format="mm-dd-yyyy">
                                                                <input type="text" name="disposal_date" id="disposal_date" class="form-control input_size" value="{{ old('disposal_date') }}">
                                                                <span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                            @if ($errors->has('disposal_date'))
                                                                <div class="text-danger">{{ $errors->first('disposal_date') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group ml-5 mt-3">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="label_font_size" for="ship_date_disposal">Ship Date :</label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div id="add_ship_date_disposal" class="input-group date" data-date-format="mm-dd-yyyy">
                                                                <input type="text" name="ship_date_disposal" id="ship_date_disposal" class="form-control input_size" value="{{ old('ship_date_disposal') }}">
                                                                <span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                            @if ($errors->has('ship_date_disposal'))
                                                                <div class="text-danger">{{ $errors->first('ship_date_disposal') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group ml-5 mt-3">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="label_font_size" for="source_model_disposal">Source model :</label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input type="text" name="source_model_disposal" id="source_model_disposal" class="form-control input_size" value="{{ old('source_model_disposal') }}" >
                                                            @if ($errors->has('source_model_disposal'))
                                                                <div class="text-danger">{{ $errors->first('source_model_disposal') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group ml-5 mt-3">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="label_font_size" for="source_status_disposal">Source Status :</label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <select name="source_status_disposal" id="source_status_disposal" class="form-control input_size" value="{{ old('source_status_disposal') }}">
                                                                <option selected disabled>Select Source Status</option>
                                                                @foreach($source_status_lists as $source_status)
                                                                    <option value="{{ $source_status->s_status }}">{{ $source_status->sstat_desc }}</option>
                                                                @endforeach
                                                            </select>
                                                            @if ($errors->has('source_status_disposal'))
                                                                <div class="text-danger">{{ $errors->first('source_status_disposal') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group ml-5 mt-3">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="label_font_size" for="technician_disposal">Technician :</label>
                                                        </div>
                                                        <div class="col-md-6">

                                                            <select class="form-control select_tech_model input_size" name="technician_disposal" id="technician_disposal" value="{{ old('technician_disposal') }}"></select>

                                                            @if ($errors->has('technician_disposal'))
                                                                <div class="text-danger">{{ $errors->first('technician_disposal') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                        				</div>

                                        <!-- Lost Tab -->
                        				<div role="tabpanel" class="tab-pane mt-3" id="lost">
                        					<div class="box_border">
                                                <div class="form-group ml-5 mt-3">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="label_font_size" for="machine_lost">Machine lost source :</label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input type="text" name="machine_lost" id="machine_lost" class="form-control input_size" value="{{ old('machine_lost') }}" >
                                                            @if ($errors->has('machine_lost'))
                                                                <div class="text-danger">{{ $errors->first('machine_lost') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group ml-5 mt-3">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="label_font_size" for="cust_contact_lost">Customer contact :</label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input type="text" name="cust_contact_lost" id="cust_contact_lost" class="form-control input_size" value="{{ old('cust_contact_lost') }}" >
                                                            @if ($errors->has('cust_contact_lost'))
                                                                <div class="text-danger">{{ $errors->first('cust_contact_lost') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group ml-5 mt-3">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="label_font_size" for="lost_date">Lost Date :</label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div id="add_lost_date_lost" class="input-group date" data-date-format="mm-dd-yyyy">
                                                                <input type="text" name="lost_date" id="lost_date" class="form-control input_size" value="{{ old('lost_date') }}">
                                                                <span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                            @if ($errors->has('lost_date'))
                                                                <div class="text-danger">{{ $errors->first('lost_date') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group ml-5 mt-3">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="label_font_size" for="ship_date_lost">Ship Date :</label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div id="add_ship_date_lost" class="input-group date" data-date-format="mm-dd-yyyy">
                                                                <input type="text" name="ship_date_lost" id="ship_date_lost" class="form-control input_size" value="{{ old('ship_date_lost') }}">
                                                                <span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                            @if ($errors->has('ship_date_lost'))
                                                                <div class="text-danger">{{ $errors->first('ship_date_lost') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group ml-5 mt-3">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="label_font_size" for="source_model_lost">Source model :</label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input type="text" name="source_model_lost" id="source_model_lost" class="form-control input_size" value="{{ old('source_model_lost') }}" >
                                                            @if ($errors->has('source_model_lost'))
                                                                <div class="text-danger">{{ $errors->first('source_model_lost') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group ml-5 mt-3">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="label_font_size" for="source_status_lost">Source Status :</label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <select name="source_status_lost" id="source_status_lost" class="form-control input_size" value="{{ old('source_status_lost') }}">
                                                                <option selected disabled>Select Source Status</option>
                                                                @foreach($source_status_lists as $source_status)
                                                                    <option value="{{ $source_status->s_status }}">{{ $source_status->sstat_desc }}</option>
                                                                @endforeach
                                                            </select>
                                                            @if ($errors->has('source_status_lost'))
                                                                <div class="text-danger">{{ $errors->first('source_status_lost') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group ml-5 mt-3">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="label_font_size" for="technician_lost">Technician :</label>
                                                        </div>
                                                        <div class="col-md-6">

                                                            <select class="form-control select_tech_model input_size" name="technician_lost" id="technician_lost" value="{{ old('technician_lost') }}"></select>

                                                            @if ($errors->has('technician_lost'))
                                                                <div class="text-danger">{{ $errors->first('technician_lost') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                        				</div>

                                        <!-- IDC Tab -->
                        				<div role="tabpanel" class="tab-pane mt-3" id="idc">
                        					<div class="box_border">
                        						<!-- <div class="form-group ml-5 mt-3">
                        							<div class="row">
                        								<div class="col-md-3">
                        									<label class="label_font_size" for="rec_source_idc">Customer recieving source :</label>
                        								</div>
                        								<div class="col-md-6">
                        									<input type="text" name="rec_source_idc" id="rec_source_idc" class="form-control input_size" value="{{ old('rec_source_idc') }}" >
                        									@if ($errors->has('rec_source_idc'))
                        									<div class="text-danger">{{ $errors->first('rec_source_idc') }}</div>
                        									@endif
                        								</div>
                        							</div>
                        						</div>

                        						<div class="form-group ml-5 mt-3">
                        							<div class="row">
                        								<div class="col-md-3">
                        									<label class="label_font_size" for="cust_contact_idc">Customer contact :</label>
                        								</div>
                        								<div class="col-md-6">
                        									<input type="text" name="cust_contact_idc" id="cust_contact_idc" class="form-control input_size" value="{{ old('cust_contact_idc') }}" >
                        									@if ($errors->has('cust_contact_idc'))
                        									<div class="text-danger">{{ $errors->first('cust_contact_idc') }}</div>
                        									@endif
                        								</div>
                        							</div>
                        						</div> -->

                        						<div class="form-group ml-5 mt-3">
                        							<div class="row">
                        								<div class="col-md-3">
                        									<label class="label_font_size" for="ship_date_idc">Ship Date :</label>
                        								</div>
                        								<div class="col-md-6">
                        									<div id="add_ship_date_idc" class="input-group date" data-date-format="mm-dd-yyyy">
                        										<input type="text" name="ship_date_idc" id="ship_date_idc" class="form-control input_size" value="{{ old('ship_date_idc') }}">
                        										<span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
                        									</div>
                        									@if ($errors->has('ship_date_idc'))
                        									<div class="text-danger">{{ $errors->first('ship_date_idc') }}</div>
                        									@endif
                        								</div>
                        							</div>
                        						</div>

                        						<!-- <div class="form-group ml-5 mt-3">
                        							<div class="row">
                        								<div class="col-md-3">
                        									<label class="label_font_size" for="installation_date_idc">Installation Date :</label>
                        								</div>
                        								<div class="col-md-6">
                        									<div id="add_installation_date_idc" class="input-group date" data-date-format="mm-dd-yyyy">
                        										<input type="text" name="installation_date_idc" id="installation_date_idc" class="form-control input_size" value="{{ old('installation_date_idc') }}">
                        										<span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
                        									</div>
                        									@if ($errors->has('installation_date_idc'))
                        									<div class="text-danger">{{ $errors->first('installation_date_idc') }}</div>
                        									@endif
                        								</div>
                        							</div>
                        						</div> -->

                        						<div class="form-group ml-5 mt-3">
                        							<div class="row">
                        								<div class="col-md-3">
                        									<label class="label_font_size" for="source_model_idc">Source model :</label>
                        								</div>
                        								<div class="col-md-6">
                        									<!-- <input type="text" name="source_model_idc" id="source_model_idc" class="form-control input_size" value="{{ old('source_model_idc') }}" > -->
                        									<select name="source_model_idc" id="source_model_idc" class="form-control input_size" value="{{ old('source_model_idc') }}">
                                                                <option selected disabled>Select Source Model</option>
                                                                @foreach($source_mods as $source_mod)
                                                                <option value="{{ $source_mod->s_mod_num }}">{{ $source_mod->s_mod_num }}</option>
                                                                @endforeach
                                                            </select>

                                                            @if ($errors->has('source_model_idc'))
                        									<div class="text-danger">{{ $errors->first('source_model_idc') }}</div>
                        									@endif
                        								</div>
                        							</div>
                        						</div>

                        						<div class="form-group ml-5 mt-3">
                        							<div class="row">
                        								<div class="col-md-3">
                        									<label class="label_font_size" for="source_status_idc">Source Status :</label>
                        								</div>
                        								<div class="col-md-6">
                                                            <!-- <input type="text" name="source_status_idc" id="source_status_idc" class="form-control input_size" value="" > -->
                        									<select name="source_status_idc" id="source_status_idc" class="form-control input_size" value="{{ old('source_status_idc') }}">
                                                                <option selected disabled>Select Source Status</option>
                                                                @foreach($source_status_lists as $source_status)
                                                                <option value="{{ $source_status->s_status }}" {{'id'==$source_status->s_status?'selected':''}}>{{ $source_status->sstat_desc }}</option>
                                                                @endforeach
                                                            </select>
                        									@if ($errors->has('source_status_idc'))
                        									<div class="text-danger">{{ $errors->first('source_status_idc') }}</div>
                        									@endif
                        								</div>
                        							</div>
                        						</div>

                        						<div class="form-group ml-5 mt-3">
                        							<div class="row">
                        								<div class="col-md-3">
                        									<label class="label_font_size" for="technician_idc">Technician :</label>
                        								</div>
                        								<div class="col-md-6">
                                                            <select class="form-control select_tech_model input_size" name="technician_idc" id="technician_idc" value="{{ old('technician_idc') }}">
                                                                <option></option>
                                                                </select>
                        									@if ($errors->has('technician_idc'))
                        									<div class="text-danger">{{ $errors->first('technician_idc') }}</div>
                        									@endif
                        								</div>
                        							</div>
                        						</div>
                        					</div>
                        				</div>
                        			</div>
                        		</div>

                        		<div class="row">
                        			<div class="col-md-12 mt-4">
                        				<button type="submit" class="btn btn-outline-primary float-right"><i class="fa fa-save"></i> Save</button>
                        			</div>
                        		</div>
                        	</form>
                        </div>
                        <div class="card-body"></div>
                    </div>
                </div><!-- /# column -->
            </div>
            <!--  /Traffic -->
            <div class="clearfix"></div>
        </div>
    	<!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>
    <script>
    	jQuery(document).ready(function(){
    		var url = window.location.href.substr(window.location.href.lastIndexOf("/") + 1).split('#')[1];

            jQuery('#machine_tab').on('click', function(){
                jQuery('#active_tab').val('machine');
            });
            // jQuery('#storage_tab').on('click', function(){
            //     jQuery('#active_tab').val('storage');
            // });
            // jQuery('#disposal_tab').on('click', function(){
            //     jQuery('#active_tab').val('disposal');
            // });
            // jQuery('#lost_tab').on('click', function(){
            //     jQuery('#active_tab').val('lost');
            // });
            jQuery('#idc_tab').on('click', function(){
                jQuery('#active_tab').val('idc');
            });

    		if(url == 'storage'){
    			jQuery('.nav-item a[href="#machine"]').removeClass('active');
    			jQuery('.nav-item a[href="#storage"]').addClass('active show');
    			jQuery('.nav-item a[href="#storage"]').attr('aria-selected', 'true');
    			jQuery('#machine').removeClass('active');
    			jQuery('#storage').addClass('active');

    		}
    		if(url == 'disposal'){
    			jQuery('.nav-item a[href="#machine"]').removeClass('active');
    			jQuery('.nav-item a[href="#disposal"]').addClass('active show');
    			jQuery('.nav-item a[href="#disposal"]').attr('aria-selected', 'true');
    			jQuery('#machine').removeClass('active');
    			jQuery('#disposal').addClass('active');
                jQuery('#active_tab').val(url);
    		}
    		if(url == 'lost'){
    			jQuery('.nav-item a[href="#machine"]').removeClass('active');
    			jQuery('.nav-item a[href="#lost"]').addClass('active show');
    			jQuery('.nav-item a[href="#lost"]').attr('aria-selected', 'true');
    			jQuery('#machine').removeClass('active');
    			jQuery('#lost').addClass('active');
                jQuery('#active_tab').val(url);
    		}
    		if(url == 'idc'){
    			jQuery('.nav-item a[href="#machine"]').removeClass('active');
    			jQuery('.nav-item a[href="#idc"]').addClass('active show');
    			jQuery('.nav-item a[href="#idc"]').attr('aria-selected', 'true');
    			jQuery('#machine').removeClass('active');
    			jQuery('#idc').addClass('active');
                jQuery('#active_tab').val(url);
    		}

            jQuery("#add_ship_date_storage").datepicker({ 
                format: "mm/dd/yy",
                weekStart: 0,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                orientation: "auto"
            }).datepicker('update', new Date());

            jQuery("#add_installation_date_storage").datepicker({ 
                format: "mm/dd/yy",
                weekStart: 0,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                orientation: "auto"
            }).datepicker('update', new Date());

    		jQuery("#add_ship_date_idc").datepicker({ 
    			format: "mm/dd/yy",
    			weekStart: 0,
    			calendarWeeks: true,
    			autoclose: true,
    			todayHighlight: true,
    			orientation: "auto"
    		}).datepicker('update', new Date());

    		jQuery("#add_installation_date_idc").datepicker({ 
    			format: "mm/dd/yy",
    			weekStart: 0,
    			calendarWeeks: true,
    			autoclose: true,
    			todayHighlight: true,
    			orientation: "auto"
    		}).datepicker('update', new Date());

            jQuery("#add_lost_date_lost").datepicker({ 
                format: "mm/dd/yy",
                weekStart: 0,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                orientation: "auto"
            }).datepicker('update', new Date());

            jQuery("#add_ship_date_lost").datepicker({ 
                format: "mm/dd/yy",
                weekStart: 0,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                orientation: "auto"
            }).datepicker('update', new Date());

            jQuery("#add_disposal_date_disposal").datepicker({ 
                format: "mm/dd/yy",
                weekStart: 0,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                orientation: "auto"
            }).datepicker('update', new Date());

            jQuery("#add_ship_date_disposal").datepicker({ 
                format: "mm/dd/yy",
                weekStart: 0,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                orientation: "auto"
            }).datepicker('update', new Date());
    	});
    </script>

    <!-- Enter Source Number Fetch Pre-assign Source Data -->
        <script>
            jQuery(document).ready(function(){
                jQuery('.select_transfer_source_model').select2({
                    placeholder: 'Enter Source Number',
                    ajax: {
                        url: '{{ route("transferSourceAutoComplete") }}',
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: jQuery.trim(params.term)
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: data
                            };
                        },
                        cache: true
                    }
                });
            });

            jQuery(document).ready(function(){

                jQuery('.select_transfer_source_model').on('change', function(){
                    var source_id = jQuery(".select_transfer_source_model option:selected").val();
                    jQuery.ajax({
                        url:"{{ route('preAssignSourceAutofill') }}",
                        method:'GET',
                        data:{source_num:source_id},
                        dataType:'json',
                        success:function(data)
                        {
                            if(data.source_data.s_status != ''){
                                jQuery('#source_status').val(data.source_data.s_status);
                            }
                            if(data.source_data.s_mod_number != ''){
                                jQuery('#current_source_model').val(data.source_data.s_mod_number);
                                //jQuery('#source_model_idc').val(data.source_data.s_mod_number);
                            }
                            if(data.source_data.current_mach_number != ''){
                                jQuery('#current_mach').val(data.source_data.current_mach_number);
                            }
                            if(data.source_data.current_mach_status != ''){
                                jQuery('#current_mach_status').val(data.source_data.current_mach_status);
                            }
                            if(data.source_data.current_customer != ''){
                                jQuery('#current_customer').val(data.source_data.current_customer);
                            }
                            if(data.source_data.customer_contact != ''){
                                jQuery('#cust_contact_idc').val(data.source_data.customer_contact);
                            }
                            if(data.source_data.customer_rec_source != ''){
                                jQuery('#rec_source_idc').val(data.source_data.customer_rec_source);
                            }
                            // if(data.source_data.source_status != ''){
                            //     jQuery('#source_status_idc').html(html);
                            // }
                            // if(data.source_data.source_num != ''){
                            //     jQuery('#source_number').val(data.source_data.source_num);
                            // }
                        }
                    })
                });

                // jQuery('.select_machine_num').on('change', function(){
                //     var machine_number = jQuery(".select_machine_num option:selected").val();
                //     jQuery.ajax({
                //         url:"{{ route('preAssignMachineAutofill') }}",
                //         method:'GET',
                //         data:{machine_number:machine_number},
                //         dataType:'json',
                //         success:function(data)
                //         {
                //             if(data.machine_data.customer_number != ''){
                //                 jQuery('#customer_assign').val(data.machine_data.customer_number);
                //             }
                //             if(data.machine_data.customer_name != ''){
                //                 jQuery('#customer_contact').val(data.machine_data.customer_name);
                //             }
                //         }
                //     })
                // });
            });  
        </script>
    <!-- Enter Source Number Fetch Pre-assign Source Data -->
@endsection