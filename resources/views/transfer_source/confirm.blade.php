@extends('layout.app')
@section('content')
	
	<!-- Content -->
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
        	<!--  Traffic  -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                        	<div class="row">
                        		<div class="col-lg-6">
                    				<h4 class="box-title">Confirm Source Returned </h4>
                        		</div>
                        		<div class="col-lg-6">
                        			<a href="{{ route('TransferSou.index') }}" class="btn btn-outline-secondary float-right"><i class="fa fa-angle-double-left"></i> Back</a>
                        		</div>
                        	</div>
                        </div>
                        <div class="card-body">
                        	<form action="{{ route('update_confirmlog') }}" method="POST" enctype="multipart/form-data">
                        		{{csrf_field()}}
                                    <input name="id" type="hidden" value="{{ $transfer_source->id }}">

                        		<div class="box_border">

                        			<div class="form-group ml-5 mt-3">
                        				<div class="row">
                        					<div class="col-md-3">
                        						<label class="label_font_size" for="edit_source_number">Source Number :</label>
                        					</div>
                        					<div class="col-md-6">
                                                            <input type="text" name="edit_source_number" id="edit_source_number" class="form-control input_size" value="{{ $transfer_source->source_num }}" >

                        						@if ($errors->has('edit_source_number'))
                        						<div class="text-danger">{{ $errors->first('edit_source_number') }}</div>
                        						@endif
                        					</div>
                        				</div>
                        			</div>

                        			<div class="form-group ml-5 mt-3">
                        				<div class="row">
                        					<div class="col-md-3">
                        						<label class="label_font_size" for="edit_source_model">Source Model :</label>
                        					</div>
                        					<div class="col-md-6">
                        						<input type="text" name="edit_source_model" id="edit_source_model" class="form-control input_size" value="{{ $transfer_source->s_mod_num }}" >
                        						@if ($errors->has('edit_source_model'))
                        						<div class="text-danger">{{ $errors->first('edit_source_model') }}</div>
                        						@endif
                        					</div>
                        				</div>
                        			</div>

                        			<div class="form-group ml-5 mt-3">
                        				<div class="row">
                        					<div class="col-md-3">
                        						<label class="label_font_size" for="edit_machine_number">Machine Number :</label>
                        					</div>
                        					<div class="col-md-6">
                        						<input type="text" name="edit_machine_number" id="edit_machine_number" class="form-control input_size" value="{{ $transfer_source->mach_num }}" >
                        						@if ($errors->has('edit_machine_number'))
                        						<div class="text-danger">{{ $errors->first('edit_machine_number') }}</div>
                        						@endif
                        					</div>
                        				</div>
                        			</div>

                        			<div class="form-group ml-5 mt-3">
                        				<div class="row">
                        					<div class="col-md-3">
                        						<label class="label_font_size" for="edit_source_ship_date">Source Ship Date :</label>
                        					</div>
                        					<div class="col-md-6">
                                                <div id="select_edit_ship_date" class="input-group date" data-date-format="mm-dd-yyyy">
                        						<input type="text" name="edit_source_ship_date" id="edit_source_ship_date" class="form-control input_size" value="{{date('m/d/y',strtotime($transfer_source->s_ship_date))}}" >
                        						<span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
                                                </div>
                                                @if ($errors->has('edit_source_ship_date'))
                        						<div class="text-danger">{{ $errors->first('edit_source_ship_date') }}</div>
                        						@endif
                        					</div>
                        				</div>
                        			</div>

                        			<div class="form-group ml-5 mt-3">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="edit_inst_tech">Removal Technician :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <select class="form-control select_tech_model input_size" name="edit_inst_tech" id="edit_inst_tech" value="{{ old('edit_inst_tech') }}">
                                                <option value="{{ $transfer_source->tech }}">{{ $transfer_source->techname->tech_name }}</option></select>
                                                @if ($errors->has('edit_inst_tech'))
                                                <div class="text-danger">{{ $errors->first('edit_inst_tech') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group ml-5 mt-3">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="edit_recieved_date">Measure By:</label>
                                            </div>
                                            <div class="col-md-6">
                                                            <input type="text" name="edit_meas_by" id="edit_meas_by" class="form-control input_size" value="{{ $techrso->tech_name }}" >

                                                @if ($errors->has('edit_meas_by'))
                                                <div class="text-danger">{{ $errors->first('edit_meas_by') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group ml-5 mt-3">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="edit_meas_date">Measure Date:</label>
                                            </div>
                                            <div class="col-md-6">
                                                            <div id="select_edit_meas_date" class="input-group date" data-date-format="mm-dd-yyyy">
                                                                  <input type="text" name="edit_meas_date" id="edit_meas_date" class="form-control input_size" value="{{date('m/d/y',strtotime($transfer_source->s_ship_date))}}">
                                                                  <span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
                                                            </div>

                                                @if ($errors->has('edit_meas_number'))
                                                <div class="text-danger">{{ $errors->first('edit_meas_number') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group ml-5 mt-3">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="edit_recieved_date">Received Installation Technician By:</label>
                                            </div>
                                            <div class="col-md-6">
                                                            <input type="text" name="edit_rec" id="edit_rec" class="form-control input_size" value="" >

                                                @if ($errors->has('edit_rec'))
                                                <div class="text-danger">{{ $errors->first('edit_rec') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group ml-5 mt-3">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="edit_recieved_date">Received Date :</label>
                                            </div>
                                            <div class="col-md-6">
                                                            <div id="select_edit_recieved_date" class="input-group date" data-date-format="mm-dd-yyyy">
                                                                  <input type="text" name="edit_recieved_date" id="edit_recieved_date" class="form-control input_size" value="{{date('m/d/y',strtotime($transfer_source->s_ship_date))}}">
                                                                  <span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                @if ($errors->has('edit_recieved_date'))
                                                <div class="text-danger">{{ $errors->first('edit_recieved_date') }}</div>
                                                @endif
                                            </div>
                                    </div>
                        		</div>

                        		<div class="row">
                        			<div class="col-md-12 mt-4">
                        				<button type="submit" class="btn btn-outline-primary float-right">Confirm & generate wipe test</button>
                        			</div>
                        		</div>
                        	</form>
                        </div>
                        <div class="card-body"></div>
                    </div>
                </div><!-- /# column -->
            </div>
            <!--  /Traffic -->
            <div class="clearfix"></div>
        </div>
    	<!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>
    <script>
    	jQuery(document).ready(function(){
    		var url = window.location.href.substr(window.location.href.lastIndexOf("/") + 1).split('#')[1];
    		if(url == 'storage'){
    			jQuery('.nav-item a[href="#machine"]').removeClass('active');
    			jQuery('.nav-item a[href="#storage"]').addClass('active show');
    			jQuery('.nav-item a[href="#storage"]').attr('aria-selected', 'true');
    			jQuery('#machine').removeClass('active');
    			jQuery('#storage').addClass('active');
    		}
    		if(url == 'disposal'){
    			jQuery('.nav-item a[href="#machine"]').removeClass('active');
    			jQuery('.nav-item a[href="#disposal"]').addClass('active show');
    			jQuery('.nav-item a[href="#disposal"]').attr('aria-selected', 'true');
    			jQuery('#machine').removeClass('active');
    			jQuery('#disposal').addClass('active');
    		}
    		if(url == 'lost'){
    			jQuery('.nav-item a[href="#machine"]').removeClass('active');
    			jQuery('.nav-item a[href="#lost"]').addClass('active show');
    			jQuery('.nav-item a[href="#lost"]').attr('aria-selected', 'true');
    			jQuery('#machine').removeClass('active');
    			jQuery('#lost').addClass('active');
    		}
    		if(url == 'idc'){
    			jQuery('.nav-item a[href="#machine"]').removeClass('active');
    			jQuery('.nav-item a[href="#idc"]').addClass('active show');
    			jQuery('.nav-item a[href="#idc"]').attr('aria-selected', 'true');
    			jQuery('#machine').removeClass('active');
    			jQuery('#idc').addClass('active');
    		}

    		jQuery("#select_edit_recieved_date").datepicker({ 
    			format: "mm/dd/yy",
    			weekStart: 0,
    			calendarWeeks: true,
    			autoclose: true,
    			todayHighlight: true,
    			orientation: "auto"
    		});

    		jQuery("#select_edit_ship_date").datepicker({ 
    			format: "mm/dd/yy",
    			weekStart: 0,
    			calendarWeeks: true,
    			autoclose: true,
    			todayHighlight: true,
    			orientation: "auto"
    		});

            jQuery("#select_edit_meas_date").datepicker({ 
                format: "mm/dd/yy",
                weekStart: 0,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                orientation: "auto"
            });
    	});
    </script>
@endsection