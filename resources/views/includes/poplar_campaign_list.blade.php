@if(!empty($data['campaign_lists']))
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form name="poplarDataSyncForm" id="poplarDataSyncForm" method="POST" class="col-md-12">
                <input type="hidden" name="script_contact_ids" id="script_contact_ids" value="{{ implode(', ', $data['script_contact'])}}" />
                <ul class="list-group /*list-group-flush*/">
                    @foreach($data['campaign_lists'] as $item=>$key)
                    <li class="list-group-item m-1">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="poplar_campaign" id="poplar_campaign_{{ $key->campaign_id }}" value="{{ $key->campaign_id }}" data-local-id="{{ $key->id }}" required>
                            <label class="form-check-label" for="poplar_campaign_{{ $key->campaign_id }}">
                                {{ $key->campaign_name }}
                            </label>
                        </div>

                    </li>
                    @endforeach
                </ul>
            </form>
        </div>
    </div>
    <!-- List all Poplar Campaigns -->
    {{-- 
    <div class="row">
        <div class="col-md-12">

            <ul class="list-group /*list-group-flush*/">
                @foreach($data['campaign_lists'] as $item=>$key)
                <li class="list-group-item m-1">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="poplar_campaign" id="poplar_campaign_{{ $key->campaign_id }}" value="{{ $key->campaign_id }}">
                        <label class="form-check-label" for="poplar_campaign_{{ $key->campaign_id }}">
                            {{ $key->campaign_name }}
                        </label>
                    </div>

                </li>
                @endforeach
            </ul>
        </div>
    </div>--}}
    <!-- List all Poplar Campaigns -->
</div>
@endif