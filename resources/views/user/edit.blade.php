@extends('layout.app')

@section('content')
	<!-- Content -->
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
        	<!--  Traffic  -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                        	<div class="row">
                        		<div class="col-lg-6">
                    				<h4 class="box-title">Edit User </h4>
                        		</div>
                        		<div class="col-lg-6">
                        			<a href="{{ route('users.index') }}" class="btn btn-outline-secondary float-right"><i class="fa fa-angle-double-left"></i> Back</a>
                        		</div>
                        	</div>
                        </div>
                        <div class="card-body">
                        	<form action="{{ route('users.update', $user) }}" method="post" enctype="multipart/form-data">
                        		@csrf
                                <input name="_method" type="hidden" value="PUT">
                                <div class="row form-group">
	                              	<div class="col-md-6">
	                                	<label class="label_font_size" for="title">Enter Job Title :</label>
	                                	<input type="text" name="title" id="title" class="form-control input_size" value="{{ $user->title }}">
	                                	@if ($errors->has('title'))
	                                  		<div class="text-danger">{{ $errors->first('title') }}</div>
	                                	@endif
	                              	</div>
	                              	<div class="col-md-6">
	                                	<label class="label_font_size" for="name">Enter Name :</label>
	                                	<input type="text" name="name" id="name" class="form-control input_size" value="{{ $user->name }}">
	                                	@if ($errors->has('name'))
	                                  		<div class="text-danger">{{ $errors->first('name') }}</div>
	                                	@endif
	                              	</div>
	                            </div>

	                            <div class="row form-group">
	                              	<div class="col-md-6">
	                                	<label class="label_font_size" for="phone_no">Enter Phone Number :</label>
	                                	<input type="text" name="phone_no" id="phone_no" class="form-control input_size" value="{{ $user->phone_no }}">
	                                	@if ($errors->has('phone_no'))
	                                  		<div class="text-danger">{{ $errors->first('phone_no') }}</div>
	                                	@endif
	                              	</div>
	                              	<div class="col-md-6">
	                                	<label class="label_font_size" for="email">Enter Email :</label>
	                                	<input type="text" name="email" id="email" class="form-control input_size" value="{{ $user->email }}">
	                                	@if ($errors->has('email'))
	                                  		<div class="text-danger">{{ $errors->first('email') }}</div>
	                                	@endif
	                              	</div>
	                            </div>

	                            <div class="row form-group">
	                              	<div class="col-md-6">
			                            <label class="label_font_size" for="role">User Role :</label>
			                            <select class="form-control input_size" name="role" id="role" value="{{ $user->role }}">
			                                <option selected disabled>Select User Role</option>
			                                
			                                @foreach($uroles as $urole)
                                                <option value="{{ $urole->id }}" {{ $user->role == $urole->id ? 'selected' : ''}}>{{ $urole->name }}</option>
                                            @endforeach
			                            </select>
			                            @if ($errors->has('role'))
			                            	<div class="text-danger">{{ $errors->first('role') }}</div>
			                            @endif
			                        </div>
	                              	<div class="col-md-6">
	                                	<label class="label_font_size" for="password">Enter Password :</label>
	                                	<input type="password" name="password" id="password" class="form-control input_size" value="{{ old('password') }}" placeholder="Please enter new password">
	                                	@if ($errors->has('password'))
	                                  		<div class="text-danger">{{ $errors->first('password') }}</div>
	                                	@endif
	                              	</div>
	                            </div>

	                            <div class="row form-group">
	                              	<div class="col-md-6">
			                            <label class="label_font_size" for="role">Status :</label>
			                            <select class="form-control input_size" name="status" id="status" value="{{ $user->status }}">
			                                <option selected disabled>Select User Status</option>
			                                <option value="1" {{ $user->status == 1 ? 'selected' : ''}} >Active</option>
			                                <option value="0" {{ $user->status == 0 ? 'selected' : ''}} >Inactive</option>
			                                
			                            </select>
			                            @if ($errors->has('status'))
			                            	<div class="text-danger">{{ $errors->first('status') }}</div>
			                            @endif
			                        </div>
	                            </div>

                            	
                            	<button type="submit" class="btn btn-outline-primary float-right"><i class="fa fa-save"></i> Update</button>
                        	</form>
				        </div><!-- card-body -->
                        <div class="card-body"></div>
                    </div>
                </div><!-- /# column -->
            </div>
            <!--  /Traffic -->
            <div class="clearfix"></div>
        </div>
    	<!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>
@endsection