@extends('layout.app')

@section('content')

	<!-- Content -->
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
        	<!--  Traffic  -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                        	<!-- Error & Message -->
	                        	@if (session()->has('insert'))
							      	<div class="alert alert-success alert-dismissible" role="alert">
							        	{{ session()->get('insert') }}
							        	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							          		<span aria-hidden="true">&times;</span>
							        	</button>
							      	</div>
							    @elseif (session()->has('update'))
							      	<div class="alert alert-info alert-dismissible" role="alert">
							        	{{ session()->get('update') }}
							        	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							          		<span aria-hidden="true">&times;</span>
							        	</button>
							      	</div>
							    @elseif (session()->has('delete'))
							      	<div class="alert alert-danger alert-dismissible" role="alert">
							        	{{ session()->get('delete') }}
							        	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							          		<span aria-hidden="true">&times;</span>
							        	</button>
							      	</div>
							    @endif
							<!-- Error & Message -->
                        	<div class="row">
                        		<div class="col-lg-6">
                    				<h4 class="box-title">User's List </h4>
                        		</div>
                        		<div class="col-lg-6">
                        			<a href="{{ route('users.create') }}" class="btn btn-outline-success float-right"><i class="fa fa-plus"></i> ADD</a>
                        		</div>
                        	</div>
                        </div>
                        <div class="card-body nopadding">
                        	<div class="col-md-12 table-stats order-table ov-h nopadding">
	                            <table class="table">
				                  	<thead>
					                    <tr>
					                    	<!-- <th>#</th> -->
					                      	<th>Job Title</th>
					                      	<th>Name</th>
					                      	<th>Email</th>
					                      	<th>Role</th>
					                      	<th>Phone Number</th>
					                      	<th>Status</th>
					                      	<th colspan="2" style="text-align: left;">Action</th>
					                    </tr>
				                  	</thead>

				                  	<tbody class="table_format">
				                    	<!-- {{$i = 1}}  -->
				                    	@forelse($users as $key => $user)
					                      	<tr>
						                        <!-- <td>{{ $users->firstItem() + $key }}</td> -->
						                        <td>{{ $user->title }}</td> 
						                        <td>{{ $user->name }}</td> 
						                        <td>{{ $user->email }}</td> 
						                        <td>{{ $user->role_name['name'] }}</td> 
						                        <td>{{ $user->phone_no }}</td>
						                        <td @if($user->status == 1)class="text-success" @else class="text-danger" @endif>{{ $user->status==1 ? 'Active' : 'Inactive' }}</td>
						                        <td>
						                        	<a href="{{ route('users.edit', $user) }}">
						                          		<i class="fa fa-edit edit_btn" rel="tooltip" title="Edit"></i>
						                          	</a>
						                        </td>
						                        <!-- <td>
						                        	<form action="{{ route('users.destroy', $user) }}" method="POST">
						                            	{{ csrf_field() }}
						                            	<input type="hidden" name="_method" value="DELETE">
						                            	<input type="hidden" name="_token" value="{{ csrf_token() }}">
						                            	<button type="submit" class="fabutton" onclick="return myFunction();">
						                            		<i class="fa fa-trash dlt_btn" rel="tooltip" title="Delete"></i>
						                            	</button>
						                          	</form>
						                        </td> -->
					                      	</tr>
				                    	@empty
					                        <tr>
					                          <td colspan="7" class="text-center">
					                          	<p>User Not Found.</p>
					                          </td> 
					                        </tr>
				                    	@endforelse
				                  	</tbody>
				                </table>
                        	</div> <!-- /.col-md-12 -->
				        </div><!-- card-body -->
                        <div class="card-body">
                            {{ $users->onEachSide(1)->links() }}
                        </div>
                        <div class="card-body"></div>
                    </div>
                </div><!-- /# column -->
            </div>
            <!--  /Traffic -->
            <div class="clearfix"></div>
        </div>
    	<!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>
@endsection