<table class="table " id="contact_table">
                    <thead>
                        <tr>
                            <!-- <th>S.no.</th> -->
                            <th>Name</th>
                            <th>Customer</th>
                            <!-- <th>Title</th> -->
                            <th>Customer Number</th>
                            <th>Email</th>
                            
                            <th>Phone</th>
                            <th>Contact Type</th>
                            <!-- <th>Expiration Date</th> -->
                            <th>Status</th>
                            <th>Updated At</th>
                            <th colspan="2">Action</th>
                            <!-- <th>Details</th> -->



                        </tr>
                    </thead>

                    <tbody class="table_format">
                        <!-- {{ $i = 1}} -->
                        @forelse ($contacts as $key => $contact)
                            <tr>
                                <!-- <td>{{ $contacts->firstItem() + $key }}</td> -->
                                <!-- <td >{{$contact->contact_name}}</td> -->
                                <td>
                                    <a rel="tooltip" title="View" href="{{ url('contact/log_history', $contact->id) }}">
                                    <u>{{$contact->contact_name}}</u>
                                    </a>
                                </td>
                                <td>{{$contact->customer_name}}</td>
                                <!-- <td>{{$contact->contact_title}}</td> -->
                                <td>{{$contact->customer_number}}</td>
                                <td>{{$contact->contact_email}}</td>
                                <td>{{$contact->contact_phone}}</td>
                                <td>{{$contact->contact_desc}}</td>
                                
                                <!-- <td @if($contact->customer_active == 0)class="text-success" @else class="text-danger" @endif>{{$contact->customer_active}}</td> -->
                                <!-- <td>{{$contact->expiration_date}}</td> -->
                                <td @if($contact->status == 1)class="text-success" @else class="text-danger" @endif>{{$contact->status == 1 ? 'Active' : 'Inactive'}}</td>
                                <td>{{ !empty($contact->updated_at)?date('d M Y', strtotime($contact->updated_at)):'-' }}</td>
                                <td style="text-align: right; padding-left:0px;">
                                    <a class="btn fa fa-edit edit_btn" href="{{ route('contact_edit',$contact->id) }}" rel="tooltip" title="Edit"></a>
                                </td>
                                <!-- <td style="text-align: left; padding-left:0px;">
                                    <form action="{{ route('contact_delete', $contact->id) }}" method="POST">
                                        @csrf
                                        <button type="submit" class="fabutton" onclick="return myFunction();">
                                            <i class="fa fa-trash dlt_btn" rel="tooltip" title="Delete"></i>
                                        </button>
						            </form>
                                </td> -->
                                    
                            </tr>

                        @empty

                            <tr>
                                <td colspan="7" class="text-center">
                                    <p>Contact Not Found.</p>
                                </td> 
                            </tr>
                        @endforelse



                    </tbody>
            </table>

             <div class="col">
{{ $contacts->links() }} </div>