@extends('contact.layout') 

@section('contact_head_button')

<!-- Contact

<a href="{{ url('contact') }}" class="float-right btn btn-outline-primary">Back</a> -->

<div class="row">
    <div class="col-lg-6">
        <h4 class="box-title">Update Contact </h4>
    </div>
    <div class="col-lg-6">
        <a href="{{ url('contact') }}" class="btn btn-outline-secondary float-right"><i class="fa fa-angle-double-left"></i> Back</a>
    </div>
</div>

@endsection

@section('contact_content')

    <div class="card-body">

        <!-- Error & Message -->
        @if (session()->has('insert'))
            <div class="alert alert-success alert-dismissible" role="alert">
                {{ session()->get('insert') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @elseif (session()->has('update'))
            <div class="alert alert-info alert-dismissible" role="alert">
                {{ session()->get('update') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @elseif (session()->has('delete'))
            <div class="alert alert-danger alert-dismissible" role="alert">
                {{ session()->get('delete') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <!-- Error & Message -->

        <form action="{{ route('contact_update',$contact->id) }}" method="post">
        @csrf
            <div class="row form-group">
                <!-- <div class="col-md-6">
                    <div class="form-group">
                        <label for="" class="label_font_size form-control-label">Select Customer</label>
                        <input type="text"  name="customer_id" id="customer" placeholder="Customer " class="form-control select_customer">
                    </div> -->
                <!-- </div> -->

                

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="" class="label_font_size form-control-label">Contact Name</label>
                        <input type="text"  name="contact_name" id="contact_name" placeholder="Contact Name" class="form-control input_size" value="{{ $contact->contact_name }}">
                    </div>
                    @if ($errors->has('contact_name'))
                        <div class="text-danger">{{ $errors->first('contact_name') }}</div>
                    @endif
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="" class="label_font_size form-control-label">Contact Email</label>
                        <input type="text"  name="contact_email" id="contact_email" placeholder="Email" class="form-control input_size" value="{{ $contact->contact_email }}">
                    </div>
                    @if ($errors->has('contact_email'))
                        <div class="text-danger">{{ $errors->first('contact_email') }}</div>
                    @endif
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="" class="label_font_size form-control-label">Phone</label>
                        <input type="text"  name="contact_phone" id="contact_phone" placeholder="phone" class="form-control input_size"  value="{{ $contact->contact_phone }}">
                    </div>
                    @if ($errors->has('contact_phone'))
                        <div class="text-danger">{{ $errors->first('contact_phone') }}</div>
                    @endif
                </div>
            </div> <!-- .row form-group -->


            <div class="row form-group">
                
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="" class="label_font_size"> Address</label>
                        <input type="text"  name="address" id="address" placeholder="eg. 15th Avenue" class="form-control input_size" value="{{ $contact->address }}">
                    </div>
                    @if ($errors->has('address'))
                        <div class="text-danger">{{ $errors->first('address') }}</div>
                    @endif
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="" class="label_font_size"> State</label>
                        <input type="text"  name="state" id="state" placeholder="eg. 15th Avenue" class="form-control input_size" value="{{ $contact->state }}">
                    </div>
                    @if ($errors->has('state'))
                        <div class="text-danger">{{ $errors->first('state') }}</div>
                    @endif
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="" class="label_font_size"> Zip code</label>
                        <input type="text"  name="zipcode" id="zipcode" placeholder="eg. 15th Avenue" class="form-control input_size" value="{{ $contact->zipcode }}">
                    </div>
                    @if ($errors->has('zipcode'))
                        <div class="text-danger">{{ $errors->first('zipcode') }}</div>
                    @endif
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="" class="label_font_size">Contact Type</label>
                        <select class="form-control input_size" name="contact_type" id="contact_type" value="{{ old('contact_type') }}">
                            @foreach($contact_types as $contact_type)
                                <option value="{{ $contact_type->contact_type }}" {{ $contact_type->contact_type == $contact->contact_type ? 'selected' : '' }}>{{ $contact_type->contact_desc }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <!-- <div class="col-md-3">
                    <div class="form-group">
                        <label for="" class="label_font_size form-control-label"> Address</label>
                        <input type="text"  name="address" id="address" placeholder="Address" class="form-control input_size" value="{{ $contact->address }}">
                    </div>
                    @if ($errors->has('address'))
                        <div class="text-danger">{{ $errors->first('address') }}</div>
                    @endif
                </div> -->
                <!-- <div class="col-md-2">
                    <div class="form-group">
                        <label for="" class="label_font_size form-control-label"> Customer Active</label>
                        <label for="" class="label_font_size form-control-label"></label>
                        <input type="text"  name="customer_active" id="customer_active" placeholder="Customer Active" class="form-control input_size"  value="{{ $contact->customer_active }}">
                    </div>
                </div> -->

                <!-- <div class="col-md-3">
                    <div class="form-group">
                        <label for="" class="label_font_size form-control-label"> Expiration Date</label>
                        <input type="text"  name="expiration_date" id="expiration_date" placeholder="Expiration Date" class="form-control input_size"  value="{{ $contact->expiration_date }}">
                    </div>
                    @if ($errors->has('expiration_date'))
                        <div class="text-danger">{{ $errors->first('expiration_date') }}</div>
                    @endif
                </div> -->
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="label_font_size" for="role">Status</label>
                                        <select class="form-control input_size" name="status" id="status" value="{{ $contact->status }}">
                                            <option selected disabled>Select User Status</option>
                                            <option value="1" {{ $contact->status == 1 ? 'selected' : ''}} >Active</option>
                                            <option value="0" {{ $contact->status == 0 ? 'selected' : ''}} >Inactive</option>
                                            
                                        </select>
                    </div>
                </div>
                

            </div> <!-- .row form-group -->


            <div class="row form-group">
                <!-- <div class="col-md-3">
                    <div class="form-group">
                        <label for="" class="label_font_size"> Effective Date</label>
                        <div id="edit_effective_date" class="input-group date" data-date-format="mm-dd-yyyy">
                            <input type="text" name="effective_date" id="effective_date" class="form-control input_size" value="{{ $contact->effective_date }}" disabled>
                            <span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                </div> -->

                <!-- <div class="col-md-3">
                    <div class="form-group">
                        <label for="" class="label_font_size"> Expiry Date</label>
                        <div id="expiry_datepicker" class="input-group date" data-date-format="mm-dd-yyyy">
                            <input type="text" name="expiry_date" id="expiry_date" class="form-control input_size" value="{{ $contact->expiry_date }}">
                            <span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                </div> -->

                

                

            </div> <!-- .row form-group -->

            <!-- <div class="col-md-6 offset-md-6 " > -->
            <div class="pull-right">
                <button type="submit" class="btn btn-outline-primary"><i class="fa fa-save"></i> Save</button>
            </div>

        </form>

    </div> <!-- .card-body -->

    <script>
      jQuery(document).ready(function () {
        var effective_date = jQuery('#effective_date').val();
        console.log(effective_date);
        jQuery("#edit_effective_date").datepicker({ 
          format: "mm/dd/yy",
          weekStart: 0,
          calendarWeeks: true,
          autoclose: true,
          todayHighlight: true,
          orientation: "auto"
        }).datepicker('setDate', new Date(effective_date));
      });
    </script>

@endsection