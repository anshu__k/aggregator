@extends('contact.layout') 


@section('contact_head_button')


<div class="row form-group">

        <div class="col-md-3">
            <h4 class="box-title">Contact > Activity</h4>
        </div>


        <div class="col-md-6">
            <div class="input-group">
                <input class="form-control"  name="search_term" id="search_contact_term" placeholder="Search..">
                    <div class="input-group-btn">
                        <button type="button" id="search_contact" class="btn btn-outline-primary" >Search</button>
                    </div>

                    <a class="btn input-group-btn"href="{{ url('contact') }}" >Reset</a>
            </div>
        </div>

        <div class="col-md-3">
            <a href="{{ url('contact') }}" class="btn btn-outline-secondary float-right"><i class="fa fa-angle-double-left"></i> Back</a>

        </div>

</div>

    

@endsection

@section('contact_content')

@include('log.log_history_view')



@endsection