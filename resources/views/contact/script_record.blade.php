@extends('layout.app')

@section('content')
<!-- Content -->
<div class="content">
    <!-- Animated -->
    <div class="animated fadeIn">
        <!--  Traffic  -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <!-- Error & Message -->
                        @if (session()->has('insert'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            {{ session()->get('insert') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @elseif (session()->has('update'))
                        <div class="alert alert-info alert-dismissible" role="alert">
                            {{ session()->get('update') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @elseif (session()->has('delete'))
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            {{ session()->get('delete') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        <!-- Error & Message -->
                        <div class="row form-group">
                            <div class="col-md-9">
                                <h4 class="box-title">Script Record </h4>
                            </div>
                        </div>

                        <!-- Search records based on date filter -->
                        <div class="row form-group script_contact-search-box">
                            <h3 class="col-md-12 mb-3">Search Filters</h3>
                            <div class="col-md-3">
                                <label class="label_font_size mb-0">Show Bullhorn Data</label>
                                <select name="showBullhornData" id="showBullhornData" class="form-control form-select showBullhornData">
                                    <option value="all" data-badge="Show All" data-key="all" data-column="all">All</option>

                                    <option value="unsync" data-badge="Show Unsync" data-key="unsync" data-column="unsync">Show Unsync</option>
                                    <option value="unsync_contact" data-badge="Show Unsync Contact" data-key="unsync_contact" data-column="unsync_contact">Show Unsync Contacts</option>
                                    <option value="unsync_candidate" data-badge="Show Unsync Candidates" data-key="unsync_candidate" data-column="unsync_candidate">Show Unsync Candidate</option>

                                    <option value="contact" data-badge="Show Only Bullhorn Contact" data-key="contact" data-column="all">Show Only Bullhorn Contact</option>
                                    <option value="candidate" data-badge="Show Only Bullhorn Candidate" data-key="candidate" data-column="all">Show Only Bullhorn Candidate</option>
                                    <option value="both" data-badge="Both (Candiate & Contact)" data-key="both" data-column="both">Both (Candiate & Contact)</option>

                                </select>
                                    
                            </div>

                            <div class="col-md-3">
                                <label for="from_date" class="label_font_size mb-0">From Date</label>
                                <div class="input-group date" data-date-format="mm-dd-yyyy">
                                    <input type="text" name="from_date" id="from_date" class="form-control input_size" value="" readonly="readonly" placeholder="From Date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label for="to_date" class="label_font_size mb-0">To Date</label>
                                <div class="input-group date" data-date-format="mm-dd-yyyy">
                                    <input type="text" name="to_date" id="to_date" class="form-control input_size" value="" readonly="readonly" placeholder="To Date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <label for="field_type" class="label_font_size mb-0">Select Field</label>
                                <select name="select_field" id="select_field" class="form-control form-select">
                                    <option value="">Select Field</option>
                                    <option value="issuance_date">Issuance Date</option>
                                    <option value="expiration_date">Expiration Date</option>
                                    <option value="created_at">Created Date</option>
                                </select>
                            </div>

                            {{-- Poplar Campaign Serching - START --}}
                            <div class="col-md-6 mt-2">
                                <label for="search_poplar_campaign" class="label_font_size ">Poplar Campaign</label>
                                <select name="search_poplar_campaign" id="search_poplar_campaign" class="form-control form-select">
                                    <option value="">Select All Poplar Campaign</option>
                                    <optgroup label="All Pending" class="poplar_optgroup">
                                        <option value="notsent">Pending to Send Postcards</option>
                                    </optgroup>
                                    <optgroup label="Search By Sent Campaigns" class="poplar_optgroup">
                                        @foreach($poplar_campaigns as $key=>$item)
                                            <option value="sent_to|||{{ $item->campaign_id }}" data-local-value="{{ $item->id }}">{{ $item->campaign_name }}</option>
                                        @endforeach
                                    </optgroup>

                                    <optgroup label="Search By Pending to Sent Campaigns" class="poplar_optgroup">
                                        @foreach($poplar_campaigns as $key=>$item)
                                            <option value="not_sent_to|||{{ $item->campaign_id }}" data-local-value="{{ $item->id }}">{{ $item->campaign_name }}</option>
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>
                            {{-- Poplar Campaign Serching - END --}}
                                
                            <div class="col-md-3">
                                <label for="search_script_record" class="label_font_size mb-2"></label>
                                <div class="input-group-btn">
                                    <button type="button" id="search_script_record" class="btn btn-outline-primary">Search</button>
                                    <a class="btn input-group-btn" href="{{ route('contact.script_record') }}">Reset</a>
                                </div>
                            </div>
                        </div>
                        <!-- Search records based on date filter -->

                        <div class="row form-group">
                            <div class="col-md-5">
                                <div class="d-flex">
                                    <label class="label label-default">
                                        <!-- <a class="col-md-3 ml-n1">&nbsp;</a> -->
                                        <a href="javascript:void(0);" class="selectAllColumn hideAllColumn link-secondary">Select All</a> | <a href="javascript:void(0);" class="selectAllColumn showAllColumn link-secondary">Deselect All</a>
                                    </label>
                                </div>
                                <div class="d-flex">
                                    <!-- <label class="col-md-3 ml-n1 title label label-default">Select Column to hide.</label> -->
                                    <select name="showHideColumns" id="showHideColumns" class="form-control form-select showHideColumn col-md-5" multiple="multiple">
                                        <!-- <option value="all" data-badge="All" data-key="all" data-column="all">All</option> -->
                                        @foreach($columns as $key=>$val)
                                        @if(!empty($val['show_hide']))
                                        <option value="{{ $val['key'] }}" data-badge="{{ $val['title'] }}" data-key="{{ $val['key'] }}" data-column="{{ $val['index'] }}" @if(isset($val['default_hide']) && $val['default_hide']) selected @endif>
                                            {{ $val['title'] }}
                                        </option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="d-flex">
                                    <label class="label label-default">&nbsp;</label>
                                </div>
                                <div class="d-flex">
                                    <!-- <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#poplarDataSyncModal"> -->
                                    <button type="button" class="btn btn-warning btn__poplarDataSyncModal" >
                                        <i class="fa fa-spin fa-spinner fa-spinner-poplar-modal" style="display: none;"></i>
                                        Sync All Selected Data to Poplar
                                    </button>
                                </div>
                            </div>
                            <!-- <div class="col-md-3">
                                <div class="d-flex">
                                    <label class="label label-default">Show Bullhorn Data</label>
                                </div>
                                <div class="d-flex">
                                    <select name="showBullhornData" id="showBullhornData" class="form-control form-select showBullhornData">
                                        <option value="all" data-badge="Show All" data-key="all" data-column="all">All</option>

                                        <option value="unsync" data-badge="Show Unsync" data-key="unsync" data-column="unsync">Show Unsync</option>
                                        <option value="unsync_contact" data-badge="Show Unsync Contact" data-key="unsync_contact" data-column="unsync_contact">Show Unsync Contacts</option>
                                        <option value="unsync_candidate" data-badge="Show Unsync Candidates" data-key="unsync_candidate" data-column="unsync_candidate">Show Unsync Candidate</option>

                                        <option value="contact" data-badge="Show Only Bullhorn Contact" data-key="contact" data-column="all">Show Only Bullhorn Contact</option>
                                        <option value="candidate" data-badge="Show Only Bullhorn Candidate" data-key="candidate" data-column="all">Show Only Bullhorn Candidate</option>
                                        <option value="both" data-badge="Both (Candiate & Contact)" data-key="both" data-column="both">Both (Candiate & Contact)</option>

                                    </select>
                                </div>
                            </div> -->
                        </div>

                        <div class="card-body nopadding">
                            <div id="searchid" class="col-md-12 table-stats order-table nopadding">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            @foreach($columns as $key=>$val)
                                            @php
                                            $width = $class = $ordering = "";
                                            if(!empty($val['class'])) {
                                                $class = "class=" . $val['class'];
                                            }

                                            if(!empty($val['width'])) {
                                                $width = "width=" . $val['width'];
                                            }

                                            if(isset($val['ordering'])) {
                                                $ordering = "data-orderable=" . $val['ordering'];
                                            }
                                            @endphp

                                            <th {{ $width }} {{ $class }} {{ $ordering }}> {!! $val['title'] !!}</th>
                                            @endforeach
                                        </tr>
                                    </thead>
                                    <tbody class="table_format"></tbody>

                                    {{--

                                @forelse($sources as $key => $source)
                                <tr>
                                    <td>{{ $source->id }}</td>
                                    <td>{{ $source->firstName }}</td>
                                    <td>{{ $source->lastName }}</td>
                                    <td>{{ $source->zip }}</td>
                                    <td>{{ $source->city }}</td>
                                    <td>{{ $source->state }}</td>
                                    <td>{{ $source->county }}</td>
                                    <td>{{ $source->licenseNumber }}</td>
                                    <td><a target="_blank" href="{{ $source->details }}">Click here </a></td>
                                    <td>{{ date('m/d/y h:i a',strtotime($source->created_at))   }}</td>
                                    <td style="text-align:center;">{{ $source->bullhorn_candidate_id ?? '-' }}</td>
                                    <td style="text-align:center;">{{ $source->bullhorn_contact_id ?? '-' }}</td>
                                    <td>actions</td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="5" class="text-center">
                                            <p>Script Not Found.</p>
                                        </td>
                                    </tr>
                                    @endforelse
                                    --}}
                                </table>
                            </div> <!-- /.col-md-12 -->
                        </div><!-- card-body -->
                        <div class="card-body page_hide">
                            {{-- $sources->onEachSide(1)->links() --}}
                        </div>
                        <div class="card-body"></div>
                    </div>
                </div><!-- /# column -->
            </div>
            <!--  /Traffic -->
            <div class="clearfix"></div>
        </div>
        <!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>
    <!-- Search Source -->
    

    <!-- Modal Example -->
    <div class="modal fade" id="poplarDataSyncModal" tabindex="-1" role="dialog" aria-labelledby="poplarDataSyncModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="poplarDataSyncModalLabel">Sync Data to Poplar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                HERE LOAD ALL CAMPAIGNS FROM POPLAR, AND BUTTON FOR SYNC INFO SO ALL SELECTED SCRIPT RECORDS GETS SYNC TO POPLAR
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn__sendPoplarPostCard" >
                    <i class="fa fa-spin fa-spinner fa-spinner-poplar-submit-modal" style="display: none;"></i>&nbsp;Sync Data
                </button>
            </div>
            </div>
        </div>
    </div>
    <!-- Modal Example -->
    @endsection


    @push('styles')
    <link href="//use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet" crossorigin="anonymous">
    <link href="{{ asset('public/css/script_records.css') }}" rel="stylesheet" />
    @endpush

    @push('script')
    <script src="{{ asset('public/js/jquery.validate.min.js') }}"></script>
    <script>
        function updateDataTableSelectAllCtrl(table) {
            var $table = table.table().node();
            var $chkbox_all = jQuery('tbody input[type="checkbox"]', $table);
            var $chkbox_checked = jQuery('tbody input[type="checkbox"]:checked', $table);
            var chkbox_select_all = jQuery('thead input[name="select_all"]', $table).get(0);

            // If none of the checkboxes are checked
            if ($chkbox_checked.length === 0) {
                chkbox_select_all.checked = false;
                if ('indeterminate' in chkbox_select_all) {
                    chkbox_select_all.indeterminate = false;
                }

                // If all of the checkboxes are checked
            } else if ($chkbox_checked.length === $chkbox_all.length) {
                chkbox_select_all.checked = true;
                if ('indeterminate' in chkbox_select_all) {
                    chkbox_select_all.indeterminate = false;
                }

                // If some of the checkboxes are checked
            } else {
                chkbox_select_all.checked = true;
                if ('indeterminate' in chkbox_select_all) {
                    chkbox_select_all.indeterminate = true;
                }
            }
        }

        function validateSendPopluarPostCard() {
            jQuery('#poplarDataSyncModal #poplarDataSyncForm').validate({
                rules: {
                    script_contact_ids: {
                        required: true
                    },
                    poplar_campaign: "required"
                },
                messages: {
                    "script_contact_ids": {
                        "required": "Please select records to send postcard.",
                    },
                    "poplar_campaign": "Please select campaign"
                },
                errorElement: 'div',
                errorClass: 'help-block',
                focusInvalid: false,
                onclick: false,
                highlight: function (e) {
                    jQuery(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                },
                success: function (e) {
                    jQuery(e).closest('.form-group').removeClass('has-error');
                    jQuery(e).remove();
                },
                errorPlacement: function (error, element) {
                    console.log(error, element);
                    if (element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                        var controls = element.closest('div[class*="col-"]');
                        if (controls.find(':checkbox,:radio').length > 1)
                            controls.append(error);
                        else
                            error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                    } else if (element.is('#medical-type')) {
                        error.insertAfter($('.multiselect-container'));
                    } else if (element.is('.chosen-select')) {
                        error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                    } else
                        error.insertAfter(element);
                },
                invalidHandler: function (form) {
                },
                submitHandler: function (form) {
                    console.log('form submit', form);
                    jQuery('#poplarDataSyncModal').find('[type=button]').attr('disabled', true);
                    jQuery('#poplarDataSyncModal').find('.fa-spinner-poplar-submit-modal').show();

                    let script_contact_ids = jQuery('#poplarDataSyncModal #poplarDataSyncForm #script_contact_ids').val();
                    let poplar_campaign = jQuery('#poplarDataSyncModal #poplarDataSyncForm input[name="poplar_campaign"]:checked').val();
                    jQuery.ajax({
                        type: "POST",
                        url: "{{ route('poplar.submit_scriptrecord_poplar') }}",
                        data: {
                            '_token': "{{ csrf_token() }}",
                            poplar_campaign: poplar_campaign,
                            script_contact_ids: script_contact_ids,
                        },
                        dataType: "JSON",
                        success: function (response) {
                            if(response.success) {
                                alert('Postcard Successfully Sent...');
                                jQuery('#poplarDataSyncModal').modal('hide');

                            } else {
                                alert('Failure');
                            }
                            // $this.attr('disabled', false);
                            jQuery('#poplarDataSyncModal').find('[type=button]').attr('disabled', false);
                            jQuery('#poplarDataSyncModal').find('.fa-spinner-poplar-submit-modal').hide();
                        },
                        error: function (request, error) {
                            // toastr.error(SOMETHING_ERROR, error_text);
                            jQuery('#poplarDataSyncModal').find('[type=button]').attr('disabled', false);
                            jQuery('#poplarDataSyncModal').find('.fa-spinner-poplar-submit-modal').hide();

                            alert('Something went wrong!!!');
                        },
                    });
                },
            });
        }

        // Poplar Sync button click event 
        jQuery(document).on('click', '.btn__poplarDataSyncModal', function(){
            $this = jQuery(this);
            let checkboxCheckedlength = jQuery('#searchid').find('tbody input[type="checkbox"]:checked').length;
            if(checkboxCheckedlength > 0) {
                let selectedRecordObjcts = [];
                $this.attr('disabled', true);
                $this.find('.fa-spinner-poplar-modal').show();
                jQuery('.agg_contact_list:checked').each(function(key, item){
                    // console.log(key), console.log(jQuery(item).attr('data-val'));
                    selectedRecordObjcts.push(jQuery(item).attr('data-val'));
                });

                jQuery.ajax({
                    type: "GET",
                    url: "{{ route('poplar.get_poplar_campaigns') }}",
                    data: {
                        selectedRecordObjcts
                    },
                    dataType: "JSON",
                    success: function (response) {
                        if(response.success) {
                            // Push data to modal and show modal
                            jQuery('#poplarDataSyncModal .modal-body').html(response.html);
                            jQuery('#poplarDataSyncModal').modal('show');

                            /** 
                             * Set-up validation for the form
                             * 
                             */
                            validateSendPopluarPostCard();
                        } else {
                            alert('Failure');
                        }
                        $this.attr('disabled', false);
                        $this.find('.fa-spinner-poplar-modal').hide();
                    },
                    error: function (request, error) {
                        // toastr.error(SOMETHING_ERROR, error_text);
                        $this.attr('disabled', false);
                        $this.find('.fa-spinner-poplar-modal').hide();

                        alert('Something went wrong!!!');
                    },
                });
            } else {
                alert('Please select record to sync with the Poplar.');
            }
        });

        /**
         * Submit Poplar PostCard
         * 
         */
        jQuery(document).on('click', '.btn__sendPoplarPostCard', function(){
            if(jQuery('#poplarDataSyncModal #poplarDataSyncForm').valid()) {
                jQuery('#poplarDataSyncModal #poplarDataSyncForm').submit();
            }
        });

        /** Search date filters  */
        jQuery(document).ready(function() {
            jQuery("#from_date").datepicker({ 
                format: "mm/dd/yyyy",
                weekStart: 0,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                orientation: "auto"
            });//.datepicker('update', new Date());

            jQuery("#to_date").datepicker({ 
                format: "mm/dd/yyyy",
                weekStart: 0,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                orientation: "auto"
            });//.datepicker('update', new Date());

            jQuery(document).on('click', '#search_script_record', function() {
                // var query = jQuery('#search_script_record_term').val();
                // var status = jQuery('#status').val();

                // if (query != '' || null != status) {
                //     fetch_source_data(query, status, 1);

                // }

                table.ajax.reload();
            });

            // jQuery(document).on('click', '.pagination a', function(event) {


            //     var query = jQuery('#search_script_record_term').val();
            //     var status = jQuery('#status').val();

            //     if (query != '' || null != status) {
            //         event.preventDefault();
            //         var page = jQuery(this).attr('href').split('page=')[1];

            //         fetch_source_data(query, status, page);

            //     }
            // });

            table = jQuery('.table').DataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                autoWidth: false,
                order: [
                    [14, 'desc']
                ],
                "lengthMenu": [[50, 100, 250, 500, 700, 1000], [50, 100, 250, 500, 700, 1000]],
                ajax: {
                    url: "{{ route('contact.script_record_paginate') }}",
                    type: "POST",
                    data: function(d) {
                        d._token = "{{csrf_token()}}";
                        /*d.showOnlyCandidate = jQuery('#showBullhornCandidate:checked').val();
                        d.showOnlyContact = jQuery('#showBullhornContacts:checked').val();
                        d.showCandidateAndContact = jQuery('#showBullhornCandidateContact:checked').val();
                        d.showAllRecords = jQuery('#showAllRecords:checked').val();*/
                        d.showBullhornData = jQuery('#showBullhornData').val();
                        d.fromDate = jQuery('#from_date').val();
                        d.toDate = jQuery('#to_date').val();
                        d.select_field = jQuery('#select_field').val();
                        d.search_poplar_campaign = jQuery('#search_poplar_campaign').val();
                    }
                },
                dom: "lBfrtip",
                'columnDefs': [{
                    targets: 0,
                    // 'searchable': false,
                    'orderable': false,
                    // 'width':'1%',
                    class: 'text-center dt-body-center',
                    render: function(data, type, full, meta) {
                        return '<input type="checkbox" class="agg_contact_list" name="checkbox_' + full.id + '" id="checkbox_' + full.id + '" value="1" data-val="' + full.id + '">';
                    }
                }],
                select: {
                    style: 'os',
                    selector: 'td:first-child'
                },
                // 'rowCallback': function(row, data, dataIndex){
                //     // Get row ID
                //     var rowId = data[0];

                //     // If row ID is in the list of selected row IDs
                //     if(jQuery.inArray(rowId, rows_selected) !== -1){
                //         jQuery(row).find('input[type="checkbox"]').prop('checked', true);
                //         jQuery(row).addClass('selected');
                //     }
                // },
                columns: [{
                        data: "checkbox",
                        class: "dt-checkboxes-cell"
                    },
                    {
                        data: "id",
                        class: "text-center"
                    },
                    {
                        data: "firstName",
                    },
                    {
                        data: "lastName"
                    },
                    {
                        data: "zip"
                    },
                    {
                        data: "address"
                    },
                    {
                        data: "city"
                    },
                    {
                        data: "state"
                    },
                    {
                        data: "county"
                    },
                    {
                        data: "licenseNumber",
                        class: "text-center"
                    },
                    {
                        data: "licenseIssuanceDate",
                        class: "text-center"
                    },
                    {
                        data: "licenseExpirationDate",
                        class: "text-center"
                    },
                    {
                        data: "licenseStatus",
                        class: "text-center"
                    },
                    {
                        data: "details",
                        class: "text-center"
                    },
                    {
                        data: "created_at"
                    },
                    {
                        data: "bullhorn_candidate_id",
                        class: "text-center"
                    },
                    {
                        data: "bullhorn_contact_id",
                        class: "text-center"
                    },
                    {
                        data: "poplar_info",
                        class: "text-center"
                    },
                    {
                        data: "Action",
                        class: "text-center"
                    },
                    // { data: "county", render: $.fn.dataTable.render.number( ',', '.', 0, '$' ) }
                ],
                //  buttons: ['copy', 'csv', 'excel', 'pdf']
                buttons: [{
                        extend: 'pdf',
                        title: 'Scrapped Data',
                        filename: 'scrapped_data_pdf',
                        className: '',
                        exportOptions: {
                            columns: ':not(.notexport)'
                        }
                    }, {
                        extend: 'excel',
                        title: 'Scrapped EXCEL',
                        filename: 'scrapped_data_excel',
                        className: '',
                        exportOptions: {
                            columns: ':not(.notexport)'
                            // columns: 'th:not(:last-child)'
                        }
                    }
                    /*, {
                                            extend: 'csv',
                                            filename: 'customized_csv'
                                        }*/
                ]
            });

            // Handle click on checkbox
            let rows_selected = [];
            jQuery(document).on('click change', 'input[type="checkbox"]:not(input[name="select_all"])', function(e) {
                var $row = jQuery(this).closest('tr');

                // Get row data
                var data = table.row($row).data();
                // console.log(data);

                // Get row ID
                var rowId = data[0];

                // Determine whether row ID is in the list of selected row IDs 
                var index = jQuery.inArray(rowId, rows_selected);

                // If checkbox is checked and row ID is not in list of selected row IDs
                if (this.checked && index === -1) {
                    rows_selected.push(rowId);

                    // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
                } else if (!this.checked && index !== -1) {
                    rows_selected.splice(index, 1);
                }

                if (this.checked) {
                    $row.addClass('selected');
                } else {
                    $row.removeClass('selected');
                }

                // Update state of "Select all" control
                updateDataTableSelectAllCtrl(table);

                // Prevent click event from propagating to parent
                e.stopPropagation();
            });


            /** 
             * Checked select all checkbox of datatable 
             * 
            */
            jQuery(document).on('click', '#select_all', function(){
                var rows = table.rows().nodes();
                let $chkElem = jQuery('input[type="checkbox"]', rows)
                if(jQuery(this)[0].checked == true) {
                    let $row = $chkElem.prop('checked', true);
                    $row.parents('tr').addClass('selected');
                } else {
                    let $row = $chkElem.prop('checked', false);
                    $row.parents('tr').removeClass('selected');
                }
            });
        });

        /** Work with Checkboxed */
        // jQuery(document).on('click', '.showHideColumn', function(e) {
        //     // e.preventDefault();
        //     var column = table.column(jQuery(this).attr('data-column'));

        //     if (jQuery(this).is(':checked')) {
        //         localStorage.setItem(`headerColumns${column.to$()}`, jQuery(this).is(':checked'));
        //     } else {
        //         localStorage.removeItem(`headerColumns${column.to$()}`);
        //     }
        //     // columnsNodeId = column.to$();
        //     currentColumnHeader = column.header();
        //     jQuery(currentColumnHeader).addClass('notexport')

        //     // Toggle the visibility
        //     column.visible(!column.visible());
        // });
        /** Work with Checkboxed */

        jQuery('#search_script_record_term').keypress(function(e) {
            var key = e.which;
            if (key == 13) // the enter key code
            {
                jQuery('#search_script_record').click();
                return false;
            }
        });

        // Hide All Columns 
        jQuery(document).on('click', '.hideAllColumn', function() {
            jQuery('#showHideColumns option').each(function() {
                jQuery(this).prop('selected', true).trigger('change');
            });
        });

        // Show All Columns 
        jQuery(document).on('click', '.showAllColumn', function() {
            jQuery('#showHideColumns option').each(function() {
                jQuery(this).prop('selected', false).trigger('change');
            });
        });

        jQuery(document).on('click', '.pc__show_bullhorn_contacts', function() {
            // console.log(jQuery(this).attr('data-column'));
            table.ajax.reload();
        });

        // Commented auto search while changing dropdown value, and merge with Search button click
        // jQuery(document).on('change', '.showBullhornData', function() {
        //     table.ajax.reload();
        // });

        /** Select Dropdown Change Event */
        jQuery(document).on('change', '#showHideColumns', function() {
            jQuery('option', this).each(function() {

                var column = table.column(jQuery(this).attr('data-column'));
                currentColumnHeader = column.header();
                // alert(column.to$());

                if (jQuery(this).is(':selected')) {
                    localStorage.setItem(`headerColumns${column.to$()}`, jQuery(this).is(':selected'));
                    jQuery(currentColumnHeader).addClass('notexport');
                    columnVisibility = false;
                } else {
                    localStorage.removeItem(`headerColumns${column.to$()}`);
                    jQuery(currentColumnHeader).removeClass('notexport');
                    columnVisibility = true;
                }
                // Toggle the visibility
                column.visible(columnVisibility);
            });
            table.columns.adjust().draw();
        });

        jQuery('#showHideColumns').select2({
            closeOnSelect: false,
            placeholder: "Select Columns to Hide",
            allowHtml: true,
            allowClear: true,
            tags: false // создает новые опции на лету
        });

        setTimeout(function() {
            jQuery('#showHideColumns').trigger('change');
        }, 1000);
    </script>
    @endpush