@extends('contact.layout') 


@section('contact_head_button')


<div class="row form-group">

        <div class="col-md-9">
            <h4 class="box-title">Contact</h4>
        </div>

        <div class="col-md-3">
            <a href="{{ url('contact/create') }}" class="btn btn-outline-success float-right"><i class="fa fa-plus"></i> ADD</a>

        </div>
</div>   
<div class="row form-group">
        <div class="col-md-4">
            <div class="input-group">
                <input class="form-control"  name="search_term" id="search_contact_term" placeholder="Search Name, Customer,Customer Number, Email, Phone">
            </div>        
        </div>
           <div class="col-md-3">    
             <div class="input-group">
                <select class="form-control" name="status" id="status">
                    <option selected disabled>Select Status</option>
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                </select>
            </div>
           </div>
           
           <!-- <div class="col-md-3">    
             <div class="input-group">
                <select class="form-control" name="contact_type" id="contact_type">
                        <option selected disabled>Select Contact Type</option>
                            @foreach($contact_types as $contact_type)
                                <option value="{{ $contact_type->contact_type }}">{{ $contact_type->contact_desc }}</option>
                            @endforeach
                        </select>
            </div>
           </div> -->

            <div class="col-md-2">    
                    <div class="input-group-btn">
                        <button type="button" id="search_contact" class="btn btn-outline-primary" >Search</button>
                    

                    <a class="btn input-group-btn"href="{{ url('contact') }}" >Reset</a></div>
            </div>
</div>

    

@endsection

@section('contact_content')

    <div class="card-body nopadding">

            <!-- Error & Message -->
            @if (session()->has('insert'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    {{ session()->get('insert') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @elseif (session()->has('update'))
                <div class="alert alert-info alert-dismissible" role="alert">
                    {{ session()->get('update') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @elseif (session()->has('delete'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    {{ session()->get('delete') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
         <!-- Error & Message -->

        <div id="searchid" class="col-md-12 table-stats order-table nopadding">
            <table class="table " id="contact_table">
                    <thead>
                        <tr>
                            <!-- <th>S.no.</th> -->
                            <th>Name</th>
                            <th>Email</th>
                            
                            <th>Phone</th>
                            
                            <th>Address</th>
                            <!-- <th>Title</th> -->
                            <th>State</th>
                            <th>Zip</th>
                            

                            <!-- <th>Expiration Date</th> -->
                            <th>Status</th>
                            <th>Updated At</th>
                            <th colspan="2">Action</th>
                            <!-- <th>Details</th> -->



                        </tr>
                    </thead>

                    <tbody class="table_format">
                        <!-- {{ $i = 1}} -->
                        @forelse ($contacts as $key => $contact)
                            <tr>
                                <!-- <td>{{ $contacts->firstItem() + $key }}</td> -->
                                <!-- <td >{{$contact->contact_name}}</td> -->
                                <td>
                                    <a rel="tooltip" title="View" href="{{ url('contact/log_history', $contact->id) }}">
                                    <u>{{$contact->contact_name}}</u>
                                    </a>
                                </td>
                                <td>{{$contact->contact_email}}</td>
                                <td>{{$contact->contact_phone}}</td>
                                
                                <td>{{$contact->address}}</td>
                                <!-- <td>{{$contact->contact_title}}</td> -->
                                <td>{{$contact->state}}</td>
                                <td>{{$contact->zipcode}}</td>
                                
                                <td @if($contact->status == 1)class="text-success" @else class="text-danger" @endif>{{$contact->status == 1 ? 'Active' : 'Inactive'}}</td>
                                <td>{{ !empty($contact->updated_at)?date('d M Y', strtotime($contact->updated_at)):'-' }}</td>
                                <td style="text-align: right; padding-left:0px;">
                                    <a class="btn fa fa-edit edit_btn" href="{{ route('contact_edit',$contact->id) }}" rel="tooltip" title="Edit"></a>
                                </td>
                                <!-- <td style="text-align: left; padding-left:0px;">
                                    <form action="{{ route('contact_delete', $contact->id) }}" method="POST">
                                        @csrf
                                        <button type="submit" class="fabutton" onclick="return myFunction();">
                                            <i class="fa fa-trash dlt_btn" rel="tooltip" title="Delete"></i>
                                        </button>
						            </form>
                                </td> -->
                                    
                            </tr>

                        @empty

                            <tr>
                                <td colspan="7" class="text-center">
                                    <p>Contact Not Found.</p>
                                </td> 
                            </tr>
                        @endforelse



                    </tbody>
            </table>

            <div class="col page_hide">
{{ $contacts->links() }} </div>
        </div>
    </div>



<!-- Search Contact -->
        <script>
            jQuery(document).ready(function(){

                function fetch_contact_data(query,status,contact_type,page)
                {

                    // alert(query);
                    jQuery.ajax({
                        url:"{{ route('contact_search') }}",
                        method:'GET',
                        data:{query:query,status:status,contact_type:contact_type,page:page},
                        dataType:'html',
                        success:function(data)
                        {
                            jQuery('#searchid').html(data);
                            jQuery('.page_hide').css('cssText', 'display:none');

                            // jQuery('.pagination').css("display:none;");
                            // $('#total_records').text(data.total_data);
                        }
                    })
                }

                

                    jQuery(document).on('click', '#search_contact', function(){
                        
                        var query = jQuery('#search_contact_term').val();
                        var contact_type = jQuery('#contact_type').val();
                        var status = jQuery('#status').val();

                        if (query != '' || null != contact_type || null !=status) {
                            fetch_contact_data(query,status,contact_type,1);
                        }
                    });
            
                
            jQuery(document).on('click', '.pagination a', function(event){
                  
                var query = jQuery('#search_contact_term').val();
                var contact_type = jQuery('#contact_type').val();
                var status = jQuery('#status').val();

                        if (query != '' || null != contact_type || null !=status){
                            
                  event.preventDefault(); 
                  var page = jQuery(this).attr('href').split('page=')[1];
                  
                     fetch_contact_data(query,status,contact_type,page);

                     } 
                 });

                       

            jQuery('#search_contact_term').keypress(function (e) {
                var key = e.which;
                if(key == 13)  // the enter key code
                {
                    jQuery('#search_contact').click();
                    return false;  
                }
            });  

            });

        </script>
    <!-- Search Contact -->


@endsection