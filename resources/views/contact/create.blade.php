@extends('contact.layout') 

@section('contact_head_button')

<!-- Contact
<a href="#" class="float-right btn btn-outline-primary">Back</a> -->
    <div class="row">
        <div class="col-lg-6">
            <h4 class="box-title">Add New Contact </h4>
        </div>
        <div class="col-lg-6">
            <a href="{{ url('contact') }}" class="btn btn-outline-secondary float-right"><i class="fa fa-angle-double-left"></i> Back</a>
        </div>
    </div>
@endsection

@section('contact_content')

    <div class="card-body">

        <form action="{{ url('contact/create') }}" method="post">
        @csrf
            <div class="row form-group">

                

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="" class="label_font_size form-control-label">Contact Name</label>
                        <input type="text"  name="contact_name" id="contact_name" placeholder="eg. John Doe" class="form-control input_size" >
                    </div>
                    @if ($errors->has('contact_name'))
                        <div class="text-danger">{{ $errors->first('contact_name') }}</div>
                    @endif
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="" class="label_font_size form-control-label">Contact Email</label>
                        <input type="email"  name="contact_email" id="contact_email" placeholder="eg. John@gmail.com" class="form-control input_size">
                    </div>
                    @if ($errors->has('contact_email'))
                        <div class="text-danger">{{ $errors->first('contact_email') }}</div>
                    @endif
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="" class="label_font_size form-control-label">Phone</label>
                        <input type="text"  name="contact_phone" id="contact_phone" placeholder="eg. 8743562343" class="form-control input_size">
                    </div>
                    @if ($errors->has('contact_phone'))
                        <div class="text-danger">{{ $errors->first('contact_phone') }}</div>
                    @endif
                </div>

            </div> <!-- .row form-group -->


            <div class="row form-group">
                
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="" class="label_font_size"> Address</label>
                        <input type="text"  name="address" id="address" placeholder="eg. 15th Avenue" class="form-control input_size">
                    </div>
                    @if ($errors->has('address'))
                        <div class="text-danger">{{ $errors->first('address') }}</div>
                    @endif
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="" class="label_font_size"> State</label>
                        <input type="text"  name="state" id="state" placeholder="eg. 15th Avenue" class="form-control input_size">
                    </div>
                    @if ($errors->has('state'))
                        <div class="text-danger">{{ $errors->first('state') }}</div>
                    @endif
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="" class="label_font_size"> Zip code</label>
                        <input type="text"  name="zipcode" id="zipcode" placeholder="eg. 15th Avenue" class="form-control input_size">
                    </div>
                    @if ($errors->has('zipcode'))
                        <div class="text-danger">{{ $errors->first('zipcode') }}</div>
                    @endif
                </div>
                <!-- <div class="col-md-2">
                    <div class="form-group">
                        <label for="" class="label_font_size"> Customer Active</label>
                        <input type="text"  name="customer_active" id="customer_active" placeholder="eg. active" class="form-control input_size">
                    </div>
                </div> -->

                <!-- <div class="col-md-3">
                    <div class="form-group">
                        <label for="" class="label_font_size"> Expiration Date</label>
                        <input type="text"  name="expiration_date" id="expiration_date" placeholder="eg. 2019-08-18" class="form-control input_size">
                    </div>
                    @if ($errors->has('expiration_date'))
                        <div class="text-danger">{{ $errors->first('expiration_date') }}</div>
                    @endif
                </div> -->
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="" class="label_font_size">Contact Type</label>
                        <select class="form-control input_size" name="contact_type" id="contact_type" value="{{ old('contact_type') }}">
                            @foreach($contact_types as $contact_type)
                                <option value="{{ $contact_type->contact_type }}" {{ old('contact_type') == $contact_type->contact_type ? 'selected' : '' }}>{{ $contact_type->contact_desc }}</option>
                            @endforeach
                        </select>
                    </div>
                    @if ($errors->has('contact_type'))
                        <div class="text-danger">{{ $errors->first('contact_type') }}</div>
                    @endif
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                    <label class="label_font_size" for="role">Status</label>
                    <select class="form-control input_size" name="status" id="status">
                        <option selected disabled>Select User Status</option>
                        <option value="1">Active</option>
                        <option value="0">Inactive</option>
                    </select>
                        @if ($errors->has('status'))
                            <div class="text-danger">{{ $errors->first('status') }}</div>
                        @endif
                    </div>
                </div>

                


            </div> <!-- .row form-group -->


            <div class="row form-group">
                <!-- <div class="col-md-3">
                    <div class="form-group">
                        <label for="" class="label_font_size"> Effective Date</label>
                        <div id="add_effective_date" class="input-group date" data-date-format="mm-dd-yyyy">
                            <input type="text" name="effective_date" id="effective_date" class="form-control input_size" value="{{ old('effective_date') }}" disabled>
                            <span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                </div> -->

                <!-- <div class="col-md-3">
                    <div class="form-group">
                        <label for="" class="label_font_size"> Expiry Date</label>
                        <div id="expiry_datepicker" class="input-group date" data-date-format="mm-dd-yyyy">
                            <input type="text" name="expiry_date" id="expiry_date" class="form-control input_size" value="{{ old('expiry_date') }}">
                            <span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                </div> -->

                

            </div> <!-- .row form-group -->

            <!-- <div class="col-md-6 offset-md-6 " > -->

            <div class="pull-right">
                <button type="submit" class="btn btn-outline-primary"><i class="fa fa-save"></i> Save</button>
            </div>

        </form>

    </div> <!-- .card-body -->
    <script>
      jQuery(document).ready(function () {
        jQuery("#add_effective_date").datepicker({ 
          format: "mm/dd/yy",
          weekStart: 0,
          calendarWeeks: true,
          autoclose: true,
          todayHighlight: true,
          orientation: "auto"
        }).datepicker('update', new Date());
      });
    </script>
@endsection