@extends('layout.app')

@section('content')
	<!-- Content -->
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
        	<!--  Traffic  -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                        	<div class="row">
                        		<div class="col-lg-6">
                    				<h4 class="box-title">Upload New Signature </h4>
                        		</div>
                        		<div class="col-lg-6">
                        			<a href="{{ route('signatures.index') }}" class="btn btn-outline-secondary float-right"><i class="fa fa-angle-double-left"></i> Back</a>
                        		</div>
                        	</div>
                        </div>
                        <div class="card-body">
                        	<form action="{{ route('signatures.store') }}" method="POST" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="row form-group">
                                  	<div class="col-md-6">
                                    	<label class="label_font_size" for="image">Select Signature Image :</label>
                                    	<input type="file" name="image" id="image" class="form-control input_size" value="{{ old('image') }}" style="padding: 1px 0px 0px 9px !important;">
                                    	@if ($errors->has('image'))
                                      		<div class="text-danger">{{ $errors->first('image') }}</div>
                                    	@endif
                                  	</div>
                                </div>

                                <button type="submit" class="btn btn-outline-primary float-right"><i class="fa fa-save"></i> Save</button>
                        	</form>
                        </div>
                        <div class="card-body"></div>
                    </div>
                </div><!-- /# column -->
            </div>
            <!--  /Traffic -->
            <div class="clearfix"></div>
        </div>
    	<!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>
@endsection