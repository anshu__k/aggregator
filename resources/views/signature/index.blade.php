@extends('layout.app')

@section('content')

	<!-- Content -->
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
        	<!--  Traffic  -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                        	<!-- Error & Message -->
	                        	@if (session()->has('insert'))
							      	<div class="alert alert-success alert-dismissible" role="alert">
							        	{{ session()->get('insert') }}
							        	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							          		<span aria-hidden="true">&times;</span>
							        	</button>
							      	</div>
							    @elseif (session()->has('delete'))
							      	<div class="alert alert-danger alert-dismissible" role="alert">
							        	{{ session()->get('delete') }}
							        	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							          		<span aria-hidden="true">&times;</span>
							        	</button>
							      	</div>
							    @endif
							<!-- Error & Message -->
                        	<div class="row form-group">
                        		<div class="col-md-3">
                    				<h4 class="box-title">Signature List </h4>
                        		</div>

                        		<div class="col-md-6">
                        			<div class="input-group">
                        				<input class="form-control"  name="search_term" id="search_sign_term" placeholder="Search..">
                        				<div class="input-group-btn">
                        					<button type="button" id="search_signature" class="btn btn-outline-primary" >Search</button>
                        				</div>

                        				<a class="btn input-group-btn"href="{{ route('signatures.index') }}" >Reset</a>
                        			</div>
                        		</div>

                        		<div class="col-md-3">
                        			<a href="{{ route('signatures.create') }}" class="btn btn-outline-success float-right"><i class="fa fa-plus"></i> ADD</a>
                        		</div>
                        	</div>
                        </div>
                        <div class="card-body nopadding">
                        	<div class="col-md-12 table-stats order-table ov-h nopadding">
	                            <table class="table">
				                  	<thead>
					                    <tr>
					                    	<!-- <th>#</th> -->
					                      	<th>User Name</th>
					                      	<th>File</th>
					                      	<th style="text-align: left;">Action</th>
					                    </tr>
				                  	</thead>

				                  	<tbody class="table_format">
				                    	<!-- {{$i = 1}}  -->
				                    	@if(!empty($signature))
				                      	<tr>
					                        <!-- <td>1</td>  -->
					                        <td>{{ $signature->user->name }}</td> 
					                        <td>
					                        	

					                        	<a href="{{ url('public/signature/'.$signature->image_name) }}" target="_blank">
					                        		<img src="{{ url('public/signature/'.$signature->image_name) }}" />
					                        	</a>
					                        </td> 
					                        <td style="text-align: left;">
					                        	<form action="{{ route('signatures.destroy', $signature) }}" method="POST">
					                            {{ csrf_field() }}
					                            	<input type="hidden" name="_method" value="DELETE">
					                            	<input type="hidden" name="_token" value="{{ csrf_token() }}">
					                            	<button type="submit" class="fabutton" onclick="return myFunction();">
					                              		<i class="fa fa-trash dlt_btn" rel="tooltip" title="Delete"></i>
					                            	</button>
					                          	</form>
					                        </td>
				                      	</tr>
				                    	@else
					                        <tr>
					                          <td colspan="3" class="text-center">
					                          	<p>Signature Not Found.</p>
					                          </td> 
					                        </tr>
				                    	@endif
				                  	</tbody>
				                </table>
                        	</div> <!-- col-md-12 -->
				        </div><!-- card-body -->
                        <div class="card-body">
                            
                        </div>
                        <div class="card-body"></div>
                    </div>
                </div><!-- /# column -->
            </div>
            <!--  /Traffic -->
            <div class="clearfix"></div>
        </div>
    	<!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>
@endsection