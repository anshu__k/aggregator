<table class="table">
                                    <thead>
                                        <tr>
                                            <!-- <th>#</th> -->
                                            <th>Source Number</th>
                                            <th>Source Model</th>
                                            <th>To Machine Number</th>
                                            <th>Customer Name</th>
                                            <th>Contact Name</th>
                                            <th>Source Ship Method</th>
                                            <th>Ship Date Button</th>
                                            <th>Ship Date</th>
                                            <th>Push Wipe Test</th>
                                            <th>Inst. Wipe Test</th>
                                            <th>Confirm</th>
                                            <th>Rem. Wipe Test</th>
                                            <th colspan="3" style="text-align: center;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody class="table_format">
                                        @forelse($source_locs as $key => $source_loc)
                                       
                                            <tr source-log="{{ $source_loc->id }}">
                                                <td>{{ $source_loc->source_num }}</td> 
                                                <td>{{ $source_loc->source_model->s_mod_num }}</td> 
                                                <td>{{ $source_loc->mach_num }}</td> 
                                                <!-- <td>{{ isset($source_loc->machine_num_id->mach_number)?$source_loc->machine_num_id->mach_number:'' }}</td>  -->
                                                <td>{{ isset($source_loc->machine_num->mach_loc->customer->customer_name) ? $source_loc->machine_num->mach_loc->customer->customer_name : 'NA' }}</td> 

                                                <td>{{ isset($source_loc->contact_name->contact_name) ? $source_loc->contact_name->contact_name : 'NA'}}</td>

                                                <td>{{ isset($source_loc->source_ship_method->sstat_desc) ? $source_loc->source_ship_method->sstat_desc : 'NA'}}</td> 
                                                
                                                <!-- <td>
                                                    <a href="{{ route('preAssignSou.edit', $source_loc) }}">
                                                        <i class="fa fa-edit edit_btn" rel="tooltip" title="Edit"></i>
                                                    </a>
                                                </td> -->
                                                <td style="text-align: center;">
                                                    <!-- <i class="fa fa-check edit_btn ship_date" rel="tooltip" title="Confirm Shift Date" data-toggle="modal" data-target="#confirmShiftDate_{{ $source_loc->id }}" ship_date="{{ date('m/d/Y', strtotime($source_loc->s_ship_date)) }}" sourceloc_id="{{ $source_loc->id }}"></i> -->
                                                    <button style="padding: 3px 5px; font-size: 10px; font-weight: 700" class="btn btn-outline-primary ship_date" data-toggle="modal" data-target="#confirmShiftDate_{{ $source_loc->id }}" ship_date="{{ !empty($source_loc->s_ship_date)?date('m/d/Y', strtotime($source_loc->s_ship_date)):date('m/d/Y') }}" sourceloc_id="{{ $source_loc->id }}">Ship Date</button>
                                                </td>
                                                <div class="modal fade" id="confirmShiftDate_{{ $source_loc->id }}" role="dialog">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <h4 class="modal-title">Ship Date</h4>
                                                            </div>
                                                            <form action="{{ route('confirmShipDate') }}" method="post">
                                                                <input type="hidden" name="sourceloc_id" value="{{ $source_loc->id }}">
                                                                @csrf
                                                                <div class="modal-body">
                                                                    <!-- <p>Are You Sure Want To Confirm Shift Date ?</p> -->
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <label class="label_font_size" for="ship_date_storage">Ship Date :</label>
                                                                        </div>
                                                                        <div class="col-md-6">

                                                                            <div id="edit_source_ship_date_{{ $source_loc->id }}" class="input-group date" data-date-format="mm-dd-yyyy">
                                                                                <input type="text" name="ship_date" class="form-control input_size">
                                                                                <span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
                                                                            </div>
                                                                            @if ($errors->has('ship_date'))
                                                                                <div class="text-danger">{{ $errors->first('ship_date') }}</div>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                                                                    <button type="submit" class="btn btn-success" >yes</button>
                                                                </div>
                                                            </form>
                                                        </div>      
                                                    </div>
                                                </div>
                                                <td>
                                                    @if($source_loc->s_ship_date)
                                                        {{ date('m/d/y', strtotime($source_loc->s_ship_date)) }}
                                                    @else
                                                        NA
                                                    @endif
                                                </td>
                                                <td style="text-align: center;">
                                                    <!-- <i class="fa fa-plus edit_btn" rel="tooltip" title="Push Wipe Test" data-toggle="modal" data-target="#pushWipeDate_{{ $source_loc->id }}"></i> -->
                                                    <button style="padding: 3px 5px; font-size: 10px; font-weight: 700" class="btn btn-outline-primary" data-toggle="modal" data-target="#pushWipeDate_{{ $source_loc->id }}">Push Wipe Test</button>
                                                </td>
                                                <div class="modal fade" id="pushWipeDate_{{ $source_loc->id }}" role="dialog">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <h4 class="modal-title">Push Wipe Date</h4>
                                                            </div>
                                                            <form action="{{ route('createWipe') }}" method="post">
                                                                @csrf
                                                                <input type="hidden" name="source_loc_id" value="{{ $source_loc->id }}">
                                                                <input type="hidden" name="ship_dt" value="{{date('m/d/y',strtotime($source_loc->s_ship_date)) }}">
                                                                <div class="modal-body">
                                                                    <div class="row">

                                                                        <div class="col-md-4">
                                                                            <label class="label_font_size" for="measure_by">Source Number :</label>
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                            <input type="text" class="form-control input_size" value="{{ $source_loc->source_num }}" disabled>
                                                                        </div>

                                                                        <div class="col-md-4">
                                                                            <label class="label_font_size" for="measure_by">To Machine Number :</label>
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                            <input type="text" class="form-control input_size" value="{{isset($source_loc->machine_num_id->mach_number)?$source_loc->machine_num_id->mach_number:''}}" disabled>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <label class="label_font_size" for="measure_by">Customer Name :</label>
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                            <input type="text" class="form-control input_size" value="{{ isset($source_loc->machine_num->mach_loc->customer->customer_name) ? $source_loc->machine_num->mach_loc->customer->customer_name : 'NA' }}" disabled>
                                                                        </div>    
                                                                        <div class="col-md-4">
                                                                            <label class="label_font_size" for="measure_by">Customer Number :</label>
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                            <input type="text" class="form-control input_size" value="{{ isset($source_loc->machine_num->mach_loc->customer->customer_number) ? $source_loc->machine_num->mach_loc->customer->customer_number : 'NA' }}" disabled>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <label class="label_font_size" for="select_customer">Contact Name :</label> 
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                             <input type="text" class="form-control input_size" value="{{ isset($source_loc->contact_name->contact_name) ? $source_loc->contact_name->contact_name : 'NA' }}" disabled>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <label class="label_font_size" for="measure_by">Contact Number :</label>
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                            <input type="text" class="form-control input_size" value="{{ isset($source_loc->contact_name->contact_phone) ? $source_loc->contact_name->contact_phone : 'NA' }}" disabled>
                                                                        </div>

                                                                        <div class="col-md-4">
                                                                            <label class="label_font_size" for="select_customer">Technician :</label>
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                            <select class="form-control select_tech_model input_size" name="tech" value="{{ old('tech') }}"></select>
                                                                            @if ($errors->has('tech'))
                                                                            <div class="text-danger">{{ $errors->first('tech') }}</div>
                                                                            @endif
                                                                        </div>

                                                                        <div class="col-md-4">
                                                                            <label class="label_font_size" for="select_customer">Discrepcod :</label>
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                            <select class="form-control input_size" name="discrepcod" value="{{ old('discrepcod') }}">
                                                                                <option selected disabled>Select Discrepancies</option>
                                                                                @foreach($discreps as $discrep)
                                                                                <option value="{{ $discrep->discrepcod }}" {{ old('discrepcod') == $discrep->discrepcod ? 'selected' : '' }}>{{ $discrep->descriptn }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                            @if ($errors->has('discrepcod'))
                                                                            <div class="text-danger">{{ $errors->first('discrepcod') }}</div>
                                                                            @endif
                                                                        </div>

                                                                        <div class="col-md-4">
                                                                            <label class="label_font_size" for="select_customer">Wipe Date :</label>
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                            <div id="add_wipe_date" class="add_wipe_date input-group date" data-date-format="mm-dd-yyyy">
                                                                                <input type="text" name="wipe_date" class="form-control input_size" value="{{ old('wipe_date') }}">
                                                                                <span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
                                                                            </div>
                                                                            @if ($errors->has('wipe_date'))
                                                                                <div class="text-danger">{{ $errors->first('wipe_date') }}</div>
                                                                            @endif
                                                                        </div>

                                                                        <div class="col-md-4">
                                                                            <label class="label_font_size" for="measure_date">Measure Date :</label>
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                            <div id="add_measure_date" class="add_measure_date input-group date" data-date-format="mm-dd-yyyy">
                                                                                <input type="text" name="measure_date" class="form-control input_size" value="{{ old('measure_date') }}">
                                                                                <span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
                                                                            </div>
                                                                            @if($errors->has('measure_date'))
                                                                                <div class="text-danger">{{ $errors->first('measure_date') }}</div>
                                                                            @endif
                                                                        </div>

                                                                        <div class="col-md-4">
                                                                            <label class="label_font_size" for="measure_by">Measure By :</label>
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                            <select class="form-control input_size" name="measure_by" value="{{ old('measure_by') }}">
                                                                                <option selected disabled>Select measure by</option>
                                                                                @foreach($measurers as $measurer)
                                                                                <option value="{{ $measurer->meas_by }}">{{ $measurer->meas_name }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                            @if($errors->has('measure_by'))
                                                                                <div class="text-danger">{{ $errors->first('measure_by') }}</div>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                                                                    <button class="btn btn-success" type="submit">Create Wipe</button>
                                                                </div>
                                                            </form>
                                                        </div>      
                                                    </div>
                                                </div>
                                                <td>{{ ($source_loc->installation_wipe_test) ? 'Submited' : '' }}</td>
                                                @if(isset($source_loc->installation_wipe_test) && $source_loc->installation_wipe_test !='')
                                                <td> <input type="checkbox" sourcelogid="{{ $source_loc->id }}" source_num="{{ $source_loc->source_num }}" mach_number="{{ $source_loc->mach_num }}" customer_id="{{isset($source_loc->machine_num->mach_loc->customer->id)?$source_loc->machine_num->mach_loc->customer->id:''}} "name="confirm" class="form-control confirmWipeTest" id="confirmWipeTest"> </td>
                                                @else
                                                <td> <input type="checkbox" sourcelogid="{{ $source_loc->id }}" source_num="{{ $source_loc->source_num }}" mach_number="{{ $source_loc->mach_num }}" customer_id="{{isset($source_loc->machine_num->mach_loc->customer->id)?$source_loc->machine_num->mach_loc->customer->id:''}} "name="confirm" class="form-control confirmWipeTest" id="confirmWipeTest"  title="add inst. wipe test first" disabled> </td>
                                                @endif
                                                <td>{{ ($source_loc->renewable_wipe_test) ? $source_loc->customer_number['customer_number'] : '' }}</td>
                                                @if(isset($source_loc->s_ship_date) && $source_loc->s_ship_date !='')
                                                @if($source_loc->s_status=='cp' || $source_loc->s_status=='ip')
                                                <td>
                                                    <a href="{{ route('pdfView', [$source_loc, 'action' => 'preAssignSourcePdf', 'type' => 'pdf1']) }}" target="_blank">
                                                        <i class="fa fa-file-text edit_btn" rel="tooltip" title="View Pre-Assign Source PDF"></i>
                                                    </a>
                                                </td>
                                                @else
                                                 <td>
                                                    <a href="{{ route('pdfView', [$source_loc, 'action' => 'preAssignSourcePdf', 'type' => 'pdf2']) }}" target="_blank">
                                                        <i class="fa fa-file-text edit_btn" rel="tooltip" title="View Pre-Assign Source PDF"></i>
                                                    </a>
                                                </td>   
                                                @endif
                                                @endif
                                                <td style="text-align: right; ">
                                                    <a href="{{ route('preAssignSou.edit', $source_loc) }}">
                                                        <i class="fa fa-edit edit_btn" rel="tooltip" title="Edit"></i>
                                                    </a>
                                                </td>
                                                <td style="text-align: left;">
                                                    <form action="{{ route('preAssignSou.destroy', $source_loc) }}" method="POST">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <button type="submit" class="fabutton" onclick="return myFunction();">
                                                            <i class="fa fa-trash dlt_btn" rel="tooltip" title="Delete"></i>
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="5" class="text-center">
                                                    <p>Pre-Assign Source Not Found.</p>
                                                </td> 
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>

                                <div class="card-body">
                                    {{ $source_locs->links() }}
                                </div>

                                 <script>
            jQuery(document).ready(function(){
                jQuery('.select_tech_model').select2({
                    placeholder: 'Select technician by name',
                    ajax: {
                        url: '{{ route("technician_autocomplete") }}',
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: jQuery.trim(params.term)
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: data
                            };
                        },
                        cache: true
                    }
                });
            });
        </script>
        <script type="text/javascript">
        jQuery(document).ready(function(){
            
            jQuery('.confirmWipeTest').click(function(){
                jQuery('.source_num').val(jQuery(this).attr('source_num'));
                jQuery('.mach_num').val(jQuery(this).attr('mach_number'));
                jQuery('.sourcelogid').val(jQuery(this).attr('sourcelogid'));
                jQuery('#confirmWipe').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                jQuery('#confirmWipe').modal('show');
            });

            jQuery(".add_wipe_date").datepicker({ 
                format: "mm/dd/yy",
                weekStart: 0,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                orientation: "auto"
            }).datepicker('update', new Date());

            jQuery(".add_measure_date").datepicker({ 
                format: "mm/dd/yy",
                weekStart: 0,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                orientation: "auto"
            }).datepicker('update', new Date());

            jQuery('.ship_date').on('click', function(){

                var set_ship_date = jQuery(this).attr('ship_date');
                var sourceloc_id = jQuery(this).attr('sourceloc_id');
                console.log(set_ship_date);
                console.log(sourceloc_id);
                console.log('enter');
                var dateToday = new Date();

                jQuery("#edit_source_ship_date_"+sourceloc_id).datepicker({ 
                    format: "mm/dd/yyyy",
                    weekStart: 0,
                    calendarWeeks: true,
                    autoclose: true,
                    todayHighlight: true,
                    startDate: dateToday,
                    orientation: "auto"
                }).datepicker('setDate', set_ship_date);
            });

            
            jQuery("select[name=discrepcod]").change(function () {
                var end = this.value;
               var dt = jQuery(this).closest(".modal-content").children('input[name=ship_dt]').val();
                if(end==9){
                   jQuery('input[name=wipe_date]').val(dt);
                   jQuery('input[name=measure_date]').val(dt);
                  jQuery('input[name=wipe_date]').attr( 'readOnly' , 'true' );
                   jQuery('input[name=measure_date]').attr( 'readOnly' , 'true' );
                   jQuery(".add_wipe_date"). datepicker("remove");
                   jQuery(".add_measure_date"). datepicker("remove");

                }else{
                    jQuery('input[name=wipe_date]').removeAttr("readOnly"); 
                   jQuery('input[name=measure_date]').removeAttr("readOnly"); 
                   jQuery(".add_wipe_date").datepicker({ 
                        format: "mm/dd/yy",
                        weekStart: 0,
                        calendarWeeks: true,
                        autoclose: true,
                        todayHighlight: true,
                        orientation: "auto", 
                    }).datepicker('update', new Date());

                    jQuery(".add_measure_date").datepicker({ 
                        format: "mm/dd/yy",
                        weekStart: 0,
                        calendarWeeks: true,
                        autoclose: true,
                        todayHighlight: true,
                        orientation: "auto", 
                    }).datepicker('update', new Date());
                        }
                    });
                });
            

            jQuery(document).ready(function(){

                function fetch_pre_assign_source_data(query,page)
                {
                    jQuery.ajax({
                        url:"{{ route('pre_assign_source_search') }}",
                        method:'GET',
                        data:{query:query,page:page},
                        dataType:'html',
                        success:function(data)
                        {
                            jQuery('#searchid').html(data);
                            jQuery('.page_hide').css('cssText', 'display:none');
                            //searchData();
                        }
                    })
                }
                
                    jQuery(document).on('click', '#search_wipe', function(){
                        
                        var query = jQuery('#search_wipe_term').val();
                        
                         if (query != '') {

                            fetch_pre_assign_source_data(query,1);
                        }
                     
                     });

                    jQuery(document).on('click', '.pagination a', function(event){
                      

                      var query = jQuery('#search_wipe_term').val();
                      

                         if (query != '') {
                            event.preventDefault(); 
                      var page = jQuery(this).attr('href').split('page=')[1];
                      
                         fetch_pre_assign_source_data(query,page);

                       }  
                     });
                

            });

            jQuery('#search_wipe_term').keypress(function (e) {
                var key = e.which;
                if(key == 13)  // the enter key code
                {
                    jQuery('#search_wipe').click();
                    return false;  
                }
            });  

            
        </script>