@extends('layout.app')

@section('content')
    <style type="text/css">
        .confirmWipeTest{
            height: calc(1.25rem + 2px);
        }
    </style>
	<!-- Content -->
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
        	<!--  Traffic  -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <!-- Error & Message -->
                                @if (session()->has('insert'))
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        {{ session()->get('insert') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @elseif (session()->has('update'))
                                    <div class="alert alert-info alert-dismissible" role="alert">
                                        {{ session()->get('update') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @elseif (session()->has('delete'))
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        {{ session()->get('delete') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                            <!-- Error & Message -->
                            <div class="row form-group">
                                <div class="col-md-3">
                                    <h4 class="box-title">Pre-Assign Source List </h4>
                                </div>

                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input class="form-control"  name="search_term" id="search_wipe_term" placeholder="Search..">
                                        <div class="input-group-btn">
                                            <button type="button" id="search_wipe" class="btn btn-outline-primary" >Search</button>
                                        </div>

                                        <a class="btn input-group-btn"href="{{ route('preAssignSou.index') }}" >Reset</a>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <a href="{{ route('preAssignSou.create') }}" class="btn btn-outline-success float-right"><i class="fa fa-plus"></i> ADD</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body nopadding">
                            <div id="searchid" class="col-md-12 table-stats order-table ov-h nopadding">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <!-- <th>#</th> -->
                                            <th>Source Number</th>
                                            <th>Source Model</th>
                                            <th>To Machine Number</th>
                                            <th>Customer Name</th>
                                            <th>Contact Name</th>
                                            <th>Source Ship Method</th>
                                            <th>Ship Date</th>
                                            <th>Inst. Wipe Test</th>
                                            <th>Rem. Wipe Test</th>
                                            
                                            <th colspan="3" style="text-align: center;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody class="table_format">
                                        @forelse($source_locs as $key => $source_loc)
                                       
                                            <tr source-log="{{ $source_loc->id }}">
                                                <td>{{ $source_loc->source_num }}</td> 
                                                <td>{{ $source_loc->source_model->s_mod_num }}</td> 
                                                <td>{{ $source_loc->mach_num }}</td> 
                                                <!-- <td>{{ isset($source_loc->machine_num_id->mach_number)?$source_loc->machine_num_id->mach_number:'' }}</td>  -->
                                                <td>{{ isset($source_loc->machine_num->mach_loc->customer->customer_name) ? $source_loc->machine_num->mach_loc->customer->customer_name : 'NA' }}</td> 

                                                <td>{{ isset($source_loc->contact_name->contact_name) ? $source_loc->contact_name->contact_name : 'NA'}}</td>

                                                <td>{{ isset($source_loc->source_ship_method_prev->sstat_desc) ? $source_loc->source_ship_method_prev->sstat_desc : 'NA'}}</td> 
                                                

                                                <!-- <td>
                                                    <a href="{{ route('preAssignSou.edit', $source_loc) }}">
                                                        <i class="fa fa-edit edit_btn" rel="tooltip" title="Edit"></i>
                                                    </a>
                                                </td> -->
                                                
                                                <td>
                                                    @if($source_loc->s_ship_date)
                                                        {{ date('m/d/y', strtotime($source_loc->s_ship_date)) }}
                                                    @else
                                                        NA
                                                    @endif
                                                </td> 

                                                
                                                <td>{{ ($source_loc->installation_wipe_test) ? 'Submited' : '' }}</td>
                                                <td>{{ ($source_loc->renewable_wipe_test) ? $source_loc->renewable_wipe_test : '' }}</td>
                                                <td style="text-align: left;">
                                                   @if(isset($source_loc->s_ship_date) && $source_loc->s_ship_date !='')
                                                @if($source_loc->s_status=='cp' || $source_loc->s_status=='ip')
                                                <td>
                                                    <a href="{{ route('pdfView', [$source_loc, 'action' => 'preAssignSourcePdf', 'type' => 'pdf1']) }}" target="_blank">
                                                        <i class="fa fa-file-text edit_btn" rel="tooltip" title="View Pre-Assign Source PDF"></i>
                                                    </a>
                                                </td>
                                                @else
                                                 <td>
                                                    <a href="{{ route('pdfView', [$source_loc, 'action' => 'preAssignSourcePdf', 'type' => 'pdf2']) }}" target="_blank">
                                                        <i class="fa fa-file-text edit_btn" rel="tooltip" title="View Pre-Assign Source PDF"></i>
                                                    </a>
                                                </td>   
                                                @endif
                                                @endif 
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="5" class="text-center">
                                                    <p>Pre-Assign Source Not Found.</p>
                                                </td> 
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div> <!-- /.col-md-12 -->
                        </div><!-- card-body -->
                        <div class="card-body page_hide">
                            {{ $source_locs->onEachSide(1)->links() }}
                        </div>
                        <div class="card-body"></div>
                    </div>
                </div><!-- /# column -->
            </div>
            <!--  /Traffic -->
            <div class="clearfix"></div>
        </div>
    	<!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>


    <!-- ========================== Modal pop-up=====================-->
    <div class="modal fade" id="confirmWipe" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: darkgrey">
                    <h5 class="modal-title">Confirm Wipe Test</h5>
                </div>
                <div class="modal-body">
                    <div class="row" style="padding-left: 15px;">
                        <h5> Are you sure, you want to update this source log?  </h5>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="confirmYes" class="btn btn-success">Yes</button>
                    <button type="button" id="confirmNo" class="btn btn-danger">No</button>
                </div>
            </div>      
        </div>
    </div>
    <input type="hidden" class="source_num">
    <input type="hidden" class="mach_num">
    <input type="hidden" class="sourcelogid">
    <input type="hidden" class="customer_id">

    <script type="text/javascript">
        jQuery(document).ready(function(){
            
            jQuery('.confirmWipeTest').click(function(){
                jQuery('.source_num').val(jQuery(this).attr('source_num'));
                jQuery('.mach_num').val(jQuery(this).attr('mach_number'));
                jQuery('.sourcelogid').val(jQuery(this).attr('sourcelogid'));
                jQuery('.customer_id').val(jQuery(this).attr('customer_id'));
                jQuery('#confirmWipe').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                jQuery('#confirmWipe').modal('show');
            });

            jQuery('#confirmYes').click(function(){
                var sourcelog = jQuery('.sourcelogid').val();
                var source_num = jQuery('.source_num').val();
                jQuery.ajax({
                    type: "GET",
                    dataType: "JSON",
                    data:{"sourceloc_id" : sourcelog,"source_num" : source_num},
                    url: "{{ route('update-source-log') }}",
                    success: function(data) {
                        location.reload();
                        //jQuery("tr[source-log="+sourcelog+"]").hide();
                        //jQuery('#confirmWipe').modal('hide');
                    },
                    error:function(error){
                        console.log('error');
                    }
                });
            });

            jQuery('#confirmNo').click(function(){
                var sourcelog = jQuery('.sourcelogid').val();
                jQuery("input[sourcelogid="+sourcelog+"]").prop("checked", false);
                jQuery('#confirmWipe').modal('hide');
            });


            jQuery(".add_wipe_date").datepicker({ 
                format: "mm/dd/yy",
                weekStart: 0,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                orientation: "auto", 
            }).datepicker('update', new Date());

            jQuery(".add_measure_date").datepicker({ 
                format: "mm/dd/yy",
                weekStart: 0,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                orientation: "auto", 
            }).datepicker('update', new Date());

            jQuery('.ship_date').on('click', function(){

                var set_ship_date = jQuery(this).attr('ship_date');
                var sourceloc_id = jQuery(this).attr('sourceloc_id');
                var dateToday = new Date();

                jQuery("#edit_source_ship_date_"+sourceloc_id).datepicker({ 
                    format: "mm/dd/yyyy",
                    weekStart: 0,
                    calendarWeeks: true,
                    autoclose: true,
                    todayHighlight: true,
                    startDate: dateToday,
                    orientation: "auto"
                }).datepicker('setDate', set_ship_date);
            
        });
           
            jQuery("select[name=discrepcod]").change(function () {
                var end = this.value;
               var dt = jQuery(this).closest(".modal-content").children('input[name=ship_dt]').val();
                if(end==9){
                   jQuery('input[name=wipe_date]').val(dt);
                   jQuery('input[name=measure_date]').val(dt);
                  jQuery('input[name=wipe_date]').attr( 'readOnly' , 'true' );
                   jQuery('input[name=measure_date]').attr( 'readOnly' , 'true' );
                   jQuery(".add_wipe_date"). datepicker("remove");
                   jQuery(".add_measure_date"). datepicker("remove");

                }else{
                    jQuery('input[name=wipe_date]').removeAttr("readOnly"); 
                   jQuery('input[name=measure_date]').removeAttr("readOnly"); 
                   jQuery(".add_wipe_date").datepicker({ 
                        format: "mm/dd/yy",
                        weekStart: 0,
                        calendarWeeks: true,
                        autoclose: true,
                        todayHighlight: true,
                        orientation: "auto", 
                    }).datepicker('update', new Date());

                    jQuery(".add_measure_date").datepicker({ 
                        format: "mm/dd/yy",
                        weekStart: 0,
                        calendarWeeks: true,
                        autoclose: true,
                        todayHighlight: true,
                        orientation: "auto", 
                    }).datepicker('update', new Date());
                        }
                    }); 
                });
            

            jQuery(document).ready(function(){

                function fetch_pre_assign_source_data(query,page)
                {
                    jQuery.ajax({
                        url:"{{ route('pre_assign_source_search') }}",
                        method:'GET',
                        data:{query:query,page:page},
                        dataType:'html',
                        success:function(data)
                        {
                            jQuery('#searchid').html(data);
                            jQuery('.page_hide').css('cssText', 'display:none');
                            //searchData();
                        }
                    })
                }
                
                    jQuery(document).on('click', '#search_wipe', function(){
                        
                        var query = jQuery('#search_wipe_term').val();
                        
                         if (query != '') {

                            fetch_pre_assign_source_data(query,1);
                        }
                     
                     });

                    jQuery(document).on('click', '.pagination a', function(event){
                      

                      var query = jQuery('#search_wipe_term').val();
                      

                         if (query != '') {
                            event.preventDefault(); 
                      var page = jQuery(this).attr('href').split('page=')[1];
                      
                         fetch_pre_assign_source_data(query,page);

                       }  
                     });
                

            });

            jQuery('#search_wipe_term').keypress(function (e) {
                var key = e.which;
                if(key == 13)  // the enter key code
                {
                    jQuery('#search_wipe').click();
                    return false;  
                }
            });  

            
        </script>
    <!-- Search Pre Assign -->
<style type="text/css">
    .show_data{
        background-color: aliceblue;
    }

    .show_data td{
        border: 0px !important;
        padding: 3px !important;
    }
</style>
@endsection