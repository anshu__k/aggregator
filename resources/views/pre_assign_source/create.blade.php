@extends('layout.app')

@section('content')

    <!-- Content -->
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
            <!--  Traffic  -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-lg-6">
                                    <h4 class="box-title">Add New Pre-Assign Sources </h4>
                                </div>
                                <div class="col-lg-6">
                                    <a href="{{ route('preAssignSou.index') }}" class="btn btn-outline-secondary float-right"><i class="fa fa-angle-double-left"></i> Back</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('preAssignSou.store') }}" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="source_number" id="source_number">
                                {{csrf_field()}}

                                <div class="box_border">
                                    <div class="heading_font_size font-weight-bold">
                                        1. Verify the selected source and the its associated machine information.
                                    </div>

                                    <div class="form-group ml-5 mt-3">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="source_num">Source Being Issued :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <select class="form-control select_pre_assign_source_model  input_size" name="source_num" id="source_num" value="{{ old('source_num') }}"></select>
                                                @if ($errors->has('source_num'))
                                                    <div class="text-danger">{{ $errors->first('source_num') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="source_status">Current Source Status :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="source_status" id="source_status" class="form-control input_size" value="{{ old('source_status') }}" readonly>
                                                @if ($errors->has('source_status'))
                                                    <div class="text-danger">{{ $errors->first('source_status') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="current_source_model">Current Source Model :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="current_source_model" id="current_source_model" class="form-control input_size" value="{{ old('current_source_model') }}" readonly>
                                                @if ($errors->has('current_source_model'))
                                                    <div class="text-danger">{{ $errors->first('current_source_model') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="s_status_desc">Current Status Desc :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="s_status_desc" id="s_status_desc" class="form-control input_size" value="{{ old('s_status_desc') }}" readonly>
                                                @if ($errors->has('s_status_desc'))
                                                    <div class="text-danger">{{ $errors->first('s_status_desc') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group ml-5 mt-3">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="current_mach">Current Machine :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="current_mach" id="current_mach" class="form-control input_size" value="{{ old('current_mach') }}" readonly>
                                                @if ($errors->has('current_mach'))
                                                <div class="text-danger">{{ $errors->first('current_mach') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="current_mach_status">Current Machine Status :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="current_mach_status" id="current_mach_status" class="form-control input_size" value="{{ old('current_mach_status') }}" readonly>
                                                @if ($errors->has('current_mach_status'))
                                                <div class="text-danger">{{ $errors->first('current_mach_status') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="current_customer">Current Customer :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="current_customer" id="current_customer" class="form-control input_size" value="{{ old('current_customer') }}" readonly>
                                                @if ($errors->has('current_customer'))
                                                <div class="text-danger">{{ $errors->first('current_customer') }}</div>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="customer_number">Customer Number:</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="customer_number" id="customer_number" class="form-control input_size" value="{{ old('customer_number') }}" readonly>
                                                @if ($errors->has('customer_number'))
                                                <div class="text-danger">{{ $errors->first('customer_number') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box_border mt-4">
                                    <div class="heading_font_size font-weight-bold">
                                        2. Select the pre-assign machine and enter corresponding data.
                                    </div>

                                    <div class="form-group ml-5 mt-3">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="machine_num">Machine assigning to :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <select class="form-control select_machine_num input_size" name="machine_num" id="machine_num" value="{{ old('machine_num') }}"></select>
                                                @if ($errors->has('machine_num'))
                                                    <div class="text-danger">{{ $errors->first('machine_num') }}</div>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="customer_assign">Customer assigning to :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="customer_assign" id="customer_assign" class="form-control input_size" value="{{ old('customer_assign') }}" readonly>
                                                @if ($errors->has('customer_assign'))
                                                <div class="text-danger">{{ $errors->first('customer_assign') }}</div>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="customer_contact">Customer contact :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <!-- <input type="text" name="customer_contact" id="customer_contact" class="form-control input_size" value="{{ old('customer_contact') }}" readonly> -->
                                                
                                                <select name="customer_contact" id="customer_contact" class="form-control input_size" value="{{ old('customer_contact') }}">
                                                    <option selected disabled>Select Customer Contact</option>
                                                    
                                                </select>

                                                @if ($errors->has('customer_contact'))
                                                <div class="text-danger">{{ $errors->first('customer_contact') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group ml-5 mt-3">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="source_model">Source Model :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <select name="source_model" id="source_model" class="form-control input_size" value="{{ old('source_model') }}">
                                                    <option selected disabled>Select Source Model</option>
                                                    @foreach($source_mods as $source_mod)
                                                    <option value="{{ $source_mod->s_mod_num }}">{{ $source_mod->s_mod_num }}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('source_model'))
                                                    <div class="text-danger">{{ $errors->first('source_model') }}</div>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="source_ship_method">Source Ship Method :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <select name="source_ship_method" id="source_ship_method" class="form-control input_size" value="{{ old('source_ship_method') }}">
                                                    <option selected disabled>Select Source Ship Method</option>
                                                    @foreach($ship_methods as $ship_method)
                                                    <option value="{{ $ship_method->s_status }}">{{ $ship_method->sstat_desc }}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('source_ship_method'))
                                                    <div class="text-danger">{{ $errors->first('source_ship_method') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 mt-4">
                                        <button type="submit" class="btn btn-outline-primary float-right"><i class="fa fa-save"></i> Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card-body"></div>
                    </div>
                </div><!-- /# column -->
            </div>
            <!--  /Traffic -->
            <div class="clearfix"></div>
        </div>
        <!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>


    <!-- Enter Source Number Fetch Pre-assign Source Data -->
        <script>
            jQuery(document).ready(function(){
                jQuery('.select_pre_assign_source_model').select2({
                    placeholder: 'Enter Source Number',
                    ajax: {
                        url: '{{ route("preAssignSourceSearch") }}',
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: jQuery.trim(params.term)
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: data
                            };
                        },
                        cache: true
                    }
                });
            });

            jQuery(document).ready(function(){

                jQuery('.select_pre_assign_source_model').on('change', function(){
                    var source_num = jQuery(".select_pre_assign_source_model option:selected").val();
                    jQuery.ajax({
                        url:"{{ route('preAssignSourceAutofill') }}",
                        method:'GET',
                        data:{source_num:source_num},
                        dataType:'json',
                        success:function(data)
                        {
                            if(data.source_data.source_status != ''){
                                jQuery('#source_status').val(data.source_data.source_status);
                            }
                            if(data.source_data.s_status_desc != ''){
                                jQuery('#s_status_desc').val(data.source_data.s_status_desc);
                            }
                            if(data.source_data.curr_source_model != ''){
                                jQuery('#current_source_model').val(data.source_data.curr_source_model);
                            }
                            if(data.source_data.current_mach != ''){
                                jQuery('#current_mach').val(data.source_data.current_mach);
                            }
                            if(data.source_data.current_mach_status != ''){
                                jQuery('#current_mach_status').val(data.source_data.current_mach_status);
                            }
                            if(data.source_data.current_customer != ''){
                                jQuery('#current_customer').val(data.source_data.current_customer);
                            } 
                            if(data.source_data.source_num != ''){
                                jQuery('#source_number').val(data.source_data.source_num);
                            }
                            if(data.source_data.customer_number != ''){
                                jQuery('#customer_number').val(data.source_data.customer_number);
                            }
                        }
                    })
                });

                jQuery('.select_machine_num').on('change', function(){
                    var machine_number = jQuery(".select_machine_num option:selected").val();
                    jQuery.ajax({
                        url:"{{ route('preAssignMachineAutofill') }}",
                        method:'GET',
                        data:{machine_number:machine_number},
                        dataType:'json',
                        success:function(data)
                        {
                            if(data.machine_data.customer_number != ''){
                                jQuery('#customer_assign').val(data.machine_data.customer_number);
                            }

                                jQuery('#customer_contact').empty();
                                jQuery('#customer_contact').append(`<option selected disabled>Select Customer Contact</option>`);
                                jQuery.each(data.customer_data.customer_name, function( key, optionValue ) {
                                   if(optionValue !='') 
                                  jQuery('#customer_contact').append(`<option value="${key}">${optionValue}</option>`);

                                    });            
                       

                                //jQuery('#customer_contact').val(data.machine_data.customer_contact);
                              
                        }
                    })
                });
            });  
        </script>
    <!-- Enter Source Number Fetch Pre-assign Source Data -->

@endsection