<!DOCTYPE html>
<html lang="en">
<head>
	<title>{{ $title }}</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<style>
		.set_content{
			margin-right: 150px;
			padding-top: 150px;
			font-family: monospace;
			font-weight: 600;
		}
		.first_add p{
			margin-bottom: 0px;
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="set_content">
			<div class="row">
				<div class="first_add" style="float: right;">
					<p>ATTN : {{ $contact_name }}</p>
					<p>{{ $address }}</p>
					<p>{{ $country }}, {{ $state }}</p>
					<p>{{ $city }} - {{ $zipcode }}</p>
				</div>
			</div>
		</div>
	</div>
</body>
</html>