<!DOCTYPE html>
<html lang="en">
<head>
	<title>{{ $title }}</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<style>
		.set_content{
			margin: 0px 30px;
			font-family: monospace;
		}
		.header_line{
			font-weight: bold; 
			font-size: 15px; 
			padding-top: 5px;
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="set_content">
			<div class="row text-center header_line">
				<p style="border-bottom: 2px solid black; width:100%;">NOTIFICATION OF RECEIPT OF RADIOISOTOPE SOURCE</p>
			</div>

			<br>

			<div class="row">
				<div class="first_add" style="float: left;">
					<p>{{ date('d M Y', strtotime(now())) }}</p>
					<p style="margin-bottom: 0px;">{{ $contact_name }}</p>
					<p style="margin-bottom: 0px;">{{ $address }}</p>
					<p style="margin-bottom: 0px;">{{ $country }}, {{ $state }}</p>
					<p style="margin-bottom: 20px;">{{ $city }} - {{ $zipcode }}</p>
					<p style="margin-bottom: 40px;">Dear, {{$contact_name}}</p>
					<p>This letter acknowkedge that industrial dynamics has received the radioisotope source described below :</p>
				</div>
			</div>

			<div class="row">
				<p style="border-bottom: 2px solid black; width:15%; font-weight: 600;">SOURCE DATA</p>
			</div>
			<div class="row">
				<div class="col-xs-3">Serial Number :</div>
				<div class="col-xs-6">{{ $s_serial_no }}</div>
			</div>
			<div class="row">
				<div class="col-xs-3">Model Number :</div>
				<div class="col-xs-6">{{ $s_mod_no }}</div>
			</div>
			<div class="row">
				<div class="col-xs-3">Date Of Receipt :</div>
				<div class="col-xs-6"></div>
			</div>
			<div class="row">
				<div class="col-xs-3">Date Manufactured :</div>
				<div class="col-xs-6"></div>
			</div>
			<div class="row">
				<div class="col-xs-3">Type :</div>
				<div class="col-xs-6"></div>
			</div>
			<div class="row">
				<div class="col-xs-3">Strength :</div>
				<div class="col-xs-6">{{ $discrepancie }}</div>
			</div>

			<br>

			<div class="row">
				<p style="border-bottom: 2px solid black; width:51%; font-weight: 600;">EQUIPMENT FROM WHICH SOURCE WAS REMOVED</p>
			</div>
			<div class="row">
				<div class="col-xs-3">Model Number :</div>
				<div class="col-xs-6">{{ $m_model_no }}</div>
			</div>
			<div class="row">
				<div class="col-xs-3">Serial Number :</div>
				<div class="col-xs-6">{{ $m_serial_no }}</div>
			</div>
			<div class="row">
				<div class="col-xs-3">Manufacturer :</div>
				<div class="col-xs-6"></div>
			</div>

			<br>

			<div class="row">
				<p>This information must be kept in your files as proof that this radioisotope source/device has been handled as requried by the nuclear regulatory commission.</p>
			</div>

			<br>

			<div class="row">
				<div class="col-xs-6"></div>
				<div class="col-xs-6">
					<p>Yours very truly,</p>
					<p>INDUSTRIAL DYNAMICS COMPANY, LTD.</p>
				</div>
			</div>

			<br>
			<br>

			<div class="row">
				<div class="col-xs-6"></div>
				<div class="col-xs-6">
					<p>Terry Williams,</p>
					<p>Radiation Safety Officer</p>
				</div>
			</div>
		</div>
	</div>
</body>
</html>