<!DOCTYPE html>
<html lang="en">
<head>
	<title>{{ $title }}</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<style>
		.set_content{
			padding-top: 50px;
			margin: 0px 30px;
			font-family: monospace;
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="set_content">
			<div class="row">
				<div class="row">
					<div class="col-xs-12">
						<p style="float: right;"><span style="font-weight: 600;">PREFRENCE :</span> FILTEC MODEL FILTEC 3-G</p><br>
						<p style="float: right;"><span style="font-weight: 600;">Inspection System Serial Number :</span> {{ $inspection_system_serial_number }} </p><br>
						<p style="float: right;"><span style="font-weight: 600;">Source Serial Number :</span> {{ $source_serial_number }} </p><br>
						<p style="float: right;"><span style="font-weight: 600;">Date :</span> {{ date('m/d/Y', strtotime($date)) }}</p><br>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<p style="float: left;">{{ $customer_name }}</p><br>
						<p style="float: left;">{{ $address }}</p><br>
						<p style="float: left;">{{ $country }}, {{ $state }}</p><br>
						<p style="float: left;">{{ $city }} - {{ $zipcode }}</p><br>
					</div>
				</div>

				<div class="row" style="margin-top: 10px;">
					<div class="col-xs-12">
						<p style="float: left;">Dear {{ $customer_salutation}} {{ $contact_number }}:</p><br>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<p style="float: left;">Industrial Dynamics FILTEC utilizes a radioisotope as a source of radiation. This radioisotope source (SOURCE) is distributed to you under a radioactive material license issued to us by the state of california. As a general license, using a generally licensed device, you must operate and maintain the FILTEC in accordance with the rules and regulations set forth by your state's regulatory Agency.These regulations require that you register the SOURCE within 30 days after its receipt! This is your responsibility and should be attended to immediately! We have supplied information to assist you in the registration of the SOURCE, but if you should require additional data, please contact us immediately. </p>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-2">
						<p style="border-bottom: 1px solid black;">&nbsp;</p>
					</div>
					<div class="col-xs-10">
						<p>We have enclosed the registration forms and instructions for registering the SOURCE in your state. Please complete the forms and mail to your state agency.</p>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-2">
						<p style="border-bottom: 1px solid black; text-align: center;">x</p>
					</div>
					<div class="col-xs-10">
						<p>Please contact your regulatory agency (see enclosure) and request the necessary registration forms and instructions for registering the SOURCE. (Your agency prefers to send the necessary forms directly to you.)</p>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<p style="float: left;">The following information is enclosed pertaining to the FILTEC and its SOURCE. File them in a safe place for future reference.</p>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-1">
						<p style="float: right;">1.</p>
					</div>
					<div class="col-xs-11">
						<p>Industrial Dynamics "FILTEC  Radioisotope Source Information Manual".</p>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-1">
						<p style="float: right;">2.</p>
					</div>
					<div class="col-xs-11">
						<p>Radiation Rules and Regulations applicable to your FILTEC.</p>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-1">
						<p style="float: right;">3.</p>
					</div>
					<div class="col-xs-11">
						<p>The address and telephone number of the agency having ragulatory responsibility for your SOURCE.</p>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<p style="float: left;">The specifications on the FILTEC and its SOURCE are listed below:</p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-11">
						<p>*Name and Model of Machine:  FILTEC, Model FILTEC 3-G</p>
						<p>*Radioactive Material (Ceramic Enamel Form):  Americium-241</p>
						<p>*Sealed SOURCE Model NO.: {{$source_mod_number}}</p>
						<p>*Quantity of Radioactive Material:{{$source_mod_desc}}</p>
						<p>*Use: Gamma Density Measuring Gauge</p>
						<p>*Industrial Dynamics’ General Distribution License No. 1586-19GL</p>
						
						
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p style="float: left;">The FILTEC will be shipped to your plant with or without the SOURCE as indicated below.  When you are prepared to install and check out the FILTEC, please contact our Field Services Department to confirm service date.  Once this date is confirmed, the SOURCE, if required, will be sent to you by the following method and a qualified service technician will complete the installation of the FILTEC unit and perform all the required tests:</p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-2">
						@if($type=='pdf1')
						<p style="border-bottom: 1px solid black; text-align: center;">&nbsp;</p>
						@else
						<p style="border-bottom: 1px solid black; text-align: center;">x</p>
						@endif
					</div>
					<div class="col-xs-10">
						<p>The SOURCE(s) will be shipped within the FILTEC unit(s) as an ‘Excepted Package, UN2911’ to your plant. You may temporarily store the unit within a controlled area.  If the package is damaged, please notify us immediately.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-2">
						@if($type=='pdf1')
						<p style="border-bottom: 1px solid black; text-align: center;">x</p>
						@else
						<p style="border-bottom: 1px solid black; text-align: center;">&nbsp;</p>
						@endif
					</div>
					<div class="col-xs-10">
						<p>The SOURCE(s) will be shipped by Airfreight directly to your facility, to the attention of our Authorized Field Service Representative who will handle the package acceptance, installation and checkout.</p>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<p style="float: left;"><b><u>IMPORTANT NOTICE</u></b></p>
					</div>
				</div>	
				<div class="row">	
					<div class="col-xs-12">
						<b style="float: left;">Regulations prohibit unqualified and/or unlicensed personnel from:</b>
					</div>
				</div>

				<div class="row">
				
					<div class="col-xs-10">
						<p><b>a)	Performing the installation of the SOURCE.</b></p>
						<p><b>b)	Servicing and testing involving the SOURCE or its shielding.</b></p>
						<p><b>c)	Initial start-up, dismantling or relocating the FILTEC unit with the SOURCE.</b></p>
					</div>
				</div>

				<div class="row">	
					<div class="col-xs-12">
						<p style="float: left;">These tasks should only be performed by an Industrial Dynamics <b><u>specifically licensed</b></u> technician.</p>
					</div>
				</div>				
				<div class="row">	
					<div class="col-xs-12">
						<p style="float: left;">Attachments to this letter show the information given on the label (s) affixed to your FILTEC equipment.  The information on the label(s), as well as the other enclosed data should be carefully studied to ensure that you comply with all the radiation regulations and that proper operation and maintenance on your FILTEC unit is achieved.</p>
					</div>
				</div>

				<div class="row">	
					<div class="col-xs-12">
						<p style="float: left;">One very important regulation will be mentioned regarding the radiation wipe test requirements on the SOURCE. <u> A wipe test will be performed when the FILTEC is installed, and the tests must be conducted at thirty-six-month intervals thereafter.</u> Industrial Dynamics will gladly assist you in complying with this requirement. Our company is authorized and employs properly trained technicians to perform these tests.  Please contact our Field Service Department to schedule this service visit. A factory employee may contact you as a courtesy to let you know that your plant is due for this service.</p>
					</div>
				</div>				
				<div class="row">	
					<div class="col-xs-12" style="float: left;">
						<p>Yours very truly,</p>
						<p>{{$rso_name}}</p>
						<!-- <p>Radiation Safety Officer</p> -->
					</div>
				</div>


			</div>
		</div>
	</div>
</body>
</html>