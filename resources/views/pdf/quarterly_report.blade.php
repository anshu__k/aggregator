<!DOCTYPE html>
<html lang="en">
<head>
	<title></title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<style>
		.set_content{
			padding-top: 20px;
			margin: 0px 30px;
			font-family: monospace;
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="set_content">
			<div class="row">
				<div class="row">
					<div class="col-xs-3"></div>
					<div class="col-xs-6"><?php //echo "<pre>"; print_r($sourcedata); die;?>
						<h5 style="text-align:center">TRANSFERS OF INDUSTRIAL DEVICE(S) REPORT</h5> 
						<h5 style="text-align:center">QUARTERLY REPORT FOR THE PERIOD: {{ date('d/m/Y', strtotime($from_date)) }} - {{ date('d/m/Y', strtotime($to_date))}}</h5> 
						<p style="text-align:center">Industrial Dynamics Company, Ltd.
3100 Fujita Street
Torrance, CA 90505</p>
					</div>
					<div class="col-xs-3"></div>
				</div>
				<div class="row">
					<div class="col-xs-12">NOTICE: The information in this report is proprietary and considered to be a ”trade secret” and, therefore, must
not be disclosed to the public nor any other parties without the prior written consent of Industrial Dynamics or by
proper court authority</div>
				</div>
				<div class="row" style="margin-top: 20px;">
					<div class="col-xs-12">SUBJECT: Transfer of radioisotope sources utilized in FILTEC FILL LEVEL INPSECTION EQUIPMENT. Each sealed
source contains 100 (or 300 as denoted by LQ, LV, CW, LX, or AR after the Source Serial No.) Millicuries of
Americium-241 distributed under California General License 1586-19GL.</div>
				</div>
				<div class="row" style="margin-top: 10px;">
					<div class="col-xs-12">
						<p style="float: left;">AGREEMENT STATE </p><br>
						<p style="float: left;">State of California</p><br>
						<p style="float: left;">Radiologic Health Branch</p><br>
						<p style="float: left;">California Dept of Health Services</p><br>
						<p style="float: left;">Mailstop 7610, PO Box 997414</p><br>
						<p style="float: left;">Sacramento, CA 95899-7414</p><br>
					</div>
				</div>
				<div class="row" style="margin-top: 10px; border-style: solid; border-width: 1px;">
					<div class="col-xs-12">
						TRANSFERS TO GENERAL LICENSEES
					</div>
				</div>

				@forelse($sourcedata as $key => $data) <?php //echo "<pre>";print_r($data->source_model['s_mod_num']); die;?>
				<div class="row" style="margin-top: 10px;">
					<div class="col-xs-4">
						<p style="float: left;">End User Information </p><br>
						<p style="float: left;">{{isset($data->mach_num->mach_loc->customer->customer_name) ? $data->mach_num->mach_loc->customer->customer_name : 'NA'}} </p><br>
					</div>
					<div class="col-xs-8">
						<p style="float: left;">{{isset($data->mach_num->mach_loc->customer->contact_type_data['contact_name']) ? $data->mach_num->mach_loc->customer->contact_type_data['contact_name'] : 'NA'}}</p><br>
						<p style="float: left;">{{isset($data->mach_num->mach_loc->customer->contact_type_data['contact_phone']) ? $data->mach_num->mach_loc->customer->contact_type_data['contact_phone'] : 'NA'}}</p><br>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<table class="table">
							 <thead>
                                <tr>
                                    <!-- <th>#</th> -->
                                    <th>Ship Date</th>
                                    <th>Device Model</th>
                                    <th>Device Serial No.</th>
                                    <th>Source Model</th>
                                    <th>Source Serial No.</th>
                                    <th>Activity</th>
                                </tr>
                                <tr>
                                	<td>{{ date('d/m/Y', strtotime($data->s_ship_date)) }}</td>
                                	<td></td>
                                	<td></td>
                                	<td>{{ $data->source_model['s_mod_num'] }}</td>
                                	<td>{{ $data->source_num }}</td>
                                	<td></td>
                                </tr>	
                            </thead>
						</table>	
					</div>
				</div>
				@endforeach

			</div>
		</div>
	</div>
</body>
</html>