<!DOCTYPE html>
<html lang="en">
<head>
	<title>{{ $title }}</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<style>
		.border_box{
			border:2px solid black; 
		}
		.set_content{
			margin: 0px 30px;
			font-family: monospace;
		}
		.header_line{
			font-weight: bold; 
			font-size: 15px; 
			padding-top: 5px;
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="border_box">
			<div class="set_content">
				<div class="row text-center header_line">
					<p>INDUSTRIAL DYNAMICS COMPANY, LTD.</p>
					<p>3100 FUJITA STREET</p>
					<p>TORRANCE, CA 90505</p>
					<p>(310) 325-5633 CUSTOMER SERVICE CENTER (888) 4-FILTEC</p>
					<p>WIPE TEST SOURCE INSPECTION CERTIFICATE</p>
				</div>

				<br>

				<div class="row">
					<p style="margin-bottom: 0px;"><span style="font-weight: bold; ">Number Of IDC Sources At Site (Total) : </span>{{ $count_mach }}</p>
					<div class="row">
						<div class="col-xs-6">
							<p style="font-weight: bold; margin-bottom: 0px;">{{ $address }}</p>
						</div>
						<div class="col-xs-6">
							<span style="font-weight: bold;">Account : </span>{{ $account }}
						</div>
					</div>
					<p style="font-weight: bold; margin-bottom: 0px;">{{ $country }}, {{ $state }}</p>
					<p style="font-weight: bold; margin-bottom: 0px;">{{ $city }} - {{ $zipcode }}</p>
					<div class="row">
						<div class="col-xs-6">
							<span style="font-weight: bold;">Attn : </span>{{ $contact_name }}
						</div>
						<div class="col-xs-6">
							<span style="font-weight: bold;">Phone No : </span>{{ $contact_phone }}
						</div>				
					</div>
				</div>

				<br>

				<div class="row" style="border-top: 2px solid black;">
					<p class="text-center" style="font-weight: bold; padding-top: 5px;">WIPE TEST AND CERTIFICATION DATA</p>
					<text style="font-size: 12px;">
						<div class="row">
							<div class="col-xs-6">
								<span style="font-weight: bold;">Wipe Test & Seals Affixed By : </span>{{ $test_seal }}
							</div>
							<div class="col-xs-6">
								<span style="font-weight: bold;">Date : </span>{{ $test_seal_date }}
							</div>
						</div>
						<div class="row">
							<div class="col-xs-6">
								<span style="font-weight: bold;">Wipe Test Measurements By : </span>{{ $measurement }}
							</div>
							<div class="col-xs-6">
								<span style="font-weight: bold;">Date : </span>{{ $measurement_date }}
							</div>
						</div>
						<p style="float: left; margin-bottom: 0px;">
							<span style="font-weight: bold;">Test Results Reported As : </span><span>Statisfactory*</span>
						</p><br>
						@if($discrepancie!=8)
						<p style="float: left; margin-bottom: 0px;">
							<span style="font-weight: bold;">Next Wipe Test Due Date : </span>
								<span>
									{{date('m/d/y', strtotime("+35 months", strtotime($test_seal_date)))}}
								</span>
						</p>
						@endif
						<br>
					</text>
					<p style="padding-top: 10px;">
						<span style="float: left; font-weight: bold">Note : </span>
						<span>Source model nos. {{ $s_mod_no }} and 06765 are 100mCi (3.7GBq) of Am-241.<br>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Source model no. 19567 is 300 nCi (11.1 GBq) of Am-241.</span>
					</p>
					<table style="border: 1px solid black;">		
						<tr>
							<th style="padding: 5px 0px; border: 1px solid black; text-align: center;">Mach Number </th>
							<th style="padding: 5px 0px; border: 1px solid black; text-align: center;">Machine Model No</th>
							<th style="padding: 5px 0px; border: 1px solid black; text-align: center;">Source Serial No</th>
							<th style="padding: 5px 0px; border: 1px solid black; text-align: center;">Source Model No</th>
							<th style="padding: 5px 0px; border: 1px solid black; text-align: center;">Inspection Discrepancies</th>
						</tr>
						
						
						@if(count($entrygroupb)>1)
						@foreach($entrygroupb as $entrygroups)
						<tr>
							<td style="padding: 5px 0px; border: 1px solid black; text-align: center;">{{ $entrygroups['mach_num'] }}</td>
							<td style="padding: 5px 0px; border: 1px solid black; text-align: center;">{{ $entrygroups['mach_model'] }}</td>
							<td style="padding: 5px 0px; border: 1px solid black; text-align: center;">{{ $entrygroups['source_num'] }}</td>
							<td style="padding: 5px 0px; border: 1px solid black; text-align: center;">{{ $entrygroups['source_model'] }}</td>
							<td style="padding: 5px 0px; border: 1px solid black; text-align: center;">{{ $entrygroups['discrepcod'] }}</td>
						</tr>
						@endforeach
						@endif

						@if(isset($entrygroup['source_num']) && $entrygroup['source_num'] !='')
						<tr>
							<td style="padding: 5px 0px; border: 1px solid black; text-align: center;">{{ $entrygroup['mach_num'] }}</td>
							<td style="padding: 5px 0px; border: 1px solid black; text-align: center;">{{ $entrygroup['mach_model'] }}</td>
							<td style="padding: 5px 0px; border: 1px solid black; text-align: center;">{{ $entrygroup['source_num'] }}</td>
							<td style="padding: 5px 0px; border: 1px solid black; text-align: center;">{{ $entrygroup['source_model'] }}</td>
							<td style="padding: 5px 0px; border: 1px solid black; text-align: center;">{{ $entrygroup['discrepcod'] }}</td>
						</tr>
						@endif
						
					</table>
				</div>

				<br>

				<div class="row" style="border-top: 2px solid black; font-size: 12px;">
					<p style="font-weight: 600; padding-top: 10px">Discrepancies : </p>
					<div class="row">
						<div class="col-xs-6">
							0. No Discrepancies
						</div>
						<div class="col-xs-6">
							5. Window Replacement Required
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							1. Incorrect Source Model
						</div>
						<div class="col-xs-6">
							6. Source Box Replacement Requried
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							2. Manual Shutter Repair Required
						</div>
						<div class="col-xs-6">
							7. Source Installation
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							3. Auto Shutter Repair Required
						</div>
						<div class="col-xs-6">
							8. Source Removal
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							4. Label Replacement Required
						</div>
						<div class="col-xs-6">
							9. Other
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6"></div>
						<div class="col-xs-5">
							<p style="border-bottom: 1px solid black;">&nbsp;</p>
						</div>
					</div>
					<br>
					<p style="font-weight: 600;">CERTIFIED : </p>
					<!-- <div class="row">
						<div class="col-xs-5">
							<p style="font-weight: 600;">BY : <span style="border-bottom: 1px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
						</div>
						<div class="col-xs-4">
							<p style="margin-left: 50px;">{{ $measurement_date }}</p>
						</div>
					</div> -->
					<div class="row">
						<div class="col-xs-6">
							@if(!empty($signature))
					<img src="{{ url('public/signature/'.$signature) }}" style="width: 70px; height: auto;">
					@endif	
							<br>
							<p>{{ $measurement_date }}</p>
							<p>By<span style="border-bottom: 1px solid black;"> </span> {{$rso}}<br>
							Radiation Safety Officer </p>

						</div>
						<div class="col-xs-6">
							<p style="float: left;">* Less Than 0.005 Microcuries 0.005 <br> &nbsp;&nbsp; Microcuries = .185 RBq</p>
						</div>
					</div>
				</div>

				<br>

				<div class="row text-center" style="border-top: 2px solid black;">
					<p style="font-weight: 800; padding-top: 5px;">
						PLEASE FILE THIS WIPE TEST CERTIFICATE TO BE PRESENTED <br>TO YOUR LOCAL REGULATORY AGENCY WHEN REQUESTED.
					</p>
				</div>
			</div>
		</div>
	</div>
</body>
</html>