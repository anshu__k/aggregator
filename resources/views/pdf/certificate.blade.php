<!DOCTYPE html>
<html lang="en">
<head>
	<title>{{ $title }}</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<style>
		.set_content{
			margin-left: 50px;
			padding-top: 100px;
			margin-right: 50px;
			font-family: monospace;
			font-weight: 600;
		}
		.first_add p{
			margin-bottom: 0px;
		}
		.faithfully p{
			margin-bottom: 0px;
		}
		.container{
			
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="set_content">
			<div class="row">
				<div class="first_add">
					<p>ATTN : {{ $contact_name }}</p>
					<p>{{ $address }}</p>
					<p>{{ $country }}, {{ $state }}</p>
					<p>{{ $city }} - {{ $zipcode }}</p>
				</div>
				<p style="margin-top: 40px;">Dear Mr.{{ $account }}:</p>

				<p style="margin-top: 20px;">Attached is the wipe test and source inspection certificate on the radioisotope sources used in your FILTEC equipment. Please file it for future reference.</p>

				<p style="margin-top: 20px;">Refer to the wipe test certificate for your next wipe test due date.</p>

				<div class="faithfully" style="margin-top: 20px; float: right;">
					<p>Yours very truly,</p>
					<p>INDUSTIRAL DYNAMICS COMPANY, LTD. </p>
				</div>
			</div>
		</div>
	</div>
</body>
</html>