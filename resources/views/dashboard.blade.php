@extends('layout.app')

@section('content')

<style>
    .table-stats.order-table.ov-h {
        max-height: 370px;
        overflow-y: scroll;
    }

    span.badge.badge-complete.showdetail {
        cursor: pointer;
    }

    .table-stats table th .name,
    .table-stats table td .name {
        color: #343a40;
        font-size: 12px;
        text-transform: capitalize;
    }

    .table-stats table td {
        font-size: 12px;
    }

    .datanotfound {
        color: red;
    }

    .filter-select {
        float: right;
    }

    .box-title-log {
        float: left;
        ;
    }

    .boldNumber {
        font-weight: 900;
    }

    .stat-widget-five .stat-icon {
        font-size: 40px;
        line-height: 50px;
        position: absolute;
        left: 17px;
        top: 33px;
    }

    .stat-widget-five .stat-content {
        margin-left: 60px;
    }

    .activityTable>tr>td {
        padding: 4px;
    }

    .activityTableold>tr>td {
        padding: 4px;
    }

    .activityTable>tr>th {
        padding: 4px;
    }

    .activityTableold>tr>th {
        padding: 4px;
    }
</style>

<div class="content">
    <div class="animated fadeIn">
        <!-- Widgets  -->
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <h4 class="box-title">Script Records</h4>
                                    <div class="stat-icon dib flat-color-2">
                                        <i class="pe-7s-users"></i>
                                    </div>
                                    <div class="stat-heading">Total -
                                        <span class="count boldNumber">{{ $data['all_script_records']}}</span>
                                    </div>
                                    <div class="stat-heading">Last Month Total -
                                        <span class="count">
                                            {{ $data['last_month_script_records']}}
                                        </span>
                                    </div>
                                    <!-- <div class="stat-heading">Bullhorn Candidate -
                                        <span class="count">
                                        {{ $data['all_bullhorn_candidate']}}
                                        </span>
                                    </div>
                                    <div class="stat-heading">Bullhorn Contact -
                                        <span class="count">
                                        {{ $data['all_bullhorn_contact']}}
                                        </span>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <h4 class="box-title">Synced Bullhorn Contact</h4>
                                    <div class="stat-icon dib flat-color-4">
                                        <i class="pe-7s-user"></i>
                                    </div>
                                    <div class="stat-heading">Total -
                                        <span class="count boldNumber">{{ $data['all_bullhorn_contact']}}</span>
                                    </div>
                                    <div class="stat-heading">&nbsp;
                                        <!-- Synced Last Month -
                                        <span class="count">{{ $data['last_month_bullhorn_contact']}}</span> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <h4 class="box-title">Synced Bullhorn Candidate</h4>
                                    <div class="stat-icon dib flat-color-3">
                                        <i class="pe-7s-global"></i>
                                    </div>
                                    <div class="stat-heading">Total -
                                        <span class="count boldNumber">{{ $data['all_bullhorn_candidate']}}</span>
                                    </div>
                                    <div class="stat-heading">&nbsp;
                                        <!-- Synced Last Month -
                                        <span class="count">{{ $data['last_month_bullhorn_candidate']}}</span> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-4">
                                <i class="fa fa-user-md" aria-hidden="true"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <h4 class="box-title">Mailchimp Campaigns</h4>
                                    <div class="stat-heading">Total -
                                        <span class="count boldNumber">{{ $data['all_mailchimp_campaign']}}</span>
                                    </div>
                                    <div class="stat-heading">&nbsp;
                                        <!-- Active - <span class="count">{{ $data['all_active_technician']}}</span> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-1">
                                <i class="pe-7s-id"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <h4 class="box-title">Sent Postcards</h4>
                                    <div class="stat-heading">Total -
                                        <span class="count boldNumber">{{ $data['total_postcard_sent']}}</span>
                                    </div>
                                    <div class="stat-heading">Unique-
                                        <span class="count">{{ $data['unique_postcard_sent']}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bar Graph for CPA data - START -->
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="box-title">CPA Record </h4>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="box-body">
                                <div class="chart">
                                    <canvas id="areaChart" style="height:250px"></canvas>
                                </div>
                            </div>
                        </div>
                        {{--
                        <div class="col-lg-6 d-none">
                            <div class="box-body">
                                <div class="chart">
                                    <canvas id="barChart" style="height:250px"></canvas>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="box-title">Bullhorn Information</h4>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-lg-12">
                            <div class="box-body">
                                <div class="chart">
                                    <canvas id="pieChart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bar Graph for CPA data - END -->
        <!-- PIE Chart for the Bullhorn data - START -->
        <div class="row">
            
        </div>
        <!-- PIE Chart for the Bullhorn data - END -->
        <div class="row d-none">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="box-title">Last 6 month records </h4>
                    </div>
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="card-body">
                                <div id="traffic-chart" class="traffic-chart"></div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="card-body">
                                <div class="progress-box progress-1">
                                    <h4 class="por-title">Customers</h4>
                                    <div class="por-txt" id="all_customer_count">

                                    </div>
                                    <div class="progress mb-2" style="height: 5px;">
                                        <div class="progress-bar bg-flat-color-1" role="progressbar" style="width: 40%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                                <div class="progress-box progress-2">
                                    <h4 class="por-title">Contact</h4>
                                    <div class="por-txt" id="all_contact_count">

                                    </div>
                                    <div class="progress mb-2" style="height: 5px;">
                                        <div class="progress-bar bg-flat-color-4" role="progressbar" style="width: 90%;" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                                <div class="progress-box progress-2">
                                    <h4 class="por-title">Machine</h4>
                                    <div class="por-txt" id="all_machine_count">

                                    </div>
                                    <div class="progress mb-2" style="height: 5px;">
                                        <div class="progress-bar bg-flat-color-2" role="progressbar" style="width: 24%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body"></div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="orders">
            <div class="row">
                <div class="col-xl-8">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="box-title box-title-log"> Logs </h4>
                            <div class="filter-select">
                                <select class="form-control" onchange="filterfunction(this)">
                                    <option value="all">All Logs</option>
                                    <option value="customer_id">Customers Logs</option>
                                    <option value="machine_id">Machine Logs</option>
                                    <option value="contact_id">Contact Logs</option>
                                    <option value="technician_id">Technician Logs</option>
                                    <option value="source_id">Source Logs</option>
                                    <option value="login">Login</option>
                                    <option value="logout">Logout</option>
                                </select>
                            </div>
                        </div>
                        <div class="card-body--">
                            <div class="table-stats order-table ov-h">
                                <table class="table ">
                                    <thead>
                                        <tr>
                                            <th>S.No.</th>
                                            <th>Description</th>
                                            <th>Date</th>
                                            <th>Module</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tabledata">
                                        {!!$data['html']!!}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4">

                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>


    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: darkgrey">
                    <h5 class="modal-title" id="exampleModalLongTitle"> Activity Detail </h5>
                </div>
                <div class="modal-body showJSONData">
                    <div class="row">
                        <div class="col-md-6">
                            <h3> New Data </h3>
                            <table class="table activityTable">
                            </table>
                        </div>
                        <div class="col-md-6">
                            <h3> Old Data </h3>
                            <table class="table activityTableold">
                            </table>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/js/custom.js') }}"></script>

<script type="text/javascript">
    jQuery(document).ready(function($) {
        "use strict";

        /**
         * Sample Chart for the Last 6 Month data 
         */
        /*
         jQuery.ajax({
            type: "GET",
            dataType: "JSON",
            url: "{{ route('getGraphData') }}",
            success: function(data) {
                jQuery('#all_customer_count').text(data.all_customer);
                jQuery('#all_contact_count').text(data.all_contact);
                jQuery('#all_machine_count').text(data.all_machine);
                jQuery('#all_technician_count').text(data.all_technician);

                if (jQuery('#traffic-chart').length) {
                    var chart = new Chartist.Line('#traffic-chart', {
                        labels: data.last_six_months,
                        series: [
                            [data.customer_six_months[6], data.customer_six_months[5], data.customer_six_months[4], data.customer_six_months[3], data.customer_six_months[2], data.customer_six_months[1]],
                            [data.contact_six_months[6], data.contact_six_months[5], data.contact_six_months[4], data.contact_six_months[3], data.contact_six_months[2], data.contact_six_months[1]],
                            [data.machine_six_months[6], data.machine_six_months[5], data.machine_six_months[4], data.machine_six_months[3], data.machine_six_months[2], data.machine_six_months[1]]
                        ]
                    }, {
                        low: 0,
                        showArea: true,
                        showLine: false,
                        showPoint: false,
                        fullWidth: true,
                        axisX: {
                            showGrid: true
                        }
                    });

                    chart.on('draw', function(data) {
                        if (data.type === 'line' || data.type === 'area') {
                            data.element.animate({
                                d: {
                                    begin: 2000 * data.index,
                                    dur: 2000,
                                    from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
                                    to: data.path.clone().stringify(),
                                    easing: Chartist.Svg.Easing.easeOutQuint
                                }
                            });
                        }
                    });
                }
            },
            error: function() {
                console.log('error');
            }
        });
        */

        /**
         * Chart for the CPA data 
         */
        jQuery.ajax({
            type: "GET",
            dataType: "JSON",
            url: "{{ route('getCPAAdditionData') }}",
            data: {},
            success: function(data) {
                let labelObject = [];
                let labelArray = [];
                let labelSingleArray = [];
                let graphData = [];

                // X-AXIS Data
                if (data.dates) {
                    labelObject = data.dates;
                    labelSingleArray = Object.values(labelObject);
                    // labelSingleArray = Object.keys(labelObject);
                    // labelArray = Object.entries(labelObject);
                }

                // Y-AXIS Data
                if (data.counts) {
                    graphData = data.counts;
                    graphData = Object.values(graphData);
                }

                var areaChartData = {
                    labels: labelSingleArray,
                    datasets: [{
                        label: 'CAP Counts',
                        // backgroundColor: [
                        //     'rgba(255, 99, 132, 0.2)',
                        //     'rgba(54, 162, 235, 0.2)',
                        //     'rgba(255, 206, 86, 0.2)',
                        //     'rgba(75, 192, 192, 0.2)',
                        //     'rgba(153, 102, 255, 0.2)',
                        //     'rgba(255, 159, 64, 0.2)'
                        // ],
                        backgroundColor: '#fb9678',
                        // fillColor: 'rgba(210, 214, 222, 1)',
                        // strokeColor: 'rgba(210, 214, 222, 1)',
                        // pointColor: 'rgba(210, 214, 222, 1)',
                        fillColor: 'rgba(255, 99, 132, 0.2)',
                        strokeColor: 'rgba(255, 99, 132, 0.2)',
                        pointColor: 'rgba(255, 99, 132, 0.2)',
                        pointStrokeColor: '#c1c7d1',
                        pointHighlightFill: '#fff',
                        // pointHighlightStroke: 'rgba(220, 220, 220, 1)',
                        pointHighlightStroke: 'rgba(255, 99, 132, 0.2)',
                        // data: [65, 59, 80, 81, 56, 55, 40]
                        data: graphData
                    }]
                }

                new Chart(areaChart, {
                    type: 'line',
                    // type: 'pie',
                    data: areaChartData,
                    // options: areaChartOptions
                });


                // new Chart(barChart, {
                //     type: 'bar',
                //     data: areaChartData,
                // });
            },
            error: function() {
                console.log('error');
            }
        });


        /**
         * Pie Chart for Bullhorn Synced Candidate, Contacts and Pending to Sync data
         */
        jQuery.ajax({
            type: "GET",
            dataType: "JSON",
            url: "{{ route('getBullhornSyncedData') }}",
            data: {},
            success: function(data) {
                let labelObject = [];
                let labelSingleArray = [];
                let graphData = [];

                // X-AXIS Data
                if (data.titles) {
                    labelObject = data.titles;
                    labelSingleArray = Object.values(labelObject);
                }

                // Y-AXIS Data
                if (data.counts) {
                    graphData = data.counts;
                    graphData = Object.values(graphData);
                }

                var pieChartData = {
                    labels: labelSingleArray,
                    datasets: [{
                        label: 'CAP Counts',
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.3)',
                            'rgba(54, 162, 235, 0.3)',
                            'rgba(255, 206, 86, 0.3)',
                            'rgba(75, 192, 192, 0.3)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        fillColor: 'rgba(255, 99, 132, 0.2)',
                        strokeColor: 'rgba(255, 99, 132, 0.2)',
                        pointColor: 'rgba(255, 99, 132, 0.2)',
                        pointStrokeColor: '#c1c7d1',
                        pointHighlightFill: '#fff',
                        // pointHighlightStroke: 'rgba(220, 220, 220, 1)',
                        pointHighlightStroke: 'rgba(255, 99, 132, 0.2)',
                        // data: [65, 59, 80, 81, 56, 55, 40]
                        data: graphData
                    }]
                }

                new Chart(pieChart, {
                    type: 'pie',
                    data: pieChartData,
                });
            },
            error: function() {
                console.log('error');
            }
        });
    });

    function filterfunction(s) {
        jQuery.ajax({
            type: "GET",
            dataType: "JSON",
            url: "{{ route('getLogFilterData') }}",
            data: {
                column: s.value
            },
            success: function(data) {
                if (data['status'] == 'success') {
                    jQuery('#tabledata').html(data['html']);
                    showDetail();
                } else {

                }
            },
            error: function() {
                console.log('error');
            }
        });
    }


    /**
     * CHART JS Script
     */
    // jQuery(function() {
    //     /* ChartJS
    //      * -------
    //      * Here we will create a few charts using ChartJS
    //      */

    //     //--------------
    //     //- AREA CHART -
    //     //--------------

    //     // Get context with jQuery - using jQuery's .get() method.
    //     var areaChartCanvas = jQuery('#areaChart').get(0).getContext('2d')
    //     console.log('areaChartCanvas', areaChartCanvas);
    //     console.log('document ready', document.getElementById('areaChart').getContext('2d'));
    //     // This will get the first returned node in the jQuery collection.
    //     var areaChart = new Chart(areaChartCanvas)
    //     console.log('areaChart', areaChart);

    //     var areaChartData = {
    //         labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    //         datasets: [{
    //                 label: 'Electronics',
    //                 backgroundColor: [
    //                     'rgba(255, 99, 132, 0.2)',
    //                     'rgba(54, 162, 235, 0.2)',
    //                     'rgba(255, 206, 86, 0.2)',
    //                     'rgba(75, 192, 192, 0.2)',
    //                     'rgba(153, 102, 255, 0.2)',
    //                     'rgba(255, 159, 64, 0.2)'
    //                 ],
    //                 // fillColor: 'rgba(210, 214, 222, 1)',
    //                 // strokeColor: 'rgba(210, 214, 222, 1)',
    //                 // pointColor: 'rgba(210, 214, 222, 1)',
    //                 fillColor: 'rgba(255, 99, 132, 0.2)',
    //                 strokeColor: 'rgba(255, 99, 132, 0.2)',
    //                 pointColor: 'rgba(255, 99, 132, 0.2)',
    //                 pointStrokeColor: '#c1c7d1',
    //                 pointHighlightFill: '#fff',
    //                 // pointHighlightStroke: 'rgba(220, 220, 220, 1)',
    //                 pointHighlightStroke: 'rgba(255, 99, 132, 0.2)',
    //                 data: [65, 59, 80, 81, 56, 55, 40]
    //             },
    //             {
    //                 label: 'Digital Goods',
    //                 fillColor: 'rgba(60,141,188,0.9)',
    //                 strokeColor: 'rgba(60,141,188,0.8)',
    //                 pointColor: '#3b8bba',
    //                 pointStrokeColor: 'rgba(60,141,188,1)',
    //                 pointHighlightFill: '#fff',
    //                 pointHighlightStroke: 'rgba(60,141,188,1)',
    //                 data: [28, 48, 40, 19, 86, 27, 90]
    //             }
    //         ]
    //     }

    //     var areaChartOptions = {
    //         //Boolean - If we should show the scale at all
    //         showScale: true,
    //         //Boolean - Whether grid lines are shown across the chart
    //         scaleShowGridLines: false,
    //         //String - Colour of the grid lines
    //         scaleGridLineColor: 'rgba(0,0,0,.05)',
    //         //Number - Width of the grid lines
    //         scaleGridLineWidth: 1,
    //         //Boolean - Whether to show horizontal lines (except X axis)
    //         scaleShowHorizontalLines: true,
    //         //Boolean - Whether to show vertical lines (except Y axis)
    //         scaleShowVerticalLines: true,
    //         //Boolean - Whether the line is curved between points
    //         bezierCurve: true,
    //         //Number - Tension of the bezier curve between points
    //         bezierCurveTension: 0.3,
    //         //Boolean - Whether to show a dot for each point
    //         pointDot: false,
    //         //Number - Radius of each point dot in pixels
    //         pointDotRadius: 4,
    //         //Number - Pixel width of point dot stroke
    //         pointDotStrokeWidth: 1,
    //         //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
    //         pointHitDetectionRadius: 20,
    //         //Boolean - Whether to show a stroke for datasets
    //         datasetStroke: true,
    //         //Number - Pixel width of dataset stroke
    //         datasetStrokeWidth: 2,
    //         //Boolean - Whether to fill the dataset with a color
    //         datasetFill: true,
    //         //String - A legend template
    //         legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
    //         //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    //         maintainAspectRatio: true,
    //         //Boolean - whether to make the chart responsive to window resizing
    //         responsive: true
    //     }

    //     //Create the line chart
    //     // Chartist.Line('#areaChart', areaChartData, areaChartOptions)
    //     const areaChartDraw = new Chart(areaChart, {
    //         type: 'line',
    //         // type: 'bar',
    //         // type: 'pie',
    //         data: areaChartData,
    //         options: areaChartOptions
    //     });
    //     // const myChart = new Chart(areaChart, {
    //     //     type: 'line',
    //     //     data: {
    //     //         labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
    //     //         datasets: [{
    //     //             label: '# of Votes',
    //     //             data: [12, 19, 3, 5, 2, 3],
    //     //             backgroundColor: [
    //     //                 'rgba(255, 99, 132, 0.2)',
    //     //                 'rgba(54, 162, 235, 0.2)',
    //     //                 'rgba(255, 206, 86, 0.2)',
    //     //                 'rgba(75, 192, 192, 0.2)',
    //     //                 'rgba(153, 102, 255, 0.2)',
    //     //                 'rgba(255, 159, 64, 0.2)'
    //     //             ],
    //     //             borderColor: [
    //     //                 'rgba(255, 99, 132, 1)',
    //     //                 'rgba(54, 162, 235, 1)',
    //     //                 'rgba(255, 206, 86, 1)',
    //     //                 'rgba(75, 192, 192, 1)',
    //     //                 'rgba(153, 102, 255, 1)',
    //     //                 'rgba(255, 159, 64, 1)'
    //     //             ],
    //     //             borderWidth: 1
    //     //         }]
    //     //     },
    //     //     options: {
    //     //         scales: {
    //     //             y: {
    //     //                 beginAtZero: true
    //     //             }
    //     //         }
    //     //     }
    //     // });


    //     //-------------
    //     //- BAR CHART -
    //     //-------------
    //     var barChartCanvas = jQuery('#barChart').get(0).getContext('2d')
    //     var barChart = new Chart(barChartCanvas)
    //     var barChartData = areaChartData
    //     barChartData.datasets[1].fillColor = '#00a65a'
    //     barChartData.datasets[1].strokeColor = '#00a65a'
    //     barChartData.datasets[1].pointColor = '#00a65a'
    //     var barChartOptions = {
    //         //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
    //         scaleBeginAtZero: true,
    //         //Boolean - Whether grid lines are shown across the chart
    //         scaleShowGridLines: true,
    //         //String - Colour of the grid lines
    //         scaleGridLineColor: 'rgba(0,0,0,.05)',
    //         //Number - Width of the grid lines
    //         scaleGridLineWidth: 1,
    //         //Boolean - Whether to show horizontal lines (except X axis)
    //         scaleShowHorizontalLines: true,
    //         //Boolean - Whether to show vertical lines (except Y axis)
    //         scaleShowVerticalLines: true,
    //         //Boolean - If there is a stroke on each bar
    //         barShowStroke: true,
    //         //Number - Pixel width of the bar stroke
    //         barStrokeWidth: 2,
    //         //Number - Spacing between each of the X value sets
    //         barValueSpacing: 5,
    //         //Number - Spacing between data sets within X values
    //         barDatasetSpacing: 1,
    //         //String - A legend template
    //         legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
    //         //Boolean - whether to make the chart responsive
    //         responsive: true,
    //         maintainAspectRatio: true
    //     }

    //     barChartOptions.datasetFill = false
    //     // barChart.Bar(barChartData, barChartOptions)
    //     const barChartDraw = new Chart(barChart, {
    //         type: 'bar',
    //         data: barChartData
    //     });
    // })
</script>
@endsection