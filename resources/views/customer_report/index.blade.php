@extends('layout.app')

@section('content')

	<!-- Content -->
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
        	<!--  Traffic  -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <!-- Error & Message -->
                                @if (session()->has('insert'))
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        {{ session()->get('insert') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @elseif (session()->has('update'))
                                    <div class="alert alert-info alert-dismissible" role="alert">
                                        {{ session()->get('update') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @elseif (session()->has('delete'))
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        {{ session()->get('delete') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                            <!-- Error & Message -->
                            <div class="row form-group">
                                <div class="col-md-3">
                                    <h4 class="box-title">Customer Report </h4>
                                </div>

                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input class="form-control"  name="search_term" id="search_wipe_term" placeholder="Search..">
                                        <div class="input-group-btn">
                                            <button type="button" id="search_wipe" class="btn btn-outline-primary" >Search</button>
                                        </div>

                                        <a class="btn input-group-btn"href="{{ route('regulatory_report.index') }}" >Reset</a>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <!-- <a href="{{ route('regulatory_report.create') }}" class="btn btn-outline-success float-right"><i class="fa fa-plus"></i> ADD</a> -->
                                </div>
                            </div>
                        </div>
                        <div class="card-body nopadding">
                            <div class="col-md-12 table-stats order-table ov-h nopadding">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <!-- <th>#</th> -->
                                            <th>Source Number</th>
                                            <th>Wipe Date</th>
                                            <th>Tech</th>
                                            <th>Discrepcod</th>
                                            <th>Machine Number</th>
                                            <th>Customer Number</th>
                                            <th colspan="2" style="text-align: left;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody class="table_format">
                                        @forelse($wipes as $key => $wipe)
                                            <tr>
                                                <!-- <td>{{ $wipes->firstItem() + $key }}</td>  -->
                                                <td>{{ $wipe->source_num }}</td> 
                                                <td>{{ date('d M Y', strtotime($wipe->wipe_date)) }}</td> 
                                                <td>{{ $wipe->tech_name->tech_name }}</td> 
                                                <td>{{ $wipe->discrepcod }}</td> 
                                                <td>{{ $wipe->mach_num }}</td> 
                                                <td>{{ $wipe->cust_num }}</td> 
                                                <td>
                                                    <a href="{{ route('pdfView', [$wipe, 'action' => 'envelope']) }}" target="_blank">
                                                        <i class="fa fa-envelope edit_btn" rel="tooltip" title="View Envelope Report PDF"></i>
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="{{ route('pdfView', [$wipe, 'action' => 'regulatoryReport']) }}" target="_blank">
                                                        <i class="fa fa-file-text edit_btn" rel="tooltip" title="View Regulatory PDF"></i>
                                                    </a>
                                                </td>
                                                <!-- <td>
                                                    <a href="{{ route('preAssignSou.edit', $wipe) }}">
                                                        <i class="fa fa-edit edit_btn" rel="tooltip" title="Edit"></i>
                                                    </a>
                                                </td> -->
                                                <!-- <td style="text-align: left;">
                                                    <form action="{{ route('preAssignSou.destroy', $wipe) }}" method="POST">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <button type="submit" class="fabutton" onclick="return myFunction();">
                                                            <i class="fa fa-trash dlt_btn" rel="tooltip" title="Delete"></i>
                                                        </button>
                                                    </form>
                                                </td> -->
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="7" class="text-center">
                                                    <p>Pre-Assign Source Not Found.</p>
                                                </td> 
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div> <!-- /.col-md-12 -->
                        </div><!-- card-body -->
                        <div class="card-body">
                            {{ $wipes->onEachSide(1)->links() }}
                        </div>
                        <div class="card-body"></div>
                    </div>
                </div><!-- /# column -->
            </div>
            <!--  /Traffic -->
            <div class="clearfix"></div>
        </div>
    	<!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>

@endsection