@extends('layout.app')

@section('content')

	<!-- Content -->
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
        	<!--  Traffic  -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <!-- Error & Message -->
                                @if (session()->has('insert'))
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        {{ session()->get('insert') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @elseif (session()->has('update'))
                                    <div class="alert alert-info alert-dismissible" role="alert">
                                        {{ session()->get('update') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @elseif (session()->has('delete'))
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        {{ session()->get('delete') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                            <!-- Error & Message -->
                            <div class="row form-group">
                                <div class="col-md-3">
                                    <h4 class="box-title">Transfer Machine List </h4>
                                </div>

                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input class="form-control"  name="search_term" id="search_wipe_term" placeholder="Search..">
                                        <div class="input-group-btn">
                                            <button type="button" id="search_wipe" class="btn btn-outline-primary" >Search</button>
                                        </div>

                                        <a class="btn input-group-btn"href="{{ route('TransferMach.index') }}" >Reset</a>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <a href="{{ route('TransferMach.create') }}" class="btn btn-outline-success float-right"><i class="fa fa-plus"></i> ADD</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body nopadding">
                            <div class="col-md-12 table-stats order-table ov-h nopadding">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <!-- <th>#</th> -->
                                            <th>Machine Number</th>
                                            <th>Customer Number</th>
                                            <th>Ship Date</th>
                                            <th>Install Date</th>
                                            <th>Source Ship Method</th>
                                            <th style="text-align: left;">Technician</th>
                                            <!-- <th style="text-align: left;">Action</th> -->
                                        </tr>
                                    </thead>

                                    <tbody class="table_format">
                                        @forelse($transfer_machs as $key => $transfer_mach)
                                            <tr>
                                                <!-- <td>{{ $transfer_machs->firstItem() + $key }}</td>  -->
                                                <td>{{ $transfer_mach->mach_num }}</td> 
                                                <td>{{ $transfer_mach->customer_num }}</td> 
                                                <td>{{ date('d M Y', strtotime($transfer_mach->ship_date)) }}</td> 
                                                <td>{{ date('d M Y', strtotime($transfer_mach->install_date)) }}</td> 
                                                <td>{{ $transfer_mach->source_ship_method_desc->sstat_desc }}</td> 
                                                <td style="text-align: left;">{{ $transfer_mach->tech_name->tech_name }}</td> 
                                                <!-- <td style="text-align: left; width: 5%;">
                                                    <a href="{{ route('TransferMach.edit', $transfer_mach) }}" class="btn btn-outline-primary">
                                                        Confirm Machine Returned
                                                    </a>
                                                </td> -->
                                            </tr>
                                        @empty
                                            <tr>
                                              <td colspan="6" class="text-center">
                                                <p>Transfer Machine Not Found.</p>
                                              </td> 
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div> <!-- /.col-md-12 -->
                        </div><!-- card-body -->
                        <div class="card-body">
                            {{ $transfer_machs->onEachSide(1)->links() }}
                        </div>
                        <div class="card-body"></div>
                    </div>
                </div><!-- /# column -->
            </div>
            <!--  /Traffic -->
            <div class="clearfix"></div>
        </div>
    	<!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>

@endsection