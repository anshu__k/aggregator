@extends('layout.app')
@section('content')
    <!-- Content -->
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
            <!--  Traffic  -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-lg-6">
                                    <h4 class="box-title">Add Transfer Machine </h4>
                                </div>
                                <div class="col-lg-6">
                                    <a href="{{ route('TransferMach.index') }}" class="btn btn-outline-secondary float-right"><i class="fa fa-angle-double-left"></i> Back</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('TransferMach.store') }}" method="POST" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <input type="hidden" name="active_tab" id="active_tab" value="">

                                <div class="box_border">
                                    <div class="heading_font_size font-weight-bold">
                                        FROM : Select the moving machine. Review the machine information. 
                                    </div>

                                    <div class="form-group ml-5 mt-3 mb-1">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="machine_number">Moving Machine Number :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <select class="form-control select_machine_num input_size" name="machine_number" id="machine_number" value="{{ old('machine_number') }}"></select>

                                                @if ($errors->has('machine_number'))
                                                <div class="text-danger">{{ $errors->first('machine_number') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group ml-5 mb-1">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="machine_model">Machine Model :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="machine_model" id="machine_model" class="form-control input_size" value="{{ old('machine_model') }}" readonly>
                                                @if ($errors->has('machine_model'))
                                                <div class="text-danger">{{ $errors->first('machine_model') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group ml-5 mb-1">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="machine_status">Machine Status :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="machine_status" id="machine_status" class="form-control input_size" value="{{ old('machine_status') }}" readonly>
                                                @if ($errors->has('machine_status'))
                                                <div class="text-danger">{{ $errors->first('machine_status') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group ml-5 mb-1">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="current_customer">Current Customer :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="current_customer" id="current_customer" class="form-control input_size" value="{{ old('current_customer') }}" readonly>
                                                @if ($errors->has('current_customer'))
                                                <div class="text-danger">{{ $errors->first('current_customer') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group ml-5 mb-1">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="customer_contact">Customer Contact :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="customer_contact" id="customer_contact" class="form-control input_size" value="{{ old('customer_contact') }}" readonly>
                                                @if ($errors->has('customer_contact'))
                                                <div class="text-danger">{{ $errors->first('customer_contact') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group ml-5 mb-1">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="current_state">Current State :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="current_state" id="current_state" class="form-control input_size" value="{{ old('current_state') }}" readonly>
                                                @if ($errors->has('current_state'))
                                                <div class="text-danger">{{ $errors->first('current_state') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group ml-5 table_show" style="display: none;">
                                        <div class="row">
                                            <div class="col-md-12 table-stats order-table ov-h nopadding" style="padding-left: 307px !important;">
                                                <table class="table" style="width: 555px !important;">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Machine Source's</th>
                                                            <th style="text-align: left;">Source Status</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="table_format">
                                                        <tr>
                                                            <td>1</td>
                                                            <td id="machine_source"></td>
                                                            <td style="text-align: left;" id="source_status"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box_border mt-4">
                                    <div class="heading_font_size font-weight-bold">
                                        TO : Select new customer who receive the machine.
                                    </div>

                                    <div class="form-group ml-5 mt-3 mb-1">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="customer_number">Customer Number :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="hidden" name="rec_customer_number" id="rec_customer_number">

                                                <select class="form-control select_customer input_size" name="customer_number" id="customer_number" value="{{ old('customer_number') }}"></select>

                                                @if ($errors->has('customer_number'))
                                                <div class="text-danger">{{ $errors->first('customer_number') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group ml-5 mb-1">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="rec_customer_contact">Customer Contact :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="rec_customer_contact" id="rec_customer_contact" class="form-control input_size" value="{{ old('rec_customer_contact') }}" readonly>
                                                @if ($errors->has('rec_customer_contact'))
                                                <div class="text-danger">{{ $errors->first('rec_customer_contact') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group ml-5 mb-1">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="rec_state">State :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="rec_state" id="rec_state" class="form-control input_size" value="{{ old('rec_state') }}" readonly>
                                                @if ($errors->has('rec_state'))
                                                <div class="text-danger">{{ $errors->first('rec_state') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group ml-5 mb-1">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="ship_date">Ship Date :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <div id="add_ship_date" class="input-group date" data-date-format="mm-dd-yyyy">
                                                    <input type="text" name="ship_date" id="ship_date" class="form-control input_size" value="{{ old('ship_date') }}">
                                                    <span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
                                                </div>
                                                @if ($errors->has('ship_date'))
                                                <div class="text-danger">{{ $errors->first('ship_date') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group ml-5 mb-1">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="install_date">Target Install Date :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <div id="add_install_date" class="input-group date" data-date-format="mm-dd-yyyy">
                                                    <input type="text" name="install_date" id="install_date" class="form-control input_size" value="{{ old('install_date') }}">
                                                    <span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
                                                </div>
                                                @if ($errors->has('install_date'))
                                                <div class="text-danger">{{ $errors->first('install_date') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group ml-5 mb-1">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="source_ship_method">Source Ship Method :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <select name="source_ship_method" id="source_ship_method" class="form-control input_size" value="{{ old('source_ship_method') }}">
                                                    <option selected disabled>Select Source Ship Method</option>
                                                    @foreach($ship_methods as $ship_method)
                                                    <option value="{{ $ship_method->s_status }}">{{ $ship_method->sstat_desc }}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('source_ship_method'))
                                                <div class="text-danger">{{ $errors->first('source_ship_method') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group ml-5 mb-1">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="technician">Technician :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <select class="form-control select_tech_model input_size" name="technician" id="technician" value="{{ old('technician') }}"></select>
                                                @if ($errors->has('technician'))
                                                <div class="text-danger">{{ $errors->first('technician') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 mt-4">
                                        <button type="submit" class="btn btn-outline-primary float-right"><i class="fa fa-save"></i> Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card-body"></div>
                    </div>
                </div><!-- /# column -->
            </div>
            <!--  /Traffic -->
            <div class="clearfix"></div>
        </div>
        <!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>
    <script>
        jQuery(document).ready(function(){
            jQuery("#add_ship_date").datepicker({ 
                format: "mm/dd/yy",
                weekStart: 0,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                orientation: "auto"
            }).datepicker('update', new Date());

            jQuery("#add_install_date").datepicker({ 
                format: "mm/dd/yy",
                weekStart: 0,
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true,
                orientation: "auto"
            }).datepicker('update', new Date());
        });
    </script>

    <!-- Enter Source Number Fetch Pre-assign Source Data -->
        <script>

            jQuery(document).ready(function(){

                jQuery('.select_machine_num').on('change', function(){
                    var machine_number = jQuery(".select_machine_num option:selected").val();
                    jQuery.ajax({
                        url:"{{ route('transferMachineAutofill') }}",
                        method:'GET',
                        data:{machine_number:machine_number},
                        dataType:'json',
                        success:function(data)
                        {
                            if(data.machine_data.mach_model != ''){
                                jQuery('#machine_model').val(data.machine_data.mach_model);
                                jQuery("#select2-machine_number-container").text(data.machine_data.mach_number);
                            }
                            if(data.machine_data.mach_status != ''){
                                jQuery('#machine_status').val(data.machine_data.mach_status);
                            }
                            if(data.machine_data.current_customer != ''){
                                jQuery('#current_customer').val(data.machine_data.current_customer);
                            }
                            if(data.machine_data.customer_contact != ''){
                                jQuery('#customer_contact').val(data.machine_data.customer_contact);
                            }
                            if(data.machine_data.current_state != ''){
                                jQuery('#current_state').val(data.machine_data.current_state);
                            }
                            if(data.machine_data.machine_source != '' || data.machine_data.source_status != ''){
                                jQuery('#machine_source').text(data.machine_data.machine_source);
                                jQuery('#source_status').text(data.machine_data.source_status);
                                jQuery('.table_show').css('display', '');
                            }
                        }
                    })
                });

                jQuery('.select_customer').on('change', function(){
                    var customer_number = jQuery(".select_customer option:selected").val();
                    jQuery.ajax({
                        url:"{{ route('transferMachineAutofill') }}",
                        method:'GET',
                        data:{customer_number:customer_number},
                        dataType:'json',
                        success:function(data)
                        {
                            if(data.machine_data.current_customer != ''){
                                jQuery('#rec_customer_number').val(data.machine_data.current_customer);
                                jQuery("#select2-customer_number-container").text(data.machine_data.current_customer);
                            }
                            if(data.machine_data.customer_contact != ''){
                                jQuery('#rec_customer_contact').val(data.machine_data.customer_contact);
                            }
                            if(data.machine_data.current_state != ''){
                                jQuery('#rec_state').val(data.machine_data.current_state);
                            }
                        }
                    })
                });
            });  
        </script>
    <!-- Enter Source Number Fetch Pre-assign Source Data -->
@endsection