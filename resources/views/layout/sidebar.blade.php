
    <!-- Left Panel -->
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
			
                <ul class="nav navbar-nav">
                    <li>
                        <a href="{{ route('home') }}"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
                    </li>

                    <li class="{{ (request()->is('users/*')) ? 'active' : '' }}">
                        <a href="{{ route('users.index') }}"><i class="menu-icon fa fa-users"></i>Users </a>
                    </li>

                  <li class="menu-item-has-children dropdown {{ (request()->is('customer/*')) || (request()->is('contact/*')) || (request()->is('contact/script_record')) ? 'active show' : '' }}">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Data Management</a>

                        <ul class="sub-menu children dropdown-menu {{ (request()->is('customer/*')) || (request()->is('contact/*')) || (request()->is('contact/script_record')) ? 'show' : '' }}">     

                            <!-- <li class="{{ (request()->is('customer/*')) ? 'active' : '' }}" >
                                <i class="fa fa-id-card-o"></i><a href="{{ url('customer') }}">Customer</a>
                            </li> -->
                            <li class="{{ (request()->is('contact/*')) ? 'active' : '' }}" >
                                <i class="fa fa-address-book-o"></i><a href="{{ url('contact') }}">Contact</a>
                            </li>
                            <li class="{{ (request()->is('contact/scriptrecord')) ? 'active' : '' }}">
                                <i class="fa fa-book"></i><a href="{{ route('contact.script_record') }}">Script Record</a>
                            </li>
                            <!-- <li class="{{ (request()->is('machines/*')) ? 'active' : '' }}" >
                                <i class="fa fa-cogs"></i><a href="{{ route('machines.index') }}">Machine </a>
                            </li>
                            <li class="{{ (request()->is('technicians/*')) ? 'active' : '' }}" >
                                <i class="fa fa-male"></i><a href="{{ route('technicians.index') }}">Technician </a>
                            </li> -->
                        </ul>
                    </li>  
                    <li class="menu-item-has-children dropdown {{ (request()->is('script/*')) || (request()->is('script/logs')) ? 'active show' : '' }}">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Script Log Mng</a>
                        <ul class="sub-menu children dropdown-menu {{ (request()->is('script/*')) || (request()->is('script/logs')) ? 'show' : '' }}">
                            
                            <li class="{{ (request()->is('script/list')) ? 'active' : '' }}">
                                <i class="fa fa-list"></i><a href="{{ route('script.list') }}">Script List</a>
                            </li>
                            <li class="{{ (request()->is('script/logs')) ? 'active' : '' }}">
                                <i class="fa fa-history"></i><a href="{{ route('script.logs') }}">Script Log</a>
                            </li>
                           
                        </ul>
                    </li>


                    <li class="menu-item-has-children dropdown {{ (request()->is('mailchimp/*')) ? 'active show' : '' }}">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Mailchimp Management</a>

                        <ul class="sub-menu children dropdown-menu {{ (request()->is('mailchimp/*')) ? 'show' : '' }}">     

                            <li class="{{ (request()->is('mailchimp/*')) ? 'active' : '' }}" >
                                <i class="fa fa-address-book-o"></i><a href="{{ route('mailchimp.listing') }}">Mailchimp Listing</a>
                            </li>
                        </ul>
                    </li>  
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside>
    <!-- /#left-panel -->