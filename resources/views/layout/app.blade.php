<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Web Application</title>
    <meta name="description" content="pacific - Web Application">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="{{ url('public/images/favicon.ico') }}" type="image/x-icon" />
	<link rel="apple-touch-icon" href="{{ url('public/images/apple-touch-icon.png') }}" />
	<link rel="apple-touch-icon" sizes="57x57" href="{{ url('public/images/apple-touch-icon-57x57.png') }}" />
	<link rel="apple-touch-icon" sizes="72x72" href="{{ url('public/images/apple-touch-icon-72x72.png') }}" />
	<link rel="apple-touch-icon" sizes="76x76" href="{{ url('public/images/apple-touch-icon-76x76.png') }}" />
	<link rel="apple-touch-icon" sizes="114x114" href="{{ url('public/images/apple-touch-icon-114x114.png') }}" />
	<link rel="apple-touch-icon" sizes="120x120" href="{{ url('public/images/apple-touch-icon-120x120.png') }}" />
	<link rel="apple-touch-icon" sizes="144x144" href="{{ url('public/images/apple-touch-icon-144x144.png') }}" />
	<link rel="apple-touch-icon" sizes="152x152" href="{{ url('public/images/apple-touch-icon-152x152.png') }}" />
	<link rel="apple-touch-icon" sizes="180x180" href="{{ url('public/images/apple-touch-icon-180x180.png') }}" />


    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">

    <link rel="stylesheet" href="{{ url('public/css/cs-skin-elastic.css') }}">
    <link rel="stylesheet" href="{{ url('public/css/style.css') }}">
    

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
    <link href="//cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.css" rel="stylesheet">
    <link href="//cdn.jsdelivr.net/npm/jqvmap@1.5.1/dist/jqvmap.min.css" rel="stylesheet">

    <!-- <link href="//cdn.jsdelivr.net/npm/weathericons@2.1.0/css/weather-icons.css" rel="stylesheet" /> -->
    <link href="//cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.css" rel="stylesheet" />

    <!-- DataTable -->
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.css">
    <!-- <link rel="stylesheet" href="{{ url('public/public/css/dataTables.bootstrap.css') }}"> -->

    <!-- Select2 -->
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

    <!-- Date Picker -->
    <link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ url('public/css/custom.css') }}">

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    
    <!-- Date Picker -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

    @stack('styles')
    
</head>

<body>
    
    @include('layout.sidebar') 
    @include('layout.header')
        @yield('content')
    @include('layout.footer')

    <!-- Scripts -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script> -->
    <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
    <script src="//cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    <script src="{{ url('public/js/main.js') }}"></script>

    <!--  Chart js -->
    <script src="//cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.bundle.min.js"></script>

    <!--Chartist Chart-->
    <script src="//cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/chartist-plugin-legend@0.6.2/chartist-plugin-legend.min.js"></script>
<!-- 
    <script src="//cdn.jsdelivr.net/npm/jquery.flot@0.8.3/jquery.flot.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/flot-pie@1.0.0/src/jquery.flot.pie.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/flot-spline@0.0.1/js/jquery.flot.spline.min.js"></script> -->

    
    <script src="//cdnjs.cloudflare.com/ajax/libs/flot/0.8.3/jquery.flot.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/flot/0.8.3/jquery.flot.pie.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/flot-spline@0.0.1/js/jquery.flot.spline.min.js"></script>

    <!-- <script src="//cdn.jsdelivr.net/npm/simpleweather@3.1.0/jquery.simpleWeather.min.js"></script> -->
    <!-- <script src="{{ url('public/public/js/init/weather-init.js') }}"></script> -->

    <script src="//cdn.jsdelivr.net/npm/moment@2.22.2/moment.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.js"></script>
    <script src="{{ url('public/js/init/fullcalendar-init.js') }}"></script>

    <!-- DataTable -->
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script> <!-- Used for Buttons maker Export -->
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>  <!-- Used for HTML5Buttons maker Export -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>   <!-- Used for HTML5Excel Export -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>  <!-- Used for HTML5PDF maker Export -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>    <!-- Used for HTML5CSV Export -->
    

     <!-- Select2 -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <!-- Date Picker -->
    <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script> -->

    <!-- Logout, Delete And Menu Active -->
        <script>
            function logout(event){
                event.preventDefault();
                var check = confirm("Do you really want to logout ?");
                if(check){ 
                    document.getElementById('logout-form').submit();
                }
            }

            function myFunction() {
                if(!confirm("Are you sure you want to delete ?"))
                event.preventDefault();
            }

            jQuery(document).ready(function(){

                var url = window.location.href.substr(window.location.href.lastIndexOf("/") + 1);

                if(url.split('?')[0] == 'customer' || url.split('?')[0] == 'machines' || url.split('?')[0] == 'contact' || url.split('?')[0] == 'technicians' || url.split('?')[0] == 'wipes' || url.split('?')[0] == 'sources' || url.split('?')[0] == 'preAssignSou' || url.split('?')[0] == 'TransferSou' || url.split('?')[0] == 'signatures' || url.split('?')[0] == 'home' || url.split('?')[0] == 'users' || url.split('?')[0] == 'regulatory_report' || url.split('?')[0] == 'TransferMach'){
                    url = url.split('?')[0];
                    jQuery('[href$="'+url+'"]').parent().addClass("active");
                    jQuery('[href$="'+url+'"]').parent().parent().addClass("show");
                    jQuery('[href$="'+url+'"]').parent().parent().parent().addClass("active show");
                }
            });
        </script>
    <!-- Logout, Delete And Menu Active -->

    <!-- Tooltip -->
        <script type="text/javascript">
            jQuery(document).ready(function(){
                jQuery("[rel=tooltip]").tooltip({ placement: 'top'});
            });
        </script>
    <!-- Tooltip -->

    <!-- Machine Model Autocomplete -->
        <script>
            jQuery(document).ready(function(){
                jQuery('.select_mach_model').select2({
                    placeholder: 'Select machine by name',
                    ajax: {
                        url: '{{ route("machine_autocomplete") }}',
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: jQuery.trim(params.term)
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: data
                            };
                        },
                        cache: true
                    }
                });
            });
        </script>
    <!-- Machine Model Autocomplete -->

    <!-- Machine Model Autocomplete -->
        <script>
            jQuery(document).ready(function(){
                jQuery('.select_machine_num').select2({
                    placeholder: 'Select machine by number',
                    ajax: {
                        url: '{{ route("select_machine_num") }}',
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: jQuery.trim(params.term)
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: data
                            };
                        },
                        cache: true
                    }
                });
            });
        </script>
    <!-- Machine Model Autocomplete -->

    <!-- Customer Autocomplete -->
        <script>
            jQuery(document).ready(function(){
                jQuery('.select_customer').select2({
                    placeholder: 'Select customer by customer no, name and city',
                    ajax: {
                        url: '{{ route("customer_autocomplete") }}',
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: jQuery.trim(params.term)
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: data
                            };
                        },
                        cache: true
                    }
                });
            });
        </script>
    <!-- Customer Autocomplete -->

    <!-- Set Sidebar Menu Open Or Close -->
        <script>
            jQuery(document).on('click', '#menuToggle', function(){
                // localStorage.setItem('is_menu_open', 'false');
                var value = localStorage.getItem('is_menu_open');

                if(value == 'false'){
                    localStorage.setItem('is_menu_open', 'true');
                }else{
                    localStorage.setItem('is_menu_open', 'false');
                }
            });

            jQuery(window).on('load', function(){
                var value = localStorage.getItem('is_menu_open');
                
                if(value == 'true' || value == null){
                    jQuery('body').removeClass('open');
                }else{
                    jQuery('body').addClass('open');
                }
            });
        </script>
    <!-- Set Sidebar Menu Open Or Close -->


   


    <!-- Search Log -->
        <script>
            jQuery(document).ready(function(){
                function fetch_log_data(query)
                {
                    jQuery.ajax({
                        url:"{{ route('log_search') }}",
                        method:'GET',
                        data:{query:query},
                        dataType:'json',
                        success:function(data)
                        {
                            jQuery('tbody').html(data.table_data);
                        }
                    })
                }

                jQuery(document).on('click', '#search_log', function(){
                    var query = jQuery('#search_log_term').val();

                    if (query != '') {
                        fetch_log_data(query);

                    }
                });
            });

            jQuery('#search_log_term').keypress(function (e) {
                var key = e.which;
                if(key == 13)  // the enter key code
                {
                    jQuery('#search_log').click();
                    return false;  
                }
            });  

            
        </script>
    <!-- Search Log -->

    <!-- Search Main Header Customer Name -->
        <script>
            jQuery(document).ready(function(){

                jQuery('#customer_main_search').on('keyup', function(){ 
                    var query = jQuery(this).val();
                    if(query != '')
                    {
                        var _token = jQuery('input[name="_token"]').val();
                        jQuery.ajax({
                            url:"{{ route('customer_main_search') }}",
                            method:"GET",
                            data:{query:query, _token:_token},
                            dataType:'json',
                            success:function(data){
                                jQuery('#customer_list').html(data.customer_data);
                            }
                        });
                    }
                });
            });
        </script>
    <!-- Search Main Header Customer Name -->

    <!-- State Autocomplete -->
        <script>
            jQuery(document).ready(function(){
                jQuery('.select_state').select2({
                    placeholder: 'Select State',
                    ajax: {
                        url: '{{ route("state_autocomplete") }}',
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: jQuery.trim(params.term)
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: data
                            };
                        },
                        cache: true
                    }
                });
            });
        </script>
    <!-- State Autocomplete -->

    <!-- Technician Autocomplete -->
        <script>
            jQuery(document).ready(function(){

                
                        jQuery.ajax({
                            url:"{{ route('technician_autocomplete') }}",
                            method:"GET",
                            dataType:'json',
                            success:function(data){
                                var st=jQuery('#edittech').val();       
                                 
                                jQuery.each(data, function( index, value ) {
                                 
                                 var res='<option value="'+value.id+'">'+value.text+'</option>';    
                                    
                                 if(st!=""){

                                jQuery('.select_tech_model').val(st);
                                 } 
                                jQuery('.select_tech_model').append(res);

                            });
                                jQuery('.select_tech_model').select2({
                                        placeholder: 'Enter Technician',
                                        allowClear: true
                                    });
                        }
                        });

                
            });
        </script>
    <!-- Technician Autocomplete -->


    <!-- Enter Source Number Fetch Wipe Data -->
        <script>
            jQuery(document).ready(function(){
                jQuery('.select_source_model').select2({
                    placeholder: 'Enter Source Number',
                    ajax: {
                        url: '{{ route("source_autocomplete") }}',
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: jQuery.trim(params.term)
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: data
                            };
                        },
                        cache: true
                    }
                });
            });

            jQuery(document).ready(function(){

                jQuery('.select_source_model').on('change', function(){
                    var source_num = jQuery(".select_source_model option:selected").text();
                    if(source_num !== null){
                        fetch_source_data2(source_num);
                    }
                });

                function fetch_source_data2(query)
                {
                    jQuery.ajax({
                        url:"{{ route('source_search') }}",
                        method:'GET',
                        data:{query:query},
                        dataType:'json',
                        success:function(data)
                        {
                            jQuery('.table_format').html(data.table_data);
                            jQuery('#customer_no').removeAttr('enable');
                            jQuery('#source_shipped').removeAttr('enable');
                            jQuery('#date').removeAttr('enable');
                            jQuery('#machine_number').removeAttr('enable');
                            jQuery('#source_number').removeAttr('enable');
                            jQuery('#location').removeAttr('enable');
                            jQuery('#bill_info').removeAttr('enable');
                            jQuery('#contact').removeAttr('enable');

                            jQuery('#customer_no').attr('disabled', 'disabled');
                            jQuery('#source_shipped').attr('disabled', 'disabled');
                            jQuery('#date').attr('disabled', 'disabled');
                            jQuery('#machine_number').attr('disabled', 'disabled');
                            jQuery('#source_number').attr('disabled', 'disabled');
                            jQuery('#location').attr('disabled', 'disabled');
                            jQuery('#bill_info').attr('disabled', 'disabled');
                            jQuery('#contact').attr('disabled', 'disabled');

                            jQuery('#customer_no').val('');
                            jQuery('#source_shipped').val('');
                            jQuery('#date').val('');
                            jQuery('#machine_number').val('');
                            jQuery('#source_number').val('');
                            jQuery('#location').val('');
                            jQuery('#bill_info').val('');
                            jQuery('#contact').val('');

                            jQuery('.table_format td').css('cssText', 'font-size:9px !important');
                        }
                    })
                }

                jQuery('#discrepcod').on('change', function(){
                    var discrepcod = jQuery('#discrepcod').val();
                    jQuery.ajax({
                        url:"{{ route('discrepcod') }}",
                        method:'GET',
                        data:{discrepcod:discrepcod},
                        dataType:'json',
                        success:function(data)
                        {
                            if(data.descriptn !== null){
                                jQuery('#discrepancies').removeAttr('disabled');
                                jQuery('#discrepancies').val(data.descriptn);
                            }
                        }
                    })
                });

                jQuery('.select_tech_model').on('change', function(){
                    var descriptn = jQuery(".select_tech_model option:selected").text();

                    if(descriptn !== null){
                        jQuery('#wiped_by').removeAttr('disabled');
                        jQuery('#wiped_by').val(descriptn);
                    }
                });
            });  
        </script>
    <!-- Enter Source Number Fetch Wipe Data -->

    <!-- Wipe Data Autofill -->
        <script>
            jQuery(document).ready(function(){
                jQuery(document).on("click", '.fill_wipe_data', function(){
                    var source_id = jQuery(this).attr('source_id');
                    jQuery.ajax({
                        url:"{{ route('wipe_data_autofill') }}",
                        method:'GET',
                        data:{source_id:source_id},
                        dataType:'json',
                        success:function(data)
                        {
                            jQuery('#customer_no').attr('disabled', true);
                            jQuery('#customer_no').val(null);

                            jQuery('#source_shipped').attr('disabled', true);
                            jQuery('#source_shipped').val(null);

                            jQuery('#date').attr('disabled', true);
                            jQuery('#date').val(null);

                            jQuery('#machine_number').attr('disabled', true);
                            jQuery('#machine_number').val(null);

                            jQuery('#source_number').attr('disabled', true);
                            jQuery('#source_number').val(null);

                            jQuery('#location').attr('disabled', true);
                            jQuery('#location').val(null);

                            jQuery('#contact').attr('disabled', true);
                            jQuery('#contact').val(null);

                            jQuery('#bill_info').attr('disabled', true);
                            jQuery('#bill_info').text(null);

                            jQuery('#single button').removeAttr('disabled');
                            if(data.source_data.customer_name !== null){
                                jQuery('#customer_no').removeAttr('disabled');
                                jQuery('#customer_no').val(data.source_data.customer_number);
                                jQuery('#customer_number').val(data.source_data.customer_number);
                                jQuery('#email_demo_customer_name').text('Dear Mr. '+data.source_data.customer_name);
                            }
                            if(data.source_data.s_status !== null){
                                jQuery('#source_shipped').removeAttr('disabled');
                                jQuery('#source_shipped').val(data.source_data.s_status);
                            }
                            if(data.source_data.ship_date !== null){
                                jQuery('#date').removeAttr('disabled');
                                jQuery('#date').val(data.source_data.ship_date);
                            }
                            if(data.source_data.mach_num !== null){
                                jQuery('#machine_number').removeAttr('disabled');
                                jQuery('#machine_number').val(data.source_data.mach_num);
                            }
                            if(data.source_data.source_num !== null){
                                jQuery('#source_number').removeAttr('disabled');
                                jQuery('#source_number').val(data.source_data.source_num);
                            }
                            if(data.source_data.currentloc !== null){
                                jQuery('#location').removeAttr('disabled');
                                jQuery('#location').val(data.source_data.customer_name+', '+data.source_data.currentloc);
                            }
                            if(data.source_data.contact !== ' '){
                                console.log(data.source_data.contact);
                                jQuery('#contact').removeAttr('disabled');
                                jQuery('#contact').val(data.source_data.contact);
                            }
                            if(data.source_data.bill_to_info !== null){
                                jQuery('#bill_info').removeAttr('disabled');
                                jQuery('#bill_info').val(data.source_data.bill_to_info);
                            }else{
                                jQuery('#bill_info').removeAttr('disabled');
                                jQuery('#bill_info').val('No Bill To Address.');
                            }
                        }
                    });
                });
            });
        </script>
    <!-- Wipe Data Autofill -->

    @stack('script')

    
</body>
</html>