@extends('contact.layout')

@section('contact_content')
<?php define('TRUNCATE_LINK_CHARACTER', '?');   // Truncate Link with the specific character  ?>
<div class="card-body nopadding">
  <div id="campaignListingElement" class="table-responsive col-md-12 table-stats order-table nopadding">
    <table class="table table-striped table-bordered maintable" id="campainTable">
      <thead class="table-dark thead-dark">
        <tr>
          <th class="text-center align-middle" scope="col">Sr No.</th>
          <th class="text-center align-middle" scope="col">Campaign ID</th>
          <th class="text-center align-middle w-50" scope="col">Subject</th>
          <th class="text-center align-middle" scope="col">List Name</th>
          <th class="text-center align-middle" scope="col">Campaign Created At<br />Campaign Sent At</th>
          <th class="text-center align-middle" scope="col">Action</th>
        </tr>
      </thead>

      <tbody class="table_format">
        @forelse ($mailchimp_data as $key => $mailchimp)
        @php 
          $mailchimp_link_members = $mailchimp->mailchimp_campaign_link_members;
          $total_mailchimp_link_members = $mailchimp_link_members->count();
          $total_synced_mailchimp_link_members = $mailchimp_link_members->whereIn('sync_to_bullhorn', ['1', '2'])->count();
        @endphp 
        <tr>
          <td class="text-center align-middle">{{ $key + 1 }}</td>
          <td class="text-center align-middle">{{ $mailchimp->campaign_id }}</td>
          <td class="text-center align-middle">{{ $mailchimp->subject }}</td>
          <td class="text-center align-middle">{{ $mailchimp->list_name }}</td>

          <td class="text-center align-middle">
            {{ $mailchimp->campaign_create_at }}
            <br />
            {{ $mailchimp->campaign_sent_at }}
          </td>
          <td class="text-center align-middle">
            @if($total_mailchimp_link_members == 0)  
            @elseif($total_synced_mailchimp_link_members >= $total_mailchimp_link_members)
              <button class="btn btn-light disabled text-success font-weight-bold cursor-pointer">Synced</button>
            @else 
              <button class="btn btn-dark blh_mlchp__syncData" data-bullhornData="{{ $mailchimp->campaign_id}}" data-eventType='' data-mailchimpSubject="{{ $mailchimp->subject }}" data-campainSentDate="{{ $mailchimp->campaign_sent_at }}" data-opend_date=''>Sync</button>
            @endif 
            <button class="btn btn-dark toggle" value="{{ $mailchimp->campaign_id }}">Toggle</button>
          </td>
        </tr>
        @if(!empty($mailchimp->mailchimp_campaign_links))
        <tr class="subtable{{ $mailchimp->campaign_id }}" style="display:none;">
          <td colspan="2">&nbsp;</td>
          <td colspan="4">
            <table class="table table-striped table-bordered ">
              <thead class="table-dark thead-dark">
                <tr>
                  <th class="text-center align-middle" scope="col">Sr No.</th>
                  <th class="text-center align-middle" scope="col">Link ID</th>
                  <th class="text-center align-middle w-50" scope="col">Link URL</th>
                  <th class="text-center align-middle" scope="col">Total Clicks</th>
                  <th class="text-center align-middle" scope="col">Unique Clicks</th>
                  <th class="text-center align-middle" scope="col">Action</th>
                </tr>
              </thead>
              @foreach($mailchimp->mailchimp_campaign_links as $link_key => $mailchimp_links)
                @php
                  $mailchimp_link_url = $mailchimp_links->link_url;
                  $trucate_position = strpos($mailchimp_link_url, TRUNCATE_LINK_CHARACTER);
                  if(!empty($trucate_position)) {
                    $trucate_position = $trucate_position;
                  } else {
                    $trucate_position = strlen($mailchimp_link_url);
                  }
                  $original_link = substr($mailchimp_link_url, 0, $trucate_position);
                  // $original_link = substr($mailchimp_links->link_url, 0, strpos($mailchimp_links->link_url, TRUNCATE_LINK_CHARACTER));
                  
                  $link_members = $mailchimp_links->mailchimp_campaign_link_members;
                  $total_link_members = $link_members->count();
                  $total_synced_link_members = $link_members->whereIn('sync_to_bullhorn', ['1', '2'])->count();
                @endphp
                <tr style="line-height: 25px">
                  <td class="text-center align-middle"> {{ $link_key + 1 }} </td>
                  <td class="text-center align-middle"> {{ $mailchimp_links->link_id }} </td>
                  <td class="text-center align-middle"> {{ $original_link }} </td>
                  <td class="text-center align-middle"> {{ $mailchimp_links->total_clicks }} </td>
                  <td class="text-center align-middle"> {{ $mailchimp_links->unique_clicks }} </td>
                  <td class="text-center align-middle">
                    @if($total_link_members == 0) 
                      
                    @elseif($total_synced_link_members >= $total_link_members)
                      <button class="btn btn-light disabled text-success font-weight-bold cursor-pointer">Synced</button>
                    @else 
                      <button class="btn btn-dark blh_mlchp__syncData" data-bullhornData="{{ $mailchimp_links->campaign_id }}" data-eventType="{{ $mailchimp_links->link_id }}" data-mailchimpSubject="{{ $mailchimp->subject }}" data-campainSentDate="{{ $mailchimp->campaign_sent_at }}" data-opend_date=''>Sync</button>
                    @endif 
                    <button class="btn btn-dark toggleMember" value="{{ $mailchimp_links->link_id }}">Toggle</button>
                  </td>
                </tr>

                @if(!empty($mailchimp_links->mailchimp_campaign_link_members))
                  <tr class="subtablemember{{ $mailchimp_links->link_id }}" style="display:none;">
                    <td colspan="2">&nbsp;</td>
                    <td colspan="4">
                      <table class="table table-striped table-bordered ">
                        <thead class="table-dark thead-dark">
                          <tr>
                            <th class="text-center align-middle" scope="col">Sr No.</th>
                            <th class="text-center align-middle" scope="col">Email Address</th>
                            <th class="text-center align-middle" scope="col">Clicks</th>
                            <th class="text-center align-middle" scope="col">Contact Status</th>
                            <th class="text-center align-middle" scope="col">Action</th>
                          </tr>
                        </thead>

                        @if($mailchimp_links->mailchimp_campaign_link_members->count() > 0)
                          @foreach($mailchimp_links->mailchimp_campaign_link_members as $mem_key => $mailchimp_member)
                            <tr style="line-height: 25px">
                              <td class="text-center align-middle"> {{ $mem_key + 1 }} </td>
                              <td class="text-center align-middle"> {{ $mailchimp_member->email_address }} </td>
                              <td class="text-center align-middle"> {{ $mailchimp_member->clicks }} </td>
                              <td class="text-center align-middle"> {{ $mailchimp_member->contact_status }} </td>
                              <td class="text-center align-middle">
                                @if($mailchimp_member->sync_to_bullhorn == '0')
                                  <button class="btn btn-dark blh_mlchp__syncData" data-bullhornData="{{ $mailchimp_member->campaign_id }}" data-eventType="{{ $mailchimp_member->url_id }}" data-mailchimpSubject="{{ $mailchimp->subject }}" data-campainSentDate="{{ $mailchimp->campaign_sent_at }}" data-memberid='{{ $mailchimp_member->id }}' data-opend_date=''>Sync</button>
                                @else 
                                  @if($mailchimp_member->sync_to_bullhorn == '1')
                                    <button class="btn btn-light disabled text-success font-weight-bold cursor-pointer">Synced</button>
                                  @else 
                                    <button class="btn btn-light disabled text-danger font-weight-bold cursor-pointer" data-title="Data Not Found in Bullhorn" title="Data Not Found in Bullhorn">Sync Error</button>
                                  @endif
                                @endif
                              </td>
                            </tr>
                          @endforeach
                        @else 
                          <tr class="lh-lg">
                            <td colspan="5" class="text-center font-weight-bold">No Records!!!</td>
                          </tr>
                        @endif 
                      </table>
                    </td>
                  </tr>
                @endif
              @endforeach
            </table>
          </td>
        </tr>
        @endif

        @empty

        <tr>
          <td colspan="7" class="text-center align-middle">
            <p>Contact Not Found.</p>
          </td>
        </tr>
        @endforelse

      </tbody>
    </table>
    <div class="col page_hide d-flex justify-content-between">
      <div class="">
          {{ $mailchimp_data->onEachSide(5)->links() }}
          {{-- $mailchimp_data->links() --}}
          {{-- custom pagination --- $mailchimp_data->links('view.name') --}}
        </div>
        <div class="">
          {{-- Showing  1  to {{ $mailchimp_data->perPage() * $mailchimp_data->currentPage() }} of {{ $mailchimp_data->total() }} entries --}}
          Showing {{ $custom_pagination['startRecordNumber'] }} to {{ $custom_pagination['endRecordNumber'] }} of {{ $mailchimp_data->total() }} entries
          
      </div>
    </div>
  </div>
</div>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script>
  let APP_URL = "{{ config('app.url') }}";
  jQuery(document).on('click', '.blh_mlchp__syncData', function() {
    var clickedElem = jQuery(this);
    bullHornData = clickedElem.attr('data-bullhornData');
    // parseBullHornData = JSON.parse(bullHornData);
    parseBullHornData = bullHornData;
    eventType = clickedElem.attr('data-eventType');
    // localDbData = clickedElem.attr('data-localDbData');
    mailchimpSubjectDbData = clickedElem.attr('data-mailchimpSubject');
    openDate = clickedElem.attr('data-opend_date');
    dataMemberid = clickedElem.attr('data-memberid');
    campainSentDate = clickedElem.attr('data-campainSentDate');
    //        parseLocalDbData = JSON.parse(localDbData);	
    // parseLocalDbData = localDbData;
    parseMailchimpDbData = mailchimpSubjectDbData;
    parseCampainSentDate = campainSentDate;

    const swalWithBootstrapButtons = Swal.mixin({
      // customClass: {
      //     confirmButton: 'btn btn-success',
      //     cancelButton: 'btn btn-danger'
      // },
      // buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'Are you sure to sync data?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, sync it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        // Add Spinner to the sync button
        clickedElem.attr('disabled', true).append('<i class="fa fa-spinner fa-spin"></i>');
        jQuery.ajax({
          type: 'POST',
          url: APP_URL + '/bullhorn/mailchimp/manualMailchimpDataSyncToBullhornData.php',
          cache: false,
          dataType: 'json',
          data: {
            'bullHornData': parseBullHornData,
            'eventType': eventType,
            'openDate': openDate ?? "",
            // 'localData': parseLocalDbData,
            'mailchimpSubjectData': parseMailchimpDbData,
            'campainSentDate': parseCampainSentDate,
            'dataMemberid': dataMemberid ?? ""

          },
          success: function(response) {
            // console.log(response.status);
            if (response.status) {
              swalWithBootstrapButtons.fire(
                'Synced!',
                'Your data has been synced.',
                'success'
              )
              // clickedElem.parent('td').html('<span style="font-weight: 900; color: green;">' + response.text + '</span> ');
            } else {
              swalWithBootstrapButtons.fire(
                'Synced!',
                'Error! in syncing data to Bullhorn.',
                'error'
              )
            }
            clickedElem.attr('disabled', false).find('.fa-spinner').remove();
          },
          error: function() {
            // console.log('Response Error ');
            clickedElem.attr('disabled', false).find('.fa-spinner').remove();
            swalWithBootstrapButtons.fire(
              'Synced!',
              'Error! in syncing data to Bullhorn.',
              'error'
            )
          }
        });
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelled',
          'Your sync data request was cancelled :)',
          'error'
        )
      }
    });
  });

  jQuery(document).on('click', '.toggle', function() {
    var tbody = jQuery('tr.subtable' + jQuery(this).val());
    tbody.slideToggle();
  });
  jQuery(document).on('click', '.toggleMember', function() {
    var tbody = jQuery('tr.subtablemember' + jQuery(this).val());
    tbody.slideToggle();
  });
</script>
@endsection