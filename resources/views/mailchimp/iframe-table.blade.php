@extends('customer.layout')


@section('customer_head_button')

@php
@endphp

<iframe name="mailchimpDataFrame" src="{{ config('app.url') . '/bullhorn/mailchimp/mailchimp-reports-list.php'}}" width="100%" height="1200px" frameBorder="0" onload="resizeIframe(this)" scroll="no"></iframe>

<script>
  function resizeIframe(obj) {
    obj.style.height = obj.contentWindow.document.documentElement.scrollHeight + 'px';
  }
</script>


@endsection