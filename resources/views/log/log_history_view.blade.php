
<div class="card-body nopadding">

        <div class="col-md-12 table-stats order-table ov-h nopadding">
            <table class="table " id="customer_table">
                    <thead>
                        <tr>
                            <th>S.no.</th>
                            <th>User</th>
                            <th>Date Time</th>
                            <th>Description</th>
                            <th>Actions</th>



                        </tr>
                    </thead>

                    <tbody class="table_format">
                        <!-- {{ $i = 1}} -->
                        @forelse ($log_data as $key => $log)
                        
                            <tr>
                                <td>{{ $i}}</td>
                                <td>{{$log->user_name}}</td>
                                <td>{{$log->created_at}}</td>
                                <td>{{$log->description}}</td>
                                <td>
                                    <span class="badge badge-complete showdetail" className="showmoreactivity{{$log->id}}">
                                        Show More
                                    </span>
                                    <input type="hidden" class="showmoreactivity{{$log->id}}old" value="{{$log->old_value}}">
                                    <input type="hidden" class="showmoreactivity{{$log->id}}new" value="{{$log->new_value}}">
                                </td>
                                    
                            </tr>

                        <!-- {{$i++}} -->
                        @empty

                        <tr>
                            <td colspan="10" class="text-center">
                                <p>No activity.</p>
                            </td> 
                        </tr>
                        @endforelse



                    </tbody>
            </table>

       
        </div>
        <div class="col-md-12 table-stats order-table ov-h nopadding">
             <table class="table" id="customer_table">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Document</th>
                            <th>Docs Creation Date</th>
                            <th>Upload Date</th>
                        </tr>     
                    </thead>

                    <tbody class="table_format">
                        @forelse($uploaddoc as $key => $data) 
                        <tr>
                            <td>{{ $data->title }}</td>
                            <td><a href="{{ url('sourcetracker/'.$data->contact_id.'/'.$data->document) }}" target="_blank"><i class="fa fa-file fa-lg text-info" aria-hidden="true"></i></a></td>
                            <td>{{ date('d M Y', strtotime($data->created_at)) }}</td>
                             <td>{{ date('d M Y', strtotime($data->upload_date)) }}</td>
                        </tr>
                        @empty
                        <tr>
                          <td colspan="7" class="text-center">
                            <p>Document Not Found.</p>
                          </td> 
                        </tr>
                        @endforelse
                    </tbody>
            </table>
        </div>
</div>
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header" style="background-color: darkgrey">
            <h5 class="modal-title" id="exampleModalLongTitle"> Activity Detail </h5>
          </div>
          <div class="modal-body showJSONData">
            <div class="row">
                <div class="col-md-6">
                    <h3> New Data </h3>
                    <table class="table activityTable">   
                    </table>
                </div>
                <div class="col-md-6">
                    <h3> Old Data </h3>
                    <table class="table activityTableold">    
                    </table>
                </div>
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
<script src="{{ url('public/js/custom.js') }}"></script>