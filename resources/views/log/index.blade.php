@extends('layout.app')

@section('content')

	<!-- Content -->
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
        	<!--  Traffic  -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                        	<div class="row form-group">
                        		<div class="col-md-3">
                    				<h4 class="box-title">Log List </h4>
                        		</div>

                        		<div class="col-md-6">
                        			<div class="input-group">
                        				<input class="form-control"  name="search_term" id="search_log_term" placeholder="Search..">
                        				<div class="input-group-btn">
                        					<button type="button" id="search_log" class="btn btn-outline-primary" >Search</button>
                        				</div>

                        				<a class="btn input-group-btn"href="{{ route('logs.index') }}" >Reset</a>
                        			</div>
                        		</div>

                        		<div class="col-md-3"></div>
                        	</div>
                        </div>
                        <div class="card-body">
                        	<div class="col-md-12 table-stats order-table ov-h">
	                            <table class="table">
				                  	<thead>
					                    <tr>
					                    	<th>#</th>
					                      	<th>Log Name</th>
					                      	<th>Subject</th>
					                      	<th>Causer</th>
					                      	<th>User Name</th>
					                      	<th>User Email</th>
					                      	<th style="text-align: left;">Created At</th>
					                    </tr>
				                  	</thead>

				                  	<tbody>
				                    	<!-- {{$i = 1}}  -->
				                    	@forelse($logs as $key => $log)
					                      	<tr>
						                        <td>{{ $logs->firstItem() + $key }}</td>
						                        <td>{{ $log->description }}</td> 
						                        <td>{{ $log->subject_type }}</td> 
						                        <td>{{ $log->causer_type }}</td> 
						                        <td>{{ $log->getExtraProperty('name') }}</td> 
						                        <td>{{ $log->getExtraProperty('email') }}</td>
						                        <td style="text-align: left;">{{ date('d M Y', strtotime($log->created_at)) }}</td>
					                      	</tr>
				                    	@empty
					                        <tr>
					                          <td colspan="7" class="text-center">
					                          	<p>Log Not Found.</p>
					                          </td> 
					                        </tr>
				                    	@endforelse
				                  	</tbody>
				                </table>
                        	</div> <!-- /.col-md-12 -->
				        </div><!-- card-body -->
                        <div class="card-body">
                            {{ $logs->onEachSide(1)->links() }}
                        </div>
                        <div class="card-body"></div>
                    </div>
                </div><!-- /# column -->
            </div>
            <!--  /Traffic -->
            <div class="clearfix"></div>
        </div>
    	<!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>
@endsection