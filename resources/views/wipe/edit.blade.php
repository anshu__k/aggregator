@extends('layout.app')

@section('content')
<style>
  .wipe_data th{
    font-size: 9px !important;
    font-weight: 700 !important;
    padding: 0px !important;
    text-align: center;
  }
  .wipe_data tr, .table_format tr{
    text-align: center;
  }
  .table_format td{
    padding: 0px !important;
  }
  .fill_wipe_data td{
    font-size: 8px !important;
    vertical-align: middle;
  }
</style>
<style>
  .wipe_data th{
    font-size: 13px !important;
    font-weight: 700 !important;
  }
  .wipe_data tr, .table_format tr{
    text-align: center;
  }
  .fill_wipe_data td{
    font-size: 8px !important;
  }
  .table_format td{
    padding: 0px !important;
    vertical-align: middle;
  }
  #exampleModalLabel{
    margin-block-end: -25px;
    font-weight: 700;
    font-size: larger;
  }
  .email_demo_font{
    font-family: monospace;
    /*margin-top: 10px;*/
  }

  .table-input{
    max-width: 114px;
    border: 0px;
  }
  .wipelistcombinations{
    font-size: 12px;
  }
</style>
  <!-- Content -->
  <div class="content">
    <!-- Animated -->
    <div class="animated fadeIn">
      <!--  Traffic  -->
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header">
                @if(session()->has('insert'))
                  <div class="alert alert-success alert-dismissible" role="alert">
                    {{ session()->get('insert') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                @endif
                <div class="row">
                  <div class="col-lg-6">
                  <h4 class="box-title">Update Single Record Wipe </h4>
                  </div>
                  <div class="col-lg-6">
                    <a href="{{ route('wipes.index') }}" class="btn btn-outline-secondary float-right"><i class="fa fa-angle-double-left"></i> Back</a>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <ul class="nav nav-tabs" role="tablist">
                  <li class="nav-item" style="width: 10%;">
                    <a class="nav-link" id="singletabopen" href="#single" role="tab" data-toggle="tab" style="text-align: center;" onclick="location.href='#single'"></a>
                  </li>
                  <li class="nav-item" style="width: 10%;">
                    <a class="nav-link" id="multipletabopen" href="#multiple" role="tab" data-toggle="tab" style="text-align: center;" onclick="location.href='#multiple'"></a>
                  </li>
                </ul>

                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane mt-3 active" id="single">
                    <form action="{{ route('wipes.update', $wipe_data) }}" method="POST" enctype="multipart/form-data" id="single_wipe_form">
                      {{ csrf_field() }}
                      <input name="_method" type="hidden" value="PUT">
                      <div class="row form-group">
                        <div class="col-md-6">
                          <div class="box_border">
                            <div class="box_border">
                              <div class="heading_font_size font-weight-bold">
                                1. Enter or select a source number if you wish to type in single source. 
                              </div>
                              <div class="form-group ml-5 mt-3">
                                <div class="row">
                                  <div class="col-md-11">
                                    <!-- <input type="text" name="source_num" id="source_num" class="form-control input_size" value="{{ old('source_num') }}" placeholder="Enter source number"> -->
                                    <select class="form-control select_source_models input_size" name="source_num" id="source_num" value="{{ old('source_num') }}" readonly="readonly">
                                      @if(!empty($wipe_data->source_num))
                                        <option value="{{ $wipe_data->source_num }}">{{ $wipe_data->source_num }}</option>
                                      @endif
                                    </select>
                                    @if ($errors->has('source_num'))
                                        <div class="text-danger">{{ $errors->first('source_num') }}</div>
                                    @endif
                                  </div>
                                </div>
                              </div>

                              <div class="heading_font_size font-weight-bold">
                                2. Enter or select a default technician for wipe tests. 
                              </div>
                              <div class="form-group ml-5 mt-3">
                                <div class="row">
                                  <div class="col-md-11">
                                    <select class="form-control select_tech_model input_size" name="tech" id="tech" value="{{ old('tech') }}">
                                      <option value="{{ $wipe_data->tech }}">{{ $wipe_data->tech_name->tech_name }}</option>
                                    </select>
                                    @if ($errors->has('tech'))
                                        <div class="text-danger">{{ $errors->first('tech') }}</div>
                                    @endif
                                  </div>
                                </div>
                              </div>

                              <div class="heading_font_size font-weight-bold">
                                3. Enter or select a default discrepency for wipe tests. 
                              </div>
                              <div class="form-group ml-5 mt-3">
                                <div class="row">
                                  <div class="col-md-11">
                                    <select class="form-control input_size" name="discrepcod" id="discrepcod" value="{{ old('discrepcod') }}">
                                      <option selected disabled>Select Discrepancies</option>
                                      @foreach($discreps as $discrep)
                                      <option value="{{ $discrep->discrepcod }}" {{ $wipe_data->discrepcod == $discrep->discrepcod ? 'selected' : '' }}>{{ $discrep->descriptn }}</option>
                                      @endforeach
                                    </select>
                                    @if ($errors->has('discrepcod'))
                                    <div class="text-danger">{{ $errors->first('discrepcod') }}</div>
                                    @endif
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div class="box_border mt-3">
                              <div class="heading_font_size font-weight-bold">
                                4. Select an item where you wish to enter wipe test. 
                              </div>
                              <div class="form-group mt-3">
                                <div class="row">
                                  <div class="col-md-12">
                                    <table class="table table-bordered table-responsive">
                                      <thead>
                                        <tr class="wipe_data">
                                          <th>#</th>
                                          <th>Machine Number</th>
                                          <th>Source Number</th>
                                          <th>Customer Name</th>
                                          <th>Source Status</th>
                                          <th>Source Modal</th>
                                          <th>Inst Date</th>
                                          @if($wipe_data->discrepcod!=8)
                                          <th>Ship Date</th>
                                          @endif
                                          <th>Technician</th>
                                        </tr>
                                      </thead>
                                      <tbody class="table_format">
                                        @if($source !="")
                                        <tr class="fill_wipe_data" source_ids="{{ @$source->id }}">
                                          <!-- <td>1</td> -->
                                          <td><input type="radio" name="wipe_select" class="fill_wipe_data" style="width:10px;" checked></td>
                                          <td>{{ $wipe_data->mach_num }}</td>
                                          <td>{{ $wipe_data->source_num }}</td>
                                          <td>{{ $wipe_data->cust_num }}</td>
                                          <td>{{ @$source->source_status->sstat_desc }}</td>
                                          <td>{{ @$source->s_mod_num }}</td>
                                          <td>{{ date('m/d/y', strtotime($wipe_data->wipe_date)) }}</td>
                                          @if($wipe_data->discrepcod!=8)
                                          <td>{{ date('m/d/y', strtotime($wipe_data->meas_date)) }}</td>
                                          @endif
                                          <td>{{ $wipe_data->tech_name->tech_name }}</td>
                                        </tr>
                                        @else
                                         @foreach($sourcesAll as $k=>$ssingle)
                                         <tr class="fill_wipe_data_m" source_ids="{{ @$ssingle->id }}" state_id="{{$sourcesDes[$k]}}">
                                          <!-- <td>1</td> -->
                                          <td><input type="checkbox" name="wipe_select_old" class="fill_wipe_data" style="width:10px;" checked disabled></td>
                                          <td>{{ $ssingle->mach_num }}</td>
                                          <td>{{ $ssingle->source_num }}</td>
                                          <td>{{ $wipe_data->cust_num }}</td>
                                          <td >{{ @$ssingle->source_status->sstat_desc }}</td>
                                          <td>{{ @$ssingle->s_mod_num }}</td>
                                          <td>{{ date('m/d/y', strtotime($ssingle->inst_date)) }}</td>
                                          @if($wipe_data->discrepcod!=8)
                                          <td>{{ date('m/d/y', strtotime($ssingle->ship_date)) }}</td>
                                          @endif
                                          <td>{{ @$ssingle->tech }}</td>
                                        </tr>
                                         @endforeach

                                        @endif
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="box_border">
                            <div class="box_border">
                              <div class="heading_font_size font-weight-bold">
                                5. Review the customer or machine information. 
                              </div>
                              <div class="form-group ml-5 mt-3">

                                
                                <div class="row">
                                  <div class="col-md-4">
                                    <label class="label_font_size" for="source_number">Source Number :</label>
                                  </div>
                                  <div class="col-md-8">
                                    <input type="text" name="source_number" id="source_number" class="form-control input_size" value="{{ $source_data['source_num'] }}" @if(!empty($source_data['source_num'])) enable @else disabled @endif>
                                    @if ($errors->has('source_number'))
                                    <div class="text-danger">{{ $errors->first('source_number') }}</div>
                                    @endif
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-4">
                                    <label class="label_font_size" for="machine_number">Machine Number :</label>
                                  </div>
                                  <div class="col-md-8">
                                    <input type="text" name="machine_number" id="machine_number" class="form-control input_size" value="{{ $wipe_data->mach_num }}" @if(!empty($wipe_data->mach_num)) enable @else disabled @endif>
                                    @if ($errors->has('machine_number'))
                                    <div class="text-danger">{{ $errors->first('machine_number') }}</div>
                                    @endif
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="col-md-4">
                                    <label class="label_font_size" for="discrepancies">Discrepancies :</label>
                                  </div>
                                  <div class="col-md-8">
                                    @foreach($discreps as $discrep)
                                      @if($discrep->discrepcod == $wipe_data->discrepcod)
                                        <input type="text" name="discrepancies" id="discrepancies" class="form-control input_size" value="{{ $discrep->descriptn }}" @if($discrep->discrepcod == $wipe_data->discrepcod) enable @else disabled @endif>
                                      @endif
                                    @endforeach
                                    @if ($errors->has('discrepancies'))
                                      <div class="text-danger">{{ $errors->first('discrepancies') }}</div>
                                    @endif
                                  </div>
                                </div>
                                @if($wipe_data->discrepcod!=8)
                                  
                                <div class="row">
                                  <div class="col-md-4">
                                    <label class="label_font_size" for="source_shipped">Source Shipped Date:</label>
                                  </div>
                                  <div class="col-md-8">
                                    <input type="text" name="date" id="date" class="form-control input_size" value="{{ $source_data['ship_date'] }}" @if(!empty($source_data['ship_date'])) enable @else disabled @endif>
                                    @if ($errors->has('date'))
                                      <div class="text-danger">{{ $errors->first('date') }}</div>
                                    @endif
                                    </div>
                                  </div>
                                    @endif


                                <div class="row">
                                  <div class="col-md-4">
                                    <label class="label_font_size" for="customer_no">Customer Number :</label>
                                  </div>
                                  <div class="col-md-8">
                                    <input type="hidden" name="customer_number" id="customer_number" value="{{ $wipe_data->cust_num }}">
                                    <input type="text" name="customer_no" id="customer_no" class="form-control input_size" value="{{ $wipe_data->cust_num }}" @if(!empty($source_data['customer_name'])) enable @else disabled @endif>
                                    @if ($errors->has('customer_no'))
                                      <div class="text-danger">{{ $errors->first('customer_no') }}</div>
                                    @endif
                                  </div>
                                </div>
                                 <!-- <div class="row">
                                  <div class="col-md-4">
                                    <label class="label_font_size" for="customer_name">Customer Name :</label>
                                  </div>
                                  <div class="col-md-8">
                                    <input type="hidden" name="customer_name" id="customer_name" value="{{ $wipe_data->cust_name }}">
                                    <input type="text" name="customer_name" id="customer_name" class="form-control input_size" value="{{ $wipe_data->cust_name }}" @if(!empty($source_data['customer_name'])) enable @else disabled @endif>
                                    @if ($errors->has('customer_name'))
                                      <div class="text-danger">{{ $errors->first('customer_name') }}</div>
                                    @endif
                                  </div>
                                </div> -->
                                <div class="row mt-1">
                                  <div class="col-md-4">
                                    <label class="label_font_size" for="contact">Contact :</label>
                                  </div>
                                  <div class="col-md-8">
                                    <input type="text" name="contact" id="contact" class="form-control input_size" value="{{ $wipe_data->contact_num }}" disabled>
                                    @if ($errors->has('contact'))
                                    <div class="text-danger">{{ $errors->first('contact') }}</div>
                                    @endif
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="col-md-4">
                                    <label class="label_font_size" for="wiped_by">Wiped By :</label>
                                  </div>
                                  <div class="col-md-8">
                                    <input type="text" name="wiped_by" id="wiped_by" class="form-control input_size" value="{{ $wipe_data->tech_name->tech_name }}" @if(!empty($wipe_data->tech_name->tech_name)) enable @else disabled @endif>
                                    @if ($errors->has('wiped_by'))
                                      <div class="text-danger">{{ $errors->first('wiped_by') }}</div>
                                    @endif
                                  </div>
                                </div>
                                
                                
                                
                                
                                

                                <!-- <div class="row">
                                  <div class="col-md-4">
                                    <label class="label_font_size" for="location">Location :</label>
                                  </div>
                                  <div class="col-md-8">
                                    <textarea type="text" name="location" id="location" class="form-control input_size" value="{{ old('location') }}" @if(!empty($source_data['currentloc'])) enable @else disabled @endif>{{ $source_data['currentloc'] }}</textarea>
                                    @if ($errors->has('location'))
                                    <div class="text-danger">{{ $errors->first('location') }}</div>
                                    @endif
                                  </div>
                                </div> -->

                                <!-- <div class="row mt-1">
                                  <div class="col-md-4">
                                    <label class="label_font_size" for="bill_info">Bill To Info :</label>
                                  </div>
                                  <div class="col-md-8">
                                    <textarea type="text" name="bill_info" id="bill_info" class="form-control input_size" value="{{ old('bill_info') }}" disabled></textarea>
                                    @if ($errors->has('bill_info'))
                                    <div class="text-danger">{{ $errors->first('bill_info') }}</div>
                                    @endif
                                  </div>
                                </div> -->

                                
                              </div>
                            </div>

                            <div class="box_border mt-3">
                              <div class="heading_font_size font-weight-bold">
                                6. Enter wipe date, measure date and measurer. 
                              </div>
                              <div class="form-group ml-5 mt-3">
                                <div class="row">
                                  <div class="col-md-4">
                                    <label class="label_font_size" for="select_customer">Wipe Date :</label>
                                  </div>
                                  <div class="col-md-8">
                                    <div id="edit_wipe_date" class="input-group date" data-date-format="mm-dd-yyyy">
                                      <input type="text" name="wipe_date" id="wipe_date" class="form-control input_size" value="{{ $wipe_data->wipe_date }}">
                                      <span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
                                    </div>
                                    @if ($errors->has('wipe_date'))
                                    <div class="text-danger">{{ $errors->first('wipe_date') }}</div>
                                    @endif
                                  </div>

                                  <div class="col-md-4">
                                    <label class="label_font_size" for="measure_date">Measure Date :</label>
                                  </div>
                                  <div class="col-md-8">
                                    <div id="edit_measure_date" class="input-group date" data-date-format="mm-dd-yyyy">
                                      <input type="text" name="measure_date" id="measure_date" class="form-control input_size" value="{{ $wipe_data->meas_date }}">
                                      <span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
                                    </div>
                                    @if ($errors->has('measure_date'))
                                    <div class="text-danger">{{ $errors->first('measure_date') }}</div>
                                    @endif
                                  </div>

                                  <div class="col-md-4">
                                    <label class="label_font_size" for="measure_by">Measure By :</label>
                                  </div>
                                  <div class="col-md-8">
                                    @if($wipe_data->discrepcod!=8)
                                    <select class="form-control input_size" name="measure_by" id="measure_by" value="{{ old('measure_by') }}">
                                      <option selected disabled>Select measure by</option>
                                      @foreach($measurers as $measurer)
                                        <option value="{{ $measurer->meas_by }}" {{ ($wipe_data->meas_by == $measurer->meas_by) ? 'selected' : '' }}>{{ $measurer->meas_name }}</option>
                                      @endforeach
                                    </select>
                                    @else 
                                    <select class="form-control input_size" name="measure_by" id="measure_by" value="{{ old('measure_by') }}">
                                      <option selected disabled>Select measure by</option>
                                      @foreach($techname as $technamez)
                                        <option value="{{ $technamez->id }}" {{ ($wipe_data->meas_by == $technamez->id) ? 'selected' : '' }}>{{ $technamez->tech_name }}</option>
                                      @endforeach
                                    </select>
                                    @endif
                                    @if ($errors->has('measure_by'))
                                    <div class="text-danger">{{ $errors->first('measure_by') }}</div>
                                    @endif
                                  </div>

                                  <!-- <div class="col-md-8 mt-3">
                                    <label class="label_font_size" for="measure_date">Do You Want To Print The Wipe Certificate. :</label>
                                  </div>
                                  <div class="col-md-4 mt-3">
                                    <input type="radio" name="print_job" value="0" {{ ($wipe_data->print_job == 0) ? 'checked' : ''}} > Yes &nbsp;&nbsp;
                                    <input type="radio" name="print_job" value="1" {{ ($wipe_data->print_job == 1) ? 'checked' : ''}}> No 
                                  </div> -->

                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="row form-group">
                        
                      </div>

                      <button type="submit" class="btn btn-outline-primary float-right"><i class="fa fa-save"></i> Update</button>
                    </form>
                  </div>

                  <div role="tabpanel" class="tab-pane mt-3" id="multiple">
                    <form action="{{ route('wipes.storeMultiple') }}" method="POST" enctype="multipart/form-data" id="single_wipe_form_multiple">
                      {{ csrf_field() }}
                      <div class="row form-group">
                        <div class="col-md-12">
                          <div class="box_border">
                            <div class="">
                              <div class="heading_font_size font-weight-bold">
                                1. Enter or select a customer number if you wish to type in customer. 
                              </div>
                              <div class="form-group ml-5 mt-3">
                                <div class="row">
                                  <div class="col-md-11">
                                    <select class="form-control select_source_model_multiple input_size" name="source_num_multiple" id="source_num_multiple" value="{{ old('source_num') }}">
                                      <option value="{{ $wipe_data->cust_num }}">{{ $wipe_data->cust_num }}</option>
                                    </select>
                                    @if ($errors->has('source_num'))
                                        <div class="text-danger">{{ $errors->first('source_num') }}</div>
                                    @endif
                                  </div>
                                </div>
                              </div>
                              <input type="hidden" name="wipe_unq_data" value="{{$wipe_unq_data}}" id="wipe_unq_data">
                              <div class="heading_font_size font-weight-bold">
                                2. Enter or select a default technician for wipe tests. 
                              </div>
                              <div class="form-group ml-5 mt-3">
                                <div class="row">
                                  <div class="col-md-11">
                                    <select class="form-control select_tech_model input_size" name="tech_multiple" id="tech_multiple" value="{{ $wipe_data->tech }}">
                                     <option value="{{ $wipe_data->tech }}">{{ $wipe_data->tech_name->tech_name }}</option> 
                                    </select>
                                    @if ($errors->has('tech'))
                                        <div class="text-danger">{{ $errors->first('tech') }}</div>
                                    @endif
                                  </div>
                                </div>
                              </div>
                            </div>

                             <div class=" mt-3">
                              <div class="heading_font_size font-weight-bold">
                                3. Enter wipe date, measure date and measurer. 
                              </div>
                              <div class="form-group ml-5 mt-3">
                                <div class="row">
                                  <div class="col-md-4">
                                    <label class="label_font_size" for="select_customer">Wipe Date :</label>
                                  </div>
                                  <div class="col-md-7">
                                    <div id="edit_wipe_date_multiple" class="input-group date" data-date-format="mm-dd-yyyy">
                                      <input type="text" name="wipe_date_multiple" id="wipe_date_multiple" class="form-control input_size" value="{{ $wipe_data->wipe_date }}">
                                      <span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
                                    </div>
                                    @if ($errors->has('wipe_date_multiple'))
                                    <div class="text-danger">{{ $errors->first('wipe_date_multiple') }}</div>
                                    @endif
                                  </div>

                                  <div class="col-md-4">
                                    <label class="label_font_size" for="measure_date_multiple">Measure Date :</label>
                                  </div>
                                  <div class="col-md-7">
                                    <div id="edit_measure_date_multiple" class="input-group date" data-date-format="mm-dd-yyyy">
                                      <input type="text" name="measure_date_multiple" id="measure_date_multiple" class="form-control input_size" value="{{ $wipe_data->meas_date }}">
                                      <span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
                                    </div>
                                    @if ($errors->has('measure_date_multiple'))
                                    <div class="text-danger">{{ $errors->first('measure_date_multiple') }}</div>
                                    @endif
                                  </div>

                                  <div class="col-md-4">
                                    <label class="label_font_size" for="measure_by_multiple">Measure By :</label>
                                  </div>
                                  <div class="col-md-7">
                                    @if($wipe_data->discrepcod!=8)
                                    <select class="form-control input_size" name="measure_by" id="measure_by_multiple" value="{{ old('measure_by') }}">
                                      <option selected disabled>Select measure by</option>
                                      @foreach($measurers as $measurer)
                                        <option value="{{ $measurer->meas_by }}" {{ ($wipe_data->meas_by == $measurer->meas_by) ? 'selected' : '' }}>{{ $measurer->meas_name }}</option>
                                      @endforeach
                                    </select>
                                    @else 
                                    <select class="form-control input_size" name="measure_by" id="measure_by_multiple" value="{{ old('measure_by') }}">
                                      <option selected disabled>Select measure by</option>
                                      @foreach($techname as $technamew)
                                        <option value="{{ @$technamew->id }}" {{ ($wipe_data->meas_by == @$technamew->id) ? 'selected' : '' }}>{{ @$technamew->tech_name }}</option>
                                      @endforeach
                                    </select>
                                    @endif
                                    @if ($errors->has('measure_by_multiple'))
                                    <div class="text-danger">{{ $errors->first('measure_by_multiple') }}</div>
                                    @endif
                                  </div>
                                  <!-- <div class="col-md-8 mt-3">
                                    <label class="label_font_size" for="measure_date">Do You Want To Print The Wipe Certificate. :</label>
                                  </div>
                                  <div class="col-md-4 mt-3">
                                    <input type="radio" name="print_job_multiple" value="0"> Yes &nbsp;&nbsp;
                                    <input type="radio" name="print_job_multiple" value="1"> No 
                                  </div> -->
                                </div>
                              </div>
                            </div>

                            <div class="mt-3">
                              <div class="heading_font_size font-weight-bold">
                                4. Select an item where you wish to enter wipe test. 
                              </div>
                              <div class="form-group mt-3">
                                <div class="row">
                                  <div class="col-md-12">
                                    <table class="table table-bordered table-responsive">
                                      <thead>
                                        <tr class="wipe_data">
                                          
                                          <th>#</th>
                                          <th>Machine Number</th>
                                          <th>Source Number</th>
                                          <!-- <th>Customer Number</th> -->
                                          <th>Source Status</th>
                                          <th>Source Modal</th>
                                          <th>Inst Date</th>
                                          <th>Ship Date</th>
                                          <th>Technician</th>
                                          <th>Select</th>

                                        </tr>
                                      </thead>
                                      <tbody class="table_format_multiple">
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="box_border">
                            <!-- <div class="box_border">
                              <div class="heading_font_size font-weight-bold">
                                5. List of all selected combinations: 
                              </div>
                              
                              <table class="table table-bordered wipelistcombinations">
                                <tr>
                                    <th>S. No.</th>
                                    <th>Source Number</th>
                                    <th>Machine Number</th>
                                    <th>Action</th>
                                </tr>
                                
                                
                              </table>
                             
                            </div> -->

                           
                          </div>
                        </div>
                      </div>
                    </form>
                     <button id="submit_multiple_wipe" type="button" class="btn btn-outline-primary float-right"><i class="fa fa-save"></i> Save</button>
                  </div>
                  </div>
                </div>
              </div>
              <div class="card-body"></div>
            </div>
          </div><!-- /# column -->
        </div>
      <!--  /Traffic -->
      <div class="clearfix"></div>
    </div>
    <!-- .animated -->
  </div>
  <!-- /.content -->
  <div class="clearfix"></div>
  <script src="{{ url('public/js/toastr.js') }}"></script>
  <script>
    jQuery(document).ready(function(){

      var wipe_unq_data = jQuery('#wipe_unq_data').val();
      if(wipe_unq_data==""){
        jQuery('#singletabopen').click();
      }else{
      jQuery('#multipletabopen').click();
      }
      var url = window.location.href.substr(window.location.href.lastIndexOf("/") + 1).split('#')[1];
      if(url == 'multiple'){
        jQuery('.nav-item a[href="#single"]').removeClass('active');
        jQuery('.nav-item a[href="#multiple"]').addClass('active show');
        jQuery('.nav-item a[href="#multiple"]').attr('aria-selected', 'true');
        jQuery('#single').removeClass('active');
        jQuery('#multiple').addClass('active');
      }

      var set_wipe_date = jQuery('#wipe_date').val();
      jQuery("#edit_wipe_date").datepicker({ 
        format: "mm/dd/yy",
        weekStart: 0,
        calendarWeeks: true,
        autoclose: true,
        todayHighlight: true,
        orientation: "auto"
      }).datepicker('setDate', new Date(set_wipe_date));

      var set_measure_date = jQuery('#measure_date').val();
      jQuery("#edit_measure_date").datepicker({ 
        format: "mm/dd/yy",
        weekStart: 0,
        calendarWeeks: true,
        autoclose: true,
        todayHighlight: true,
        orientation: "auto"
      }).datepicker('setDate', new Date(set_measure_date));

      var set_ssd=jQuery('#date').val();
      jQuery("#date").datepicker({ 
        format: "mm/dd/yy",
        weekStart: 0,
        calendarWeeks: true,
        autoclose: true,
        todayHighlight: true,
        orientation: "auto"
      }).datepicker('setDate', new Date(set_ssd));

    });

    // jQuery(document).ready(function(){
    //   jQuery("input[name='print_job']").click(function(){
    //     var radioValue = jQuery("input[name='print_job']:checked").val();
    //     if(radioValue == 0){
    //       jQuery('#single_wipe_form').attr('target', '_blank');
    //     }else{
    //       jQuery('#single_wipe_form').removeAttr('target');
    //     }
    //   });
    // });

    // jQuery(window).on('load', function(){
    //   var radioValue = jQuery("input[name='print_job']:checked").val();
    //   if(radioValue == 0){
    //     jQuery('#single_wipe_form').attr('target', '_blank');
    //   }else{
    //     jQuery('#single_wipe_form').removeAttr('target');
    //   }
    // });
  </script>


  <script>

    toastr.options = {
      "closeButton": false,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }

    jQuery(document).ready(function(){
//    jQuery('#multipleSelection').trigger('click');


var set_wipe_date = jQuery('#wipe_date_multiple').val();
      jQuery("#edit_wipe_date_multiple").datepicker({ 
        format: "mm/dd/yy",
        weekStart: 0,
        calendarWeeks: true,
        autoclose: true,
        todayHighlight: true,
        orientation: "auto"
      }).datepicker('setDate', new Date(set_wipe_date));

      var set_measure_date = jQuery('#measure_date_multiple').val();
      jQuery("#edit_measure_date_multiple").datepicker({ 
        format: "mm/dd/yy",
        weekStart: 0,
        calendarWeeks: true,
        autoclose: true,
        todayHighlight: true,
        orientation: "auto"
      }).datepicker('setDate', new Date(set_measure_date));

      var set_ssd=jQuery('#date_multiple').val();
      jQuery("#date_multiple").datepicker({ 
        format: "mm/dd/yy",
        weekStart: 0,
        calendarWeeks: true,
        autoclose: true,
        todayHighlight: true,
        orientation: "auto"
      }).datepicker('setDate', new Date(set_ssd));

    jQuery('#submit_wipe_form').on('click', function(){
      jQuery('#single_wipe_form').submit();
    });
});



    jQuery(document).ready(function(){


      var url = location.href;
      if(url.indexOf('#multiple') != -1){
          jQuery('.box-title').text('Add New  Record Wipe');
      }

      jQuery('#singleSelection').click(function(){
        jQuery('.box-title').text('Add New Record Wipe');
      });
      jQuery('#multipleSelection').click(function(){
        jQuery('.box-title').text('Add New  Record Wipe');
      });

      jQuery('.select_source_model_multiple').select2({
        placeholder: 'Enter customer Number',
        ajax: {
          url: '{{ route("source_autocomplete_wipe") }}',
          dataType: 'json',
          delay: 250,
          data: function (params) {
              return {
                  q: jQuery.trim(params.term)
              };
          },
          processResults: function (data) {
              return {
                  results: data
              };
          },
          cache: true
        }
      });
});

      jQuery('.select_source_model_multiple').on('change', function(){
          var source_num = jQuery(".select_source_model_multiple option:selected").text();
          if(source_num !== null){
              fetch_source_data_multiple(source_num);
          }
      });

setTimeout(
  function() 
  {
      jQuery('.select_source_model_multiple').change();
}, 2000);

setTimeout(
   function() 
  {
   jQuery(".fill_wipe_data_m").each(function () {
        var source_id_m = jQuery(this).attr('source_ids');
        var stat_id_s = jQuery(this).attr('state_id');
        
        
        jQuery(".fill_wipe_data_multiple").each(function () {
        var source_id_s = jQuery(this).attr('source_id');
        if(source_id_s==source_id_m){
          jQuery(this).prop('checked', true);
        jQuery(this).parent().parent().find('select').val(stat_id_s);
        }  
        


});
      });
  makeactivesubmit();
}, 4000);

      function fetch_source_data_multiple(query) {
          jQuery.ajax({
              url:"{{ route('source_search_wipe') }}",
              method:'GET',
              data:{query:query, "searchTyp":"multi"},
              dataType:'json',
              success:function(data){
          jQuery('.select_source_model_multiple').val(query);
                if(data.table_data !=""){
                  jQuery('.table_format_multiple').html(data.table_data);
                  jQuery('.table_format_multiple td').css('cssText', 'font-size:12px !important');
                }else{
                  jQuery('.table_format_multiple').html('');
                }
              }
          })
      }


     function makeactivesubmit(){ 
      jQuery('#submit_multiple_wipe').click(function(){
          var sourceNumber = [];
          var machNumber = [];
          var discrepcod_multiple = [];
          

          jQuery("input[name='wipe_select']").each(function () {

            var value = jQuery(this).is(":checked");

            if(value){
               var a = jQuery(this).parent().parent().find('select').val();
               if(a){
                     sourceNumber.push(jQuery(this).siblings().attr('source'));
                     machNumber.push(jQuery(this).siblings().attr('mach'));
                     discrepcod_multiple.push(a);
               }else{
                   toastr.error('please select discrepency');
                   jQuery(this).parent().parent().find('select').focus(); 
               }
            }  
          });         

          

          var technitian = jQuery('#tech_multiple').val();
          //var discrepcod = jQuery('#discrepcod_multiple').val();
          var wipe_date = jQuery('#wipe_date_multiple').val();
          var measure_date = jQuery('#measure_date_multiple').val();
          var measure_by = jQuery('#measure_by_multiple').val();
          var printwipe = jQuery("input[name='print_job_multiple']:checked").val();

          if(sourceNumber.length > 0){
            if(!technitian){
              console.log('e1');
              toastr.error('please select technitian');
              return false;
            }
           
            if(!wipe_date){
              console.log('e11');
              toastr.error('please select wipe date');
              return false;
            }
            if(!measure_date){
              console.log('e1dsds');
              toastr.error('please select measure date');
              return false;
            }
            if(!measure_by){
              console.log('e1w');
              toastr.error('please select measure by');
              return false;
            }
            

            var customerNumber = jQuery('.select_source_model_multiple').val();
            var wipe_unq_data = jQuery('#wipe_unq_data').val();
            //alert(customerNumber);
            jQuery.ajax({
                url:"{{ route('wipes.storeMultiple') }}",
                method:'POST',
                data:{
                  "_token": "{{ csrf_token() }}",
                  "sourceNumber":sourceNumber, 
                  "machineNumber":machNumber,
                  "technitian":technitian,
                  "discrepcod_multiple":discrepcod_multiple,
                  "wipe_date":wipe_date,
                  "measure_date":measure_date,
                  "measure_by":measure_by,
                  "customerNumber":customerNumber,
                  "printwipe":printwipe,
                  "edittype":wipe_unq_data
                },
                dataType:'json',
                success:function(res){
                  if(res.status == "success"){
                    toastr.success("Process is running to save these details in the server side");
                    location.href = "/wipes"
                  }else{
                      toastr.error(res.message);
                  }
                }
            })     
        }else{
          console.log('asd');
          toastr.error('please select at least one reocord from the list');
        }
      });
}
 


  </script>

@endsection