 @forelse($wipes as $key => $wipe) 
    <tr id="wipes-{{ $wipe->id }}">
        <td>{{ $wipe->source_num }}</td> 
        <td>{{ $wipe->mach_num }}</td> 
        <td>{{ $wipe->cust_num }}</td>
        <td>{{ $wipe->customer->customer_contact['contact_name'] ? $wipe->customer->customer_contact['contact_name'] : 'NULL'}}</td>
        <td>{{ @$wipe->tech_name->tech_name }}</td> 
        @foreach($discreps as $discrep)
            @if(!empty($discrep) && $wipe->discrepcod == $discrep->discrepcod) 
                <td>{{$discrep->descriptn}} </td>
            @endif 
        @endforeach
        <td>{{ date('d M Y', strtotime($wipe->wipe_date)) }}</td> 
        
        <td>{{ date('d M Y', strtotime($wipe->meas_date)) }}</td> 
       <td>
       @if($wipe->discrepcod!=8)
            {{ @$wipe->meas_name->meas_name }}
        @else
        {{ @$wipe->tech_name->tech_name }}
        @endif    
        </td>
        </td> 
        <!-- <td> <input type="checkbox" name="confirm" class="form-control confirmWipeTest" source_num="{{ $wipe->source_num }}" mach_number="{{ $wipe->mach_num }}" wipeid="{{$wipe->id}}" id="confirmWipeTest"> </td> -->
       
        <!-- <td>
            <a href="{{ route('sendEmail', $wipe) }}">
                <i class="fa fa-paper-plane edit_btn" rel="tooltip" title="Send Mail"></i>
            </a>
        </td>
        <td>
            <a href="{{ route('pdfView', [$wipe, 'action' => 'envelope']) }}" target="_blank">
                <i class="fa fa-envelope edit_btn" rel="tooltip" title="View Envelope PDF"></i>
            </a>
        </td>
        <td>
            <a href="{{ route('pdfView', [$wipe, 'action' => 'certificate']) }}" target="_blank">
                <i class="fa fa-file-text edit_btn" rel="tooltip" title="View Certificate PDF"></i>
            </a>
        </td> -->
        <td>
            <a href="{{ route('pdfView', [$wipe, 'action' => 'pdfview']) }}" target="_blank">
                <i class="fa fa-file-text edit_btn" rel="tooltip" title="View PDF"></i>
            </a>
        </td>
        <td>
            <a href="{{ route('wipes.edit', $wipe) }}">
                <i class="fa fa-edit edit_btn" rel="tooltip" title="Edit"></i>
            </a>
        </td>
        <td style="text-align: left;">
            <form action="{{ route('wipes.destroy', $wipe) }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit" class="fabutton" onclick="return myFunction();">
                    <i class="fa fa-trash dlt_btn" rel="tooltip" title="Delete"></i>
                </button>
            </form>
        </td>
    </tr>
@empty
    <tr>
      <td colspan="11" class="text-center">
        <p>Record Wipe Not Found.</p>
      </td> 
    </tr>
@endforelse