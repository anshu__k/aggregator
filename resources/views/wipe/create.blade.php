@extends('layout.app')

<link href="{{ url('public/css/toastr.css') }}" rel="stylesheet"/>

@section('content')
<style>
  .wipe_data th{
    font-size: 13px !important;
    font-weight: 700 !important;
  }
  .wipe_data tr, .table_format tr{
    text-align: center;
  }
  .fill_wipe_data td{
    font-size: 8px !important;
  }
  .table_format td{
    padding: 0px !important;
    vertical-align: middle;
  }
  #exampleModalLabel{
    margin-block-end: -25px;
    font-weight: 700;
    font-size: larger;
  }
  .email_demo_font{
    font-family: monospace;
    /*margin-top: 10px;*/
  }

  .table-input{
    max-width: 114px;
    border: 0px;
  }
  .wipelistcombinations{
    font-size: 12px;
  }
</style>
  <!-- Content -->
  <div class="content">
    <!-- Animated -->
    <div class="animated fadeIn">
      <!--  Traffic  -->
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header">
                @if(session()->has('insert'))
                  <div class="alert alert-success alert-dismissible" role="alert">
                    {{ session()->get('insert') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                @endif
                <div class="row">
                  <div class="col-lg-6">
                  <h4 class="box-title">Add New Record Wipe </h4>
                  </div>
                  <div class="col-lg-6">
                    <a href="{{ route('wipes.index') }}" class="btn btn-outline-secondary float-right"><i class="fa fa-angle-double-left"></i> Back</a>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <!-- <ul class="nav nav-tabs" role="tablist">
                 
                  <li class="nav-item" style="width: 10%;">
                    <a class="nav-link" href="#multiple" id="multipleSelection" role="tab" data-toggle="tab" style="text-align: center;" onclick="location.href='#multiple'">Multiple</a>
                  </li>
                </ul> -->

                <div class="tab-content">
                 
<!-- =============================Multiple Selection for the source ================================= -->

                   <div role="tabpanel" class="tab-pane mt-3" id="multiple">
                    <form action="{{ route('wipes.storeMultiple') }}" method="POST" enctype="multipart/form-data" id="single_wipe_form_multiple">
                      {{ csrf_field() }}
                      <div class="row form-group">
                        <div class="col-md-12">
                          <div class="box_border">
                            <div class="">
                              <div class="heading_font_size font-weight-bold">
                                1. Enter or select a customer number if you wish to type in customer. 
                              </div>
                              <div class="form-group ml-5 mt-3">
                                <div class="row">
                                  <div class="col-md-11">
                                    <select class="form-control select_source_model_multiple input_size" name="source_num_multiple" id="source_num_multiple" value="{{ old('source_num') }}"></select>
                                    @if ($errors->has('source_num'))
                                        <div class="text-danger">{{ $errors->first('source_num') }}</div>
                                    @endif
                                  </div>
                                </div>
                              </div>

                              <div class="heading_font_size font-weight-bold">
                                2. Enter or select a default technician for wipe tests. 
                              </div>
                              <div class="form-group ml-5 mt-3">
                                <div class="row">
                                  <div class="col-md-11">
                                    <select class="form-control select_tech_model input_size" name="tech_multiple" id="tech_multiple" value="{{ old('tech') }}"></select>
                                    @if ($errors->has('tech'))
                                        <div class="text-danger">{{ $errors->first('tech') }}</div>
                                    @endif
                                  </div>
                                </div>
                              </div>

                              <!-- <div class="heading_font_size font-weight-bold">
                                3. Enter or select a default discrepency for wipe tests. 
                              </div>
                              <div class="form-group ml-5 mt-3">
                                <div class="row">
                                  <div class="col-md-11">
                                    <select class="form-control input_size" name="discrepcod_multiple" id="discrepcod_multiple" value="{{ old('discrepcod') }}">
                                      <option selected disabled>Select Discrepancies</option>
                                      @foreach($discreps as $discrep)
                                      <option value="{{ $discrep->discrepcod }}" {{ old('discrepcod') == $discrep->discrepcod ? 'selected' : '' }}>{{ $discrep->descriptn }}</option>
                                      @endforeach
                                    </select>
                                    @if ($errors->has('discrepcod'))
                                    <div class="text-danger">{{ $errors->first('discrepcod') }}</div>
                                    @endif
                                  </div>
                                </div>
                              </div> -->
                            </div>

                             <div class=" mt-3">
                              <div class="heading_font_size font-weight-bold">
                                3. Enter wipe date, measure date and measurer. 
                              </div>
                              <div class="form-group ml-5 mt-3">
                                <div class="row">
                                  <div class="col-md-4">
                                    <label class="label_font_size" for="select_customer">Wipe Date :</label>
                                  </div>
                                  <div class="col-md-7">
                                    <div id="add_wipe_date_multiple" class="input-group date" data-date-format="mm-dd-yyyy">
                                      <input type="text" name="wipe_date_multiple" id="wipe_date_multiple" class="form-control input_size" value="{{ old('wipe_date_multiple') }}">
                                      <span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
                                    </div>
                                    @if ($errors->has('wipe_date_multiple'))
                                    <div class="text-danger">{{ $errors->first('wipe_date_multiple') }}</div>
                                    @endif
                                  </div>

                                  <div class="col-md-4">
                                    <label class="label_font_size" for="measure_date_multiple">Measure Date :</label>
                                  </div>
                                  <div class="col-md-7">
                                    <div id="add_measure_date_multiple" class="input-group date" data-date-format="mm-dd-yyyy">
                                      <input type="text" name="measure_date_multiple" id="measure_date_multiple" class="form-control input_size" value="{{ old('measure_date_multiple') }}">
                                      <span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
                                    </div>
                                    @if ($errors->has('measure_date_multiple'))
                                    <div class="text-danger">{{ $errors->first('measure_date_multiple') }}</div>
                                    @endif
                                  </div>

                                  <div class="col-md-4">
                                    <label class="label_font_size" for="measure_by_multiple">Measure By :</label>
                                  </div>
                                  <div class="col-md-7">
                                    <select class="form-control input_size" name="measure_by_multiple" id="measure_by_multiple" value="{{ old('measure_by_multiple') }}">
                                      <option selected disabled>Select Measure by</option>
                                      @foreach($measurers as $measurer)
                                        <option value="{{ $measurer->meas_by }}">{{ $measurer->meas_name }}</option>
                                      @endforeach
                                    </select>
                                    @if ($errors->has('measure_by_multiple'))
                                    <div class="text-danger">{{ $errors->first('measure_by_multiple') }}</div>
                                    @endif
                                  </div>
                                  <!-- <div class="col-md-8 mt-3">
                                    <label class="label_font_size" for="measure_date">Do You Want To Print The Wipe Certificate. :</label>
                                  </div>
                                  <div class="col-md-4 mt-3">
                                    <input type="radio" name="print_job_multiple" value="0"> Yes &nbsp;&nbsp;
                                    <input type="radio" name="print_job_multiple" value="1"> No 
                                  </div> -->
                                </div>
                              </div>
                            </div>

                            <div class="mt-3">
                              <div class="heading_font_size font-weight-bold">
                                4. Select an item where you wish to enter wipe test. 
                              </div>
                              <div class="form-group mt-3">
                                <div class="row">
                                  <div class="col-md-12">
                                    <table class="table table-bordered table-responsive">
                                      <thead>
                                        <tr class="wipe_data">
                                          
                                          <th>#</th>
                                          <th>Machine Number</th>
                                          <th>Source Number</th>
                                          <!-- <th>Customer Number</th> -->
                                          <th>Source Status</th>
                                          <th>Source Modal</th>
                                          <th>Inst Date</th>
                                          <th>Ship Date</th>
                                          <th>Technician</th>
                                          <th>Select</th>

                                        </tr>
                                      </thead>
                                      <tbody class="table_format_multiple">
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="box_border">
                            <!-- <div class="box_border">
                              <div class="heading_font_size font-weight-bold">
                                5. List of all selected combinations: 
                              </div>
                              
                              <table class="table table-bordered wipelistcombinations">
                                <tr>
                                    <th>S. No.</th>
                                    <th>Source Number</th>
                                    <th>Machine Number</th>
                                    <th>Action</th>
                                </tr>
                                
                                
                              </table>
                             
                            </div> -->

                           
                          </div>
                        </div>
                      </div>
                    </form>
                     <button id="submit_multiple_wipe" type="button" class="btn btn-outline-primary float-right"><i class="fa fa-save"></i> Save</button>
                  </div>
<!-- =========================== End Multiple Selection for the source =============================== -->

                </div>
              </div>
              <div class="card-body"></div>
            </div>
          </div><!-- /# column -->
        </div>
      <!--  /Traffic -->
      <div class="clearfix"></div>
    </div>
    <!-- .animated -->
  </div>
  <!-- /.content -->
  <div class="clearfix"></div>
  <input type="hidden" class="customer-number">
  
<script src="{{ url('public/js/toastr.js') }}"></script>
  <script>

    toastr.options = {
      "closeButton": false,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }

    jQuery(document).ready(function(){
		jQuery('#multipleSelection').trigger('click');

      var url = 'multiple';//window.location.href.substr(window.location.href.lastIndexOf("/") + 1).split('#')[1];
      if(url == 'multiple'){
        jQuery('.nav-item a[href="#single"]').removeClass('active');
        jQuery('.nav-item a[href="#multiple"]').addClass('active show');
        jQuery('.nav-item a[href="#multiple"]').attr('aria-selected', 'true');
        jQuery('#single').removeClass('active');
        jQuery('#multiple').addClass('active');
      }

      jQuery("#add_wipe_date").datepicker({ 
        format: "mm/dd/yy",
        weekStart: 0,
        calendarWeeks: true,
        autoclose: true,
        todayHighlight: true,
        orientation: "auto"
      }).datepicker('update', new Date());

      jQuery("#add_measure_date").datepicker({ 
        format: "mm/dd/yy",
        weekStart: 0,
        calendarWeeks: true,
        autoclose: true,
        todayHighlight: true,
        orientation: "auto"
      }).datepicker('update', new Date());


      jQuery("#add_wipe_date_multiple").datepicker({ 
        format: "mm/dd/yy",
        weekStart: 0,
        calendarWeeks: true,
        autoclose: true,
        todayHighlight: true,
        orientation: "auto"
      }).datepicker('update', new Date());

      jQuery("#add_measure_date_multiple").datepicker({ 
        format: "mm/dd/yy",
        weekStart: 0,
        calendarWeeks: true,
        autoclose: true,
        todayHighlight: true,
        orientation: "auto"
      }).datepicker('update', new Date());
    });

    jQuery('#submit_wipe_form').on('click', function(){
      jQuery('#single_wipe_form').submit();
    });




    jQuery(document).ready(function(){
      var url = location.href;
      if(url.indexOf('#multiple') != -1){
          jQuery('.box-title').text('Add New  Record Wipe');
      }

      jQuery('#singleSelection').click(function(){
        jQuery('.box-title').text('Add New Record Wipe');
      });
      jQuery('#multipleSelection').click(function(){
        jQuery('.box-title').text('Add New  Record Wipe');
      });

      jQuery('.select_source_model_multiple').select2({
        placeholder: 'Enter customer Number',
        ajax: {
          url: '{{ route("source_autocomplete_wipe") }}',
          dataType: 'json',
          delay: 250,
          data: function (params) {
              return {
                  q: jQuery.trim(params.term)
              };
          },
          processResults: function (data) {
              return {
                  results: data
              };
          },
          cache: true
        }
      });


      jQuery('.select_source_model_multiple').on('change', function(){
          var source_num = jQuery(".select_source_model_multiple option:selected").text();
          if(source_num !== null){
              fetch_source_data_multiple(source_num);
          }
      });

      function fetch_source_data_multiple(query) {
          jQuery.ajax({
              url:"{{ route('source_search_wipe') }}",
              method:'GET',
              data:{query:query, "searchTyp":"multi"},
              dataType:'json',
              success:function(data){
				  jQuery('.customer-number').val(query);
                if(data.table_data !=""){
                  jQuery('.table_format_multiple').html(data.table_data);
                  jQuery('.table_format_multiple td').css('cssText', 'font-size:12px !important');
                }else{
                  jQuery('.table_format_multiple').html('');
                }
              }
          })
      }

      var snum = 0;
      jQuery(document).on("click", '.fill_wipe_data_multiple', function(){
        var source_id = jQuery(this).attr('source_id');
        jQuery.ajax({
            url:"{{ route('wipe_data_autofill') }}",
            method:'GET',
            data:{source_id:source_id},
            dataType:'json',
            success:function(data){
              var row = "";
              row = "<tr>";
              snum++;
              row += "<td>"+snum+"</td><td><input type='text' class='table-input tableVal' name='source_multiple[]' value='" + data.source_data.source_num + "' readonly /><input type='hidden' name='customer_multiple[]' value='" + data.source_data.customer_number + "' readonly /></td><td><input class='table-input' type='text' name='mach_multiple[]' value='"+ data.source_data.mach_num + "' readonly /></td>><td><i class='fa fa-trash dlt_btn dlt_btn_multiple' rel='tooltip' title='' data-original-title='Remove'></i></td></tr>";
              jQuery(".wipelistcombinations").append(row);
              jQuery("[rel=tooltip]").tooltip({ placement: 'top'});
              jQuery('.dlt_btn_multiple').click(function(){
                if(confirm('Are you sure you want to this combination remove?')){
                 jQuery(this).parent().parent().remove();
                }
              });              
            },
            error:function(){
              alert('Something went wrong');
            }
        });
    });


      jQuery('#submit_multiple_wipe').click(function(){
          var sourceNumber = [];
          var machNumber = [];
          var discrepcod_multiple = [];
          

          jQuery("input[name='wipe_select']").each(function () {
            var value = jQuery(this).is(":checked");
            if(value){
               var a = jQuery(this).parent().parent().find('select').val();
               if(a){
                     sourceNumber.push(jQuery(this).siblings().attr('source'));
                     machNumber.push(jQuery(this).siblings().attr('mach'));
                     discrepcod_multiple.push(a);
               }else{
                   toastr.error('please select discrepency');
                   jQuery(this).parent().parent().find('select').focus(); 
               }
            }  
          });         

          //console.log(sourceNumber);

          var technitian = jQuery('#tech_multiple').val();
          //var discrepcod = jQuery('#discrepcod_multiple').val();
          var wipe_date = jQuery('#wipe_date_multiple').val();
          var measure_date = jQuery('#measure_date_multiple').val();
          var measure_by = jQuery('#measure_by_multiple').val();
          var printwipe = jQuery("input[name='print_job_multiple']:checked").val();

          if(sourceNumber.length > 0){
            if(!technitian){
              toastr.error('please select technitian');
              return false;
            }
           
            if(!wipe_date){
              toastr.error('please select wipe date');
              return false;
            }
            if(!measure_date){
              toastr.error('please select measure date');
              return false;
            }
            if(!measure_by){
              toastr.error('please select measure by');
              return false;
            }
            

            var customerNumber = jQuery('.customer-number').val();

            jQuery.ajax({
                url:"{{ route('wipes.storeMultiple') }}",
                method:'POST',
                data:{
                  "_token": "{{ csrf_token() }}",
                  "sourceNumber":sourceNumber, 
                  "machineNumber":machNumber,
                  "technitian":technitian,
                  "discrepcod_multiple":discrepcod_multiple,
                  "wipe_date":wipe_date,
                  "measure_date":measure_date,
                  "measure_by":measure_by,
                  "customerNumber":customerNumber,
                  "printwipe":printwipe
                },
                dataType:'json',
                success:function(res){
                  if(res.status == "success"){
                    toastr.success("Process is running to save these details in the server side");
                    location.href = "/wipes"
                  }else{
                      toastr.error(res.message);
                  }
                }
            })     
        }else{
          toastr.error('please select at least one reocord from the list');
        }
      });
  });



  </script>
@endsection