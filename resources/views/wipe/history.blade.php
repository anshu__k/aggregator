@extends('layout.app')

@section('content')
    <link rel="stylesheet" href="{{ url('public/css/loader.css') }}">
    <style type="text/css">
        .confirmWipeTest{
            height: calc(1.25rem + 2px);
        }   
    </style>
	<!-- Content -->
     <div class="loader hideLoader" ></div>
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
        	<!--  Traffic  -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <!-- Error & Message -->
                                @if (session()->has('insert'))
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        {{ session()->get('insert') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @elseif (session()->has('update'))
                                    <div class="alert alert-info alert-dismissible" role="alert">
                                        {{ session()->get('update') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @elseif (session()->has('delete'))
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        {{ session()->get('delete') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @elseif (session()->has('sendMail'))
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        {{ session()->get('sendMail') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                            <!-- Error & Message -->
                            <div class="row form-group">
                                <div class="col-md-4">
                                    <h4 class="box-title">Record Wipe List History </h4>
                                </div>

                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input class="form-control"  name="search_term" id="search_wipe_term" placeholder="Search..">
                                        <div class="input-group-btn">
                                            <button type="button" id="search_wipe" class="btn btn-outline-primary" >Search</button>
                                        </div>

                                        <a class="btn input-group-btn"href="{{ route('history') }}" >Reset</a>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <!-- <a href="{{ route('wipes.create') }}" class="btn btn-outline-success float-right"><i class="fa fa-plus"></i> ADD</a> -->
                                </div>
                            </div>
                        </div>
                        <div class="card-body nopadding">
                            <div class="col-md-12 table-stats order-table ov-h nopadding">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <!-- <th>#</th> -->
                                            <th>Source Number</th>
                                            <th>Machine Number</th>
                                            <th>Customer Number</th>
                                            <th>Contact Name</th>
                                            <th>Technician</th>
                                            <th>Discrepcod</th>
                                            <th>Wipe Date</th>
                                            <th>Meas Date</th>
                                            <th>Meas By</th>
                                            <!-- <th>Confirm</th> -->
                                            
                                            <th colspan="7" style="text-align: left;">Action</th>
                                        </tr>
                                    </thead>

                                    <tbody class="table_format">
                                        @forelse($wipes as $key => $wipe) 
                                            <tr>
                                                <!-- <td>{{ $wipes->firstItem() + $key }}</td>  -->
                                                <td>{{ $wipe->source_num }}</td> 
                                                <td>{{ $wipe->mach_num }}</td> 
                                                <td>{{ $wipe->cust_num }}</td>
                                                <td>{{ $wipe->customer->customer_contact['contact_name'] ? $wipe->customer->customer_contact['contact_name'] : 'NULL'}}</td>
                                                <td>{{ @$wipe->tech_name->tech_name }}</td> 
                                                @foreach($discreps as $discrep)
                                                    @if(!empty($discrep) && $wipe->discrepcod == $discrep->discrepcod) 
                                                        <td>{{$discrep->descriptn}} </td>
                                                    @endif 
                                                @endforeach
                                                <td>{{ date('m/d/y', strtotime($wipe->wipe_date)) }}</td> 
                                                
                                                <td>{{ date('m/d/y', strtotime($wipe->meas_date)) }}</td> 
                                                <td>
                                                    {{ @$wipe->user->title }} {{ @$wipe->user->name }}
                                                </td>
                                               <!--  <td> <input type="checkbox" name="confirm" class="form-control confirmWipeTest" id="confirmWipeTest"> </td>
                                                -->
                                                <!-- <td>
                                                    <a href="{{ route('sendEmail', $wipe) }}">
                                                        <i class="fa fa-paper-plane edit_btn" rel="tooltip" title="Send Mail"></i>
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="{{ route('pdfView', [$wipe, 'action' => 'envelope']) }}" target="_blank">
                                                        <i class="fa fa-envelope edit_btn" rel="tooltip" title="View Envelope PDF"></i>
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="{{ route('pdfView', [$wipe, 'action' => 'certificate']) }}" target="_blank">
                                                        <i class="fa fa-file-text edit_btn" rel="tooltip" title="View Certificate PDF"></i>
                                                    </a>
                                                </td> -->
                                                <td>
                                                    <a href="{{ route('pdfView', [$wipe, 'action' => 'pdfview']) }}" target="_blank">
                                                        <i class="fa fa-file-text edit_btn" rel="tooltip" title="View PDF"></i>
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="{{ route('wipes.edit', $wipe) }}">
                                                        <i class="fa fa-edit edit_btn" rel="tooltip" title="Edit"></i>
                                                    </a>
                                                </td>
                                                <td style="text-align: left;">
                                                    <form action="{{ route('wipes.destroy', $wipe) }}" method="POST">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <button type="submit" class="fabutton" onclick="return myFunction();">
                                                            <i class="fa fa-trash dlt_btn" rel="tooltip" title="Delete"></i>
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                              <td colspan="11" class="text-center">
                                                <p>Record Wipe Not Found.</p>
                                              </td> 
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div> <!-- /.col-md-12 -->
                        </div><!-- card-body -->
                        <div class="card-body">
                            {{ $wipes->onEachSide(1)->links() }}
                        </div>
                        <div class="card-body"></div>
                    </div>
                </div><!-- /# column -->
            </div>
            <!--  /Traffic -->
            <div class="clearfix"></div>
        </div>
    	<!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>
    <!-- ========================== Modal pop-up=====================-->
    <div class="modal fade" id="confirmWipe" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Confirmation alert</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <table>
                            <tr></tr>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="confirmYes" class="btn btn-success">Yes</button>
                    <button type="button" id="confirmNo" class="btn btn-danger">No</button>
                </div>
            </div>      
        </div>
    </div>

    <script type="text/javascript">
        jQuery(document).ready(function(){
            
           jQuery('#search_wipe').click(function(){
                var search_term = jQuery('#search_wipe_term').val();
                var searchString = jQuery.trim( search_term );
                if(searchString){
                    jQuery('.loader').removeClass('hideLoader')
                    fetch_wipe_data(searchString);
                }else{
                    alert('Please search valid content');
                }
            });

            function fetch_wipe_data(query){
                jQuery.ajax({
                    url:"{{ route('wipes_search') }}",
                    method:'GET',
                    data:{"query":query, "conf" : 1},
                    dataType:'json',
                    success:function(data){
                        jQuery('tbody').html(data.html);
                        jQuery('.loader').addClass('hideLoader');
                    },
                    error: function(){
                        jQuery('.loader').addClass('hideLoader');
                        alert('Something went to wrong.');
                    }
                });
            }

        });
    </script>
@endsection