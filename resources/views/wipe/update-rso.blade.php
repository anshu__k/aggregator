@extends('layout.app')

<link href="{{ url('public/css/toastr.css') }}" rel="stylesheet"/>

@section('content')
<style>
  .wipe_data th{
    font-size: 9px !important;
    font-weight: 700 !important;
    padding: 0px !important;
    text-align: center;
  }
  .wipe_data tr, .table_format tr{
    text-align: center;
  }
  .fill_wipe_data td{
    font-size: 8px !important;
  }
  .table_format td{
    padding: 0px !important;
    vertical-align: middle;
  }
  #exampleModalLabel{
    margin-block-end: -25px;
    font-weight: 700;
    font-size: larger;
  }
  .email_demo_font{
    font-family: monospace;
    /*margin-top: 10px;*/
  }

  .table-input{
    max-width: 114px;
    border: 0px;
  }
  .wipelistcombinations{
    font-size: 12px;
  }
</style>
  <!-- Content -->
  <div class="content">
    @if ($message = Session::get('success'))
      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
      </div>
    @endif

    @if ($message = Session::get('error'))
      <div class="alert alert-error alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
      </div>
    @endif
    <!-- Animated -->
    <div class="animated fadeIn">
      <!--  Traffic  -->
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header">
                @if(session()->has('insert'))
                  <div class="alert alert-success alert-dismissible" role="alert">
                    {{ session()->get('insert') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                @endif
                <div class="row">
                  <div class="col-lg-6">
                    <h4 class="box-title">Update Radiation Safety Officer (RSO) </h4>
                  </div>
                  
                </div>
              </div>
              <div class="card-body">
                <div class="row">
                     <form action="{{ route('update-RSO') }}" method="POST" style="width: 100%;" >
                      {{ csrf_field() }}
                    <div class="col-lg-6">
                      <input type="text" name="name" class="form-control" value="{{$rso->name}}" />
                    </div>
                    <div class="col-lg-6">
                      <input type="submit" name="Save" class="form-control btn btn-success" style="margin-top: 20px;" />
                    </div>
                  </form>
                </div>
              </div>
              <div class="card-body"></div>
            </div>
          </div><!-- /# column -->
        </div>
      <!--  /Traffic -->
      <div class="clearfix"></div>
    </div>
    <!-- .animated -->
  </div>
  <!-- /.content -->
  <div class="clearfix"></div>
@endsection