<!DOCTYPE html>
<html lang="en">
<head>
	<title>Wipe Test</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<style>
		.set_content{
			margin-left: 50px;
			padding-top: 100px;
			margin-right: 50px;
			font-family: monospace;
			font-weight: 600;
		}
		.faithfully p{
			margin-bottom: 0px;
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="set_content">
			<div class="row">
				<p style="margin-top: 40px;">Dear Mr.{{ $customer_name }}:</p>

				<p style="margin-top: 20px;">Attached is the wipe test and source inspection certificate on the radioisotope sources used in your FILTEC equipment. Please file it for future reference.</p>

				<p style="margin-top: 20px;">Refer to the wipe test certificate for your next wipe test due date.</p>

				<div class="faithfully" style="padding-bottom: 40px; float: right;">
					@if(!empty($signature))
					<img src="{{ url('public/signature/'.$signature) }}" style="width: 70px; height: auto;">
					@endif
					<p>Thank You.</p>
				</div>
			</div>
		</div>
	</div>
</body>
</html>