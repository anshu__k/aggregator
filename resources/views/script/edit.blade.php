@extends('layout.app')

@section('content')
	<!-- Content -->
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
        	<!--  Traffic  -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                        	<div class="row">
                        		<div class="col-lg-6">
                    				<h4 class="box-title">Edit Script Log </h4>
                        		</div>
                        		<div class="col-lg-6">
                        			<a href="{{ route('script.list') }}" class="btn btn-outline-secondary float-right"><i class="fa fa-angle-double-left"></i> Back</a>
                        		</div>
                        	</div>
                        </div>
                        <div class="card-body">
                        	<form action="{{ route('script.update',$source) }}" method="POST" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <input type="hidden" name="_method" value="PUT">
                                <div class="row form-group">

                                    
                                  	<div class="col-md-4">
                                        <label class="label_font_size" for="low_source_no">Name :</label>
                                      
                                                <input type="text" name="name" id="name" class="form-control input_size" value="{{ $source->name }}"> 
                                                @if ($errors->has('name'))
                                                    <div class="text-danger">{{ $errors->first('name') }}</div>
                                                @endif
                                           
                                           
                                    </div>

                                    <div class="col-md-4">
                                        <label class="label_font_size" for="low_source_no">File :</label>
                                      
                                            <input type="file" name="file_name" id="file_name" class="form-control input_size" value=""> 
                                                @if ($errors->has('file_name'))
                                                    <div class="text-danger">{{ $errors->first('file_name') }}</div>
                                                @endif
                                           <input type="hidden" name="old_file" value="{{ $source->file_name }}">
                                           
                                    </div>
                                  	
                                </div>



                                
                                <button type="submit" class="btn btn-outline-primary float-right"><i class="fa fa-save"></i> Save</button>
                        	</form>
                        </div>
                        <div class="card-body"></div>
                    </div>
                </div><!-- /# column -->
            </div>
            <!--  /Traffic -->
            <div class="clearfix"></div>
        </div>
    	<!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>
    <script>
    jQuery(function() {
        jQuery("#add_load_date").datepicker({ 
            format: "mm-dd-yy",
            weekStart: 0,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            orientation: "auto"
        });
    });    
    </script>
@endsection