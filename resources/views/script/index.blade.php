@extends('layout.app')

@section('content')

	<!-- Content -->
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
        	<!--  Traffic  -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <!-- Error & Message -->
                                @if (session()->has('insert'))
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        {{ session()->get('insert') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @elseif (session()->has('update'))
                                    <div class="alert alert-info alert-dismissible" role="alert">
                                        {{ session()->get('update') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @elseif (session()->has('delete'))
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        {{ session()->get('delete') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                            <!-- Error & Message -->
                            <div class="row form-group">
                                <div class="col-md-9">
                                    <h4 class="box-title">Script Log List </h4>
                                </div>

                                <div class="col-md-3">
                                    <a href="{{ route('script.create') }}" class="btn btn-outline-success float-right"><i class="fa fa-plus"></i> ADD</a>
                                </div>
                            </div>    
                            
                            <div class="row form-group">    
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input class="form-control"  name="search_term" id="search_source_term" placeholder="Search">
                                    </div>
                                </div>
                                   
                                <div class="col-md-3">    

                                        <div class="input-group-btn">
                                            <button type="button" id="search_source" class="btn btn-outline-primary" >Search</button>
                                        

                                        <a class="btn input-group-btn"href="{{ route('script.index') }}" >Reset</a></div>
                                </div>
                            </div>

                                
                        <div class="card-body nopadding" >
                            <div id="searchid" class="col-md-12 table-stats order-table nopadding">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <!-- <th>#</th> -->
                                            <th>Name</th>
                                            <th>File name</th>
                                            <th>type</th>
                                            <th>Upload Date</th>
                                            <th colspan="2" style="text-align: center;">Action</th>
                                        </tr>
                                    </thead>

                                    <tbody class="table_format">
                                        @forelse($sources as $key => $source)
                                            <tr>
                                                <td>{{ $source->name }}</td>
                                                <td><a href="{{asset('public/uploads/'.$source->file_name)}}">{{ $source->file_name }}</a></td>
                                                <td>{{ $source->type }}</td>
                                                <td>{{ !empty($source->created_at)?date('m/d/y', strtotime($source->created_at)):'-' }}</td>
                                                <td style=" text-align: right;">
                                                    <a href="{{ route('script.edit', $source->id) }}">
                                                        <i class="fa fa-edit edit_btn" rel="tooltip" title="Edit"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                              <td colspan="5" class="text-center">
                                                <p>Source Not Found.</p>
                                              </td> 
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div> <!-- /.col-md-12 -->
                        </div><!-- card-body -->
                        <div class="card-body page_hide">
                            {{ $sources->onEachSide(1)->links() }}
                        </div>
                        <div class="card-body"></div>
                    </div>
                </div><!-- /# column -->
            </div>
            <!--  /Traffic -->
            <div class="clearfix"></div>
        </div>
    	<!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>
<!-- Search Source -->
    <script>
        jQuery(document).ready(function(){

            function fetch_source_data(query,status,page)
            {

                // alert(query);
                jQuery('#search_source').text('Searching...');
                jQuery('#search_source').prop('disabled',true);    
                
                jQuery.ajax({
                    url:"{{ route('search_source') }}",
                    method:'GET',
                    data:{query:query,status:status,page:page},
                    dataType:'html',
                    success:function(data)
                    {
                        jQuery('#searchid').html(data);
                        jQuery('.page_hide').css('cssText', 'display:none');
                        jQuery('#search_source').text('Search');
                        jQuery('#search_source').prop('disabled',false);
                        // jQuery('.pagination').css("display:none;");
                        // $('#total_records').text(data.total_data);
                    }
                })
            }

            jQuery(document).on('click', '#search_source', function(){
                var query = jQuery('#search_source_term').val();
                var status = jQuery('#status').val();

                    if (query != '' || null !=status) {
                        fetch_source_data(query,status,1);
                    
                    }
                });

            jQuery(document).on('click', '.pagination a', function(event){
                      

                      var query = jQuery('#search_source_term').val();
                      var status = jQuery('#status').val();

                         if (query != ''|| null !=status) {
                            event.preventDefault(); 
                      var page = jQuery(this).attr('href').split('page=')[1];
                      
                         fetch_source_data(query,status,page);

                       }  
            });

            });

            jQuery('#search_source_term').keypress(function (e) {
            var key = e.which;
            if(key == 13)  // the enter key code
            {
                jQuery('#search_source').click();
                return false;  
            }
        });  
    </script>
@endsection