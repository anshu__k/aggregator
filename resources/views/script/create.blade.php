@extends('layout.app')

@section('content')
	<!-- Content -->
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
        	<!--  Traffic  -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                        	<div class="row">
                        		<div class="col-lg-6">
                    				<h4 class="box-title">Add Script File </h4>
                        		</div>
                        		<div class="col-lg-6">
                        			<a href="{{ route('script.list') }}" class="btn btn-outline-secondary float-right"><i class="fa fa-angle-double-left"></i> Back</a>
                        		</div>
                        	</div>
                        </div>
                        <div class="card-body">
                        	<form action="{{ route('script.store') }}" method="POST" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="row form-group">

                                    <div class="col-md-4">
                                        <label class="label_font_size" for="low_source_no">Name :</label>
                                      
                                                <input type="text" name="name" id="name" class="form-control input_size" value="{{ old('name') }}"> 
                                                @if ($errors->has('name'))
                                                    <div class="text-danger">{{ $errors->first('name') }}</div>
                                                @endif
                                           
                                           
                                    </div>

                                    <div class="col-md-4">
                                    	<label class="label_font_size" for="low_source_no">File :</label>
                                      
                                            <input type="file" name="file_name" id="file_name" class="form-control input_size" value="{{ old('file_name') }}"> 
                                            	@if ($errors->has('file_name'))
                                              		<div class="text-danger">{{ $errors->first('file_name') }}</div>
                                            	@endif
                                           
                                           
                                  	</div>
                                  	
                                  	
			                    </div>

                                <div class="row form-group">
                                    <!-- <div class="col-md-6">
                                        <label class="label_font_size" for="xray_no">X-ray Number :</label>
                                        <input type="text" name="xray_num" id="xray_num" class="form-control input_size" value="{{ old('xray_num') }}">
                                        @if ($errors->has('xray_num'))
                                            <div class="text-danger">{{ $errors->first('xray_num') }}</div>
                                        @endif
                                    </div> -->
                                    <!-- <div class="col-md-6">
                                        <label class="label_font_size" for="milicurie">Milicurie :</label>
                                        <input type="text" name="milicurie" id="milicurie" class="form-control input_size" value="{{ old('milicurie') }}">
                                        @if ($errors->has('milicurie'))
                                            <div class="text-danger">{{ $errors->first('milicurie') }}</div>
                                        @endif
                                    </div> -->
                                </div>

                                <!-- <div class="row form-group">
                                    <div class="col-md-6">
                                        <label class="label_font_size" for="source_no_ext">Source Number Extension :</label>
                                        <select class="form-control input_size" name="source_no_ext" id="source_no_ext" value="{{ old('source_no_ext') }}" required>
                                            <option selected disabled> Select source number extension </option>
                                        </select>
                                        @if ($errors->has('source_no_ext'))
                                            <div class="text-danger">{{ $errors->first('source_no_ext') }}</div>
                                        @endif
                                    </div>
                                    <div class="col-md-6">
                                        <label class="label_font_size" for="received_date">Received Date :</label>
                                        <div id="add_received_date" class="input-group date" data-date-format="mm-dd-yyyy">
                                            <input type="text" name="received_date" id="received_date" class="form-control input_size" value="{{ old('received_date') }}">
                                            <span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
                                        </div>
                                        @if ($errors->has('received_date'))
                                        <div class="text-danger">{{ $errors->first('received_date') }}</div>
                                        @endif
                                    </div>
                                </div> -->

                                <button type="submit" class="btn btn-outline-primary float-right"><i class="fa fa-save"></i> Save</button>
                        	</form>
                        </div>
                        <div class="card-body"></div>
                    </div>
                </div><!-- /# column -->
            </div>
            <!--  /Traffic -->
            <div class="clearfix"></div>
        </div>
    	<!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>
    <script>
        jQuery("#add_load_date").datepicker({ 
            format: "yyyy-mm-dd",
            weekStart: 0,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            orientation: "auto"
        }).datepicker('update', new Date());

        jQuery("#add_received_date").datepicker({ 
            format: "mm/dd/yy",
            weekStart: 0,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            orientation: "auto"
        }).datepicker('update', new Date());
    </script>
@endsection