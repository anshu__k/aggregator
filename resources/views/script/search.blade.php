<table class="table">
                                    <thead>
                                        <tr>
                                            <!-- <th>#</th> -->
                                            <th>Source Number</th>
                                            <th>Source Model</th>
                                            <th>Activity</th>
                                            <th>machine Number</th>
                                            <th>Load Date</th>
                                            <th>Updated Date</th>
                                            <th>Status</th>
                                            <th colspan="2" style="text-align: center;">Action</th>
                                        </tr>
                                    </thead>

                                    <tbody class="table_format">
                                        @forelse($sources as $key => $source)
                                            <tr>
                                                <!-- <td>{{ $sources->firstItem() + $key }}</td>  -->
                                                <!-- <td>{{ $source->source_num }}</td>  -->
                                                <td>
                                                   <a class=""  rel="tooltip" title="View" href="{{ url('sources/log_history', $source->id) }}">
                                                    <u>{{$source->source_num}}</u>
                                                    </a>
                                                </td>
                                                <td>{{ $source->desc }}</td>
                                                <td>{{ $source->source_model }}</td>
                                                <td>{{ $source->mach_num }}</td> 
                                                <td>{{ date('d M Y', strtotime($source->load_date)) }}</td> 
                                                <td>{{ !empty($source->updated_at)?date('d M Y', strtotime($source->updated_at)):'-' }}</td>
                                                <td @if($source->status == 1)class="text-success" @else class="text-danger" @endif>{{ $source->status==1 ? 'Active' : 'Inactive' }}</td> 
                                                <td style=" text-align: right;">
                                                    <a href="{{ route('sources.edit', $source->id) }}">
                                                        <i class="fa fa-edit edit_btn" rel="tooltip" title="Edit"></i>
                                                    </a>
                                                </td>
                                                <!-- <td style="padding-left:0px; text-align: left;">
                                                    <form action="{{ route('sources.destroy', $source->id) }}" method="POST">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <button type="submit" class="fabutton" onclick="return myFunction();">
                                                            <i class="fa fa-trash dlt_btn" rel="tooltip" title="Delete"></i>
                                                        </button>
                                                    </form>
                                                </td> -->
                                            </tr>
                                        @empty
                                            <tr>
                                              <td colspan="5" class="text-center">
                                                <p>Source Not Found.</p>
                                              </td> 
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>

                                <div class="card-body">
                            {{ $sources->onEachSide(1)->links() }}
                        </div>