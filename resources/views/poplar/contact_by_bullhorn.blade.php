@extends('layout.no_header_footer_app')

@section('content')
<!-- Content -->
<div class="content">
    <!-- Animated -->
    <div class="animated fadeIn">
        <!--  Traffic  -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header request_data">
                        <div class="row form-group">
                            <div class="col-md-9">
                                <h4 class="box-title" >Requst Data</h4>
                                @php 
                                    print_r($request_data);
                                @endphp
                            </div>
                        </div>
                    </div> 
                    <div class="card-header">
                        <div class="row form-group">
                            <div class="col-md-9">
                                <h4 class="box-title">Poplar Campaign List</h4>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered maintable" id="campainTable">
                                <thead class="table-dark">
                                    <tr>
                                        <th class="text-center align-middle w-30" scope="col">Campaign ID</th>
                                        <th class="text-center align-middle w-50" scope="col">Campaign Name</th>
                                        <th class="text-center align-middle w-20" scope="col">Action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @if(!empty($campaign_lists))
                                        @php 
                                            $script_records_postCard_sent = $script_record->script_record_sent_postcard;
                                            $contacts_sent_campaign_lists = $script_records_postCard_sent->pluck('campaign_id');
                                            $postcardSentData = $script_records_postCard_sent->groupBy('campaign_id');
                                            // dd($postcardSentData);
                                        @endphp
                                        @foreach($campaign_lists as $key=>$val)
                                        <tr style="line-height: 25px">
                                            <td class="text-center align-middle"> {{ $val->campaign_id }} </td>
                                            <td class="text-center align-middle"> {{ $val->campaign_name }} </td>
                                            <td class="text-center align-middle">
                                                @if(in_array($val->campaign_id, $contacts_sent_campaign_lists->toArray()))
                                                    <button class="btn btn-success disabled" style="cursor: pointer;" title="Postcard Sent at: {{ date('m/d/Y H:i A', strtotime($postcardSentData[$val->campaign_id][0]['created_at'])) }}">Postcard Sent</button>
                                                @else 
                                                    <button class="btn btn-dark blh_poplar__syncData" data-poplar_campaign_id="{{ $val->campaign_id }}" data-script_record_id="{{ $script_record->id }}" data-script_record_bullhorn_contact_id="{{ $script_record->bullhorn_contact_id }}">Send Postcard</button>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    @else
                                    <tr style="line-height: 25px;">
                                        <td class="text-center" colspan="3"> No Matching Data Found for Bullhorn Contact</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!-- /# column -->
            </div>
            <!--  /Traffic -->
            <div class="clearfix"></div>
        </div>
        <!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>
    <!-- Search Source -->


    @endsection


    @push('styles')

    @endpush

    @push('script')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        jQuery(document).on('click', '.blh_poplar__syncData', function() {
            var clickedElem = jQuery(this);
            let bullhornContactid = "{{ $bullhorn_id }}";
            script_contact_ids = clickedElem.attr('data-script_record_id');
            poplar_campaign = clickedElem.attr('data-poplar_campaign_id');
            
            const swalWithBootstrapButtons = Swal.mixin({
                // customClass: {
                //     confirmButton: 'btn btn-success',
                //     cancelButton: 'btn btn-danger'
                // },
                // buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Are you sure to send postcard?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, send it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    // Add Spinner to the sync button
                    clickedElem.attr('disabled', true).append('<i class="fa fa-spinner fa-spin"></i>');
                    jQuery.ajax({
                        type: 'POST',
                        url: "{{ route('poplar.bullhorn.send_postcard_contact', $bullhorn_id) }}",
                        cache: false,
                        dataType: 'json',
                        data: {
                            '_token': '{{ csrf_token() }}',
                            'script_contact_ids': script_contact_ids,
                            'poplar_campaign': poplar_campaign,
                            // 'bullHornData': parseBullHornData,
                            // 'eventType': eventType,
                            // 'openDate': openDate ?? "",
                            // // 'localData': parseLocalDbData,
                            // 'mailchimpSubjectData': parseMailchimpDbData,
                            // 'campainSentDate': parseCampainSentDate,
                            // 'dataMemberid': dataMemberid ?? ""

                        },
                        success: function(response) {
                            if (response.success) {
                                swalWithBootstrapButtons.fire(
                                    'Synced!',
                                    'Your postcard has been sent.',
                                    'success'
                                )
                                clickedElem.removeClass('btn-dark').addClass('btn-success').addClass('disabled').text('Postcard Sent');
                                // clickedElem.parent('td').html('<span style="font-weight: 900; color: green;">' + response.text + '</span> ');
                            } else {
                                swalWithBootstrapButtons.fire(
                                    'Synced!',
                                    'Error! in sending postcard.',
                                    'error'
                                )
                            }
                            clickedElem.attr('disabled', false).find('.fa-spinner').remove();
                        },
                        error: function() {
                            clickedElem.attr('disabled', false).find('.fa-spinner').remove();
                            swalWithBootstrapButtons.fire(
                                'Synced!',
                                'Error! in sending postcard.',
                                'error'
                            )
                        }
                    });
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Your send request was cancelled :)',
                        'error'
                    )
                }
            });
        })
    </script>

    @endpush