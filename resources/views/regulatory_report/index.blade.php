@extends('layout.app')

@section('content')

	<!-- Content -->
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
        	<!--  Traffic  -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <!-- Error & Message -->
                                @if (session()->has('insert'))
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        {{ session()->get('insert') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @elseif (session()->has('update'))
                                    <div class="alert alert-info alert-dismissible" role="alert">
                                        {{ session()->get('update') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @elseif (session()->has('delete'))
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        {{ session()->get('delete') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                            <!-- Error & Message -->
                             <div class="row form-group">
                                <div class="col-md-3">
                                    <h4 class="box-title">Quarterly Reports </h4>
                                </div>
                                <div class="col-md-2">
                                    <div class="input-group">  
                                        <label for="" class="label_font_size"> State</label>
                                        <select class="form-control select_state input_size" name="state" id="select_state" value=""></select>
                                        @if ($errors->has('state'))
                                        <div class="text-danger">{{ $errors->first('state') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label for="" class="label_font_size">From Date</label>
                                    <div class="input-group date" data-date-format="mm-dd-yyyy">
                                        <input type="text" name="from_date" id="from_date" class="form-control input_size" value="" readonly="readonly">
                                        <span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>    
                                <div class="col-md-2">
                                    <label for="" class="label_font_size">To Date</label>
                                    <div class="input-group date" data-date-format="mm-dd-yyyy">
                                        <input type="text" name="to_date" id="to_date" class="form-control input_size" value="" readonly="readonly">
                                        <span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <button type="submit" id="filterdata" class="btn btn-primary"><i class="fa fa-go"></i>Go</button>
                                    <a  target="_blank" id="viewpdf">
                                        <i class="fa fa-go" rel="tooltip" title="View Quarterly PDF">View PDF</i>
                                    </a>
                                </div>    
                            </div>

                        <div class="card-body nopadding">
                            <div class="col-md-12 table-stats order-table ov-h nopadding">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <!-- <th>#</th> -->
                                            <th>Source Number</th>
                                            <th>Ship Date</th>
                                            <th>Tech</th>
                                            <th>Machine Number</th>
                                            <th>Created At</th>
                                        </tr>
                                    </thead>
                                    <tbody class="table_format">
                                        
                                    </tbody>
                                </table>
                            </div> <!-- /.col-md-12 -->
                        </div>
                    </div>
                </div><!-- /# column -->
            </div>
            <!--  /Traffic -->
            <div class="clearfix"></div>
        </div>
    	<!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>
<script>
    jQuery(document).ready(function () {
        jQuery("#from_date").datepicker({ 
          format: "mm-dd-yyyy",
          weekStart: 0,
          calendarWeeks: true,
          autoclose: true,
          todayHighlight: true,
          orientation: "auto"
        }).datepicker('update', new Date());

         jQuery("#to_date").datepicker({ 
          format: "mm-dd-yyyy",
          weekStart: 0,
          calendarWeeks: true,
          autoclose: true,
          todayHighlight: true,
          orientation: "auto"
        }).datepicker('update', new Date());

        jQuery('#filterdata').on('click', function(){
            var state = jQuery("#select_state").val();
            var from_date = jQuery("#from_date").val();
            var to_date = jQuery("#to_date").val();
            jQuery.ajax({
                url:"{{ route('getQuarterlyData') }}",
                method:'GET',
                data:{state:state, from_date:from_date, to_date:to_date},
                dataType:'json',
                success:function(data)
                {   
                    jQuery('#viewpdf').attr('href', '/view_pdf/'+from_date+'/'+to_date+'/'+state);
                    jQuery('.table_format').html(data.table_data);

                }
            })
        })

    });

</script>
@endsection