@extends('layout.app')

@section('content')

    <!-- Content -->
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
            <!--  Traffic  -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-lg-6">
                                    <h4 class="box-title">Update Pre-Assign Sources </h4>
                                </div>
                                <div class="col-lg-6">
                                    <a href="{{ route('preAssignSou.index') }}" class="btn btn-outline-secondary float-right"><i class="fa fa-angle-double-left"></i> Back</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('preAssignSou.update', $source_loc) }}" method="POST" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <input name="_method" type="hidden" value="PUT">
                                <input type="hidden" name="source_number" id="source_number" value="{{ $source_loc->source_num }}">
                                <div class="box_border">
                                    <div class="heading_font_size font-weight-bold">
                                        1. Verify the selected source and the its associated machine information.
                                    </div>

                                    <div class="form-group ml-5 mt-3">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="edit_source_num">Source Being Issued :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <select class="form-control select_pre_assign_source_model input_size" name="edit_source_num" id="edit_source_num" value="{{ old('edit_source_num') }}">
                                                    @if(!empty($source_loc->source_num))
                                                        <option value="{{ $source_loc->source_num }}">{{ $source_loc->source_num }}</option>
                                                    @endif
                                                </select>
                                                @if ($errors->has('edit_source_num'))
                                                    <div class="text-danger">{{ $errors->first('edit_source_num') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="edit_source_status">Current Source Status :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="edit_source_status" id="edit_source_status" class="form-control input_size" value="{{ $source_loc->source_status->sstat_desc }}" readonly>
                                                @if ($errors->has('edit_source_status'))
                                                    <div class="text-danger">{{ $errors->first('edit_source_status') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="edit_current_source_model">Current Source Model :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="edit_current_source_model" id="edit_current_source_model" class="form-control input_size" value="{{ $source_loc->source_model->desc }}" readonly>
                                                @if ($errors->has('edit_current_source_model'))
                                                    <div class="text-danger">{{ $errors->first('edit_current_source_model') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group ml-5 mt-3">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="edit_current_mach">Current Machine :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="edit_current_mach" id="edit_current_mach" class="form-control input_size" value="{{ $machine->mach_model }}" readonly>
                                                @if ($errors->has('edit_current_mach'))
                                                <div class="text-danger">{{ $errors->first('edit_current_mach') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="edit_current_mach_status">Current Machine Status :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="edit_current_mach_status" id="edit_current_mach_status" class="form-control input_size" value="{{ $machLoc->machine_status->mstats_desc }}" readonly>
                                                @if ($errors->has('edit_current_mach_status'))
                                                <div class="text-danger">{{ $errors->first('edit_current_mach_status') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="edit_current_customer">Current Customer :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="edit_current_customer" id="edit_current_customer" class="form-control input_size" value="{{ $machLoc->customer->customer_name }}" readonly>
                                                @if ($errors->has('edit_current_customer'))
                                                <div class="text-danger">{{ $errors->first('edit_current_customer') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box_border mt-4">
                                    <div class="heading_font_size font-weight-bold">
                                        2. Select the pre-assign machine and enter corresponding data.
                                    </div>

                                    <div class="form-group ml-5 mt-3">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="edit_machine_num">Machine assigning to :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <select class="form-control select_machine_num input_size" name="edit_machine_num" id="edit_machine_num" value="{{ old('edit_machine_num') }}">
                                                    @if(!empty($source_loc->mach_num))
                                                        <option value="{{ $source_loc->mach_num }}">{{ $source_loc->mach_num }}</option>
                                                    @endif
                                                </select>
                                                @if ($errors->has('edit_machine_num'))
                                                    <div class="text-danger">{{ $errors->first('edit_machine_num') }}</div>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="edit_customer_assign">Customer assigning to :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="edit_customer_assign" id="edit_customer_assign" class="form-control input_size" value="{{ $customer->customer_number }}" readonly>
                                                @if ($errors->has('edit_customer_assign'))
                                                <div class="text-danger">{{ $errors->first('edit_customer_assign') }}</div>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="edit_customer_contact">Customer contact :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="edit_customer_contact" id="edit_customer_contact" class="form-control input_size" value="{{ $customer->customer_name }}" readonly>
                                                @if ($errors->has('edit_customer_contact'))
                                                <div class="text-danger">{{ $errors->first('edit_customer_contact') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group ml-5 mt-3">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="edit_source_model">Source Model :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <select name="edit_source_model" id="edit_source_model" class="form-control input_size" value="{{ old('edit_source_model') }}">
                                                    <option selected disabled>Select Source Model</option>
                                                    @foreach($source_mods as $source_mod)
                                                    <option value="{{ $source_mod->s_mod_num }}" {{ $source_loc->s_mod_num == $source_mod->s_mod_num ? 'selected' : '' }} >{{ $source_mod->desc }}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('edit_source_model'))
                                                    <div class="text-danger">{{ $errors->first('edit_source_model') }}</div>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="label_font_size" for="edit_source_ship_method">Source Ship Method :</label>
                                            </div>
                                            <div class="col-md-6">
                                                <select name="edit_source_ship_method" id="edit_source_ship_method" class="form-control input_size" value="{{ old('edit_source_ship_method') }}">
                                                    <option selected disabled>Select Source Ship Method</option>
                                                    @foreach($ship_methods as $ship_method)
                                                        <option value="{{ $ship_method->s_status }}" {{ $source_loc->s_status == $ship_method->s_status ? 'selected' : '' }} >{{ $ship_method->sstat_desc }}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('edit_source_ship_method'))
                                                    <div class="text-danger">{{ $errors->first('edit_source_ship_method') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 mt-4">
                                        <button type="submit" class="btn btn-outline-primary float-right"><i class="fa fa-save"></i> Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card-body"></div>
                    </div>
                </div><!-- /# column -->
            </div>
            <!--  /Traffic -->
            <div class="clearfix"></div>
        </div>
        <!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>

        <!-- Enter Source Number Fetch Pre-assign Source Data -->
            <script>
                jQuery(document).ready(function(){
                    jQuery('.select_pre_assign_source_model').select2({
                        placeholder: 'Enter Source Number',
                        ajax: {
                            url: '{{ route("preAssignSourceSearch") }}',
                            dataType: 'json',
                            delay: 250,
                            data: function (params) {
                                return {
                                    q: jQuery.trim(params.term)
                                };
                            },
                            processResults: function (data) {
                                return {
                                    results: data
                                };
                            },
                            cache: true
                        }
                    });
                });

                jQuery(document).ready(function(){

                    jQuery('.select_pre_assign_source_model').on('change', function(){
                        var source_id = jQuery(".select_pre_assign_source_model option:selected").val();
                        jQuery.ajax({
                            url:"{{ route('preAssignSourceAutofill') }}",
                            method:'GET',
                            data:{source_id:source_id},
                            dataType:'json',
                            success:function(data)
                            {
                                if(data.source_data.s_status != ''){
                                    jQuery('#edit_source_status').val(data.source_data.s_status);
                                }
                                if(data.source_data.s_mod_num != ''){
                                    jQuery('#edit_current_source_model').val(data.source_data.s_mod_num);
                                }
                                if(data.source_data.current_mach != ''){
                                    jQuery('#edit_current_mach').val(data.source_data.current_mach);
                                }
                                if(data.source_data.current_mach_status != ''){
                                    jQuery('#edit_current_mach_status').val(data.source_data.current_mach_status);
                                }
                                if(data.source_data.current_customer != ''){
                                    jQuery('#edit_current_customer').val(data.source_data.current_customer);
                                }
                                if(data.source_data.source_num != ''){
                                    jQuery('#source_number').val(data.source_data.source_num);
                                }
                            }
                        })
                    });

                    jQuery('.select_machine_num').on('change', function(){
                        var machine_number = jQuery(".select_machine_num option:selected").val();
                        jQuery.ajax({
                            url:"{{ route('preAssignMachineAutofill') }}",
                            method:'GET',
                            data:{machine_number:machine_number},
                            dataType:'json',
                            success:function(data)
                            {
                                console.log(data);
                                if(data.machine_data.customer_number != ''){
                                    jQuery('#customer_assign').val(data.machine_data.customer_number);
                                }
                                if(data.machine_data.customer_name != ''){
                                    jQuery('#customer_contact').val(data.machine_data.customer_name);
                                }
                            }
                        })
                    });
                });  
            </script>
        <!-- Enter Source Number Fetch Pre-assign Source Data -->

@endsection