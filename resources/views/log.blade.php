 
 @foreach($data['all_activity'] as $val)
 <tr>
                                                
            <td> {{ $loop->iteration }}</td>
            <td> 
                <span class="name">{{ $val->description }}</span> 
            </td>
            <td> 
                <span class="product">
                    {{date('d-m-Y H:i:s', strtotime($val->created_at))}}
                </span> 
            </td>
            <td>
                <span>
                    <?php
                        if($val->customer_id){
                            echo "Customer";
                        }else if($val->machine_id){
                            echo "Machine";
                        }else if($val->technician_id){
                            echo "Technician";
                        }else if($val->contact_id){
                            echo "Contact";
                        }else if($val->source_id){
                            echo "source";
                        }else if($val->login){
                            echo "Login";
                        }else if($val->logout){
                            echo "Logout";
                        }else{
                            echo "NULL";
                        }
                    ?>
                </span>
            </td>
            <td>
                <input type="hidden" class="showmoreactivity{{$val->id}}old" value="{{$val->old_value}}">
                <input type="hidden" class="showmoreactivity{{$val->id}}new" value="{{$val->new_value}}">

                <span class="badge badge-complete showdetail" className="showmoreactivity{{$val->id}}">Show More</span>
            </td>
        </tr>
                                                    @endforeach