@extends('layout.app')

@section('content')
	<!-- Content -->
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
        	<!--  Traffic  -->
            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-header">
                           
                            @yield('customer_head_button')
                        </div>

                        @yield('customer_content')
                       
                       
                    </div>
                </div><!-- /# column -->
            </div>
            <!--  /Traffic -->
            <div class="clearfix"></div>
        </div>
    	<!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>
@endsection
