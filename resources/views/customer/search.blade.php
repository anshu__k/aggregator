<table class="table " id="customer_table">
                    <thead>
                        <tr>
                            <!-- <th>S.no.</th> -->
                            <th>Customer Name</th>
                            <th>Customer Number</th>
                            <!-- <th>Filing Name</th> -->
                            <th>Address</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Zipcode</th>
                            <th>Country</th>
                            <th>Status</th>
                            <th>Last Wipetest</th>
                            <th>Updated At</th>
                            <th>Actions</th>



                        </tr>
                    </thead>

                    <tbody class="table_format">
                        <!-- {{ $i = 1}} -->
                        @forelse ($customers as $key => $customer)
                            <tr>
                                <!-- <td>{{ $customers->firstItem() + $key }}</td> -->
                                <td> <a href="{{ url('customer/view', $customer->id) }}">
                                   <u> {{$customer->customer_name}} </u>
                                    </a> 
                                </td>
                                <td>{{$customer->customer_number}}</td>
                                <!-- <td>{{$customer->filing_name}}</td> -->
                                <td>{{$customer->address}}</td>
                                <td>{{$customer->city}}</td>
                                <td>{{$customer->state}}</td>
                                <td>{{$customer->zipcode}}</td>
                                <td>{{$customer->country}}</td>
                                <td @if($customer->status == 1)class="text-success" @else class="text-danger" @endif>{{ $customer->status==1 ? 'Active' : 'Inactive' }}</td>
                                <td>{{App\Customer::get_last_wipetest($customer->customer_number)}}</td>
                                <!-- <td style="text-align: right; padding-right:0px;">
                                    <a class="btn fa fa-eye edit_btn"  rel="tooltip" title="View History" href="{{ route('log_history',['customer_id'=>$customer->id]) }}"></a>
                                </td> -->
                                <td>{{ !empty($customer->updated_at)?date('d M Y', strtotime($customer->updated_at)):'-' }}</td>
                                <td>
                                    <a class="btn fa fa-edit edit_btn" href="{{ route('customer_edit',$customer->id) }}"></a>
                                </td> 
                                <!-- <td style="text-align: left; padding-left:0px;">
                                    <form action="{{ route('customer_delete', $customer->id) }}" method="POST">
                                        @csrf
                                        <button type="submit" class="fabutton" onclick="return myFunction();">
                                            <i class="fa fa-trash dlt_btn" rel="tooltip" title="Delete"></i>
                                        </button>
                                    </form>
                                </td> -->
                                    
                            </tr>

                        @empty

                        <tr>
                            <td colspan="9" class="text-center">
                                <p>Customer Not Found.</p>
                            </td> 
                        </tr>
                        @endforelse



                    </tbody>
            </table>

            <div class="col">
{{ $customers->links() }} </div>