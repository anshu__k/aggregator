@extends('customer.layout') 

@section('customer_head_button')

<!-- Customers
<a href="#" class="float-right btn btn-outline-primary">Back</a> -->
<div class="row">
    <div class="col-lg-6">
        <h4 class="box-title">Edit Customer </h4>
    </div>
    <div class="col-lg-6">
        <a href="{{ url('customer') }}" class="btn btn-outline-secondary float-right"><i class="fa fa-angle-double-left"></i> Back</a>
    </div>
</div>
@endsection

@section('customer_content')

    <div class="card-body">

    <!-- Error & Message -->
    @if (session()->has('insert'))
            <div class="alert alert-success alert-dismissible" role="alert">
                {{ session()->get('insert') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @elseif (session()->has('update'))
            <div class="alert alert-info alert-dismissible" role="alert">
                {{ session()->get('update') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @elseif (session()->has('delete'))
            <div class="alert alert-danger alert-dismissible" role="alert">
                {{ session()->get('delete') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    <!-- Error & Message -->

    <form action="{{ route('customer_update',$customer->id) }}" method="post">
        @csrf
            <div class="row form-group">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="customer_num" class=" form-control-label">Customer Number</label>
                        <input type="text"  name="customer_number" id="customer_number" placeholder="eg. ALBRAXFRXI" class="form-control input_size" value="{{ $customer->customer_number }}">
                    </div>
                    @if ($errors->has('customer_number'))
                        <div class="text-danger">{{ $errors->first('customer_number') }}</div>
                    @endif
                </div>

                <div class="col-md-6">
                    <!-- <div class="form-group">
                        <label for="" class=" form-control-label">Filing Name</label>
                        <input type="text"  name="filing_name" id="filing_name" placeholder="eg. ALBRA" class="form-control input_size" value="{{ $customer->filing_name }}">
                    </div>
                    @if ($errors->has('filing_name'))
                        <div class="text-danger">{{ $errors->first('filing_name') }}</div>
                    @endif -->
                <div class="form-group">
                        <label for="" class=" form-control-label">Customer Name</label>
                        <input type="text"  name="customer_name" id="customer_name" placeholder="eg. Albra (Heineken)" class="form-control input_size" value="{{$customer->customer_name}}">
                    </div>
                    @if ($errors->has('customer_name'))
                        <div class="text-danger">{{ $errors->first('customer_name') }}</div>
                    @endif
                </div>
            </div> <!-- .row form-group -->


            <div class="row form-group">
                
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="" class=" form-control-label">Address</label>
                        <input type="text"  name="address" id="address" placeholder="eg. 15th Avenue" class="form-control input_size" value="{{$customer->address}}">
                    </div>
                    @if ($errors->has('address'))
                        <div class="text-danger">{{ $errors->first('address') }}</div>
                    @endif
                </div>
            </div>



            <div class="row form-group">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="" class=" form-control-label">City</label>
                        <input type="text"  name="city" id="city" placeholder="eg. Queens" class="form-control input_size" value="{{$customer->city}}">
                    </div>
                    @if ($errors->has('city'))
                        <div class="text-danger">{{ $errors->first('city') }}</div>
                    @endif
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="" class="label_font_size"> State</label>
                        <input type="text"  name="state" id="state" placeholder="eg. IN" class="form-control input_size" value="{{$customer->state}}">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="" class="label_font_size"> Zipcode</label>
                        <input type="text"  name="zipcode" id="zipcode" placeholder="eg. 46515" class="form-control input_size" value="{{$customer->zipcode}}">
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="" class="label_font_size"> Country</label>
                        <input type="text"  name="country" id="country" placeholder="eg. USA" class="form-control input_size" value="{{$customer->country}}">
                    </div>
                </div>

                

            </div> <!-- .row form-group -->


            <div class="row form-group">
                <!-- <div class="col-md-6">
                    <div class="form-group">
                        <label for="wipetest_inhouse" class="label_font_size">Performs Wipetest themselves?</label>
                        <input type="text"  name="customer_number" id="customer_number" placeholder="Customer Number" class="form-control">
                    </div> 
                </div> -->

                <!-- <div class="form-check-inline form-check">
                    <label for="inline-checkbox1" class="form-check-label label_font_size">
                    <input type="checkbox" id="wipetest_inhouse" name="wipetest_inhouse" value="1" class="form-check-input" 
                    @if($customer->wipetest_inhouse==1)
                    checked="checked"
                    @endif
                    >Performs Wipetest themselves?
                    </label>
                </div> -->

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="" class="label_font_size">Wipetest Frequency</label>
                        <select class="form-control input_size" name="wipetest_frequency" id="wipetest_frequency" value="{{ $customer->wipetest_frequency }}">
                        <option value="">Select Wipetest Frequency</option>
                        <option value="6" {{ $customer->wipetest_frequency == 6 ? 'selected' : ''}} >6</option>
                        <option value="12" {{ $customer->wipetest_frequency == 12 ? 'selected' : ''}} >12</option>
                        <option value="36" {{ $customer->wipetest_frequency == 36 ? 'selected' : ''}} >36</option>
                        </select>    
                        <!-- <input type="text"  name="wipetest_frequency" id="wipetest_frequency" placeholder="eg. 3.6" class="form-control input_size" value="{{$customer->wipetest_frequency}}"> -->
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="additional_details" class="label_font_size">Additional Details</label>
                        <textarea type="text"  name="notes" id="notes" placeholder="eg. our oldest customer" class="form-control input_size" value="{{$customer->notes}}"></textarea>
                    </div> 
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label class="label_font_size" for="role">Status :</label>
                            <select class="form-control input_size" name="status" id="status" value="{{ $customer->status }}">
                                <option selected disabled>Select Status</option>
                                <option value="1" {{ $customer->status == 1 ? 'selected' : ''}} >Active</option>
                                <option value="0" {{ $customer->status == 0 ? 'selected' : ''}} >Inactive</option>
                                
                            </select>
                    </div>
                </div>

            </div> <!-- .row form-group -->

            <!-- <div class="col-md-6 offset-md-6 " > -->

            <div class="pull-right">
                <button type="submit" class="btn btn-outline-primary"><i class="fa fa-save"></i> Save</button>
            </div>

        </form>

    </div> <!-- .card-body -->


@endsection