@extends('layout.app')
<style type="text/css">
	.hide_data{
		display: none;
	}
	.show-more-activity{
		padding: 4px;
	}
</style>
@section('content')


	<!-- Content -->
				<!-- Content -->
		<div class="content" style="padding-bottom: 0px;">
        <!-- Animated -->
			<div class="animated fadeIn">
				<!--  Traffic  -->
				<div class="row">
					<div class="col-lg-12">
						<div class="card" style="padding-bottom: 0px;">
							<div class="card-header">

								<div class="row">
									<div class="col-lg-6">
										<h4 class="box-title">{{$customer->customer_name }} ( {{ $customer->customer_number }} )</h4>
									</div>
									<div class="col-lg-6">
										<a href="{{ url('customer') }}" class="btn btn-outline-secondary float-right"><i class="fa fa-angle-double-left"></i> Back</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


	<!-- Content -->
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
        	<!--  Traffic  -->
			<div class="row">
				<div class="col-lg-8">
					<div class="card">
						<div class="card-header">
							<div class="row">
								<div class="col-lg-7">
									<h5><b>Machine</b></h5>
								</div>
								<div class="col-lg-5">
									<div class="input-group">  
                                        <label for="" class="label_font_size" style="padding-right: 10px;">  Machine Status</label>
                                        <select class="form-control input_size" name="mach_status" id="select_mach_status">
		                                		<option value="aa" selected>Active</option>
		                                		<option value="all">Inactive</option>
		                                </select>
                                    </div>
								</div>
							</div>
						</div>
                    	<div class="card-body nopadding">
                        	<div class="col-md-12 table-stats order-table ov-h nopadding">
                        		<input type="hidden" name="customer_id" id="customer_id" value="{{$customer->id}}">
	                            <table class="table" id="machine_table">
				                  	<thead>
					                    <tr>	
					                      <th>Machine Number</th>
					                      <th>Machine Model</th>
					                      <th>Ship Date</th>
					                      <th>Next Test Date</th>
					                      <th>Status</th>
					                    </tr>
				                  	</thead>

				                  	<tbody class="machine_table_format">
				                    	<!-- {{$i = 1}}  -->
				                    	@forelse($machines as $key => $machine) 
					                      	<tr>
					                      	  <!--  <td class="details-control" machine_num="{{$machine->machine != null ? $machine->machine->mach_number : $machine->machine_id }}" machine_id="{{$machine->machine_id}}" customer_id="{{$machine->customer_id}}">
-						                       </td> -->
						                       <td>{{$machine->machine != null ? $machine->machine->mach_number : "" }}</td>
						                       <td>{{$machine->machine != null ? $machine->machine->mach_model : "" }}</td>
						                       <td>{{ date('d M Y', strtotime($machine->ship_date)) }}</td>
						                       <td></td>
						                       <td>{{$machine->machine_status['mstats_desc'] != null ? $machine->machine_status['mstats_desc'] : "" }}</td>					                   
						                   </tr>
						                    {!! $trs[$key]['table_data'] !!}
						                   	{!! $trs[$key]['source_table_data'] !!}
				                    	@empty
					                        <tr>
					                          <td colspan="6" class="text-center">
					                          	<p>Machine Not Found.</p>
					                          </td> 
					                        </tr>
				                    	@endforelse
				                  	</tbody>
				                </table>
                        	</div> <!-- /.col-md-12 -->
				        </div>
                        <div class="card-body">
                            {{ $machines->onEachSide(1)->appends(\Request::all())->links() }}
                        </div>
            		</div>
                </div>
			
				<div class="col-lg-4">
					<div class="card">
							<div class="card-header">
								<div class="row">
									<div class="col-lg-6">
										<h5><b>Activity</b></h5>
									</div>
								</div>
							</div>



							<div class="card-body nopadding">

								<div class="col-md-12 table-stats order-table ov-h nopadding">
									<table class="table " id="customer_table">
											<thead>
												<tr>
													<th>S.no.</th>
													<th>User</th>
													<th>Date Time</th>
													<th>Description</th>
													<th>Action</th>


												</tr>
											</thead>

											<tbody class="table_format">
												<!-- {{ $i = 1}} -->
												@forelse ($log_data as $key => $log)
												
													<tr>
														<td>{{ $i }}</td>
														<td>{{$log->user_name}}</td>
														<td>{{$log->created_at}}</td>
														<td>{{$log->description}}</td>
														<td>
															<span class="badge badge-complete showdetail" className="showmoreactivity{{$log->id}}">
																Show More
															</span>
															<input type="hidden" class="showmoreactivity{{$log->id}}old" value="{{$log->old_value}}">
                											<input type="hidden" class="showmoreactivity{{$log->id}}new" value="{{$log->new_value}}">
														</td>
													</tr>

												<!-- {{$i++}} -->
												@empty

												<tr>
													<td colspan="10" class="text-center">
														<p>No activity.</p>
													</td> 
												</tr>
												@endforelse



											</tbody>
									</table>


								</div>
							</div>

					</div>
				</div>
				<div class="col-lg-6">
					<div class="card">
						<div class="card-header">
							<div class="row">
								<div class="col-lg-6">
									<h5><b>Contact</b></h5>
								</div>
							</div>
						</div>
						<div class="card-body nopadding">
							<div class="col-lg-12 table-stats order-table ov-h nopadding">
								<table class="table " id="contact_table">
									<thead>
										<tr>
											<th>S.no.</th>
											<th>Name</th>
											<th>Contact Type</th>
											<!-- <th>Title</th> -->
											<th>Email</th>
											<th>Phone</th>
											<!-- <th>Address</th> -->
											<!-- <th>Active Customer</th> -->
											<!-- <th>Expiration Date</th> -->
											<th>Status</th>



										</tr>
									</thead>
									<tbody class="table_format">
										@forelse ($contacts as $key => $contact)
										<tr>
											<td>{{ $contacts->firstItem() + $key }}</td>
											<td>{{$contact->contact_name}}</td>
											<td>{{$contact->contact_desc}}</td>
											<!-- <td>{{$contact->customer_name}}</td> -->
											<!-- <td>{{$contact->contact_title}}</td> -->
											<td>{{$contact->contact_email}}</td>
											<td>{{$contact->contact_phone}}</td>
											<!-- <td>{{$contact->address}}</td> -->
											<!-- <td>{{$contact->customer_active}}</td> -->
											<!-- <td>{{$contact->expiration_date}}</td> -->
											<!-- <td>{{$contact->status}}</td> -->
											<td @if($contact->status == 1)class="text-success" @else class="text-danger" @endif style="text-align: left;">{{$contact->status == 1 ? 'Active' : 'Deactive'}}</td>
										</tr>

										@empty

										<tr>
											<td colspan="10" class="text-center">
												<p>Contact Not Found.</p>
											</td> 
										</tr>
										@endforelse
									</tbody>
								</table>
							</div>
						</div>
						<div class="card-body">
							{{ $contacts->onEachSide(1)->appends(\Request::all())->links() }}
						</div>
					</div>
				</div>
				
				
				<div class="col-lg-6">
					<div class="card">
						<div class="card-header">
							<div class="row">
								<div class="col-lg-6">
									<h5><b>Document</b></h5>
								</div>
							</div>
						</div>
						<div class="card-body nopadding">
							<div class="col-lg-12 table-stats order-table ov-h nopadding">
								<table class="table" id="customer_table">
			                    <thead>
			                        <tr>
			                            <th>Title</th>
			                            <th>Document</th>
			                            <th>Docs Creation Date</th>
			                            <th>Upload Date</th>
			                        </tr>     
			                    </thead>

			                    <tbody class="table_format">
			                        @forelse($uploaddoc as $key => $data) 
			                        <tr>
			                            <td>{{ $data->title }}</td>
			                            <td><a href="{{ url('sourcetracker/'.$customer_id.'/'.$data->document) }}" target="_blank"><i class="fa fa-file fa-lg text-info" aria-hidden="true"></i></a></td>
			                            <td>{{ date('d M Y', strtotime($data->created_at)) }}</td>
			                             <td>{{ date('d M Y', strtotime($data->upload_date)) }}</td>
			                        </tr>
			                        @empty
			                        <tr>
			                          <td colspan="7" class="text-center">
			                            <p>Document Not Found.</p>
			                          </td> 
			                        </tr>
			                        @endforelse
			                    </tbody>
			            	</table>
							</div>
						</div>
					</div>
				</div>



            <!--  /Traffic -->
				<div class="clearfix"></div>
			</div>
        </div>
    	<!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>


    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header" style="background-color: darkgrey">
	        <h5 class="modal-title" id="exampleModalLongTitle"> Activity Detail </h5>
	      </div>
	      <div class="modal-body showJSONData">
	        <div class="row">
	            <div class="col-md-6">
	                <h3> New Data </h3>
	                <table class="table activityTable">   
	                </table>
	            </div>
	            <div class="col-md-6">
	                <h3> Old Data </h3>
	                <table class="table activityTableold">    
	                </table>
	            </div>
	        
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>
<script src="{{ url('js/custom.js') }}"></script>

<script>
	jQuery(document).ready(function() {

		// jQuery('.details-control').on('click', function()
		// {
		// 	$td = jQuery(this).closest('td')
		// 	$tr = jQuery(this).closest('tr')
		// 	$this = jQuery(this)

			// jQuery('tbody').find('.details-control i').removeClass('fa-minus').addClass('fa-plus')
			// jQuery('tbody').find('.show_data').remove();
			// if($this.find('i').hasClass('fa-minus')){
			// jQuery('tbody').find('.details-control i').removeClass('fa-minus').addClass('fa-plus')
			// 	$this.find('i').removeClass('fa-minus').addClass('fa-plus')
			// }else{
			// jQuery('tbody').find('.details-control i').removeClass('fa-minus').addClass('fa-plus')
			// 	$this.find('i').removeClass('fa-plus').addClass('fa-minus')

				// var machine_id = $td.attr('machine_id');
				// var customer_id = $td.attr('customer_id');
				// var machine_num = $td.attr('machine_num');

				// jQuery.ajax({
				// 	url:"{{ route('duplicateMachine') }}",
				// 	method:'GET',
				// 	data:{machine_id:machine_id, customer_id:customer_id, machine_num: machine_num},
				// 	dataType:'json',
				// 	success:function(data)
				// 	{
				// 		$tr.after(data.table_data);
				// 		jQuery('#machine_table tbody .show_data').last().after(data.source_table_data);
				// 	}
				// })

			//}

			// jQuery('tbody').find('.details-control i').toggleClass('fa-minus');
			
			// if(jQuery('tbody').find('.show_data').length > 0){
			// 	jQuery('tbody').find('.show_data').toggleClass('hide_data');
			// }
			// if(jQuery('tbody').find('.show_data.hide_data').length == 0){
			// 	jQuery('tbody').find('.show_data').remove();
			// 	var machine_id = jQuery(this).attr('machine_id');
			// 	var customer_id = jQuery(this).attr('customer_id');

			// 	jQuery.ajax({
			// 		url:"{{ route('duplicateMachine') }}",
			// 		method:'GET',
			// 		data:{machine_id:machine_id, customer_id:customer_id},
			// 		dataType:'json',
			// 		success:function(data)
			// 		{
			// 			jQuery('#machine_table tbody .appendMachineData').after(data.table_data);
			// 			jQuery('#machine_table tbody .show_data').last().after(data.source_table_data);
			// 		}
			// 	})
			// }
		//});

		jQuery('#select_mach_status').on('change', function(){ 
            var mach_status = jQuery("#select_mach_status option:selected").val();
            var customer_id = jQuery("#customer_id").val();
            jQuery.ajax({
                url:"{{ route('filterMachineStatus') }}",
                method:'GET',
                data:{mach_status:mach_status, customer_id:customer_id},
                dataType:'json',
                success:function(data)
                {    
                	jQuery('.machine_table_format').html(data.table_data);
                }
            })
        });
	} );
</script>
<style type="text/css">
	.show_data{
		background-color: aliceblue;
	}

	.show_data td{
		border: 0px !important;
		padding: 3px !important;
	}
</style>
@endsection

