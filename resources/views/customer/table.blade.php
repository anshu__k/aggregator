@extends('customer.layout') 


@section('customer_head_button')


<div class="row form-group">

        <div class="col-md-3">
            <h4 class="box-title">Customers</h4>
        </div>
        <div class="col-md-6">
        </div>   
        <div class="col-md-3">
            <a href="{{ url('customer/create') }}" class="btn btn-outline-success float-right"><i class="fa fa-plus"></i> ADD</a>
        </div>

</div>   
<div class="row form-group">

        <div class="col-md-4">
            <div class="input-group">
                <input class="form-control"  name="search_term" id="search_term" placeholder="Search Customer Name, Customer Number, City, Zipcode">
            </div>
        </div>
        
        <div class="col-md-3">
            <div class="input-group">
                <select class="form-control" name="status" id="status">
                    <option selected disabled>Select Status</option>
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="input-group">
                        <select class="form-control select_state" name="state" id="select_state" value="{{ old('state') }}"></select>
                    
            </div>            
        </div>
        <div class="col-md-2">
        <div class="input-group-btn">
                        <button type="button" id="search_customer" class="btn btn-outline-primary" >Search</button>
                                     <a class="btn input-group-btn"href="{{ url('customer') }}" >Reset</a>    
                    </div>

       
        </div>            
</div>

    

@endsection

@section('customer_content')

    <div class="card-body nopadding">
        <!-- Error & Message -->
        @if (session()->has('insert'))
            <div class="alert alert-success alert-dismissible" role="alert">
                {{ session()->get('insert') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @elseif (session()->has('update'))
            <div class="alert alert-info alert-dismissible" role="alert">
                {{ session()->get('update') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @elseif (session()->has('delete'))
            <div class="alert alert-danger alert-dismissible" role="alert">
                {{ session()->get('delete') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
							<!-- Error & Message -->
        <div id="searchId" class="col-md-12 table-stats order-table ov-h nopadding">
            <table class="table " id="customer_table">
                    <thead>
                        <tr>
                            <!-- <th>S.no.</th> -->
                            <th>Customer Name</th>
                            <th>Customer Number</th>
                            <!-- <th>Filing Name</th> -->
                            <th>Address</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Zipcode</th>
                            <th>Country</th>
                            <th>Status</th>
                            <th>Last Wipetest</th>
                            <th>Updated At</th>
                            <th>Actions</th>



                        </tr>
                    </thead>

                    <tbody class="table_format">
                        <!-- {{ $i = 1}} -->
                        @forelse ($customers as $key => $customer)
                            <tr>
                                <!-- <td>{{ $customers->firstItem() + $key }}</td> -->
                                <td> <a href="{{ url('customer/view', $customer->id) }}">
                                   <u> {{$customer->customer_name}} </u>
                                    </a> 
                                </td>
                                <td>{{$customer->customer_number}}</td>
                                <!-- <td>{{$customer->filing_name}}</td> -->
                                <td>{{$customer->address}}</td>
                                <td>{{$customer->city}}</td>
                                <td>{{$customer->state}}</td>
                                <td>{{$customer->zipcode}}</td>
                                <td>{{$customer->country}}</td>
                                <td @if($customer->status == 1)class="text-success" @else class="text-danger" @endif>{{ $customer->status==1 ? 'Active' : 'Inactive' }}</td>
                                <td>{{App\Customer::get_last_wipetest($customer->customer_number)}}</td>
                                <!-- <td style="text-align: right; padding-right:0px;">
                                    <a class="btn fa fa-eye edit_btn"  rel="tooltip" title="View History" href="{{ route('log_history',['customer_id'=>$customer->id]) }}"></a>
                                </td> -->
                                <td>{{ !empty($customer->updated_at)?date('d M Y', strtotime($customer->updated_at)):'-' }}</td>
                                <td>
                                    <a class="btn fa fa-edit edit_btn" href="{{ route('customer_edit',$customer->id) }}"></a>
                                </td> 
                                <!-- <td style="text-align: right; padding-left:0px;">
                                    <a class="btn fa fa-eye edit_btn" href="#" data-toggle="tooltip" data-placement="top" title="{{$customer->notes}}"></a>
                                </td> -->
                                <!-- <td style="text-align: left; padding-left:0px;">
                                    <form action="{{ route('customer_delete', $customer->id) }}" method="POST">
                                        @csrf
                                        <button type="submit" class="fabutton" onclick="return myFunction();">
                                            <i class="fa fa-trash dlt_btn" rel="tooltip" title="Delete"></i>
                                        </button>
						            </form>
                                </td> -->
                                    
                            </tr>

                        @empty

                        <tr>
                            <td colspan="9" class="text-center">
                                <p>Customer Not Found.</p>
                            </td> 
                        </tr>
                        @endforelse



                    </tbody>
            </table>

            <div class="col page_hide">
{{ $customers->links() }} </div>
        </div>
    </div>

<script>
    jQuery(document).ready(function(){
        jQuery('[data-toggle="tooltip"]').tooltip();
    });
</script>


    <!-- Search Customer -->
        <script>
            jQuery(document).ready(function(){

                function fetch_customer_data(query,status,state,page)
                {

                    // alert(query);
                    jQuery.ajax({
                        url:"{{ route('customer_search') }}",
                        method:'GET',
                        data:{query:query,status:status,state:state,page:page},
                        dataType:'html',
                        success:function(data)
                        {
                            //console.log(data);
                            jQuery('#searchId').html(data);
                            jQuery('.page_hide').css('cssText', 'display:none');

                         }
                    })
                }

                jQuery(document).on('click', '#search_customer', function(){
                    var query = jQuery('#search_term').val();
                    var status = jQuery('#status').val();
                    var state = jQuery('#select_state').val();
                        
                        fetch_customer_data(query,status,state,1);
                        
                        
                    });

                jQuery(document).on('click', '.pagination a', function(event){
                      

                      
                         var query = jQuery('#search_term').val();
                         var status = jQuery('#status').val();
                         var state = jQuery('#select_state').val();
                        
                        if (query != '' || null != state || null !=status){    
                         event.preventDefault(); 
                         var page = jQuery(this).attr('href').split('page=')[1];
                            
                         fetch_customer_data(query,status,state,page);
                        } 

                     });


                });

                jQuery('#search_term').keypress(function (e) {
                var key = e.which;
                if(key == 13)  // the enter key code
                {
                    jQuery('#search_customer').click();
                    return false;  
                }
            });  
        </script>
    <!-- Search Customer -->

@endsection