@extends('layout.app')

@section('content')

	<!-- Content -->
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
        	<!--  Traffic  -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                        	<div class="row">
                        		<div class="col-lg-6">
                    				<h4 class="box-title">Add New Machine </h4>
                        		</div>
                        		<div class="col-lg-6">
                        			<a href="{{ route('machines.index') }}" class="btn btn-outline-secondary float-right"><i class="fa fa-angle-double-left"></i> Back</a>
                        		</div>
                        	</div>
                        </div>
                        <div class="card-body">
				                  <form action="{{ route('machines.store') }}" method="POST" enctype="multipart/form-data">
					                  {{csrf_field()}}

                          		<div class="box_border">
                          			<div class="heading_font_size font-weight-bold">
                          				1. Enter Machine Number And Machine Model Number. 
                          			</div>

	                              <div class="form-group ml-5 mt-3">
                          				<div class="row">
                          					<div class="col-md-2">
	                                    		<label class="label_font_size" for="mach_number">Machine No :</label>
                          					</div>
                          					<div class="col-md-6">
                                    	<input type="text" name="mach_number" id="mach_number" class="form-control input_size" value="{{ old('mach_number') }}" >
                                    	@if ($errors->has('mach_number'))
                                      		<div class="text-danger">{{ $errors->first('mach_number') }}</div>
                                    	@endif
                                  	</div>
                          				</div>
                                </div>

                              	<div class="form-group ml-5 mt-3">
                              		<div class="row">
                      					    <div class="col-md-2">
                                    	<label class="label_font_size" for="mach_model">Machine Model :</label>
                                    </div>
                                    <div class="col-md-6">
                                			<!-- <input type="text" name="mach_model" id="mach_model" class="form-control input_size" value="{{ old('mach_model') }}" > -->
                                    	<select class="form-control select_mach_model input_size" name="mach_model" id="mach_model" value="{{ old('mach_model') }}">
                                    	</select>

                                    	@if ($errors->has('mach_model'))
                                      		<div class="text-danger">{{ $errors->first('mach_model') }}</div>
                                    	@endif
                                		</div>
                                  </div>
                                </div>

                              	<div class="form-group ml-5 mt-3">
                              		<div class="row">
                          					<div class="col-md-2">
                                    	<label class="label_font_size" for="mach_status">Machine Status :</label>
                                    </div>
                                		<div class="col-md-6">
                                    	<select class="form-control input_size" name="mach_status" id="mach_status" value="{{ old('mach_status') }}">
                                    		<option value="" selected disabled>Select Machine Status</option>
                                        @foreach($machine_status as $m_status)
                                    		  <option value="{{ $m_status->m_status }}" {{ old('mach_status') == $m_status->m_status ? 'selected' : ''}} >{{ $m_status->mstats_desc }}</option>
                                        @endforeach
                                    	</select>
                                    	@if ($errors->has('mach_status'))
                                      		<div class="text-danger">{{ $errors->first('mach_status') }}</div>
                                    	@endif
                                		</div>
                                	</div>
                              	</div>
                              </div>

                                <div class="box_border mt-4">
                          			<div class="heading_font_size font-weight-bold">
                          				2. Select a exisiting customer or create a new customer. 
                          			</div>

	                              	<div class="form-group ml-5 mt-3">
                          				<div class="row">
                          					<div class="col-md-2">
	                                    		<label class="label_font_size" for="select_customer">Customer No :</label>
                          					</div>
                          					<div class="col-md-6">
		                                    	<select class="form-control select_customer input_size" name="select_customer" id="select_customer" value="{{ old('select_customer') }}">
		                                    	</select>
		                                    	@if ($errors->has('select_customer'))
		                                      		<div class="text-danger">{{ $errors->first('select_customer') }}</div>
		                                    	@endif
		                                  	</div>
		                                  	<!-- <div class="col-md-2">
		                                  		<a href="#" class="btn btn-success"> New</a>
		                                  	</div> -->
                          				</div>
                                  	</div>
                                </div>

                                <div class="box_border mt-4">
                          			<div class="heading_font_size font-weight-bold">
                          				3. Enter the machine ship date. 
                          			</div>

	                              	<div class="form-group ml-5 mt-3">
                          				<div class="row">
                          					<div class="col-md-2">
	                                    		<label class="label_font_size" for="mach_number">Ship Date :</label>
                          					</div>
                          					<div class="col-md-6">
                          						
		                                    	<div id="add_ship_date" class="input-group date" data-date-format="mm-dd-yyyy">
		                                    		<input type="text" name="ship_date" id="ship_date" class="form-control input_size" value="{{ old('ship_date') }}" readonly="readonly">
		                                    		<span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
		                                    	</div>
		                                    	@if ($errors->has('ship_date'))
		                                      		<div class="text-danger">{{ $errors->first('ship_date') }}</div>
		                                    	@endif
		                                  	</div>
                          				</div>
                                  	</div>
                                </div>

                                <div class="row">
	                                <div class="col-md-12 mt-4">
	                                	<button type="submit" class="btn btn-outline-primary float-right"><i class="fa fa-save"></i> Save</button>
	                                </div>
	                            </div>
				    		          </form>
                        </div>
                        <div class="card-body"></div>
                    </div>
                </div><!-- /# column -->
            </div>
            <!--  /Traffic -->
            <div class="clearfix"></div>
        </div>
    	<!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>

    <script>
      jQuery(document).ready(function () {
        jQuery("#add_ship_date").datepicker({ 
          format: "mm/dd/yyyy",
          weekStart: 0,
          calendarWeeks: true,
          autoclose: true,
          todayHighlight: true,
          orientation: "auto"
        }).datepicker('update', new Date());
      });
    </script>

@endsection