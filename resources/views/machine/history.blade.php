@extends('layout.app')

@section('content')

	<!-- Content -->
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
        	<!--  Traffic  -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                        	
                        	<div class="row form-group">
                        		<div class="col-md-9">
                    				<h4 class="box-title"> Machine Tracker ({{ isset($machinedata->mach_number)?$machinedata->mach_number:''}})</h4>
                        		</div>

                        		<div class="col-md-3">
									<a href="{{ route('machines.index') }}" class="btn btn-outline-secondary float-right"><i class="fa fa-angle-double-left"></i> Back</a>
                        		</div>
                        	</div>
                        </div>
                        
                            @include('machine.history_table')


                        <div class="card-body"></div>
                    </div>
                </div><!-- /# column -->
            </div>
            <!--  /Traffic -->
            <div class="clearfix"></div>
        </div>
    	<!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>

@endsection