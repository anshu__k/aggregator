@extends('layout.app')

@section('content')

	<!-- Content -->
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
        	<!--  Traffic  -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                        	<!-- Error & Message -->
	                        	@if (session()->has('insert'))
							      	<div class="alert alert-success alert-dismissible" role="alert">
							        	{{ session()->get('insert') }}
							        	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							          		<span aria-hidden="true">&times;</span>
							        	</button>
							      	</div>
							    @elseif (session()->has('update'))
							      	<div class="alert alert-info alert-dismissible" role="alert">
							        	{{ session()->get('update') }}
							        	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							          		<span aria-hidden="true">&times;</span>
							        	</button>
							      	</div>
							    @elseif (session()->has('delete'))
							      	<div class="alert alert-danger alert-dismissible" role="alert">
							        	{{ session()->get('delete') }}
							        	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							          		<span aria-hidden="true">&times;</span>
							        	</button>
							      	</div>
							    @endif
							<!-- Error & Message -->
                        	<div class="row form-group">
                        		<div class="col-md-9">
                    				<h4 class="box-title">Machine List </h4>
                        		</div>
                        		<div class="col-md-3">
                        			<a href="{{ route('machines.create') }}" class="btn btn-outline-success float-right"><i class="fa fa-plus"></i> ADD</a>
                        		</div>
                        	</div>   
							<div class="row form-group">

                        		<div class="col-md-6">
                        			<div class="input-group">
                        				<input class="form-control"  name="search_term" id="search_mach_term" placeholder="Search Machine Number, Machine Model">
                        				
                        			</div>
                        		</div>
                        		<div class="col-md-3">    
					             <div class="input-group">
					                <select class="form-control" name="status" id="status">
					                    <option selected disabled>Select Status</option>
					                    <option value="1">Active</option>
					                    <option value="0">Inactive</option>
					                </select>
					            </div>
					            </div>
					            <div class="col-md-3">    
					            <div class="input-group-btn">
                        					<button type="button" id="search_machine" class="btn btn-outline-primary" >Search</button>
                        				

                        				<a class="btn input-group-btn"href="{{ route('machines.index') }}" >Reset</a></div>
                        				
                        		</div>
                        	</div>
                        </div>
                        <div class="card-body nopadding">
                        	<div id="searchid" class="col-md-12 table-stats order-table ov-h nopadding">
	                            <table class="table">
				                  	<thead>
					                    <tr>
					                      <!-- <th>#</th> -->
					                      <th>Machine Number</th>
					                      <th>Machine Model</th>
					                      <th>Machine Status</th>
					                      <th>Source Number</th>
					                      <th>Source Model</th>
					                      <th>Ship Date</th>
					                      <th>Activity</th>
					                      <th>Status</th>
					                      <!-- <th>Updated At</th> -->
					                      <th>Action</th>
					                    </tr>
				                  	</thead>

				                  	<tbody class="table_format">
				                    	<!-- {{$i = 1}}  -->
				                    	@forelse($machines as $key => $machine) 				            
				                    	<?php
				                    	if($machine->wipedata){
				                    		$frequency = @$machine->wipedata->customer->wipetest_frequency;
				                    		$nextdate = date('d M Y', strtotime( "+".$frequency." months", strtotime($machine->wipedata['wipe_date'])));
				                    	} 
				                    	
				                    	?>
					                      	<tr>
						                       <!--  <td class="details-control" machine_num="{{ $machine->mach_number }}" machine_id="{{$machine->id}}">
						                        	<i class="fa fa-plus"></i>
						                        </td> --> 
												<td>
													<a class=""  rel="tooltip" title="View" href="{{ url('machines/log_history',$machine->id) }}">
														<u>{{$machine->mach_number}}</u>
													</a>
												</td>
						                        <!-- <td>{{ $machine->mach_number }}</td>  -->
						                        <td>{{ $machine->mach_model }}</td>
						                        <td>{{ $machine->mstats_desc }}</td> 
						                        <td> @forelse($machine->sourceloc_data as $k => $mach) {{ $mach->source_num }} @if( !$loop->last) , @endif @endforeach</td> 
						                        <td> @forelse($machine->sourceloc_data as $k => $mach) {{ $mach->s_mod_num }} @if( !$loop->last) , @endif @endforeach</td> 
						                        <td>
						                        	{{ ($machine->ship_date !== null) ? date('d M Y', strtotime($machine->ship_date)) : '' }} 
						                        </td> 
						                        <td>{{App\Machine::getGbx($machine->mach_number) }}</td>
						                        <td @if($machine->status == 1)class="text-success" @else class="text-danger" @endif>{{ $machine->status==1 ? 'Active' : 'Inactive' }}</td> 
						                        <!-- <td>{{ !empty($machine->updated_at)?date('d M Y', strtotime($machine->updated_at)):'-' }}</td> -->
												<td style="text-align: right; ">
						                        	<a href="{{ route('machines.edit', $machine->id) }}">
						                          		<i class="fa fa-edit edit_btn" rel="tooltip" title="Edit"></i>
						                          	</a>
						                        </td>
						                        <!-- <td style="padding-left:0px; text-align: left;">
						                        	<form action="{{ route('machines.destroy', $machine) }}" method="POST">
						                            	{{ csrf_field() }}
						                            	<input type="hidden" name="_method" value="DELETE">
						                            	<input type="hidden" name="_token" value="{{ csrf_token() }}">
						                            	<button type="submit" class="fabutton" onclick="return myFunction();">
						                              		<i class="fa fa-trash dlt_btn" rel="tooltip" title="Delete"></i>
						                            	</button>
						                          	</form>
						                        </td> -->
					                      	</tr>
				                    	@empty
					                        <tr>
					                          <td colspan="7" class="text-center">
					                          	<p>Machine Not Found.</p>
					                          </td> 
					                        </tr>
				                    	@endforelse
				                  	</tbody>
				                </table>
                        	</div> <!-- /.col-md-12 -->
				        </div><!-- card-body -->
                        <div class="card-body page_hide">
                            {{ $machines->onEachSide(1)->links() }}
                        </div>
                        <div class="card-body"></div>
                    </div>
                </div><!-- /# column -->
            </div>
            <!--  /Traffic -->
            <div class="clearfix"></div>
        </div>
    	<!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>
<script>
	jQuery(document).ready(function() {

		jQuery('.details-control').on('click', function()
		{

			$td = jQuery(this).closest('td')
			$tr = jQuery(this).closest('tr')
			$this = jQuery(this)

			// jQuery('tbody').find('.details-control i').removeClass('fa-minus').addClass('fa-plus')
			jQuery('tbody').find('.show_data').remove();
			if($this.find('i').hasClass('fa-minus')){
			jQuery('tbody').find('.details-control i').removeClass('fa-minus').addClass('fa-plus')
				$this.find('i').removeClass('fa-minus').addClass('fa-plus')
			}else{
			jQuery('tbody').find('.details-control i').removeClass('fa-minus').addClass('fa-plus')
				$this.find('i').removeClass('fa-plus').addClass('fa-minus')

				var machine_id = $td.attr('machine_id');
				var machine_num = $td.attr('machine_num');

				console.log($td)
				// console.log(machine_id)
				console.log(machine_num)
				

				jQuery.ajax({
					url:"{{ route('machineSource') }}",
					method:'GET',
					data:{machine_id:machine_id, machine_num: machine_num},
					dataType:'json',
					success:function(data)
					{
						$tr.after(data.source_table_data);
						// jQuery('#machine_table tbody .show_data').last().after(data.source_table_data);
					}
				})

			}
		});
	} );
</script>


    <!-- Search Machine -->
        <script>
            jQuery(document).ready(function(){
                function fetch_machine_data(query,status,page)
                {
                    jQuery.ajax({
                        url:"{{ route('machine_search') }}",
                        method:'GET',
                        data:{query:query,status:status,page:page},
                        dataType:'html',
                        success:function(data)
                        {
                            jQuery('#searchid').html(data);
                            jQuery('.page_hide').css('cssText', 'display:none');
                            //searchData();
                        }
                    })
                }
                
	                jQuery(document).on('click', '#search_machine', function(){
	                    
	                    var query = jQuery('#search_mach_term').val();
	                    var status = jQuery('#status').val();
	                     if (query != ''|| null !=status) {

	                        fetch_machine_data(query,status,1);
	                    }
					 
					 });

	                jQuery(document).on('click', '.pagination a', function(event){
	                  

	                  var query = jQuery('#search_mach_term').val();
	                  var status = jQuery('#status').val();

	                     if (query != ''|| null !=status) {
	                     	event.preventDefault(); 
	                  var page = jQuery(this).attr('href').split('page=')[1];
	                  
	                     fetch_machine_data(query,status,page);

	                   }  
	                 });
                

            });

            jQuery('#search_mach_term').keypress(function (e) {
                var key = e.which;
                if(key == 13)  // the enter key code
                {
                    jQuery('#search_machine').click();
                    return false;  
                }
            });  

            function searchData(){
                jQuery('.details-control').on('click', function()
                {
                    $td = jQuery(this).closest('td')
                    $tr = jQuery(this).closest('tr')
                    $this = jQuery(this)

                    // jQuery('tbody').find('.details-control i').removeClass('fa-minus').addClass('fa-plus')
                    jQuery('tbody').find('.show_data').remove();
                    if($this.find('i').hasClass('fa-minus')){
                    jQuery('tbody').find('.details-control i').removeClass('fa-minus').addClass('fa-plus')
                        $this.find('i').removeClass('fa-minus').addClass('fa-plus')
                    }else{
                    jQuery('tbody').find('.details-control i').removeClass('fa-minus').addClass('fa-plus')
                        $this.find('i').removeClass('fa-plus').addClass('fa-minus')

                        var machine_id = $td.attr('machine_id');
                        var machine_num = $td.attr('machine_num');

                        console.log($td)
                        // console.log(machine_id)
                        console.log(machine_num)
                        

                        jQuery.ajax({
                            url:"{{ route('machineSource') }}",
                            method:'GET',
                            data:{machine_id:machine_id, machine_num: machine_num},
                            dataType:'json',
                            success:function(data)
                            {
                                $tr.after(data.source_table_data);
                                // jQuery('#machine_table tbody .show_data').last().after(data.source_table_data);
                            }
                        })

                    }
                });
            }
        </script>
    <!-- Search Machine -->
<style type="text/css">
	.show_data{
		background-color: aliceblue;
	}

	.show_data td{
		border: 0px !important;
		padding: 3px !important;
	}
</style>
@endsection