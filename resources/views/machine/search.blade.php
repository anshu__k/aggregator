<table class="table">
				                  	<thead>
					                    <tr>
					                      <!-- <th>#</th> -->
					                      <th>Machine Number</th>
					                      <th>Machine Model</th>
					                      <th>Machine Status</th>
					                      <th>Source Number</th>
					                      <th>Source Model</th>
					                      <th>Ship Date</th>
					                      <th>Activity</th>
					                      <th>Status</th>
					                      <th>Action</th>
					                    </tr>
				                  	</thead>
				                  	
				                  	<tbody class="table_format">
				                    	<!-- {{$i = 1}}  -->
				                    	<?php if(count($machines) >0){ ?>	
				                    	@forelse($machines as $key => $machine) 				            
				                    	<?php
				                    	if($machine->wipedata){
				                    		$frequency = @$machine->wipedata->customer->wipetest_frequency;
				                    		$nextdate = date('d M Y', strtotime( "+".$frequency." months", strtotime($machine->wipedata['wipe_date'])));
				                    	} 
				                    	
				                    	?>
					                      	<tr>
						                       <!--  <td class="details-control" machine_num="{{ $machine->mach_number }}" machine_id="{{$machine->id}}">
						                        	<i class="fa fa-plus"></i>
						                        </td> --> 
												<td>
													<a class=""  rel="tooltip" title="View" href="{{ url('machines/log_history',$machine->id) }}">
														<u>{{$machine->mach_number}}</u>
													</a>
												</td>
						                        <!-- <td>{{ $machine->mach_number }}</td>  -->
						                        <td>{{ $machine->mach_model }}</td>
						                        <td>{{ $machine->mstats_desc }}</td>  
						                        <td> @forelse($machine->sourceloc_data as $k => $mach) {{ $mach->source_num }} @if( !$loop->last) , @endif @endforeach</td> 
						                        <td> @forelse($machine->sourceloc_data as $k => $mach) {{ $mach->s_mod_num }} @if( !$loop->last) , @endif @endforeach</td> 
						                        <td>
						                        	{{ ($machine->ship_date !== null) ? date('d M Y', strtotime($machine->ship_date)) : '' }} 
						                        </td> 
						                        <td>{{App\Machine::getGbx($machine->mach_number) }}</td>
						                        <td @if($machine->status == 1)class="text-success" @else class="text-danger" @endif>{{ $machine->status==1 ? 'Active' : 'Inactive' }}</td> 
						                        <td style="text-align: right; ">
						                        	<a href="{{ route('machines.edit', $machine) }}">
						                          		<i class="fa fa-edit edit_btn" rel="tooltip" title="Edit"></i>
						                          	</a>
						                        </td>
						                        <!-- <td style="padding-left:0px; text-align: left;">
						                        	<form action="{{ route('machines.destroy', $machine) }}" method="POST">
						                            	{{ csrf_field() }}
						                            	<input type="hidden" name="_method" value="DELETE">
						                            	<input type="hidden" name="_token" value="{{ csrf_token() }}">
						                            	<button type="submit" class="fabutton" onclick="return myFunction();">
						                              		<i class="fa fa-trash dlt_btn" rel="tooltip" title="Delete"></i>
						                            	</button>
						                          	</form>
						                        </td> -->
					                      	</tr>
				                    	@empty
					                        <tr>
					                          <td colspan="7" class="text-center">
					                          	<p>Machine Not Found.</p>
					                          </td> 
					                        </tr>
				                    	@endforelse
				                    <?php }else{ ?>
				                    	<tr>
					                          <td colspan="7" class="text-center">
					                          	<p>Machine Not Found.</p>
					                          </td> 
					                        </tr>
					                    <?php } ?>
				                  	</tbody>
				                </table>
				                <div class="card-body">
                            {{ $machines->links() }}
                        </div>