@extends('layout.app')

@section('content')

    <!-- Content -->
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
            <!--  Traffic  -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-lg-6">
                                    <h4 class="box-title">Add New Regulatory Agency </h4>
                                </div>
                                <div class="col-lg-6">
                                    <a href="{{ route('regulatory_agency.index') }}" class="btn btn-outline-secondary float-right"><i class="fa fa-angle-double-left"></i> Back</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <form action="{{ url('regulatory_agency/store') }}" method="post">
                             @csrf
                            <div class="row form-group">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="" class="label_font_size form-control-label">First Name</label>
                                         <input type="text"  name="r_fname" id="r_fname" placeholder="eg. John Doe" class="form-control input_size" >
                                    </div>
                                    @if ($errors->has('r_fname'))
                                        <div class="text-danger">{{ $errors->first('r_fname') }}</div>
                                    @endif
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="" class="label_font_size form-control-label">Last Name</label>
                                        <input type="text"  name="r_lname" id="r_lname" placeholder="eg. John Doe" class="form-control input_size" >
                                    </div>
                                    @if ($errors->has('r_lname'))
                                        <div class="text-danger">{{ $errors->first('r_lname') }}</div>
                                    @endif
                                </div>
                            </div> <!-- .row form-group -->


                            <div class="row form-group">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="" class="label_font_size">Reg Title1 </label>
                                        <input type="text"  name="r_title1" id="r_title1" placeholder="eg. Title" class="form-control input_size">
                                    </div>
                                    @if ($errors->has('r_title1'))
                                        <div class="text-danger">{{ $errors->first('r_title1') }}</div>
                                    @endif
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="" class="label_font_size form-control-label">Reg Title2</label>
                                        <input type="text"  name="r_title2" id="r_title2" placeholder="eg. Title" class="form-control input_size">
                                    </div>
                                    @if ($errors->has('r_title2'))
                                        <div class="text-danger">{{ $errors->first('r_title2') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="" class="label_font_size">Phone</label>
                                        <input type="text"  name="r_phone" id="r_phone" placeholder="eg. Title" class="form-control input_size">
                                    </div>
                                    @if ($errors->has('r_phone'))
                                        <div class="text-danger">{{ $errors->first('r_phone') }}</div>
                                    @endif
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="" class="label_font_size form-control-label">Rhonorific</label>
                                        <input type="text"  name="rhonorific" id="rhonorific" placeholder="eg. Rhonorific" class="form-control input_size">
                                    </div>
                                    @if ($errors->has('rhonorific'))
                                        <div class="text-danger">{{ $errors->first('rhonorific') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="" class="label_font_size">R Address1</label>
                                        <textarea name="r_address1" id="r_address1" class="form-control input_size">
                                        </textarea>    
                                    </div>
                                    @if ($errors->has('r_address1'))
                                        <div class="text-danger">{{ $errors->first('r_address1') }}</div>
                                    @endif
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="" class="label_font_size form-control-label">R Address2</label>
                                        <textarea name="r_address2" id="r_address2" class="form-control input_size">
                                        </textarea> 
                                    </div>
                                    @if ($errors->has('r_address2'))
                                        <div class="text-danger">{{ $errors->first('r_address2') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="" class="label_font_size form-control-label">R Address3</label>
                                        <textarea name="r_address3" id="r_address3" class="form-control input_size">
                                        </textarea> 
                                    </div>
                                    @if ($errors->has('r_address3'))
                                        <div class="text-danger">{{ $errors->first('r_address3') }}</div>
                                    @endif
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="" class="label_font_size form-control-label">R Address4</label>
                                        <textarea name="r_address4" id="r_address4" class="form-control input_size">
                                        </textarea> 
                                    </div>
                                    @if ($errors->has('r_address4'))
                                        <div class="text-danger">{{ $errors->first('r_address4') }}</div>
                                    @endif
                                </div>   
                            </div>

                             <div class="row form-group">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="" class="label_font_size form-control-label">R Address5</label>
                                        <textarea name="r_address5" id="r_address5" class="form-control input_size">
                                        </textarea> 
                                    </div>
                                    @if ($errors->has('r_address5'))
                                        <div class="text-danger">{{ $errors->first('r_address5') }}</div>
                                    @endif
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="" class="label_font_size form-control-label">R Address6</label>
                                        <textarea name="r_address6" id="r_address6" class="form-control input_size">
                                        </textarea> 
                                    </div>
                                    @if ($errors->has('r_address6'))
                                        <div class="text-danger">{{ $errors->first('r_address6') }}</div>
                                    @endif
                                </div>                            
                            </div>

                            <div class="row form-group">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="" class="label_font_size">Send Reg</label>
                                        <input type="text"  name="send_reg" id="send_reg" class="form-control input_size">
                                    </div>
                                    @if ($errors->has('send_reg'))
                                        <div class="text-danger">{{ $errors->first('send_reg') }}</div>
                                    @endif
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="" class="label_font_size form-control-label">Must Reg</label>
                                        <input type="text"  name="must_reg" id="must_reg" placeholder="eg. Title" class="form-control input_size">
                                    </div>
                                    @if ($errors->has('must_reg'))
                                        <div class="text-danger">{{ $errors->first('must_reg') }}</div>
                                    @endif
                                </div>
                            </div>

                             <div class="row form-group">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="" class="label_font_size">State Id</label>
                                        <select class="form-control input_size" name="state_id" id="state_id" value="{{ old('state_id') }}">
                                        @foreach($statedata as $states) 
                                        <option value="{{ $states['state'] }}" {{ old('state_id') == $states['state'] ? 'selected' : '' }}>{{ $states['name'] }}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                    @if ($errors->has('state_id'))
                                        <div class="text-danger">{{ $errors->first('state_id') }}</div>
                                    @endif
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="" class="label_font_size form-control-label">Additional Info</label>
                                        <input type="text"  name="additional_info" id="additional_info" class="form-control input_size">
                                    </div>
                                    @if ($errors->has('additional_info'))
                                        <div class="text-danger">{{ $errors->first('additional_info') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="" class="label_font_size form-control-label">Reg Days</label>
                                        <input type="numeric"  name="reg_days" id="reg_days" class="form-control input_size">
                                    </div>
                                    @if ($errors->has('reg_days'))
                                        <div class="text-danger">{{ $errors->first('reg_days') }}</div>
                                    @endif
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="" class="label_font_size">R Zip</label>
                                        <input type="text"  name="r_zip" id="r_zip" class="form-control input_size">
                                    </div>
                                    @if ($errors->has('r_zip'))
                                        <div class="text-danger">{{ $errors->first('r_zip') }}</div>
                                    @endif
                                </div>

                                <div class="col-md-3">
                                   <div class="form-group">
                                        <label for="" class="label_font_size">R City</label>
                                        <input type="text"  name="r_city" id="r_city" class="form-control input_size">
                                    </div>
                                    @if ($errors->has('r_city'))
                                        <div class="text-danger">{{ $errors->first('r_city') }}</div>
                                    @endif
                                </div>  
                            </div> <!-- .row form-group -->

                            <!-- <div class="col-md-6 offset-md-6 " > -->

                            <div class="pull-right">
                                <button type="submit" class="btn btn-outline-primary"><i class="fa fa-save"></i> Save</button>
                            </div>

                        </form>
                        </div>
                        <div class="card-body"></div>
                    </div>
                </div><!-- /# column -->
            </div>
            <!--  /Traffic -->
            <div class="clearfix"></div>
        </div>
        <!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>


    <!-- Enter Source Number Fetch Pre-assign Source Data -->
        <script>
       
        </script>
    <!-- Enter Source Number Fetch Pre-assign Source Data -->

@endsection