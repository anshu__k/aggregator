@extends('layout.app')

@section('content')

	<!-- Content -->
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
        	<!--  Traffic  -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                        	<!-- Error & Message -->
	                        	@if (session()->has('insert'))
							      	<div class="alert alert-success alert-dismissible" role="alert">
							        	{{ session()->get('insert') }}
							        	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							          		<span aria-hidden="true">&times;</span>
							        	</button>
							      	</div>
							    @elseif (session()->has('update'))
							      	<div class="alert alert-info alert-dismissible" role="alert">
							        	{{ session()->get('update') }}
							        	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							          		<span aria-hidden="true">&times;</span>
							        	</button>
							      	</div>
							    @elseif (session()->has('delete'))
							      	<div class="alert alert-danger alert-dismissible" role="alert">
							        	{{ session()->get('delete') }}
							        	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							          		<span aria-hidden="true">&times;</span>
							        	</button>
							      	</div>
							    @endif
							<!-- Error & Message -->
                        	<div class="row form-group">
                        		<div class="col-md-9">
                    				<h4 class="box-title">Technician Tracker ({{ $technician['tech_name'] }})</h4>
                        		</div>

                        		<div class="col-md-3">
								<a href="{{ route('technicians.index') }}" class="btn btn-outline-secondary float-right"><i class="fa fa-angle-double-left"></i> Back</a>
                        		</div>
                        	</div>
                        </div>
                        
                        
                        @include('technician.history_table')
                        <div class="card-body"></div>
                    </div>
                </div><!-- /# column -->
            </div>
            <!--  /Traffic -->
            <div class="clearfix"></div>
        </div>
    	<!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>
@endsection