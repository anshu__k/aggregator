<div class="card-body nopadding">
        <div class="col-md-12 table-stats order-table ov-h nopadding">
            <table class="table " id="customer_table">
                    <thead>
                        <tr>
                            <th>Wipe Date</th>
                            <th>Discrepency</th>
                            <th>Source Number</th>
                            <th>Machine Number</th>
                            <th>Customer Number</th>
                            <th>Measure Date</th>
                        </tr>
                    </thead>

                    <tbody class="table_format">
                        @forelse($wipehistory as $key => $wipedata) 
                        <tr>
                            <td>{{ date('d-m-Y', strtotime($wipedata->wipe_date)) }}</td>
                            <td>{{ $wipedata->discrep_code['descriptn'] }}</td>
                            <td>{{ $wipedata->source_num }}</td>
                            <td>{{ $wipedata->mach_num }}</td>
                            <td>{{ $wipedata->cust_num }}</td>
                            <td>{{ date('d-m-Y', strtotime($wipedata->meas_date)) }}</td>
                        </tr>
                        @empty
                        <tr>
                          <td colspan="5" class="text-center">
                            <p>Wipe History Not Found.</p>
                          </td> 
                        </tr>
                        @endforelse
                    </tbody>
            </table>
             <table class="table" id="customer_table">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Document</th>
                            <th>Docs Creation Date</th>
                            <th>Upload Date</th>
                        </tr>     
                    </thead>

                    <tbody class="table_format">
                        @forelse($uploaddoc as $key => $data) 
                        <tr>
                            <td>{{ $data->title }}</td>
                            <td><a href="{{ url('sourcetracker/'.$data->technician_id.'/'.$data->document) }}" target="_blank"><i class="fa fa-file fa-lg text-info" aria-hidden="true"></i></a></td>
                            <td>{{ date('d M Y', strtotime($data->created_at)) }}</td>
                             <td>{{ date('d M Y', strtotime($data->upload_date)) }}</td>
                        </tr>
                        @empty
                        <tr>
                          <td colspan="7" class="text-center">
                            <p>Document Not Found.</p>
                          </td> 
                        </tr>
                        @endforelse
                    </tbody>
            </table>
        </div>
</div>