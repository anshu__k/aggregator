@extends('layout.app')

@section('content')
	<!-- Content -->
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
        	<!--  Traffic  -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                        	<div class="row">
                        		<div class="col-lg-6">
                    				<h4 class="box-title">Add New Technician </h4>
                        		</div>
                        		<div class="col-lg-6">
                        			<a href="{{ route('technicians.index') }}" class="btn btn-outline-secondary float-right"><i class="fa fa-angle-double-left"></i> Back</a>
                        		</div>
                        	</div>
                        </div>
                        <div class="card-body">
                        	<form action="{{ route('technicians.store') }}" method="POST" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="row form-group">
                                  	<div class="col-md-6">
                                    	<label class="label_font_size" for="tech_name">Enter Technician Name :</label>
                                    	<input type="text" name="tech_name" id="tech_name" class="form-control input_size" value="{{ old('tech_name') }}" placeholder="eg. MSCH">
                                    	@if ($errors->has('tech_name'))
                                      		<div class="text-danger">{{ $errors->first('tech_name') }}</div>
                                    	@endif
                                  	</div>
                                  	<div class="col-md-6">
                                    	<label class="label_font_size" for="t_firstname">Enter Technician First Name :</label>
                                    	<input type="text" name="t_firstname" id="t_firstname" class="form-control input_size" value="{{ old('t_firstname') }}" placeholder="eg. Michael">
                                    	@if ($errors->has('t_firstname'))
                                      		<div class="text-danger">{{ $errors->first('t_firstname') }}</div>
                                    	@endif
                                  	</div>
                                </div>
                                <div class="row form-group">
                                  	<div class="col-md-6">
                                    	<label class="label_font_size" for="t_lastname">Enter Technician Last Name :</label>
                                    	<input type="text" name="t_lastname" id="t_lastname" class="form-control input_size" value="{{ old('t_lastname') }}" placeholder="eg. Bernard">
                                    	@if ($errors->has('t_lastname'))
                                      		<div class="text-danger">{{ $errors->first('t_lastname') }}</div>
                                    	@endif
                                  	</div>
                                  	<div class="col-md-6">
			                            <label class="label_font_size" for="current">Select Technician Status:</label>
			                            <select class="form-control input_size" name="current" id="current" value="{{ old('current') }}">
			                                <option selected disabled>Select Technician Status</option>
			                                <option value="0">Active</option>
			                                <option value="1">Deactve</option>
			                            </select>
			                            @if ($errors->has('current'))
			                            	<div class="text-danger">{{ $errors->first('current') }}</div>
			                            @endif
			                        </div>
			                    </div>
                                <div class="row form-group">
                                    <div class="col-md-6">
                                        <label class="label_font_size" for="role">Technician Type :</label>
                                        <select class="form-control input_size" name="type" id="role" value="{{ old('type') }}">
                                            <option selected disabled>Select Technician Type</option>
                                            @foreach($uroles as $urole)
                                                <option value="{{ $urole->id }}">{{ $urole->technician_type }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('type'))
                                            <div class="text-danger">{{ $errors->first('type') }}</div>
                                        @endif
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-outline-primary float-right"><i class="fa fa-save"></i> Save</button>
                        	</form>
                        </div>
                        <div class="card-body"></div>
                    </div>
                </div><!-- /# column -->
            </div>
            <!--  /Traffic -->
            <div class="clearfix"></div>
        </div>
    	<!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>
@endsection