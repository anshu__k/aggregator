 <table class="table">
				                  	<thead>
					                    <tr>
					                    	<!-- <th>#</th> -->
					                      	<th>Technician name</th>
					                      	<th>First Name</th>
					                      	<th>Last Name</th>
					                      	<th>Current Status</th>
					                      	<th>Type</th>
					                      	<th>Updated At</th>
					                      	<th colspan="2" style="text-align: center;">Action</th>
					                    </tr>
				                  	</thead>

				                  	<tbody class="table_format">
				                    	<!-- {{$i = 1}}  -->
				                    	@forelse($technicians as $key => $technician)
					                      	<tr>
						                        <!-- <td>{{ $technicians->firstItem() + $key }}</td>  -->
						                        <!-- <td>{{ $technician->tech_name }}</td>  -->
												<td>
                                                    <a class=""  rel="tooltip" title="View" href="{{ url('technicians/log_history',$technician->id) }}">
                                                    <u>{{$technician->tech_name}}</u>
                                                    </a>
                                                </td>
						                        <td>{{ $technician->t_firstname }}</td> 
						                        <td>{{ $technician->t_lastname }}</td> 
						                        <td @if($technician->current == 0)class="text-success" @else class="text-danger" @endif>{{ $technician->current == 0 ? 'Active' : 'Inactive' }}</td> 
						                        <td>{{ $technician->tech_type['technician_type'] }}</td> 
						                        <td>{{ !empty($technician->updated_at)?date('d M Y', strtotime($technician->updated_at)):'-' }}</td>
						                        <td style="text-align: right;">
						                          <a href="{{ route('technicians.edit', $technician) }}">
						                          	<i class="fa fa-edit edit_btn technician_edit_btn" rel="tooltip" title="Edit"></i>
						                          </a>
						                        </td>
						                        <!-- <td style="padding-left: 0px; text-align: left;">
						                        	<form action="{{ route('technicians.destroy', $technician) }}" method="POST">
						                            {{ csrf_field() }}
						                            	<input type="hidden" name="_method" value="DELETE">
						                            	<input type="hidden" name="_token" value="{{ csrf_token() }}">
						                            	<button type="submit" class="fabutton" onclick="return myFunction();">
						                              		<i class="fa fa-trash dlt_btn" rel="tooltip" title="Delete"></i>
						                            	</button>
						                          	</form>
						                        </td> -->
					                      	</tr>
				                    	@empty
					                        <tr>
					                          <td colspan="6" class="text-center">
					                          	<p>Technician Not Found.</p>
					                          </td> 
					                        </tr>
				                    	@endforelse
				                  	</tbody>
				                </table>


				                <div class="card-body">
                            {{ $technicians->onEachSide(1)->links() }}
                        </div>