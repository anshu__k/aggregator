@extends('layout.app')

@section('content')

	<!-- Content -->
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
        	<!--  Traffic  -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                        	<!-- Error & Message -->
	                        	@if (session()->has('insert'))
							      	<div class="alert alert-success alert-dismissible" role="alert">
							        	{{ session()->get('insert') }}
							        	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							          		<span aria-hidden="true">&times;</span>
							        	</button>
							      	</div>
							    @elseif (session()->has('update'))
							      	<div class="alert alert-info alert-dismissible" role="alert">
							        	{{ session()->get('update') }}
							        	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							          		<span aria-hidden="true">&times;</span>
							        	</button>
							      	</div>
							    @elseif (session()->has('delete'))
							      	<div class="alert alert-danger alert-dismissible" role="alert">
							        	{{ session()->get('delete') }}
							        	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							          		<span aria-hidden="true">&times;</span>
							        	</button>
							      	</div>
							    @endif
							<!-- Error & Message -->
                        	<div class="row form-group">
                        		<div class="col-md-9">
                    				<h4 class="box-title">Technician List </h4>
                        		</div>

                        		<div class="col-md-3">
                        			<a href="{{ route('technicians.create') }}" class="btn btn-outline-success float-right"><i class="fa fa-plus"></i> ADD</a>
                        		</div>
                        	</div>

                        	<div class="row form-group">

                        		<div class="col-md-6">
                        			<div class="input-group">
                        				<input class="form-control"  name="search_term" id="search_tech_term" placeholder="Search Technician Name, First Name, Last Name">
                        				
                        			</div>
                        		</div>

	                        	<div class="col-md-3">
						            <div class="input-group">
						                <select class="form-control" name="status" id="status">
						                    <option selected disabled>Select Status</option>
						                    <option value="0">Active</option>
						                    <option value="1">Inactive</option>
						                </select>
						            </div>
						        </div>
						        <div class="col-md-3">	
						        			<div class="input-group-btn">
	                        					<button type="button" id="search_technician" class="btn btn-outline-primary" >Search</button>
	                        				

	                        				<a class="btn input-group-btn"href="{{ route('technicians.index') }}" >Reset</a></div>
	                        	</div>			
                        	</div>	

                        </div>
                        <div class="card-body nopadding">
                        	<div id="searchid" class="col-md-12 table-stats order-table ov-h nopadding">
	                            <table class="table">
				                  	<thead>
					                    <tr>
					                    	<!-- <th>#</th> -->
					                      	<th>Technician name</th>
					                      	<th>First Name</th>
					                      	<th>Last Name</th>
					                      	<th>Current Status</th>
					                      	<th>Type</th>
					                      	<th>Updated At</th>
					                      	<th colspan="2" style="text-align: center;">Action</th>
					                    </tr>
				                  	</thead>

				                  	<tbody class="table_format">
				                    	<!-- {{$i = 1}}  -->
				                    	@forelse($technicians as $key => $technician)
					                      	<tr>
						                        <!-- <td>{{ $technicians->firstItem() + $key }}</td>  -->
						                        <!-- <td>{{ $technician->tech_name }}</td>  -->
												<td>
                                                    <a class=""  rel="tooltip" title="View" href="{{ url('technicians/log_history',$technician->id) }}">
                                                    <u>{{$technician->tech_name}}</u>
                                                    </a>
                                                </td>
						                        <td>{{ $technician->t_firstname }}</td> 
						                        <td>{{ $technician->t_lastname }}</td> 
						                        <td @if($technician->current == 0)class="text-success" @else class="text-danger" @endif>{{ $technician->current == 0 ? 'Active' : 'Inactive' }}</td> 
						                        <td>{{ $technician->tech_type['technician_type'] }}</td> 
						                        <td>{{ !empty($technician->updated_at)?date('d M Y', strtotime($technician->updated_at)):'-' }}</td>
						                        <td style="text-align: right;">
						                          <a href="{{ route('technicians.edit', $technician) }}">
						                          	<i class="fa fa-edit edit_btn technician_edit_btn" rel="tooltip" title="Edit"></i>
						                          </a>
						                        </td>
						                        <!-- <td style="padding-left: 0px; text-align: left;">
						                        	<form action="{{ route('technicians.destroy', $technician) }}" method="POST">
						                            {{ csrf_field() }}
						                            	<input type="hidden" name="_method" value="DELETE">
						                            	<input type="hidden" name="_token" value="{{ csrf_token() }}">
						                            	<button type="submit" class="fabutton" onclick="return myFunction();">
						                              		<i class="fa fa-trash dlt_btn" rel="tooltip" title="Delete"></i>
						                            	</button>
						                          	</form>
						                        </td> -->
					                      	</tr>
				                    	@empty
					                        <tr>
					                          <td colspan="6" class="text-center">
					                          	<p>Technician Not Found.</p>
					                          </td> 
					                        </tr>
				                    	@endforelse
				                  	</tbody>
				                </table>
                        	</div> <!-- col-md-12 -->
				        </div><!-- card-body -->
                        <div class="card-body page_hide">
                            {{ $technicians->onEachSide(1)->links() }}
                        </div>
                        <div class="card-body"></div>
                    </div>
                </div><!-- /# column -->
            </div>
            <!--  /Traffic -->
            <div class="clearfix"></div>
        </div>
    	<!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>


 <!-- Search Technician -->
        <script>
            jQuery(document).ready(function(){
                function fetch_technician_data(query,status,page)
                {
                    jQuery.ajax({
                        url:"{{ route('technician_search') }}",
                        method:'GET',
                        data:{query:query,status:status,page:page},
                        dataType:'html',
                        success:function(data)
                        {
                            jQuery('#searchid').html(data);
                            jQuery('.page_hide').css('cssText', 'display:none');
                        }
                    })
                }

                jQuery(document).on('click', '#search_technician', function(){
                    var query = jQuery('#search_tech_term').val();
                    var status = jQuery('#status').val();

                    if (query != '' || null !=status) {
                        fetch_technician_data(query,status,1);

                    }
                });

                jQuery(document).on('click', '.pagination a', function(event){
	                  

	                  var query = jQuery('#search_tech_term').val();
	                  var status = jQuery('#status').val();
	                     if (query != ''  || null !=status) {
	                     	event.preventDefault(); 
	                  var page = jQuery(this).attr('href').split('page=')[1];
	                  
	                     fetch_technician_data(query,status,page);

	                   }  
	                 });
                
            });

            jQuery('#search_tech_term').keypress(function (e) {
                var key = e.which;
                if(key == 13)  // the enter key code
                {
                    jQuery('#search_technician').click();
                    return false;  
                }
            });  
        </script>
    <!-- Search Technician -->

@endsection