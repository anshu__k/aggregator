@extends('layout.app')

@section('content')

	<!-- Content -->
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
        	<!--  Traffic  -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body ">
                        	 <h4 class="box-title">Document Upload</h4>
                            <form action="{{ route('document.upload') }}" method="POST" enctype="multipart/form-data">
                                {{csrf_field()}}
                                 <div class="row form-group">
                                     <div class="col-md-6">
                                        <label for="" class="label_font_size form-control-label">Select Type</label>
                                        <select name="select_type" id="gettype" class="form-control input_size">
                                            <option selected disabled>Select Type</option>
                                            <option value="customer">Customer</option>
                                            <option value="contact">Contact</option>
                                            <option value="machine">Machine</option>
                                            <option value="techician">Technician</option>
                                            <option value="source">Source</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="machine-data">
                                            <label for="" class="label_font_size form-control-label">Select Machine</label>
                                            <select class="form-control select_machine_num input_size" name="machine_id" id="machine_id" value="{{ old('machine_number') }}"></select>

                                            @if ($errors->has('machine_number'))
                                            <div class="text-danger">{{ $errors->first('machine_number') }}</div>
                                            @endif
                                        </div>
                                        <div id="customer-data">
                                            <div class="form-group">
                                                <label for="" class="label_font_size form-control-label">Select Customer</label>
                                                <select class="form-control select_customer autocomplete_customer input_size" name="customer_id" id="customer_id" value=""></select>
                                            </div>
                                            @if ($errors->has('customer_id'))
                                                <div class="text-danger">{{ $errors->first('customer_id') }}</div>
                                            @endif
                                        </div>
                                        <div id="source-data">
                                            <label for="" class="label_font_size form-control-label">Select Source Number</label>
                                            <select class="form-control select_transfer_source_model input_size" name="source_id" id="source_id" value="{{ old('source_id') }}"></select>

                                            @if ($errors->has('source_number'))
                                            <div class="text-danger">{{ $errors->first('source_number') }}</div>
                                            @endif
                                        </div> 
                                        <div id="contact-data">
                                            <div class="form-group">
                                                <label for="" class="label_font_size form-control-label">Select Contact</label>
                                                <select class="form-control select_contact_name autocomplete_contact input_size" name="contact_id" id="customer_id" value=""></select>
                                            </div>
                                            @if ($errors->has('contact_id'))
                                                <div class="text-danger">{{ $errors->first('contact_id') }}</div>
                                            @endif
                                        </div>
                                        <div id="technician-data">
                                            <label for="" class="label_font_size form-control-label">Select Technician</label>
                                            <select class="form-control select_tech_model input_size" name="technician_id" id="technician_id" value="{{ old('technician_id') }}"></select>

                                            @if ($errors->has('technician_storage'))
                                            <div class="text-danger">{{ $errors->first('technician_storage') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                     <div class="col-md-6">
                                        <label class="label_font_size" for="image">Title:</label>
                                        <input type="text"  name="title" id="title" class="form-control input_size" >
                                    </div>
                                    <div class="col-md-6">
                                        <label class="label_font_size" for="image">Upload Doc:</label>
                                        <input type="file" name="image" id="image" class="form-control input_size" value="{{ old('image') }}" style="padding: 1px 0px 0px 9px !important;">
                                        @if ($errors->has('image'))
                                            <div class="text-danger">{{ $errors->first('image') }}</div>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-6">
                                        <label for="" class="label_font_size"> Docs Creation Date</label>
                                        <div id="add_upload_date" class="input-group date" data-date-format="mm-dd-yyyy">
                                            <input type="text" name="upload_date" id="upload_date" class="form-control input_size" value="{{ old('upload_date') }}" >
                                            <span class="input-group-addon" ><i class="fa fa-calendar"></i></span>
                                            @if ($errors->has('upload_date'))
                                                <div class="text-danger">{{ $errors->first('upload_date') }}</div>
                                            @endif
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                    <button type="submit" class="btn btn-outline-primary float-right"><i class="fa fa-save"></i> Save</button>
                                    </div>
                                </div>
                            </form>
				        </div>
                    </div>
                </div><!-- /# column -->
            </div>
            <!--  /Traffic -->
            <div class="clearfix"></div>
        </div>
    	<!-- .animated -->
    </div>
    <!-- /.content -->
    <div class="clearfix"></div>
<script>
  jQuery(document).ready(function () {
    jQuery("#add_upload_date").datepicker({ 
      format: "mm/dd/yy",
      weekStart: 0,
      calendarWeeks: true,
      autoclose: true,
      todayHighlight: true,
      orientation: "auto"
    }).datepicker('update', new Date());
  });

 $("#machine-data").hide()
 $("#technician-data").hide()
 $("#contact-data").hide()
 $("#source-data").hide()
 $("#customer-data").hide() 
  $("#gettype").change(function(){ console.log(jQuery(this).val());
    if(jQuery(this).val() === "customer"){
        jQuery("#customer-data").show();
         jQuery("#machine-data").hide()
         jQuery("#technician-data").hide();
         jQuery("#contact-data").hide();
         jQuery("#source-data").hide();
    }else if(jQuery(this).val() === "contact"){
        jQuery("#contact-data").show();
         jQuery("#machine-data").hide()
         jQuery("#technician-data").hide();
         jQuery("#customer-data").hide();
         jQuery("#source-data").hide();
    }else if(jQuery(this).val() === "machine"){
        jQuery("#machine-data").show();
         jQuery("#customer-data").hide();
         jQuery("#technician-data").hide();
         jQuery("#contact-data").hide();
         jQuery("#source-data").hide();
    }else if(jQuery(this).val() === "techician"){
        jQuery("#technician-data").show();
         jQuery("#machine-data").hide();
         jQuery("#customer-data").hide();
         jQuery("#contact-data").hide();
         jQuery("#source-data").hide();
    }else {
        jQuery("#source-data").show();
         jQuery("#machine-data").hide();
         jQuery("#technician-data").hide();
         jQuery("#contact-data").hide();
         jQuery("#customer-data").hide();
    }
});

   jQuery(document).ready(function(){
        jQuery('.select_transfer_source_model').select2({
            placeholder: 'Enter Source Number',
            ajax: {
                url: '{{ route("transferSourceAutoComplete") }}',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: jQuery.trim(params.term)
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

        jQuery('.select_contact_name').select2({
            placeholder: 'Enter Contact Name',
            ajax: {
                url: '{{ route("transferContactAutoComplete") }}',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: jQuery.trim(params.term)
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

    });
</script>
@endsection
