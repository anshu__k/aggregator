<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Customer
Route::get('/customer', 'CustomerController@index')->name('customer')->middleware('auth');
Route::get('/customer/create', 'CustomerController@create')->middleware('auth');
Route::post('/customer/create','CustomerController@store')->middleware('auth');
Route::get('/customer/search','CustomerController@search')->name('customer_search')->middleware('auth');
Route::get('/customer/autocomplete','CustomerController@autocomplete')->name('customer_autocomplete')->middleware('auth');
Route::get('/customer/view/{id}', 'CustomerController@view')->middleware('auth')->name('customer.view');
Route::get('/customer/main_search','CustomerController@main_search')->name('customer_main_search')->middleware('auth');
Route::get('/customer/state_autocomplete','CustomerController@state_autocomplete')->name('state_autocomplete')->middleware('auth');

Route::get('/customer/edit/{id}', 'CustomerController@edit')->name('customer_edit')->middleware('auth');
Route::post('/customer/update/{id}', 'CustomerController@update')->name('customer_update')->middleware('auth');
Route::post('/customer/delete/{id}', 'CustomerController@delete')->name('customer_delete')->middleware('auth');

Route::get('/customer/filter_machine_status','CustomerController@filterMachineStatus')->name('filterMachineStatus')->middleware('auth');


// Contact
Route::get('/contact', 'ContactController@index')->name('contact')->middleware('auth');
Route::get('/contact/create', 'ContactController@create')->middleware('auth');
Route::post('/contact/create','ContactController@store')->middleware('auth');

Route::get('/contact/edit/{id}', 'ContactController@edit')->name('contact_edit')->middleware('auth');
Route::post('/contact/update/{id}', 'ContactController@update')->name('contact_update')->middleware('auth');
Route::post('/contact/delete/{id}', 'ContactController@delete')->name('contact_delete')->middleware('auth');

Route::get('/contact/search','ContactController@search')->name('contact_search')->middleware('auth');




Route::get('/', 'Auth\LoginController@loginview')->middleware('auth');
Auth::routes();

Route::get('/home', 'DashboardController@index')->name('home')->middleware('auth');

Route::get('/getGraphData', 'DashboardController@getGraphData')->name('getGraphData')->middleware('auth');
// User Controller
Route::resource('users', 'UserController')->middleware('auth');

// Machine Controller
Route::get('machines/all_machines', 'MachineController@autocomplete')->name('machine_autocomplete')->middleware('auth');
Route::get('machines/select_machine_num', 'MachineController@SelectMachineNum')->name('select_machine_num')->middleware('auth');
Route::get('/machines/search','MachineController@search')->name('machine_search')->middleware('auth');
Route::get('/machines/transfer_machine_autofill','MachineController@transferMachineAutofill')->name('transferMachineAutofill')->middleware('auth');
Route::get('/machines/duplicate_machine', 'MachineController@duplicateMachine')->name('duplicateMachine')->middleware('auth');
Route::resource('machines', 'MachineController')->middleware('auth');
// Route::get('/getGraphData', 'DashboardController@getGraphData')->name('getGraphData')->middleware('auth'); 
Route::get('/getCPAAdditionData', 'DashboardController@getCPAAdditionData')->name('getCPAAdditionData')->middleware('auth'); 
Route::get('/getBullhornSyncedData', 'DashboardController@getBullhornSyncedData')->name('getBullhornSyncedData')->middleware('auth'); 

Route::get('/getLogFilterData', 'DashboardController@getLogFilterData')->name('getLogFilterData')->middleware('auth'); 
// Technician Controller
Route::get('/technicians/autocomplete','TechnicianController@autocomplete')->name('technician_autocomplete')->middleware('auth');
Route::get('/technicians/search','TechnicianController@search')->name('technician_search')->middleware('auth');
Route::resource('technicians', 'TechnicianController')->middleware('auth');

// Log Controller
Route::get('/logs/get_history','LogController@get_history')->name('log_history')->middleware('auth');
Route::get('/logs/search','LogController@search')->name('log_search')->middleware('auth');
Route::resource('logs', 'LogController')->middleware('auth'); 
Route::get('/sources/searchwipe','ScriptController@searchwipe')->name('source_search_wipe')->middleware('auth');

// Source Controller
Route::get('/script/machine_source', 'ScriptController@machineSource')->name('machineSource')->middleware('auth');
Route::get('/script/autocomplete','ScriptController@autocomplete')->name('source_autocomplete')->middleware('auth');
Route::get('/script/autocompletewipe','ScriptController@autocompletewipe')->name('source_autocomplete_wipe')->middleware('auth');
Route::get('/script/search','ScriptController@search')->name('source_search')->middleware('auth');
Route::get('/script/search_source','ScriptController@search_source')->name('search_source')->middleware('auth');
Route::get('/script/pre_assign_source_search','ScriptController@preAssignSourceAutoComplete')->name('preAssignScriptearch')->middleware('auth');
Route::get('/script/transfer_source_auto_complete','ScriptController@transferSourceAutoComplete')->name('transferSourceAutoComplete')->middleware('auth');
Route::get('/script/pre_assign_source_auto_fill','ScriptController@preAssignSourceAutoFill')->name('preAssignSourceAutofill')->middleware('auth');
Route::get('/script/wipe_data_autofill','ScriptController@wipeDataAutoFill')->name('wipe_data_autofill')->middleware('auth');
Route::get('/script/logs','ScriptController@logs')->name('script.logs')->middleware('auth');
Route::get('/script/list','ScriptController@list')->name('script.list')->middleware('auth');
Route::resource('script', 'ScriptController')->middleware('auth');

Route::get('/contact/log_history/{id}','ContactController@getHistory')->middleware('auth')->name('contact.log_history');
Route::get('/contact/scriptrecord','ContactController@script_record')->name('contact.script_record')->middleware('auth');
Route::post('/contact/scriptrecord-paginate','ContactController@script_record_paginate')->name('contact.script_record_paginate')->middleware('auth');

// Route::get('/contact/scriptrecord-poplar','ContactController@getPoplarCampaigns')->name('contact.get_poplar_campaigns')->middleware('auth');
// Route::post('/contact/submit-scriptrecord-poplar','ContactController@sendPoplarPostcardCampaigns')->name('contact.submit_scriptrecord_poplar')->middleware('auth');

Route::group(['prefix' => 'poplar'], function(){
	Route::get('dump-poplar-campaign','PoplarController@dump_poplar_campaigns')->name('poplar.dump_poplar_campaigns'); //->middleware('auth');

	Route::get('scriptrecord-poplar','PoplarController@getPoplarCampaigns')->name('poplar.get_poplar_campaigns')->middleware('auth');
	Route::post('submit-scriptrecord-poplar','PoplarController@sendPoplarPostcardCampaigns')->name('poplar.submit_scriptrecord_poplar')->middleware('auth');

	/** Bullhorn IFrame Load */
	// Route::get('bullhorn/list-contact-campaigns/{bullhorn_id}','PoplarController@getListOfContactsByBullhornId')->name('poplar.bullhorn.list_contact_campaign');
	Route::get('bullhorn/list-contact-campaigns','PoplarController@getListOfContactsByBullhornId')->name('poplar.bullhorn.list_contact_campaign');
	Route::post('bullhorn/send-postcard-to-contact/{bullhorn_id}','PoplarController@sendPostcardToBUllhornContact')->name('poplar.bullhorn.send_postcard_contact');
});

Route::get('machines/log_history/{id}', 'MachineController@getHistory')->middleware('auth')->name('machine.log_history');
Route::get('/technicians/log_history/{id}','TechnicianController@getHistory')->middleware('auth')->name('technician.log_history');
Route::get('/script/log_history/{id}','ScriptController@getHistory')->middleware('auth');

Route::group(['prefix' => 'mailchimp'], function(){
	Route::get('listing/{page_number?}', 'MailchimpController@listing')->name('mailchimp.listing');
	Route::post('sync-data', 'MailchimpController@syncDataToBullhorn')->name('mailchimp.syncDataToBullhorn');
	Route::get('iframe-listing', 'MailchimpController@iframe_listing')->name('mailchimp.iframe_listing');
});

// Record Wipe Test Controller
Route::group(['middleware' => ['auth']], function () {
   	Route::get('/wipes/history','WipeController@history')->name('history');
   	Route::get('/wipes/wipes_search','WipeController@wipesSearch')->name('wipes_search');
	Route::get('/update-wipe','WipeController@updateWipe')->name('update-wipe');
	Route::get('/wipes/{id}/sendEmail','WipeController@sendEmail')->name('sendEmail');
	Route::get('/wipes/{id}/pdfView','WipeController@pdfView')->name('pdfView');
	Route::get('/wipes/discrepcod','WipeController@discrepcod')->name('discrepcod');
	Route::post('/wipes/storeMultiple','WipeController@storeMultiple')->name('wipes.storeMultiple');
	Route::get('/edit-RSO','WipeController@editRSO')->name('edit-RSO');
	Route::post('/update-RSO','WipeController@updateRSO')->name('update-RSO');
	Route::resource('wipes', 'WipeController');
});




// Pre-Assign Source Controller
Route::get('/preAssignSou/history','PreAssignScriptController@history')->name('prehistory')->middleware('auth');
Route::post('preAssignSou/createWipe', 'PreAssignScriptController@createWipe')->name('createWipe')->middleware('auth');
Route::post('preAssignSou/confirmShipDate', 'PreAssignScriptController@confirmShipDate')->name('confirmShipDate')->middleware('auth');
Route::get('/preAssignSou/search','PreAssignScriptController@search')->name('pre_assign_source_search')->middleware('auth');
Route::resource('preAssignSou', 'PreAssignScriptController')->middleware('auth');

Route::get('update-source-log', 'PreAssignScriptController@updateSourceLog')->name('update-source-log')->middleware('auth');

// Transfer Source Controller
Route::resource('TransferSou', 'TransferScriptController')->middleware('auth');
Route::get('TransferSou/confirm/{id}', 'TransferScriptController@confirm')->name('TransferSou.confirm')->middleware('auth');
Route::post('update_confirmlog', 'TransferScriptController@updateconfirmlog')->name('update_confirmlog')->middleware('auth');
// Signature Upload Controller
Route::resource('signatures', 'SignatureUploadController')->middleware('auth');

// Document Upload Controller
Route::resource('upload_document', 'UploadDocumentController')->middleware('auth');
Route::post('document/upload', 'UploadDocumentController@documentUpload')->name('document.upload')->middleware('auth');
Route::get('/getTypeData', 'UploadDocumentController@getTypeData')->name('getTypeData')->middleware('auth'); 
Route::get('/contacts/transfer_contact_auto_complete','ContactController@transferContactAutoComplete')->name('transferContactAutoComplete')->middleware('auth');

// Regulatory Report Controller
Route::resource('regulatory_report', 'RegulatoryReportController')->middleware('auth');
Route::get('/get_quarterly_data','RegulatoryReportController@getQuarterlyData')->name('getQuarterlyData')->middleware('auth');
Route::get('view_pdf/{fromdate}/{todate}/{state}', 'RegulatoryReportController@viewQuarterlyPdf');
// customer report controller
Route::resource('customer_report', 'CustomerReportController')->middleware('auth');

//Regulatory agency
Route::resource('regulatory_agency', 'RegulatoryAgencyController')->middleware('auth');
Route::post('/regulatory_agency/store','RegulatoryAgencyController@store')->middleware('auth');
Route::post('/regulatory_agency/update/{id}','RegulatoryAgencyController@update')->middleware('auth');

Route::get('/source/log_history/{id}', 'ScriptController@getHistory')->name('source.log_history')->middleware('auth');


// Transfer Machine Controller
Route::resource('TransferMach', 'TransferMachineController')->middleware('auth');